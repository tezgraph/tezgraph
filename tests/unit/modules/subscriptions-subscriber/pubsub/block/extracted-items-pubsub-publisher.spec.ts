import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions';
import { FromBlockExtractor } from '../../../../../../src/modules/subscriptions-subscriber/from-block-extractors/extractor';
import { PubSubPublisher } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/block-pubsub-processor';
import { ExtractedItemsPubSubPublisher } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/extracted-items-pubsub-publisher';
import { SubscriberPubSub } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { PubSubTrigger } from '../../../../../../src/utils';

interface Foo {
    bar: number;
}

describe(ExtractedItemsPubSubPublisher.name, () => {
    let target: PubSubPublisher;
    let extractor: FromBlockExtractor<Foo>;
    let trigger: PubSubTrigger<Foo>;
    let subscriberPubSub: SubscriberPubSub;

    beforeEach(() => {
        extractor = mock<FromBlockExtractor<Foo>>();
        trigger = { name: 'FOOS' } as PubSubTrigger<Foo>;
        subscriberPubSub = mock(SubscriberPubSub);
        target = new ExtractedItemsPubSubPublisher(instance(extractor), trigger);
    });

    it('should extract and publish all items', async () => {
        const block = { hash: 'hh' } as BlockNotification;
        const foo1: Foo = { bar: 11 };
        const foo2: Foo = { bar: 22 };
        when(extractor.extract(block)).thenReturn([foo1, foo2]);

        await target.publish(block, instance(subscriberPubSub));

        verify(subscriberPubSub.publish(anything(), anything())).times(2);
        verify(subscriberPubSub.publish(trigger, foo1)).once();
        verify(subscriberPubSub.publish(trigger, foo2)).once();
    });
});
