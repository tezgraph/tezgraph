import { instance, mock, verify } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions';
import {
    BlockPubSubProcessor,
    PubSubPublisher,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/block-pubsub-processor';
import { SubscriberPubSub } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { ItemProcessor } from '../../../../../../src/utils';

describe(BlockPubSubProcessor.name, () => {
    let target: ItemProcessor<BlockNotification>;
    let subscriberPubSub: SubscriberPubSub;
    let pubSubPublisher1: PubSubPublisher;
    let pubSubPublisher2: PubSubPublisher;

    beforeEach(() => {
        subscriberPubSub = 'SubscriberPubSubMock' as unknown as SubscriberPubSub;
        pubSubPublisher1 = mock<PubSubPublisher>();
        pubSubPublisher2 = mock<PubSubPublisher>();
        target = new BlockPubSubProcessor(subscriberPubSub, [instance(pubSubPublisher1), instance(pubSubPublisher2)]);
    });

    it('should publish using all pub-sub publishers', async () => {
        const block = { hash: 'hh' } as BlockNotification;

        await target.processItem(block);

        verify(pubSubPublisher1.publish(block, subscriberPubSub)).once();
        verify(pubSubPublisher2.publish(block, subscriberPubSub)).once();
    });
});
