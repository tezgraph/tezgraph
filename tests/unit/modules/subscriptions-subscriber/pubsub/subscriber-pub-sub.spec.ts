import { subscriberTriggers } from '../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { testUniqueTriggers } from '../../../utils/pubsub/pubsub-trigger-tests';

describe('subscriberTriggers', () => {
    testUniqueTriggers(subscriberTriggers);
});
