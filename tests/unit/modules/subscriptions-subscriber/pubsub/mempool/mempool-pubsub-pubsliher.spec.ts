import { anything, instance, mock, verify } from 'ts-mockito';

import { MempoolOperationGroup, OperationKind } from '../../../../../../src/entity/subscriptions';
import { MempoolPubSubPublisher } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/mempool/mempool-pubsub-publisher';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { ItemProcessor } from '../../../../../../src/utils';
import { mockOperation } from '../../../../mocks';

describe(MempoolPubSubPublisher.name, () => {
    let target: ItemProcessor<MempoolOperationGroup>;
    let subscriberPubSub: SubscriberPubSub;

    beforeEach(() => {
        subscriberPubSub = mock(SubscriberPubSub);
        target = new MempoolPubSubPublisher(instance(subscriberPubSub));
    });

    it('should publish all operations in the group', async () => {
        const operation1 = mockOperation({ kind: OperationKind.ballot });
        const operation2 = mockOperation({ kind: OperationKind.reveal });

        await target.processItem([operation1, operation2]);

        verify(subscriberPubSub.publish(anything(), anything())).times(2);
        verify(subscriberPubSub.publish(subscriberTriggers.mempoolOperations[OperationKind.ballot], operation1)).once();
        verify(subscriberPubSub.publish(subscriberTriggers.mempoolOperations[OperationKind.reveal], operation2)).once();
    });
});
