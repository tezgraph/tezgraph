/* eslint-disable jest/no-export */
import { OperationKind, OperationNotification } from '../../../../../src/entity/subscriptions';
import { FromOperationExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/extractor';

export function runTest<TNotification>(
    target: FromOperationExtractor<TNotification>,
    operation: OperationNotification,
    expected: TNotification[],
) {
    const actual = Array.from(target.extractFromOperation(operation));

    expect(actual).toEqual(expected);
}

export function itShouldReturnEmptyIfNullRespectiveProperties(
    target: FromOperationExtractor<unknown>,
    operation: OperationNotification,
) {
    it('should extract empty if respective properties are null', () => {
        runTest(target, operation, []);
    });
}

export function itShouldReturnEmptyIfNullMetadata(target: FromOperationExtractor<unknown>, kind: OperationKind) {
    it('should extract empty if null metadata', () => {
        const operation = { kind, metadata: null } as OperationNotification;
        runTest(target, operation, []);
    });
}

export function describeOperationsWithoutRespectiveNotifications(
    target: FromOperationExtractor<unknown>,
    kinds: readonly OperationKind[],
) {
    describe.each(kinds)('%s', (kind) => {
        it('should extract empty if valid metadata', () => {
            const operation = { kind, metadata: {} } as OperationNotification;
            runTest(target, operation, []);
        });

        itShouldReturnEmptyIfNullMetadata(target, kind);
    });
}
