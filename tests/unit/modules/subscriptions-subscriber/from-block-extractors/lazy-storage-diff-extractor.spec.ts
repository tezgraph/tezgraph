import { difference } from 'lodash';

import {
    ContractResultParent,
    DelegationNotification,
    InternalOperationKind,
    InternalOperationResult,
    LazyStorageDiff,
    OperationKind,
    OriginationNotification,
    RevealNotification,
    TransactionNotification,
} from '../../../../../src/entity/subscriptions';
import { LazyStorageDiffExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/lazy-storage-diff-extractor';
import { asReadonly } from '../../../../../src/utils/conversion';
import { getEnumValues } from '../../../../../src/utils/reflection';
import { mockLazyStorageDiff } from '../../../mocks';
import {
    describeOperationsWithoutRespectiveNotifications,
    itShouldReturnEmptyIfNullMetadata,
    itShouldReturnEmptyIfNullRespectiveProperties,
    runTest,
} from './extractor-test-helper';

describe(LazyStorageDiffExtractor.name, () => {
    const target = new LazyStorageDiffExtractor();

    let diff1: LazyStorageDiff;
    let diff2: LazyStorageDiff;
    let diff3: LazyStorageDiff;
    let diff4: LazyStorageDiff;
    let diff5: LazyStorageDiff;

    let internalParent1: ContractResultParent;
    let internalParent2: ContractResultParent;
    let internalResults: readonly InternalOperationResult[];

    beforeEach(() => {
        diff1 = mockLazyStorageDiff();
        diff2 = mockLazyStorageDiff();
        diff3 = mockLazyStorageDiff();
        diff4 = mockLazyStorageDiff();
        diff5 = mockLazyStorageDiff();

        internalParent1 = {
            kind: InternalOperationKind.transaction,
            result: { lazy_storage_diff: asReadonly([diff1, diff2]) },
        } as ContractResultParent;
        internalParent2 = {
            kind: InternalOperationKind.origination,
            result: { lazy_storage_diff: asReadonly([diff3]) },
        } as ContractResultParent;

        internalResults = [
            internalParent1,
            { kind: InternalOperationKind.transaction, result: {} }, // Undefined diffs.
            internalParent2,
            { kind: InternalOperationKind.reveal, result: {} }, // Unrelated operation.
        ] as InternalOperationResult[];
    });

    const kindsWithInternalResults = [OperationKind.delegation, OperationKind.reveal];
    type OperationWithInternalResults = DelegationNotification | RevealNotification;

    describe.each(kindsWithInternalResults)('%s', (kind) => {
        it('should extract diffs from internal results', () => {
            const operation = mockOperation(internalResults);
            runTest(target, operation, [
                { lazy_storage_diff: diff1, parent: internalParent1, operation },
                { lazy_storage_diff: diff2, parent: internalParent1, operation },
                { lazy_storage_diff: diff3, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(internal_operation_results?: readonly InternalOperationResult[]) {
            return {
                kind,
                metadata: { internal_operation_results },
            } as OperationWithInternalResults;
        }
    });

    const kindsWithResults = [OperationKind.origination, OperationKind.transaction];
    type OperationWithResults = OriginationNotification | TransactionNotification;

    // eslint-disable-next-line jest/no-identical-title
    describe.each(kindsWithResults)('%s', (kind) => {
        it('should extract diffs from operation result and internal results', () => {
            const operation = mockOperation([diff4, diff5], internalResults);
            runTest(target, operation, [
                { lazy_storage_diff: diff4, parent: operation, operation },
                { lazy_storage_diff: diff5, parent: operation, operation },
                { lazy_storage_diff: diff1, parent: internalParent1, operation },
                { lazy_storage_diff: diff2, parent: internalParent1, operation },
                { lazy_storage_diff: diff3, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(
            lazy_storage_diff?: readonly LazyStorageDiff[],
            internal_operation_results?: readonly InternalOperationResult[],
        ) {
            return {
                kind,
                metadata: {
                    operation_result: { lazy_storage_diff },
                    internal_operation_results,
                },
            } as OperationWithResults;
        }
    });
    describeOperationsWithoutRespectiveNotifications(
        target,
        difference(getEnumValues(OperationKind), [...kindsWithInternalResults, ...kindsWithResults]),
    );
});
