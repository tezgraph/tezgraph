import { difference } from 'lodash';

import { Micheline } from '../../../../../src/entity/scalars';
import {
    ContractResultParent,
    DelegationNotification,
    InternalOperationKind,
    InternalOperationResult,
    OperationKind,
    OriginationNotification,
    RevealNotification,
    TransactionNotification,
} from '../../../../../src/entity/subscriptions';
import { OriginationNotificationMetadata } from '../../../../../src/entity/subscriptions/operations/origination/origination-notification';
import { StorageExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/storage-extractor';
import { getEnumValues } from '../../../../../src/utils/reflection';
import { mockMichelson } from '../../../mocks';
import {
    describeOperationsWithoutRespectiveNotifications,
    itShouldReturnEmptyIfNullMetadata,
    itShouldReturnEmptyIfNullRespectiveProperties,
    runTest,
} from './extractor-test-helper';

describe(StorageExtractor.name, () => {
    const target = new StorageExtractor();

    let storage1: Micheline;
    let storage2: Micheline;
    let storage3: Micheline;

    let internalParent1: ContractResultParent;
    let internalParent2: ContractResultParent;
    let internalResults: readonly InternalOperationResult[];

    beforeEach(() => {
        storage1 = mockMichelson();
        storage2 = mockMichelson();
        storage3 = mockMichelson();

        internalParent1 = {
            kind: InternalOperationKind.transaction,
            result: { storage: storage1 },
        } as ContractResultParent;
        internalParent2 = {
            kind: InternalOperationKind.origination,
            script: { storage: storage2 },
        } as ContractResultParent;
        internalResults = [
            internalParent1,
            { kind: InternalOperationKind.transaction, result: {} }, // Undefined storage.
            internalParent2,
            { kind: InternalOperationKind.reveal, result: {} }, // Unrelated operation.
        ] as InternalOperationResult[];
    });

    const kindsWithInternalResults = [OperationKind.delegation, OperationKind.reveal];
    type OperationWithInternalResults = DelegationNotification | RevealNotification;

    describe.each(kindsWithInternalResults)('%s', (kind) => {
        it('should extract storages from internal results', () => {
            const operation = mockOperation(internalResults);
            runTest(target, operation, [
                { storage: storage1, parent: internalParent1, operation },
                { storage: storage2, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(internal_operation_results?: readonly InternalOperationResult[]) {
            return {
                kind,
                metadata: { internal_operation_results },
            } as OperationWithInternalResults;
        }
    });

    describe(OperationKind.origination, () => {
        it('should extract storages from script and internal results', () => {
            const operation = mockOperation({
                internal_operation_results: internalResults,
            } as OriginationNotificationMetadata);
            runTest(target, operation, [
                { storage: storage3, parent: operation, operation },
                { storage: storage1, parent: internalParent1, operation },
                { storage: storage2, parent: internalParent2, operation },
            ]);
        });

        it.each([
            ['null internal results', { internal_operation_results: null } as OriginationNotificationMetadata],
            ['null metadata', null],
        ])('should extract storages only from script if %s', (_desc, metadata) => {
            const operation = mockOperation(metadata);
            runTest(target, operation, [{ storage: storage3, parent: operation, operation }]);
        });

        function mockOperation(metadata: OriginationNotificationMetadata | null) {
            return {
                kind: OperationKind.origination,
                script: { storage: storage3 },
                metadata,
            } as OriginationNotification;
        }
    });

    describe(OperationKind.transaction, () => {
        it('should extract storages from script and internal results', () => {
            const operation = mockOperation(storage3, internalResults);
            runTest(target, operation, [
                { storage: storage3, parent: operation, operation },
                { storage: storage1, parent: internalParent1, operation },
                { storage: storage2, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, OperationKind.transaction);

        function mockOperation(storage?: Micheline, internal_operation_results?: readonly InternalOperationResult[]) {
            return {
                kind: OperationKind.transaction,
                metadata: {
                    operation_result: { storage },
                    internal_operation_results,
                },
            } as TransactionNotification;
        }
    });

    describeOperationsWithoutRespectiveNotifications(
        target,
        difference(getEnumValues(OperationKind), [
            ...kindsWithInternalResults,
            OperationKind.origination,
            OperationKind.transaction,
        ]),
    );
});
