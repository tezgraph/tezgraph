import { difference } from 'lodash';

import {
    ActivateAccountNotification,
    BalanceUpdate,
    BalanceUpdateParent,
    DelegationNotification,
    DoubleBakingEvidenceNotification,
    DoubleEndorsementEvidenceNotification,
    EndorsementNotification,
    EndorsementWithSlotNotification,
    InternalOperationKind,
    InternalOperationResult,
    OperationKind,
    OriginationNotification,
    RegisterGlobalConstantNotification,
    RevealNotification,
    SeedNonceRevelationNotification,
    TransactionNotification,
} from '../../../../../src/entity/subscriptions';
import { BalanceUpdateExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/balance-update-extractor';
import { asReadonly } from '../../../../../src/utils/conversion';
import { getEnumValues } from '../../../../../src/utils/reflection';
import { mockBalanceUpdate } from '../../../mocks';
import {
    describeOperationsWithoutRespectiveNotifications,
    itShouldReturnEmptyIfNullMetadata,
    itShouldReturnEmptyIfNullRespectiveProperties,
    runTest,
} from './extractor-test-helper';

describe(BalanceUpdateExtractor.name, () => {
    const target = new BalanceUpdateExtractor();

    let update1: BalanceUpdate;
    let update2: BalanceUpdate;
    let update3: BalanceUpdate;
    let update4: BalanceUpdate;
    let update5: BalanceUpdate;

    let internalParent1: BalanceUpdateParent;
    let internalParent2: BalanceUpdateParent;
    let internalResults: readonly InternalOperationResult[];

    beforeEach(() => {
        update1 = mockBalanceUpdate();
        update2 = mockBalanceUpdate();
        update3 = mockBalanceUpdate();
        update4 = mockBalanceUpdate();
        update5 = mockBalanceUpdate();

        internalParent1 = {
            kind: InternalOperationKind.transaction,
            result: { balance_updates: asReadonly([update3, update4]) },
        } as BalanceUpdateParent;
        internalParent2 = {
            kind: InternalOperationKind.origination,
            result: { balance_updates: asReadonly([update5]) },
        } as BalanceUpdateParent;

        internalResults = [
            internalParent1,
            { kind: InternalOperationKind.reveal, result: {} }, // Unrelated operation.
            { kind: InternalOperationKind.transaction, result: {} }, // Undefined updates.
            internalParent2,
        ] as InternalOperationResult[];
    });

    const kindsWithMetadata = [
        OperationKind.activate_account,
        OperationKind.double_baking_evidence,
        OperationKind.double_endorsement_evidence,
        OperationKind.endorsement,
        OperationKind.endorsement_with_slot,
        OperationKind.seed_nonce_revelation,
    ];
    type OperationWithMetadata =
        | ActivateAccountNotification
        | DoubleBakingEvidenceNotification
        | DoubleEndorsementEvidenceNotification
        | EndorsementNotification
        | EndorsementWithSlotNotification
        | SeedNonceRevelationNotification;

    describe.each(kindsWithMetadata)('%s', (kind) => {
        it('should extract updates from metadata', () => {
            const operation = mockOperation([update1, update2]);
            runTest(target, operation, [
                { balance_update: update1, parent: operation, operation },
                { balance_update: update2, parent: operation, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(balance_updates?: readonly BalanceUpdate[]) {
            return {
                kind,
                metadata: { balance_updates },
            } as OperationWithMetadata;
        }
    });

    const kindsWithMetadataAndResult = [OperationKind.delegation, OperationKind.reveal];
    type OperationWithMetadataAndResult = DelegationNotification | RevealNotification;

    // eslint-disable-next-line jest/no-identical-title
    describe.each(kindsWithMetadataAndResult)('%s', (kind) => {
        it('should extract updates from metadata and internal_operation_results', () => {
            const operation = mockOperation([update1, update2], internalResults);
            runTest(target, operation, [
                { balance_update: update1, parent: operation, operation },
                { balance_update: update2, parent: operation, operation },
                { balance_update: update3, parent: internalParent1, operation },
                { balance_update: update4, parent: internalParent1, operation },
                { balance_update: update5, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(
            balance_updates?: readonly BalanceUpdate[],
            internal_operation_results?: readonly InternalOperationResult[],
        ) {
            return {
                kind,
                metadata: { balance_updates, internal_operation_results },
            } as OperationWithMetadataAndResult;
        }
    });

    const kindsWithAll = [OperationKind.origination, OperationKind.register_global_constant, OperationKind.transaction];
    type OperationWithAll = OriginationNotification | RegisterGlobalConstantNotification | TransactionNotification;

    // eslint-disable-next-line jest/no-identical-title
    describe.each(kindsWithAll)('%s', (kind) => {
        it('should extract updates from metadata, internal_operation_results and operation_result', () => {
            const operation = mockOperation([update1], [update2], internalResults);
            runTest(target, operation, [
                { balance_update: update1, parent: operation, operation },
                { balance_update: update2, parent: operation, operation },
                { balance_update: update3, parent: internalParent1, operation },
                { balance_update: update4, parent: internalParent1, operation },
                { balance_update: update5, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(
            metadataBalanceUpdates?: readonly BalanceUpdate[],
            resultBalanceUpdates?: readonly BalanceUpdate[],
            internal_operation_results?: readonly InternalOperationResult[],
        ) {
            return {
                kind,
                metadata: {
                    balance_updates: metadataBalanceUpdates,
                    operation_result: { balance_updates: resultBalanceUpdates },
                    internal_operation_results,
                },
            } as OperationWithAll;
        }
    });

    describeOperationsWithoutRespectiveNotifications(
        target,
        difference(getEnumValues(OperationKind), [
            ...kindsWithMetadataAndResult,
            ...kindsWithMetadataAndResult,
            ...kindsWithAll,
        ]),
    );
});
