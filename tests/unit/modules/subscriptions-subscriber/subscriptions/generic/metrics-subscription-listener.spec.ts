import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, spy, verify, when } from 'ts-mockito';

import { BlockNotification, OperationOrigin } from '../../../../../../src/entity/subscriptions';
import {
    ItemMetricsData,
    MetricsSubscriptionListener,
} from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/generic/metrics-subscription-listener';
import { SubscriptionOptions } from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/subscription';
import { MetricsContainer } from '../../../../../../src/utils/metrics/metrics-container';
import { nameof } from '../../../../../../src/utils/reflection';
import { TestClock } from '../../../../mocks/test-clock';

interface ItemMetricsProvider {
    getItemMetrics(i: string): ItemMetricsData;
}

describe(MetricsSubscriptionListener.name, () => {
    let target: MetricsSubscriptionListener<string, object>;
    let metrics: MetricsContainer;
    let clock: TestClock;
    let metricsProvider: ItemMetricsProvider;

    let options: Writable<SubscriptionOptions<string, object>>;

    beforeEach(() => {
        const metricsInstance = new MetricsContainer();
        clock = new TestClock();
        metricsProvider = mock<ItemMetricsProvider>();
        target = new MetricsSubscriptionListener(metricsInstance, clock, (i) =>
            instance(metricsProvider).getItemMetrics(i),
        );

        metrics = {
            subscriptionsCurrent: spy(metricsInstance.subscriptionsCurrent),
            subscriptionsTotal: spy(metricsInstance.subscriptionsTotal),
            graphQLSubscriptionMethodCounter: spy(metricsInstance.graphQLSubscriptionMethodCounter),
            processNotificationRelativeDuration: spy(metricsInstance.processNotificationRelativeDuration),
            processNotificationAbsoluteDuration: spy(metricsInstance.processNotificationAbsoluteDuration),
        } as MetricsContainer;

        options = {
            subscriptionName: 'fooAdded',
            args: { filter: 'ff' },
        } as SubscriptionOptions<string, object>;
    });

    const argsTestCases: [object, object][] = [
        [{ mempool: 'false', replay: 'false' }, {}],
        [{ mempool: 'false', replay: 'false' }, { other: 66 }],
        [
            { mempool: 'false', replay: 'false' },
            { includeMempool: false, replayFromBlockLevel: null, other: 66 },
        ],
        [
            { mempool: 'true', replay: 'false' },
            { includeMempool: true, other: 66 },
        ],
        [
            { mempool: 'false', replay: 'true' },
            { replayFromBlockLevel: 123, other: 66 },
        ],
        [
            { mempool: 'true', replay: 'true' },
            { includeMempool: true, replayFromBlockLevel: 123, other: 66 },
        ],
    ];

    describe(nameof<MetricsSubscriptionListener<string, object>>('onConnectionOpen'), () => {
        it.each(argsTestCases)('should increase connection count for args %s', (expectedArgs, inputArgs) => {
            options.args = inputArgs;

            target.onConnectionOpen(options);

            const expectedMetrics = deepEqual({ name: 'fooAdded', ...expectedArgs });
            verify(metrics.subscriptionsCurrent.inc(expectedMetrics)).once();
            verify(metrics.subscriptionsTotal.inc(expectedMetrics)).once();
        });
    });

    describe(nameof<MetricsSubscriptionListener<string, object>>('onItemPublish'), () => {
        it.each(argsTestCases)('should measure item from mempool with args %s', (expectedArgs, inputArgs) => {
            options.args = inputArgs;
            when(metricsProvider.getItemMetrics('abc')).thenReturn({
                operation_group: { received_from_tezos_on: new Date(clock.nowDate.getTime() - 3_000) },
                block: null,
            });

            target.onItemPublish('abc', options);

            verify(
                metrics.graphQLSubscriptionMethodCounter.inc(deepEqual({ method: 'fooAdded', ...expectedArgs })),
            ).once();
            verify(metrics.processNotificationRelativeDuration.observe({ origin: OperationOrigin.mempool }, 3_000));
            verify(metrics.processNotificationAbsoluteDuration.observe(anything())).never();
        });

        it.each(argsTestCases)('should measure item from block with args %s', (expectedArgs, inputArgs) => {
            options.args = inputArgs;
            when(metricsProvider.getItemMetrics('abc')).thenReturn({
                operation_group: { received_from_tezos_on: new Date(clock.nowDate.getTime() - 3_000) },
                block: { header: { timestamp: new Date(clock.nowDate.getTime() - 5_000) } } as BlockNotification,
            });

            target.onItemPublish('abc', options);

            verify(
                metrics.graphQLSubscriptionMethodCounter.inc(deepEqual({ method: 'fooAdded', ...expectedArgs })),
            ).once();
            verify(metrics.processNotificationRelativeDuration.observe({ origin: OperationOrigin.block }, 3_000));
            verify(metrics.processNotificationAbsoluteDuration.observe({ origin: OperationOrigin.block }, 5_000));
        });
    });

    describe(nameof<MetricsSubscriptionListener<string, object>>('onConnectionClose'), () => {
        it.each(argsTestCases)('should increase connection count for args %s', (expectedArgs, inputArgs) => {
            options.args = inputArgs;

            target.onConnectionClose(options);

            verify(metrics.subscriptionsCurrent.dec(deepEqual({ name: 'fooAdded', ...expectedArgs }))).once();
        });
    });
});
