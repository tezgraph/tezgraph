import { anything, instance, mock, verify, when } from 'ts-mockito';

import { Filter, FilteringArgs, FilterOptimizer } from '../../../../../../src/entity/subscriptions';
import { FilteringSubscription } from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/generic/filtering-subscription';
import {
    Subscription,
    SubscriptionOptions,
} from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/subscription';
import { iterableToArray, toAsyncIterable } from '../../../../../../src/utils/collections/async-iterable-utils';
import { Nullish } from '../../../../../../src/utils/reflection';

describe(FilteringSubscription.name, () => {
    let target: Subscription<string, FilteringArgs<string>>;
    let innerSubscription: Subscription<string, FilteringArgs<string>>;
    let filterOptimizer: FilterOptimizer;

    beforeEach(() => {
        innerSubscription = mock<Subscription<string, FilteringArgs<string>>>();
        filterOptimizer = mock(FilterOptimizer);
        target = new FilteringSubscription(instance(filterOptimizer), instance(innerSubscription));
    });

    it('should yield item if it passes the filter', async () => {
        const unOptimizedFilter = {} as Filter<string>;
        const filter = mock<Filter<string>>();
        const options = mockOptions(unOptimizedFilter);
        when(filterOptimizer.optimize(unOptimizedFilter)).thenReturn(instance(filter));
        when(innerSubscription.subscribe(options)).thenReturn(toAsyncIterable(['a', 'b', 'c', 'd']));
        when(filter.passes('a')).thenReturn(true);
        when(filter.passes('c')).thenReturn(true);

        const operations = await iterableToArray(target.subscribe(options));

        expect(operations).toEqual(['a', 'c']);
    });

    it.each([
        ['undefined filter', undefined],
        ['null filter', null],
    ])('should return iterable from inner if %s', (_desc, filter) => {
        runReturnInnerIterableTest(filter);

        verify(filterOptimizer.optimize(anything())).never();
    });

    it('should return iterable from inner if filter optimized to null', () => {
        const filter = {} as Filter<string>;
        when(filterOptimizer.optimize(filter)).thenReturn(null);

        runReturnInnerIterableTest(filter);
    });

    function runReturnInnerIterableTest(filter: Nullish<Filter<string>>) {
        const options = mockOptions(filter);
        const innerIterable = {} as AsyncIterableIterator<string>;
        when(innerSubscription.subscribe(options)).thenReturn(innerIterable);

        const iterable = target.subscribe(options);

        expect(iterable).toBe(innerIterable);
    }

    function mockOptions(filter: Nullish<Filter<string>>) {
        return { args: { filter } } as SubscriptionOptions<string, FilteringArgs<string>>;
    }
});
