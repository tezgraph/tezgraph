import { RpcClient } from '@taquito/rpc';
import { mock } from 'ts-mockito';
import { container, DependencyContainer } from 'tsyringe';

import { ModuleDependency, ModuleName } from '../../../src/modules/module';
import {
    onlyOnePubSubModuleEnabledDependency,
    registerTezosRpcHealthWatcher,
    RequireOneOfModulesDependency,
    RequirePubSubDependency,
} from '../../../src/modules/module-helpers';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { healthChecksDIToken } from '../../../src/utils/health/health-check';
import { LoggerFactory } from '../../../src/utils/logging';
import { expectToThrow } from '../mocks';

describe(RequireOneOfModulesDependency.name, () => {
    let target: ModuleDependency;

    beforeEach(() => {
        target = new RequireOneOfModulesDependency(['Foo' as ModuleName, 'Bar' as ModuleName], { reason: 'I said so' });
    });

    it.each(['Foo', 'Bar'])('should pass if one of required modules (%s) is enabled', (moduleName) => {
        expect(() => validate(target, [moduleName])).not.toThrow();
    });

    it('should pass if all of required modules are enabled', () => {
        expect(() => validate(target, ['Foo', 'Bar'])).not.toThrow();
    });

    it('should throw if module not enabled', () => {
        const error = expectToThrow(() => validate(target, ['Enabled1', 'Enabled2']));

        expect(error.message).toIncludeMultiple([
            `'MODULES'`,
            `'This'`,
            `'Foo', 'Bar'`,
            `'Enabled1', 'Enabled2'`,
            'I said so',
        ]);
    });
});

describe(RequirePubSubDependency, () => {
    let target: ModuleDependency;

    beforeEach(() => {
        target = new RequirePubSubDependency({ reason: 'Let it be' });
    });

    it.each([ModuleName.MemoryPubSub, ModuleName.RedisPubSub])('should pass if PubSub enabled (%s)', (moduleName) => {
        expect(() => validate(target, ['Other', moduleName])).not.toThrow();
    });

    it('should throw if no PuSub', () => {
        const error = expectToThrow(() => validate(target, ['Other']));

        expect(error.message).toContain('Let it be');
    });
});

describe('OnlyOnePubSubModuleEnabledDependency', () => {
    const target = onlyOnePubSubModuleEnabledDependency;

    it.each([ModuleName.MemoryPubSub, ModuleName.RedisPubSub])(
        'should pass if only one PubSub enabled',
        (moduleName) => {
            expect(() => validate(target, ['Other', moduleName])).not.toThrow();
        },
    );
});

function validate(target: ModuleDependency, enabledModuleNames: string[]) {
    target.validate(
        'This' as ModuleName,
        enabledModuleNames.map((n) => n as ModuleName),
    );
}

describe(registerTezosRpcHealthWatcher.name, () => {
    let testContainer: DependencyContainer;

    beforeEach(() => {
        testContainer = container.createChildContainer();
        testContainer.registerInstance(RpcClient, {} as RpcClient);
        testContainer.registerInstance(EnvConfig, {} as EnvConfig);
        testContainer.registerInstance(LoggerFactory, mock(LoggerFactory));
    });

    it('should register RPC health watcher', () => {
        expect(resolveHealthChecks()).toBeEmpty();

        registerTezosRpcHealthWatcher(testContainer);

        expect(resolveHealthChecks()).toHaveLength(1);
    });

    it('should register only once', () => {
        registerTezosRpcHealthWatcher(testContainer);
        registerTezosRpcHealthWatcher(testContainer);

        expect(resolveHealthChecks()).toHaveLength(1);
    });

    function resolveHealthChecks() {
        return testContainer.isRegistered(healthChecksDIToken) ? testContainer.resolveAll(healthChecksDIToken) : [];
    }
});
