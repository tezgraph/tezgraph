import { Counter, Gauge } from 'prom-client';
import { anything, deepEqual, instance, mock, spy, verify } from 'ts-mockito';

import { BlockHeader, BlockNotification, OperationOrigin } from '../../../../../src/entity/subscriptions';
import { BlockExternalPubSubPublisher } from '../../../../../src/modules/tezos-monitor/block/block-external-pubsub-publisher';
import { ExternalPubSub, externalTriggers, ItemProcessor } from '../../../../../src/utils';
import { asReadonly } from '../../../../../src/utils/conversion';
import { LogLevel } from '../../../../../src/utils/logging';
import { MetricsContainer, OperationCountLabel } from '../../../../../src/utils/metrics/metrics-container';
import { mockOperation, TestLogger } from '../../../mocks';

describe(BlockExternalPubSubPublisher.name, () => {
    let target: ItemProcessor<BlockNotification>;
    let externalPubSub: ExternalPubSub;
    let logger: TestLogger;
    let metrics: MetricsContainer;
    let blockLevelSpy: Gauge<string>;
    let operationsCountSpy: Counter<OperationCountLabel>;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        logger = new TestLogger();
        metrics = new MetricsContainer();
        target = new BlockExternalPubSubPublisher(instance(externalPubSub), logger, metrics);
        operationsCountSpy = spy(metrics.tezosMonitorOperationCount);
        blockLevelSpy = spy(metrics.tezosMonitorBlockLevel);
    });

    it('should publish block', async () => {
        const block = {
            hash: 'SomeHash',
            operations: asReadonly([mockOperation(), mockOperation()]),
            header: { level: 123 } as BlockHeader,
        } as BlockNotification;

        await target.processItem(block);

        verify(externalPubSub.publish(anything(), anything())).once();
        verify(externalPubSub.publish(externalTriggers.blocks, block)).once();

        logger.loggedSingle().verify(LogLevel.Information, {
            hash: 'SomeHash',
            operationCount: 2,
        });

        verify(operationsCountSpy.inc(deepEqual({ origin: OperationOrigin.block }), 2)).once();
        verify(blockLevelSpy.set(123)).once();
    });
});
