import { CompositeModule, CompositeModuleName, Module, ModuleName } from '../../../src/modules/module';
import { nameof } from '../../../src/utils/reflection';

describe(CompositeModule.name, () => {
    describe(nameof<typeof CompositeModule>('create'), () => {
        it('should be created correctly', () => {
            const module1 = { name: 'Foo' as ModuleName } as Module;
            const module2 = { name: 'Bar' as ModuleName } as Module;

            const target = CompositeModule.create('Test' as CompositeModuleName, [module1, module2]);

            expect(target.name).toBe('Test');
            expect(target.mappedModuleNames).toEqual(['Foo', 'Bar']);
        });
    });
});
