import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    EndorsementNotificationMetadata,
    EndorsementWithSlotNotification,
    OperationKind,
} from '../../../../src/entity/subscriptions';
import { EndorsementMetadataConverter } from '../../../../src/rpc/converters/common/endorsement-metadata-converter';
import { InlinedEndorsementConverter } from '../../../../src/rpc/converters/common/inlined-endorsement-converter';
import {
    EndorsementWithSlotConverter,
    RpcEndorsementWithSlot,
} from '../../../../src/rpc/converters/endorsement-with-slot-converter';
import { mockInlinedEndorsement } from '../../mocks';
import { mockBaseProperties } from './rpc-mocks';

describe(EndorsementWithSlotConverter.name, () => {
    let target: EndorsementWithSlotConverter;
    let metadataConverter: EndorsementMetadataConverter;
    let inlinedEndorsementConverter: InlinedEndorsementConverter;

    const act = (e: RpcEndorsementWithSlot) => target.convert(e, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(EndorsementMetadataConverter);
        inlinedEndorsementConverter = mock(InlinedEndorsementConverter);
        target = new EndorsementWithSlotConverter(instance(metadataConverter), instance(inlinedEndorsementConverter));
    });

    it('should convert values correctly', () => {
        const rpcOperation: RpcEndorsementWithSlot = {
            kind: rpc.OpKind.ENDORSEMENT_WITH_SLOT,
            endorsement: 'rpcInlincedEndorsement' as any,
            slot: 111,
        };
        const metadata: EndorsementNotificationMetadata = 'mockedMetadata' as any;
        const inlEndorsement = mockInlinedEndorsement();
        when(metadataConverter.convert(rpcOperation)).thenReturn(metadata);
        when(inlinedEndorsementConverter.convert(rpcOperation.endorsement)).thenReturn(inlEndorsement);

        const operation = act(rpcOperation);

        expect(operation).toEqual<EndorsementWithSlotNotification>({
            graphQLTypeName: EndorsementWithSlotNotification.name,
            kind: OperationKind.endorsement_with_slot,
            slot: 111,
            metadata,
            endorsement: inlEndorsement,
            ...mockBaseProperties(),
        });
    });
});
