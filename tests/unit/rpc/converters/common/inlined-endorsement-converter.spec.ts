import * as rpc from '@taquito/rpc';
import { DeepPartial, Writable } from 'ts-essentials';

import { InlinedEndorsement, InlinedEndorsementKind } from '../../../../../src/entity/subscriptions';
import { InlinedEndorsementConverter } from '../../../../../src/rpc/converters/common';

describe(InlinedEndorsementConverter.name, () => {
    let target: InlinedEndorsementConverter;
    let rpcEndorsement: Writable<rpc.InlinedEndorsement>;

    beforeEach(() => {
        target = new InlinedEndorsementConverter();

        rpcEndorsement = {
            branch: 'br1',
            operations: {
                kind: rpc.OpKind.ENDORSEMENT,
                level: 111,
                slot: 222,
                round: 333,
                block_payload_hash: 'haha',
            },
            signature: 'sig1',
        };
    });

    it(`should convert values correctly`, () => {
        const endorsement = target.convert(rpcEndorsement);

        expect(endorsement).toEqual<InlinedEndorsement>({
            branch: 'br1',
            operations: {
                kind: InlinedEndorsementKind.endorsement,
                level: 111,
                slot: 222,
                round: 333,
                block_payload_hash: 'haha',
            },
            signature: 'sig1',
        });
    });

    it(`should handle nulls`, () => {
        rpcEndorsement.operations = { kind: rpc.OpKind.ENDORSEMENT, level: 111 };

        const endorsement = target.convert(rpcEndorsement);

        expect(endorsement).toMatchObject<DeepPartial<InlinedEndorsement>>({
            operations: {
                kind: InlinedEndorsementKind.endorsement,
                level: 111,
                slot: null,
                round: null,
                block_payload_hash: null,
            },
        });
    });
});
