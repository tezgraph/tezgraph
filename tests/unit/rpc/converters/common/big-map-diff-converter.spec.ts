import * as rpc from '@taquito/rpc';

import {
    BigMapAlloc,
    BigMapCopy,
    BigMapDiff,
    BigMapDiffAction,
    BigMapRemove,
    BigMapUpdate,
} from '../../../../../src/entity/subscriptions';
import { BigMapDiffConverter } from '../../../../../src/rpc/converters/common/big-map-diff-converter';
import { typed } from '../../../../../src/utils/reflection';
import { expectToThrow } from '../../../mocks';

describe(BigMapDiffConverter.name, () => {
    const target = new BigMapDiffConverter();

    itShouldConvert({
        input: {
            action: BigMapDiffAction.alloc,
            big_map: '111',
            key_type: { prim: 'michelson key type' },
            value_type: { prim: 'michelson value type' },
        },
        expected: typed<BigMapAlloc>({
            graphQLTypeName: BigMapAlloc.name,
            action: BigMapDiffAction.alloc,
            big_map: BigInt(111),
            key_type: { prim: 'michelson key type' },
            value_type: { prim: 'michelson value type' },
        }),
    });
    itShouldConvert({
        input: {
            action: BigMapDiffAction.copy,
            source_big_map: '222',
            destination_big_map: '333',
        },
        expected: typed<BigMapCopy>({
            graphQLTypeName: BigMapCopy.name,
            action: BigMapDiffAction.copy,
            source_big_map: BigInt(222),
            destination_big_map: BigInt(333),
        }),
    });
    itShouldConvert({
        input: {
            action: BigMapDiffAction.remove,
            big_map: '111',
        },
        expected: typed<BigMapRemove>({
            graphQLTypeName: BigMapRemove.name,
            action: BigMapDiffAction.remove,
            big_map: BigInt(111),
        }),
    });
    itShouldConvert({
        input: {
            action: BigMapDiffAction.update,
            big_map: '111',
            key_hash: 'hhh',
            key: { prim: 'michelson key' },
            value: { prim: 'michelson value' },
        },
        expected: typed<BigMapUpdate>({
            graphQLTypeName: BigMapUpdate.name,
            action: BigMapDiffAction.update,
            big_map: BigInt(111),
            key_hash: 'hhh',
            key: { prim: 'michelson key' },
            value: { prim: 'michelson value' },
        }),
    });

    function itShouldConvert(options: { input: rpc.ContractBigMapDiffItem; expected: BigMapDiff }) {
        it(`should convert ${options.input.action} correctly`, () => {
            const diff = target.convert(options.input);

            expect(diff).toEqual(options.expected);
        });
    }

    it('should throw if unsupported action', () => {
        const rpcDiff = { action: 'wtf' as any } as rpc.ContractBigMapDiffItem;

        const ex = expectToThrow(() => target.convert(rpcDiff));

        expect(ex.message).toIncludeMultiple(['wtf', BigMapDiffAction.alloc, BigMapDiffAction.update]);
    });
});
