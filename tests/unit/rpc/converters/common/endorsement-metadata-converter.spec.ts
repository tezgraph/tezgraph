import * as rpc from '@taquito/rpc';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { EndorsementNotificationMetadata } from '../../../../../src/entity/subscriptions';
import { BalanceUpdateConverter } from '../../../../../src/rpc/converters/common/balance-update-converter';
import { EndorsementMetadataConverter } from '../../../../../src/rpc/converters/common/endorsement-metadata-converter';
import { mockBalanceUpdate } from '../../../mocks';

describe(EndorsementMetadataConverter.name, () => {
    let target: EndorsementMetadataConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new EndorsementMetadataConverter(instance(balanceUpdateConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: rpc.OperationContentsAndResultEndorsement = {
            kind: rpc.OpKind.ENDORSEMENT,
            level: 111,
            metadata: {
                balance_updates: 'rpcBalanceUpdates' as any,
                delegate: 'ddd',
                slots: [222],
                endorsement_power: 333,
            },
        };
        const balanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convertArray(rpcOperation.metadata.balance_updates)).thenReturn(balanceUpdates);

        const metadata = target.convert(rpcOperation);

        expect(metadata).toEqual<EndorsementNotificationMetadata>({
            balance_updates: balanceUpdates,
            delegate: 'ddd',
            slots: [222],
            endorsement_power: 333,
            graphQLTypeName: EndorsementNotificationMetadata.name,
        });
    });

    it('should handle no metadata correctly', () => {
        const rpcOperation: rpc.OperationContentsEndorsement = {
            kind: rpc.OpKind.ENDORSEMENT,
            level: 111,
        };

        const metadata = target.convert(rpcOperation);

        expect(metadata).toBeNull();
        verify(balanceUpdateConverter.convert(anything())).never();
    });
});
