import { anything, spy, verify, when } from 'ts-mockito';

import { RpcConverter } from '../../../../../src/rpc/converters/common/rpc-converter';
import { nameof } from '../../../../../src/utils/reflection';

class TargetConverter extends RpcConverter<number, string> {
    convert(_rpcValue: number): string {
        throw new Error('Method not implemented.');
    }
}

describe(RpcConverter.name, () => {
    let target: TargetConverter;
    let targetSpy: TargetConverter;

    beforeEach(() => {
        target = new TargetConverter();
        targetSpy = spy(target);
    });

    describe(nameof<TargetConverter>('convertNullable'), () => {
        it.each([
            ['JavaScript nullish', 0],
            ['regular non-nullish', 123],
        ])('should convert %s value correctly', (_desc, input) => {
            when(targetSpy.convert(input)).thenReturn('abc');

            const value = target.convertNullable(input);

            expect(value).toBe('abc');
        });

        it.each([null, undefined])('should return null if %s', (input) => {
            const value = target.convertNullable(input);

            expect(value).toBeNull();
            verify(targetSpy.convert(anything())).never();
        });
    });

    describe(nameof<TargetConverter>('convertToNonNullable'), () => {
        it.each([0, 123, null, undefined])(
            `should pass through to ${nameof<TargetConverter>('convert')}() if %s`,
            (input) => {
                when(targetSpy.convert(input!)).thenReturn('abc');

                const value = target.convertToNonNullable(input);

                expect(value).toBe('abc');
            },
        );
    });
});
