import * as rpc from '@taquito/rpc';

import { ScriptedContracts } from '../../../../../src/entity/subscriptions';
import { ScriptedContractsConverter } from '../../../../../src/rpc/converters/common';
import { mockMichelson } from '../../../mocks';

describe(ScriptedContractsConverter, () => {
    const target = new ScriptedContractsConverter();

    it('should convert correctly', () => {
        const rpcScript: rpc.ScriptedContracts = {
            code: [mockMichelson()],
            storage: mockMichelson(),
        };

        const script = target.convert(rpcScript);

        expect(script).toEqual<ScriptedContracts>(rpcScript);
    });
});
