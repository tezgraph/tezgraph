import * as rpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { anyNumber, instance, mock, verify, when } from 'ts-mockito';

import { BalanceUpdate, InternalOperationResult } from '../../../../../src/entity/subscriptions';
import { BalanceUpdateConverter } from '../../../../../src/rpc/converters/common';
import {
    MetadataWithResult,
    OperationMetadataConverter,
    OperationResultConverter,
    RpcMetadataWithResult,
    RpcOperation,
} from '../../../../../src/rpc/converters/common/operation-metadata-converter';
import { InternalOperationResultConverter } from '../../../../../src/rpc/converters/operation-results/internal-operation-result-converter';
import { nameof } from '../../../../../src/utils/reflection';

describe(OperationMetadataConverter.name, () => {
    let target: OperationMetadataConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let internalOperationResultConverter: InternalOperationResultConverter;

    let rpcOperation: Writable<RpcOperation<RpcMetadataWithResult<number>>>;
    let resultConverter: OperationResultConverter<number, string>;
    let balance_updates: readonly BalanceUpdate[];
    let internal_operation_results: readonly InternalOperationResult[];

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        internalOperationResultConverter = mock(InternalOperationResultConverter);
        resultConverter = mock<OperationResultConverter<number, string>>();
        target = new OperationMetadataConverter(
            instance(balanceUpdateConverter),
            instance(internalOperationResultConverter),
        );

        rpcOperation = {
            kind: rpc.OpKind.BALLOT,
            metadata: {
                balance_updates: 'rpcBalanceUpdates' as any,
                internal_operation_results: [{ source: 'src-input' } as rpc.InternalOperationResult],
                operation_result: 123,
            },
        };

        balance_updates = 'mockedBalanceUpdates' as any;
        internal_operation_results = 'mockedInternalOperationResults' as any;

        when(balanceUpdateConverter.convertArray(rpcOperation.metadata!.balance_updates)).thenReturn(balance_updates);
        when(
            internalOperationResultConverter.convertNullableArray(rpcOperation.metadata!.internal_operation_results),
        ).thenReturn(internal_operation_results);
    });

    describe(nameof<OperationMetadataConverter>('convertSimple'), () => {
        it('should convert values correctly', () => {
            const metadata = target.convertSimple(rpcOperation);

            expect(metadata).toEqual({ balance_updates });
        });

        it('should return undefined if undefined metadata', () => {
            rpcOperation.metadata = undefined;

            const metadata = target.convertSimple(rpcOperation);

            expect(metadata).toBeNull();
        });
    });

    describe(nameof<OperationMetadataConverter>('convert'), () => {
        it('should convert values correctly', () => {
            when(resultConverter.convert(rpcOperation.metadata!.operation_result)).thenReturn('mockedResult');

            const metadata = target.convert(rpcOperation, instance(resultConverter), 'MyGraph');

            expect(metadata).toEqual<MetadataWithResult<string>>({
                balance_updates,
                internal_operation_results,
                graphQLTypeName: 'MyGraph',
                operation_result: 'mockedResult',
            });
        });

        it('should return undefined if undefined metadata', () => {
            rpcOperation.metadata = undefined;

            const metadata = target.convert(rpcOperation, instance(resultConverter), 'MyGraph');

            expect(metadata).toBeNull();
            verify(resultConverter.convert(anyNumber())).never();
        });
    });
});
