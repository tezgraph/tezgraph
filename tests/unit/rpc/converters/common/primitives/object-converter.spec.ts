import { ObjectConverter } from '../../../../../../src/rpc/converters/common/primitives/object-converter';
import { nameof, Nullish } from '../../../../../../src/utils/reflection';
import { safeJsonStringify } from '../../../../../../src/utils/safe-json-stringifier';
import { expectToThrow } from '../../../../mocks';

interface Foo {
    foo: number;
}
interface Bar {
    bar: number;
}

describe(ObjectConverter.name, () => {
    const target = new ObjectConverter();

    describe(nameof<ObjectConverter>('guard'), () => {
        const act = (f: Foo) => target.guard(f);

        itShouldReturnSameIfLiteralObject(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<ObjectConverter>('guardNullable'), () => {
        const act = (f: Foo) => target.guardNullable(f);

        itShouldReturnSameIfLiteralObject(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldReturnNullIfNullishInput(act);
    });

    describe(nameof<ObjectConverter>('guardNonNullable'), () => {
        const act = (f: Foo) => target.guardNonNullable(f);

        itShouldReturnSameIfLiteralObject(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<ObjectConverter>('convert'), () => {
        const act = (f: Foo) => target.convert(f, convertFunc);

        itShouldConvertIfObjectLiteral(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<ObjectConverter>('convertNullable'), () => {
        const act = (f: Foo) => target.convertNullable(f, convertFunc);

        itShouldConvertIfObjectLiteral(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldReturnNullIfNullishInput(act);
    });

    describe(nameof<ObjectConverter>('convertToNonNullable'), () => {
        const act = (f: Foo) => target.convertToNonNullable(f, convertFunc);

        itShouldConvertIfObjectLiteral(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    type ActFunc = (obj: Foo) => unknown;

    function itShouldReturnSameIfLiteralObject(act: ActFunc) {
        it('should return same if object literal on input', () => {
            const input: Foo = { foo: 123 };

            const result = act(input);

            expect(result).toBe(input);
        });
    }

    function convertFunc(obj: Foo): Bar {
        return { bar: obj.foo };
    }

    function itShouldConvertIfObjectLiteral(act: (o: Foo) => Nullish<Bar>) {
        it('should convert correctly if object literal on input', () => {
            const input: Foo = { foo: 123 };

            const result = act(input);

            expect(result).toEqual<Bar>({ bar: 123 });
        });
    }

    function itShouldReturnNullIfNullishInput(act: ActFunc) {
        it.each([null, undefined])('should return null if %s input', (input: any) => {
            const result = act(input);

            expect(result).toBeNull();
        });
    }

    function itShouldThrowIfUnsupportedInput(act: ActFunc) {
        itShouldThrowIfInput(act, ['unsupported', 123, new Date(), [123]]); // Date is also an object.
    }

    function itShouldThrowIfNullishInput(act: ActFunc) {
        itShouldThrowIfInput(act, [null, undefined]);
    }

    function itShouldThrowIfInput(act: ActFunc, inputs: any[]) {
        it.each(inputs)('should throw if %s input', (input) => {
            const error = expectToThrow(() => act(input));

            expect(error.message).toIncludeMultiple([safeJsonStringify(input), 'object literal']);
        });
    }
});
