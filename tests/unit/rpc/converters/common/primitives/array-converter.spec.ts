import { ArrayConverter } from '../../../../../../src/rpc/converters/common/primitives/array-converter';
import { nameof, Nullish } from '../../../../../../src/utils/reflection';
import { safeJsonStringify } from '../../../../../../src/utils/safe-json-stringifier';
import { expectToThrow } from '../../../../mocks';

describe(ArrayConverter.name, () => {
    const target = new ArrayConverter();

    describe(nameof<ArrayConverter>('guard'), () => {
        const act = (a: number[]) => target.guard(a);

        itShouldReturnSameIfArray(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<ArrayConverter>('guardNullable'), () => {
        const act = (a: number[]) => target.guardNullable(a);

        itShouldReturnSameIfArray(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldReturnNullIfNullishInput(act);
    });

    describe(nameof<ArrayConverter>('guardNonNullable'), () => {
        const act = (a: number[]) => target.guardNonNullable(a);

        itShouldReturnSameIfArray(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<ArrayConverter>('convert'), () => {
        const act = (a: number[]) => target.convert(a, convertFunc);

        itShouldConvertIfArray(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<ArrayConverter>('convertNullable'), () => {
        const act = (a: number[]) => target.convertNullable(a, convertFunc);

        itShouldConvertIfArray(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldReturnNullIfNullishInput(act);
    });

    describe(nameof<ArrayConverter>('convertToNonNullable'), () => {
        const act = (a: number[]) => target.convertToNonNullable(a, convertFunc);

        itShouldConvertIfArray(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    type ActFunc = (array: number[]) => Nullish<unknown[]>;

    function itShouldReturnSameIfArray(act: ActFunc) {
        it('should return same if array on input', () => {
            const input = [1, 2, 3];

            const result = act(input);

            expect(result).toBe(input);
        });
    }

    function convertFunc(x: number): string {
        return `x-${x}`;
    }

    function itShouldConvertIfArray(act: ActFunc) {
        it('should convert correctly if array on input', () => {
            const input = [1, 2, 3];

            const result = act(input);

            expect(result).toEqual(['x-1', 'x-2', 'x-3']);
        });
    }

    function itShouldReturnNullIfNullishInput(act: ActFunc) {
        it.each([null, undefined])('should return null if %s input', (input: any) => {
            const result = act(input);

            expect(result).toBeNull();
        });
    }

    function itShouldThrowIfUnsupportedInput(act: ActFunc) {
        itShouldThrowIfInput(act, ['unsupported', 123, { obj: 123 }]);
    }

    function itShouldThrowIfNullishInput(act: ActFunc) {
        itShouldThrowIfInput(act, [null, undefined]);
    }

    function itShouldThrowIfInput(act: ActFunc, inputs: any[]) {
        it.each(inputs)('should throw if %s input', (input) => {
            const error = expectToThrow(() => act(input));

            expect(error.message).toIncludeMultiple([safeJsonStringify(input), 'array']);
        });
    }
});
