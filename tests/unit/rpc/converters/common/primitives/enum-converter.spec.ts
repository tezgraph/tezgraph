import { EnumConverter } from '../../../../../../src/rpc/converters/common/primitives/enum-converter';
import { nameof, Nullish } from '../../../../../../src/utils/reflection';
import { safeJsonStringify } from '../../../../../../src/utils/safe-json-stringifier';
import { expectToThrow } from '../../../../mocks';

enum Memes {
    Omg = 'omg',
    Wtf = 'wtf',
}

describe(EnumConverter, () => {
    const target = new EnumConverter();

    describe(nameof<EnumConverter>('convert'), () => {
        const act = (s: string) => target.convert(Memes, s);

        itShouldConvertIfEnum(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    describe(nameof<EnumConverter>('convertNullable'), () => {
        const act = (s: string) => target.convertNullable(Memes, s);

        itShouldConvertIfEnum(act);
        itShouldThrowIfUnsupportedInput(act);

        it.each([null, undefined])('should return null if %s input', (input: any) => {
            const result = act(input);

            expect(result).toBeNull();
        });
    });

    describe(nameof<EnumConverter>('convertToNonNullable'), () => {
        const act = (s: string) => target.convertToNonNullable(Memes, s);

        itShouldConvertIfEnum(act);
        itShouldThrowIfUnsupportedInput(act);
        itShouldThrowIfNullishInput(act);
    });

    type ActFunc = (str: string) => Nullish<Memes>;

    function itShouldConvertIfEnum(act: ActFunc) {
        it.each([
            ['Omg', Memes.Omg],
            ['Wtf', Memes.Wtf],
        ])('should convert correctly if enum %s on input', (input, expected) => {
            const result = act(input);

            expect(result).toEqual(expected);
        });
    }

    function itShouldThrowIfNullishInput(act: ActFunc) {
        itShouldThrowIfInput(act, [null, undefined]);
    }

    function itShouldThrowIfUnsupportedInput(act: ActFunc) {
        itShouldThrowIfInput(act, ['unsupported', 123, Memes.Omg]); // Omg has different letter casing.
    }

    function itShouldThrowIfInput(act: ActFunc, inputs: any[]) {
        it.each(inputs)('should throw if %s input', (input) => {
            const error = expectToThrow(() => act(input));

            expect(error.message).toIncludeMultiple([safeJsonStringify(input), 'supported', 'Omg', 'Wtf']);
        });
    }
});
