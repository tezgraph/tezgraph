import { DateConverter } from '../../../../../../src/rpc/converters/common/primitives/date-converter';
import { expectToThrow } from '../../../../mocks';

describe(DateConverter.name, () => {
    const target = new DateConverter();

    it('should return same value if Date', () => {
        const date = new Date();

        const result = target.convert(date);

        expect(result).toBe(date);
    });

    it('should parse if string', () => {
        const result = target.convert('2020-09-27T23:57:46.123Z');

        expect(result).toEqual(new Date('2020-09-27T23:57:46.123Z'));
    });

    it.each([
        null,
        undefined,
        0,
        true,
        'gibberish',
        '2020-09-27', // Not full ISO format.
        '',
    ])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
