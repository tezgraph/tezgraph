import { BigIntConverter } from '../../../../../../src/rpc/converters/common/primitives/big-int-converter';
import { expectToThrow } from '../../../../mocks';

describe(BigIntConverter.name, () => {
    const target = new BigIntConverter();

    it.each(['-123', '0', '123'])('should convert %s correctly', (input) => {
        const result = target.convert(input);

        expect(result).toBe(BigInt(input));
    });

    it.each([
        null,
        undefined,
        123, // Int.
        '1.23', // Float.
        'abc',
        '',
        '  ', // White-space.
    ])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
