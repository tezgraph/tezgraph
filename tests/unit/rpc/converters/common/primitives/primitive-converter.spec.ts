import { spy, when } from 'ts-mockito';

import { PrimitiveConverter } from '../../../../../../src/rpc/converters/common/primitives/primitive-converter';
import { expectToThrow } from '../../../../mocks';

class TargetConverter extends PrimitiveConverter<string, number> {
    protected readonly typeDescription: string = 'Foo Bar';

    convertInternal(_rpcValue: string): number {
        throw new Error('Method not implemented.');
    }
}

describe(PrimitiveConverter.name, () => {
    let target: TargetConverter;
    let targetSpy: TargetConverter;

    beforeEach(() => {
        target = new TargetConverter();
        targetSpy = spy(target);
    });

    it('should pass correctly converted value', () => {
        when(targetSpy.convertInternal('abc')).thenReturn(123);

        const result = target.convert('abc');

        expect(result).toBe(123);
    });

    it.each([
        ['abc', '"abc"'],
        [null, 'null'],
        [undefined, 'null'],
    ])('it should wrap error if %s value', (input, expectedErrorSubstr) => {
        when(targetSpy.convertInternal(input!)).thenThrow(new Error('Oops'));

        const error = expectToThrow(() => target.convert(input!));

        expect(error.message).toIncludeMultiple(['Foo Bar', 'Oops', expectedErrorSubstr, 'Foo Bar']);
    });
});
