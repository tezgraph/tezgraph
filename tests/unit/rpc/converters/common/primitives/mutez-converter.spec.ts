import { MutezConverter } from '../../../../../../src/rpc/converters/common/primitives/mutez-converter';
import { expectToThrow } from '../../../../mocks';

describe(MutezConverter.name, () => {
    const target = new MutezConverter();

    it.each(['0', '123'])('should convert %s correctly', (input) => {
        const result = target.convert(input);

        expect(result).toBe(BigInt(input));
    });

    it.each([
        '-456',
        null,
        undefined,
        1.23, // Float.
        123, // Int but not inside a string.
        '',
    ])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
