import { RpcCollectionConverter } from '../../../../../src/rpc/converters/common/rpc-collection-converter';
import { nameof } from '../../../../../src/utils/reflection';
import { expectToThrow } from '../../../mocks';

class TargetConverter extends RpcCollectionConverter<string, string> {
    convert(rpcItem: string): string {
        return `conv-${rpcItem}`;
    }
}

describe(RpcCollectionConverter.name, () => {
    const target = new TargetConverter();

    describe(nameof<TargetConverter>('convertArray'), () => {
        itShouldConvertAllItems((i) => target.convertArray(i));

        it.each([null, undefined])('should throw if %s input', (input) => {
            expectToThrow(() => target.convertArray(input!));
        });
    });

    describe(nameof<TargetConverter>('convertNullableArray'), () => {
        itShouldConvertAllItems((i) => target.convertNullableArray(i));

        it.each([null, undefined])('should return null if %s input', (input) => {
            const converted = target.convertNullableArray(input);

            expect(converted).toBeNull();
        });
    });

    function itShouldConvertAllItems(act: (i: string[]) => readonly string[] | null) {
        it('should convert all items ', () => {
            const converted = act(['a', 'b']);

            expect(converted).toEqual(['conv-a', 'conv-b']);
        });
    }
});
