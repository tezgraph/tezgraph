import * as rpc from '@taquito/rpc';

import { RevealNotificationResult } from '../../../../../src/entity/subscriptions';
import { RevealResultConverter } from '../../../../../src/rpc/converters/operation-results/reveal-result-converter';
import { basePropertiesMock } from './base-operation-result.mock';

describe(RevealResultConverter.name, () => {
    const target = new RevealResultConverter();

    const act = (r: rpc.OperationResultReveal) => target.convert(r);

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultReveal = basePropertiesMock.rpc;

        const result = act(rpcResult);

        expect(result).toEqual<RevealNotificationResult>({
            ...basePropertiesMock.expected,
            graphQLTypeName: RevealNotificationResult.name,
        });
    });
});
