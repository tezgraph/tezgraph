import * as rpc from '@taquito/rpc';

import { DelegationNotificationResult } from '../../../../../src/entity/subscriptions/operations/delegation/delegation-notification';
import { DelegationResultConverter } from '../../../../../src/rpc/converters/operation-results/delegation-result-converter';
import { basePropertiesMock } from './base-operation-result.mock';

describe(DelegationResultConverter.name, () => {
    const target = new DelegationResultConverter();

    const act = (r: rpc.OperationResultDelegation) => target.convert(r);

    it('should convert values correctly', () => {
        const rpcResult: rpc.OperationResultDelegation = basePropertiesMock.rpc;

        const result = act(rpcResult);

        expect(result).toEqual<DelegationNotificationResult>({
            ...basePropertiesMock.expected,
            graphQLTypeName: 'DelegationNotificationResult',
        });
    });
});
