import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { TransactionNotificationResult } from '../../../../../src/entity/subscriptions';
import {
    BalanceUpdateConverter,
    BigMapDiffConverter,
    LazyStorageDiffConverter,
} from '../../../../../src/rpc/converters/common';
import { TransactionResultConverter } from '../../../../../src/rpc/converters/operation-results/transaction-result-converter';
import { mockBalanceUpdate, mockBigMapDiff, mockLazyStorageDiff } from '../../../mocks';
import { basePropertiesMock } from './base-operation-result.mock';

describe(TransactionResultConverter.name, () => {
    let target: TransactionResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let bigMapDiffConverter: BigMapDiffConverter;
    let lazyStorageDiffConverter: LazyStorageDiffConverter;

    const act = (r: rpc.OperationResultTransaction) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        bigMapDiffConverter = mock(BigMapDiffConverter);
        lazyStorageDiffConverter = mock(lazyStorageDiffConverter);
        target = new TransactionResultConverter(
            instance(balanceUpdateConverter),
            instance(bigMapDiffConverter),
            instance(lazyStorageDiffConverter),
        );
    });

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultTransaction = {
            ...basePropertiesMock.rpc,
            storage: { prim: 'storage-michelson' },
            originated_contracts: ['c1', 'c2'],
            storage_size: '222',
            paid_storage_size_diff: '333',
            allocated_destination_contract: true,
            big_map_diff: 'rpcBigMapDiffs' as any,
            balance_updates: 'rpcBalanceUpdates' as any,
            lazy_storage_diff: 'rpcLazyStorageDiffs' as any,
        };
        const balanceUpdates = [mockBalanceUpdate()];
        const bigMapDiffs = [mockBigMapDiff()];
        const lazyStorageDiffs = [mockLazyStorageDiff()];
        when(balanceUpdateConverter.convertNullableArray(rpcResult.balance_updates)).thenReturn(balanceUpdates);
        when(bigMapDiffConverter.convertNullableArray(rpcResult.big_map_diff)).thenReturn(bigMapDiffs);
        when(lazyStorageDiffConverter.convertNullableArray(rpcResult.lazy_storage_diff)).thenReturn(lazyStorageDiffs);

        const result = act(rpcResult);

        expect(result).toEqual<TransactionNotificationResult>({
            ...basePropertiesMock.expected,
            graphQLTypeName: TransactionNotificationResult.name,
            storage: { prim: 'storage-michelson' },
            originated_contracts: ['c1', 'c2'],
            storage_size: BigInt(222),
            paid_storage_size_diff: BigInt(333),
            allocated_destination_contract: true,
            balance_updates: balanceUpdates,
            big_map_diff: bigMapDiffs,
            lazy_storage_diff: lazyStorageDiffs,
        });
    });
});
