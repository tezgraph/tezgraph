import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    InternalDelegationResult,
    InternalOperationKind,
    InternalOriginationResult,
    InternalRegisterGlobalConstantResult,
    InternalRevealResult,
    InternalSetDepositsLimitResult,
    InternalTransactionResult,
    RegisterGlobalConstantNotificationResult,
    RevealNotificationResult,
    SetDepositsLimitNotificationResult,
    TransactionNotificationResult,
} from '../../../../../src/entity/subscriptions';
import { DelegationNotificationResult } from '../../../../../src/entity/subscriptions/operations/delegation/delegation-notification';
import { OriginationNotificationResult } from '../../../../../src/entity/subscriptions/operations/origination/origination-notification';
import { ScriptedContractsConverter, TransactionParametersConverter } from '../../../../../src/rpc/converters/common';
import { DelegationResultConverter } from '../../../../../src/rpc/converters/operation-results/delegation-result-converter';
import { InternalOperationResultConverter } from '../../../../../src/rpc/converters/operation-results/internal-operation-result-converter';
import { OriginationResultConverter } from '../../../../../src/rpc/converters/operation-results/origination-result-converter';
import { RegisterGlobalConstantResultConverter } from '../../../../../src/rpc/converters/operation-results/register-global-constant-result-converter';
import { RevealResultConverter } from '../../../../../src/rpc/converters/operation-results/reveal-result-converter';
import { SetDepositsLimitResultConverter } from '../../../../../src/rpc/converters/operation-results/set-deposits-limit-result-converter';
import { TransactionResultConverter } from '../../../../../src/rpc/converters/operation-results/transaction-result-converter';
import { mockScriptedContracts, mockTransactionParameters } from '../../../mocks';

describe(InternalOperationResultConverter.name, () => {
    let target: InternalOperationResultConverter;
    let delegationResultConverter: DelegationResultConverter;
    let originationResultConverter: OriginationResultConverter;
    let registerGlobalConstantResultConverter: RegisterGlobalConstantResultConverter;
    let revealResultConverter: RevealResultConverter;
    let setDepositsLimitResultConverter: SetDepositsLimitResultConverter;
    let transactionResultConverter: TransactionResultConverter;
    let transactionParametersConverter: TransactionParametersConverter;
    let scriptConverter: ScriptedContractsConverter;

    let rpcResult: rpc.InternalOperationResult;

    beforeEach(() => {
        delegationResultConverter = mock(DelegationResultConverter);
        originationResultConverter = mock(OriginationResultConverter);
        registerGlobalConstantResultConverter = mock(RegisterGlobalConstantResultConverter);
        revealResultConverter = mock(RevealResultConverter);
        setDepositsLimitResultConverter = mock(SetDepositsLimitResultConverter);
        transactionResultConverter = mock(TransactionResultConverter);
        transactionParametersConverter = mock(TransactionParametersConverter);
        scriptConverter = mock(ScriptedContractsConverter);
        target = new InternalOperationResultConverter(
            instance(delegationResultConverter),
            instance(originationResultConverter),
            instance(registerGlobalConstantResultConverter),
            instance(revealResultConverter),
            instance(setDepositsLimitResultConverter),
            instance(transactionResultConverter),
            instance(transactionParametersConverter),
            instance(scriptConverter),
        );

        rpcResult = {
            kind: rpc.OpKind.TRANSACTION,
            source: 'src',
            nonce: 111,
            amount: '222',
            destination: 'dst',
            parameters: 'rpcTranParameters' as any,
            public_key: 'kkk',
            balance: '333',
            delegate: 'ddd',
            script: 'rpcScriptedContracts' as any,
            value: { prim: 'const' },
            limit: '444',
            result: { consumed_milligas: '123000' } as rpc.InternalOperationResultEnum,
        };
    });

    it(`should convert ${rpc.OpKind.DELEGATION} correctly`, () => {
        rpcResult.kind = rpc.OpKind.DELEGATION;
        const delegation: DelegationNotificationResult = 'mockedDelegation' as any;
        when(delegationResultConverter.convert(rpcResult.result)).thenReturn(delegation);

        const result = target.convert(rpcResult);

        expect(result).toEqual<InternalDelegationResult>({
            graphQLTypeName: InternalDelegationResult.name,
            kind: InternalOperationKind.delegation,
            source: 'src',
            nonce: 111,
            delegate: 'ddd',
            result: delegation,
        });
    });

    it(`should convert ${rpc.OpKind.ORIGINATION} correctly`, () => {
        rpcResult.kind = rpc.OpKind.ORIGINATION;
        const origination: OriginationNotificationResult = 'mockedOrigination' as any;
        const script = mockScriptedContracts();
        when(originationResultConverter.convert(rpcResult.result)).thenReturn(origination);
        when(scriptConverter.convertToNonNullable(rpcResult.script)).thenReturn(script);

        const result = target.convert(rpcResult);

        expect(result).toEqual<InternalOriginationResult>({
            graphQLTypeName: InternalOriginationResult.name,
            kind: InternalOperationKind.origination,
            source: 'src',
            nonce: 111,
            balance: BigInt(333),
            delegate: 'ddd',
            script,
            result: origination,
        });
    });

    it(`should convert ${rpc.OpKind.REGISTER_GLOBAL_CONSTANT} correctly`, () => {
        rpcResult.kind = rpc.OpKind.REGISTER_GLOBAL_CONSTANT;
        const registerGlobalConstant: RegisterGlobalConstantNotificationResult = 'mockedRegisterGlobalConstant' as any;
        when(registerGlobalConstantResultConverter.convert(rpcResult.result)).thenReturn(registerGlobalConstant);

        const result = target.convert(rpcResult);

        expect(result).toEqual<InternalRegisterGlobalConstantResult>({
            graphQLTypeName: InternalRegisterGlobalConstantResult.name,
            kind: InternalOperationKind.register_global_constant,
            source: 'src',
            nonce: 111,
            value: { prim: 'const' },
            result: registerGlobalConstant,
        });
    });

    it(`should convert ${rpc.OpKind.REVEAL} correctly`, () => {
        rpcResult.kind = rpc.OpKind.REVEAL;
        const reveal: RevealNotificationResult = 'mockedReveal' as any;
        when(revealResultConverter.convert(rpcResult.result)).thenReturn(reveal);

        const result = target.convert(rpcResult);

        expect(result).toEqual<InternalRevealResult>({
            graphQLTypeName: InternalRevealResult.name,
            kind: InternalOperationKind.reveal,
            source: 'src',
            nonce: 111,
            public_key: 'kkk',
            result: reveal,
        });
    });

    it(`should convert ${rpc.OpKind.SET_DEPOSITS_LIMIT} correctly`, () => {
        rpcResult.kind = rpc.OpKind.SET_DEPOSITS_LIMIT;
        const setDepositsLimit: SetDepositsLimitNotificationResult = 'mockedSetDepositsLimit' as any;
        when(setDepositsLimitResultConverter.convert(rpcResult.result)).thenReturn(setDepositsLimit);

        const result = target.convert(rpcResult);

        expect(result).toEqual<InternalSetDepositsLimitResult>({
            graphQLTypeName: InternalSetDepositsLimitResult.name,
            kind: InternalOperationKind.set_deposits_limit,
            source: 'src',
            nonce: 111,
            limit: BigInt(444),
            result: setDepositsLimit,
        });
    });

    it(`should convert ${rpc.OpKind.TRANSACTION} correctly`, () => {
        rpcResult.kind = rpc.OpKind.TRANSACTION;
        const transaction: TransactionNotificationResult = 'mockedTransaction' as any;
        const parameters = mockTransactionParameters();
        when(transactionResultConverter.convert(rpcResult.result)).thenReturn(transaction);
        when(transactionParametersConverter.convertNullable(rpcResult.parameters)).thenReturn(parameters);

        const result = target.convert(rpcResult);

        expect(result).toEqual<InternalTransactionResult>({
            graphQLTypeName: InternalTransactionResult.name,
            kind: InternalOperationKind.transaction,
            source: 'src',
            nonce: 111,
            amount: BigInt(222),
            destination: 'dst',
            parameters,
            result: transaction,
        });
    });
});
