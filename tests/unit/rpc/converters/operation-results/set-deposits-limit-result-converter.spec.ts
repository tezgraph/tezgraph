import * as rpc from '@taquito/rpc';

import { SetDepositsLimitNotificationResult } from '../../../../../src/entity/subscriptions';
import { SetDepositsLimitResultConverter } from '../../../../../src/rpc/converters/operation-results/set-deposits-limit-result-converter';
import { basePropertiesMock } from './base-operation-result.mock';

describe(SetDepositsLimitResultConverter.name, () => {
    const target = new SetDepositsLimitResultConverter();

    const act = (r: rpc.OperationResultSetDepositsLimit) => target.convert(r);

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultSetDepositsLimit = basePropertiesMock.rpc;

        const result = act(rpcResult);

        expect(result).toEqual<SetDepositsLimitNotificationResult>({
            ...basePropertiesMock.expected,
            graphQLTypeName: SetDepositsLimitNotificationResult.name,
        });
    });
});
