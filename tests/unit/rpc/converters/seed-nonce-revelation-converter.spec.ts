import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    OperationKind,
    SeedNonceRevelationNotification,
    SimpleOperationMetadata,
} from '../../../../src/entity/subscriptions';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import {
    RpcSeedNonceRevelation,
    SeedNonceRevelationConverter,
} from '../../../../src/rpc/converters/seed-nonce-revelation-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(SeedNonceRevelationConverter.name, () => {
    let target: SeedNonceRevelationConverter;
    let metadataConverter: OperationMetadataConverter;

    const act = (o: RpcSeedNonceRevelation) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new SeedNonceRevelationConverter(instance(metadataConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcSeedNonceRevelation = {
            kind: rpc.OpKind.SEED_NONCE_REVELATION,
            level: 111,
            nonce: 'nnn',
            metadata: null!,
        };
        const metadata: SimpleOperationMetadata = 'mockedMetadata' as any;
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act(rpcOperation);

        expect(operation).toEqual<SeedNonceRevelationNotification>({
            graphQLTypeName: SeedNonceRevelationNotification.name,
            kind: OperationKind.seed_nonce_revelation,
            level: 111,
            nonce: 'nnn',
            ...mockBaseProperties(),
            metadata,
        });
    });
});
