import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    ActivateAccountNotification,
    OperationKind,
    SimpleOperationMetadata,
} from '../../../../src/entity/subscriptions';
import {
    ActivateAccountConverter,
    RpcActivateAccount,
} from '../../../../src/rpc/converters/activate-account-converter';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(ActivateAccountConverter.name, () => {
    let target: ActivateAccountConverter;
    let metadataConverter: OperationMetadataConverter;

    const act = (o: RpcActivateAccount) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new ActivateAccountConverter(instance(metadataConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcActivateAccount = {
            kind: rpc.OpKind.ACTIVATION,
            pkh: 'ppp',
            secret: 'sss',
            metadata: null!,
        };
        const metadata: SimpleOperationMetadata = 'mockedMetadata' as any;
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act(rpcOperation);

        expect(operation).toEqual<ActivateAccountNotification>({
            graphQLTypeName: ActivateAccountNotification.name,
            kind: OperationKind.activate_account,
            pkh: 'ppp',
            secret: 'sss',
            ...mockBaseProperties(),
            metadata,
        });
    });
});
