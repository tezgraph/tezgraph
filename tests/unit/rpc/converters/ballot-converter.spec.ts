import * as rpc from '@taquito/rpc';

import { BallotNotification, BallotVote, OperationKind } from '../../../../src/entity/subscriptions';
import { BallotConverter, RpcBallot } from '../../../../src/rpc/converters/ballot-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(BallotConverter.name, () => {
    const target = new BallotConverter();

    const act = (o: RpcBallot) => target.convert(o, mockBaseProperties());

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcBallot = {
            kind: rpc.OpKind.BALLOT,
            source: 'sss',
            period: 111,
            proposal: 'ppp',
            ballot: 'yay',
        };

        const operation = act(rpcOperation);

        expect(operation).toEqual<BallotNotification>({
            graphQLTypeName: BallotNotification.name,
            kind: OperationKind.ballot,
            source: 'sss',
            period: 111,
            proposal: 'ppp',
            ballot: BallotVote.yay,
            ...mockBaseProperties(),
        });
    });
});
