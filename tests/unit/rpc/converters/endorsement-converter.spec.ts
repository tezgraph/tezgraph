import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    EndorsementNotificationMetadata,
    EndorsementNotification,
    OperationKind,
} from '../../../../src/entity/subscriptions';
import { EndorsementMetadataConverter } from '../../../../src/rpc/converters/common/endorsement-metadata-converter';
import { EndorsementConverter, RpcEndorsement } from '../../../../src/rpc/converters/endorsement-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(EndorsementConverter.name, () => {
    let target: EndorsementConverter;
    let metadataConverter: EndorsementMetadataConverter;

    const act = (e: RpcEndorsement) => target.convert(e, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(EndorsementMetadataConverter);
        target = new EndorsementConverter(instance(metadataConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcEndorsement = {
            kind: rpc.OpKind.ENDORSEMENT,
            level: 111,
            round: 222,
            slot: 333,
            block_payload_hash: 'haha',
        };
        const metadata: EndorsementNotificationMetadata = 'mockedMetadata' as any;
        when(metadataConverter.convert(rpcOperation)).thenReturn(metadata);

        const operation = act(rpcOperation);

        expect(operation).toEqual<EndorsementNotification>({
            graphQLTypeName: EndorsementNotification.name,
            kind: OperationKind.endorsement,
            level: 111,
            round: 222,
            slot: 333,
            block_payload_hash: 'haha',
            metadata,
            ...mockBaseProperties(),
        });
    });
});
