import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    OperationKind,
    SetDepositsLimitNotification,
    SetDepositsLimitNotificationMetadata,
} from '../../../../src/entity/subscriptions';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { SetDepositsLimitResultConverter } from '../../../../src/rpc/converters/operation-results/set-deposits-limit-result-converter';
import {
    RpcSetDepositsLimit,
    SetDepositsLimitConverter,
} from '../../../../src/rpc/converters/set-deposits-limit-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(SetDepositsLimitConverter.name, () => {
    let target: SetDepositsLimitConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: SetDepositsLimitResultConverter;

    const act = (o: RpcSetDepositsLimit) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(SetDepositsLimitResultConverter);
        target = new SetDepositsLimitConverter(instance(metadataConverter), resultConverter);
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcSetDepositsLimit = {
            kind: rpc.OpKind.SET_DEPOSITS_LIMIT,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            limit: '555',
        };
        const metadata: SetDepositsLimitNotificationMetadata = 'mockedMetadata' as any;
        when(
            metadataConverter.convert(rpcOperation, resultConverter, SetDepositsLimitNotificationMetadata.name),
        ).thenReturn(metadata);

        const operation = act(rpcOperation);

        expect(operation).toEqual<SetDepositsLimitNotification>({
            graphQLTypeName: SetDepositsLimitNotification.name,
            kind: OperationKind.set_deposits_limit,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            ...mockBaseProperties(),
            metadata,
            limit: BigInt(555),
        });
    });
});
