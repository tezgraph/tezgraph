import * as rpc from '@taquito/rpc';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import {
    BlockNotification,
    OperationKind,
    OperationNotification,
    OperationOrigin,
} from '../../../../src/entity/subscriptions';
import { BaseOperationProperties, OperationConverter } from '../../../../src/rpc/converters/operation-converter';
import { OperationGroupConverter } from '../../../../src/rpc/converters/operation-group-converter';
import { LogLevel } from '../../../../src/utils/logging';
import { expectToThrow, mockOperation, TestLogger } from '../../mocks';
import { getRandomDate, TestClock } from '../../mocks/test-clock';

describe(OperationGroupConverter.name, () => {
    let target: OperationGroupConverter;
    let operationConverter: OperationConverter;
    let clock: TestClock;
    let logger: TestLogger;

    beforeEach(() => {
        operationConverter = mock(OperationConverter);
        clock = new TestClock();
        logger = new TestLogger();
        target = new OperationGroupConverter(instance(operationConverter), clock, logger);
    });

    it.each([
        [OperationOrigin.block, { hash: 'hh', received_from_tezos_on: getRandomDate() } as BlockNotification],
        [OperationOrigin.mempool, null],
    ])('should convert operations from %s', (origin, block) => {
        const failedRpcOp = 'bullshit' as unknown as rpc.OperationContentsAndResult;
        const rpcOpError = new Error('oops');
        when(operationConverter.convert(failedRpcOp, anything())).thenThrow(rpcOpError);

        const rpcInfo = getGroupOwnProperties();
        const [rpcOp1, op1] = setupOperation(rpc.OpKind.BALLOT, OperationKind.ballot);
        const [rpcOp2, op2] = setupOperation(rpc.OpKind.REVEAL, OperationKind.reveal);
        const skippedRpcOp = { kind: rpc.OpKind.PROPOSALS } as rpc.OperationContentsAndResult;
        const rpcOpGroup: rpc.OperationEntry = {
            ...rpcInfo,
            contents: [rpcOp1, failedRpcOp, skippedRpcOp, rpcOp2],
        };

        const operations = target.convert(rpcOpGroup, block!);

        expect(operations).toEqual([op1, op2]);

        verify(operationConverter.convert(anything(), anything())).times(4);
        for (let i = 0; i < rpcOpGroup.contents.length; i++) {
            // eslint-disable-next-line @typescript-eslint/unbound-method
            const [rpcOp, baseProps] = capture(operationConverter.convert).byCallIndex(i);

            expect(rpcOp).toBe(rpcOpGroup.contents[i]);
            expect(baseProps).toEqual<BaseOperationProperties>({
                operation_group: {
                    protocol: 'pp',
                    chain_id: block ? 'cc' : null,
                    hash: block ? 'hh' : null,
                    branch: 'bb',
                    signature: 'sig',
                    received_from_tezos_on: block?.received_from_tezos_on ?? clock.nowDate,
                },
                block,
                origin,
            });
        }

        logger.verifyLoggedCount(5);
        logger.logged(0).verify(LogLevel.Debug, { index: 0, kind: OperationKind.ballot, signature: 'sig' });
        logger
            .logged(1)
            .verify(LogLevel.Error, { index: 1, signature: 'sig', rpcOperation: failedRpcOp, error: rpcOpError });
        logger.logged(2).verify(LogLevel.Debug, { index: 2, signature: 'sig', rpcOperation: skippedRpcOp });
        logger.logged(3).verify(LogLevel.Debug, { index: 3, kind: OperationKind.reveal, signature: 'sig' });
        logger.logged(4).verify(LogLevel.Debug, { signature: 'sig', operationCount: 2 });
    });

    function setupOperation(
        rpcKind: rpc.OpKind,
        kind: OperationKind,
    ): [rpc.OperationContentsAndResult, OperationNotification] {
        const rpcOp = { kind: rpcKind } as rpc.OperationContentsAndResult;
        const op = mockOperation({ kind: OperationKind[kind] });

        when(operationConverter.convert(rpcOp, anything())).thenReturn(op);
        return [rpcOp, op];
    }

    it.each([null, { hash: 'hh' } as BlockNotification])(
        'should throw if no operation converted correctly from block %s',
        (block) => {
            const failedRpcOp = 'bullshit' as unknown as rpc.OperationContentsAndResult;
            when(operationConverter.convert(failedRpcOp, anything())).thenThrow(new Error('oops'));
            const rpcOpGroup: rpc.OperationEntry = {
                ...getGroupOwnProperties(),
                contents: [failedRpcOp],
            };

            const error = expectToThrow(() => target.convert(rpcOpGroup, block!));

            expect(error.message).toIncludeMultiple(['all operations', `'sig'`, block?.hash ?? '']);
        },
    );

    function getGroupOwnProperties() {
        return { protocol: 'pp', chain_id: 'cc', hash: 'hh', branch: 'bb', signature: 'sig' };
    }
});
