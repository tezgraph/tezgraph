import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    OperationKind,
    TransactionNotification,
    TransactionNotificationMetadata,
} from '../../../../src/entity/subscriptions';
import { TransactionParametersConverter } from '../../../../src/rpc/converters/common';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { TransactionResultConverter } from '../../../../src/rpc/converters/operation-results/transaction-result-converter';
import { RpcTransaction, TransactionConverter } from '../../../../src/rpc/converters/transaction-converter';
import { mockTransactionParameters } from '../../mocks';
import { mockBaseProperties } from './rpc-mocks';

describe(TransactionConverter.name, () => {
    let target: TransactionConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: TransactionResultConverter;
    let parametersConverter: TransactionParametersConverter;

    const act = (o: RpcTransaction) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(TransactionResultConverter);
        parametersConverter = mock(TransactionParametersConverter);
        target = new TransactionConverter(instance(metadataConverter), resultConverter, instance(parametersConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcTransaction = {
            kind: rpc.OpKind.TRANSACTION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            amount: '555',
            destination: 'ddd',
            parameters: 'rpcTranParameter' as any,
            metadata: null!,
        };
        const metadata: TransactionNotificationMetadata = 'mockedMetadata' as any;
        const parameters = mockTransactionParameters();
        when(metadataConverter.convert(rpcOperation, resultConverter, TransactionNotificationMetadata.name)).thenReturn(
            metadata,
        );
        when(parametersConverter.convertNullable(rpcOperation.parameters)).thenReturn(parameters);

        const operation = act(rpcOperation);

        expect(operation).toEqual<TransactionNotification>({
            graphQLTypeName: TransactionNotification.name,
            kind: OperationKind.transaction,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            amount: BigInt(555),
            destination: 'ddd',
            parameters,
            ...mockBaseProperties(),
            metadata,
        });
    });
});
