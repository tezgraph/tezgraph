import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    OperationKind,
    RegisterGlobalConstantNotification,
    RegisterGlobalConstantNotificationMetadata,
} from '../../../../src/entity/subscriptions';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { RegisterGlobalConstantResultConverter } from '../../../../src/rpc/converters/operation-results/register-global-constant-result-converter';
import {
    RegisterGlobalConstantConverter,
    RpcRegisterGlobalConstant,
} from '../../../../src/rpc/converters/register-global-constant-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(RegisterGlobalConstantConverter.name, () => {
    let target: RegisterGlobalConstantConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: RegisterGlobalConstantResultConverter;

    const act = (o: RpcRegisterGlobalConstant) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(RegisterGlobalConstantResultConverter);
        target = new RegisterGlobalConstantConverter(instance(metadataConverter), resultConverter);
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcRegisterGlobalConstant = {
            kind: rpc.OpKind.REGISTER_GLOBAL_CONSTANT,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            value: { prim: 'const' },
            metadata: null!,
        };
        const metadata: RegisterGlobalConstantNotificationMetadata = 'mockedMetadata' as any;
        when(
            metadataConverter.convert(rpcOperation, resultConverter, RegisterGlobalConstantNotificationMetadata.name),
        ).thenReturn(metadata);

        const operation = act(rpcOperation);

        expect(operation).toEqual<RegisterGlobalConstantNotification>({
            graphQLTypeName: RegisterGlobalConstantNotification.name,
            kind: OperationKind.register_global_constant,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            ...mockBaseProperties(),
            value: { prim: 'const' },
            metadata,
        });
    });
});
