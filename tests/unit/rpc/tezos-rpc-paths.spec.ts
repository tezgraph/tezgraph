import { tezosRpcPaths } from '../../../src/rpc/tezos-rpc-paths';
import { nameof } from '../../../src/utils/reflection';

describe('tezosRpcPaths', () => {
    describe(nameof<typeof tezosRpcPaths>('block'), () => {
        it('should build correct url path', () => {
            const path = tezosRpcPaths.block('SomeHash');

            expect(path).toBe('/chains/main/blocks/SomeHash');
        });
    });
});
