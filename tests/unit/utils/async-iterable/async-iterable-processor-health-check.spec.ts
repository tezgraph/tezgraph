import dayjs from 'dayjs';
import { instance, mock, verify, when } from 'ts-mockito';

import { AsyncIterableProcessor } from '../../../../src/utils/async-iterable/async-iterable-processor';
import {
    AsyncIterableProcessorHealthCheck,
    messages,
} from '../../../../src/utils/async-iterable/async-iterable-processor-health-check';
import { AsyncIterableWorker, ProcessingStatus } from '../../../../src/utils/async-iterable/async-iterable-worker';
import { EnvConfig } from '../../../../src/utils/configuration/env-config';
import { HealthStatus } from '../../../../src/utils/health/health-check';
import { TestLogger } from '../../mocks';
import { TestClock } from '../../mocks/test-clock';

describe(AsyncIterableProcessorHealthCheck.name, () => {
    let target: AsyncIterableProcessorHealthCheck;
    let processor: AsyncIterableProcessor;
    let worker: AsyncIterableWorker;
    let clock: TestClock;
    let logger: TestLogger;

    beforeEach(() => {
        processor = {} as AsyncIterableProcessor;
        worker = mock<AsyncIterableWorker>();
        const envConfig = { tezosProcessingMaxAgeMillis: { degraded: 100, unhealthy: 200 } } as EnvConfig;
        clock = new TestClock();
        logger = new TestLogger();
        target = new AsyncIterableProcessorHealthCheck('Foo', processor, instance(worker), envConfig, clock, logger);

        processor.lastItemInfo = 'itm';
        processor.lastSuccessTime = dayjs(clock.nowDate).subtract(50, 'millisecond').toDate();
        when(worker.status).thenReturn(ProcessingStatus.Running);
    });

    it('should have correct name', () => {
        expect(target.name).toBe('Foo');
    });

    it('should be healthy if everything fine', () => {
        const health = target.checkHealth();

        expect(health.data).toEqual({
            reason: messages.everythingWorks,
            lastItem: 'itm',
            lastItemTime: processor.lastSuccessTime,
        });
        expect(health.status).toBe(HealthStatus.Healthy);
    });

    it.each([
        [ProcessingStatus.Stopped, 1],
        [ProcessingStatus.Stopping, 0],
    ])('should report unhealthy if ', (status, expectedStartTimes) => {
        when(worker.status).thenReturn(status);

        const health = target.checkHealth();

        expect(health.data).toBe(messages.notRunning);
        expect(health.status).toBe(HealthStatus.Unhealthy);
        verify(worker.start()).times(expectedStartTimes);
    });

    it.each([
        [HealthStatus.Degraded, 150],
        [HealthStatus.Unhealthy, 250],
    ])('should report %s if no success for long time', (expectedStatus, ageMillis) => {
        processor.lastSuccessTime = dayjs(clock.nowDate).subtract(ageMillis, 'millisecond').toDate();
        processor.lastError = 'oops';

        const health = target.checkHealth();

        expect(health.data).toEqual({
            reason: messages.noSuccessForLongTime,
            lastItem: 'itm',
            lastError: 'oops',
            lastSuccessTime: processor.lastSuccessTime,
            successAgeMillis: ageMillis,
            maxAgeMillisConfig: {
                degraded: 100,
                unhealthy: 200,
            },
        });
        expect(health.status).toBe(expectedStatus);
    });

    it('should report degraded if error occurred recently', () => {
        processor.lastError = 'oops';

        const health = target.checkHealth();

        expect(health.data).toEqual({
            reason: messages.errorRecently,
            failedItem: 'itm',
            error: 'oops',
            lastSuccessTime: processor.lastSuccessTime,
        });
        expect(health.status).toBe(HealthStatus.Degraded);
    });
});
