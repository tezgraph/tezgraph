import { AbortSignal } from 'abort-controller';
import { PubSubEngine } from 'graphql-subscriptions';
import { deepEqual, instance, mock, verify } from 'ts-mockito';

import { LogLevel } from '../../../../src/utils/logging';
import { AbortablePubSubIterable } from '../../../../src/utils/pubsub/abortable-pubsub-iterable';
import { PubSubTrigger } from '../../../../src/utils/pubsub/pubsub-trigger';
import { TypedPubSub } from '../../../../src/utils/pubsub/typed-pubsub';
import { nameof } from '../../../../src/utils/reflection';
import { TestLogger } from '../../mocks';

class TargetPubSub extends TypedPubSub {}
interface Foo {
    value: string;
}

describe(TypedPubSub.name, () => {
    let target: TypedPubSub;
    let pubSub: PubSubEngine;
    let logger: TestLogger;
    let globalAbortSignal: AbortSignal;
    let trigger: PubSubTrigger<Foo>;

    beforeEach(() => {
        pubSub = mock<PubSubEngine>();
        logger = new TestLogger();
        globalAbortSignal = {} as AbortSignal;
        target = new TargetPubSub(instance(pubSub), logger, globalAbortSignal);

        trigger = new PubSubTrigger('FOO');
    });

    describe(nameof<TypedPubSub>('publish'), () => {
        it('should publish given payload for given trigger', async () => {
            const payload: Foo = { value: 'hhh' };

            await target.publish(trigger, payload);

            verify(pubSub.publish('FOO', payload)).once();
            logger.loggedSingle().verify(LogLevel.Debug, { trigger: 'FOO' });
        });
    });

    describe(nameof<TypedPubSub>('subscribe'), () => {
        it('should subscribe to given trigger', () => {
            const handler = () => {
                /* Nothing */
            };

            target.subscribe(trigger, handler);

            verify(pubSub.subscribe('FOO', handler, deepEqual({}))).once();
        });
    });

    describe(nameof<TypedPubSub>('iterate'), () => {
        it('should return iterable for given triggers', () => {
            const triggers = [trigger];

            const iterable = target.iterate(triggers) as any;

            expect(iterable).toBeInstanceOf(AbortablePubSubIterable);
            expect(iterable.pubSub).toBe(instance(pubSub));
            expect(iterable.triggers).toBe(triggers);
            expect(iterable.abortSignal).toBe(globalAbortSignal);
        });

        it('should use given abort signal', () => {
            const abortSignal = {} as AbortSignal;

            const iterable = target.iterate([trigger], abortSignal) as any;

            expect(iterable.abortSignal).toBe(abortSignal);
        });
    });
});
