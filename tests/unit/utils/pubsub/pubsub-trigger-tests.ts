import { PubSubTrigger } from '../../../../src/utils/pubsub/pubsub-trigger';

// eslint-disable-next-line jest/no-export
export function testUniqueTriggers(triggersObj: object) {
    it('should be unique', () => {
        const names = new Set<string>();

        for (const trigger of getTriggers(triggersObj)) {
            if (names.has(trigger.name)) {
                throw new Error(`Trigger '${trigger.name}' is used multiple times.`);
            } else {
                names.add(trigger.name);
            }
        }
    });
}

function* getTriggers(triggersObj: object): Iterable<PubSubTrigger<unknown>> {
    for (const [key, value] of Object.entries(triggersObj)) {
        if (value instanceof PubSubTrigger) {
            yield value;
        } else if (typeof value === 'object' && value !== null) {
            yield* getTriggers(value);
        } else {
            throw new Error(`Property '${key}' has unexpected type.`);
        }
    }
}
