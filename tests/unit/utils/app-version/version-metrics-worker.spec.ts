import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { Version } from '../../../../src/utils/app-version/version';
import { UNKNOWN_LABEL, VersionMetricsWorker } from '../../../../src/utils/app-version/version-metrics-worker';
import { VersionProvider } from '../../../../src/utils/app-version/version-provider';
import { LogLevel } from '../../../../src/utils/logging';
import { BuildInfoLabel, MetricsContainer } from '../../../../src/utils/metrics/metrics-container';
import { TestLogger } from '../../mocks';

describe(VersionMetricsWorker.name, () => {
    let target: VersionMetricsWorker;
    let counter: Counter<BuildInfoLabel>;
    let logger: TestLogger;
    let version: Version;

    beforeEach(() => {
        const versionProvider = mock(VersionProvider);
        counter = mock(Counter);
        const metrics = { buildInfoCounter: instance(counter) } as MetricsContainer;
        logger = new TestLogger();
        target = new VersionMetricsWorker(instance(versionProvider), metrics, logger);

        version = { git_sha: 'ss', tag: 'tt' } as Version;
        when(versionProvider.version).thenReturn(version);
    });

    it('should increment version metric on start', () => {
        target.start();

        verifyCounter('ss', 'tt');
        logger.verifyNothingLogged();
    });

    it('should log warning if no git_sha', () => {
        Object.assign(version, { git_sha: null });

        target.start();

        verifyCounter(UNKNOWN_LABEL, 'tt');
        logger.loggedSingle().verify(LogLevel.Warning);
    });

    it('should log warning if no tag', () => {
        Object.assign(version, { tag: null });

        target.start();

        verifyCounter('ss', UNKNOWN_LABEL);
        logger.loggedSingle().verify(LogLevel.Warning);
    });

    function verifyCounter(git_sha: string, tag: string) {
        verify(counter.inc(anything())).once();
        verify(counter.inc(deepEqual({ git_sha, tag }))).once();
    }
});
