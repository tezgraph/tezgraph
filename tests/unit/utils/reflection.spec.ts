import {
    getEnumTypeName,
    getEnumValues,
    isNotNullish,
    isNullish,
    isObjectLiteral,
    nameof,
} from '../../../src/utils/reflection';

class TestData {
    value!: string;
}

enum Memes {
    Lol = 'lol',
    Omg = 'omg',
    Wtf = 'wtf',
}

describe('Reflection utils', () => {
    describe(nameof.name, () => {
        it('should return member name', () => {
            const name = nameof<TestData>('value');

            expect(name).toBe('value');
        });
    });

    describe(isNullish.name, () => {
        it.each([
            [true, undefined],
            [true, null],
            [false, ''],
            [false, 'abc'],
            [false, 0],
            [false, 123],
        ])('should return %s for is %s and %s', (expected, value) => {
            const actual = isNullish(value);
            const actualNot = isNotNullish(value);

            expect(actual).toBe(expected);
            expect(actualNot).toBe(!expected);
        });
    });

    describe(getEnumValues.name, () => {
        it('should get all enum values', () => {
            const values = getEnumValues(Memes);

            expect(values).toEqual([Memes.Lol, Memes.Omg, Memes.Wtf]);
        });
    });

    describe(getEnumTypeName.name, () => {
        it('should get enum type name', () => {
            const name = getEnumTypeName({ Memes });

            expect(name).toBe('Memes');
        });
    });

    describe(isObjectLiteral.name, () => {
        it.each([
            [true, {}],
            [true, { hash: 'SomeHash' }],
            [true, new Object()], // eslint-disable-line no-new-object
            [false, undefined],
            [false, null],
            [false, new URL('https://tezos.com/')],
            [false, new Date()],
            [false, 'omg'],
            [false, new String('omg')], // eslint-disable-line no-new-wrappers
            [false, 123],
        ])('should return %s for %s', (expected, value) => {
            const actual = isObjectLiteral(value);

            expect(actual).toBe(expected);
        });
    });
});
