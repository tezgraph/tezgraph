import { anything, instance, mock, verify, when } from 'ts-mockito';

import { RetryHelper } from '../../../src/utils/retry-helper';
import { SleepHelper } from '../../../src/utils/time/sleep-helper';

interface Foo {
    bar(): Promise<string>;
}

describe(RetryHelper.name, () => {
    let target: RetryHelper;
    let sleepHelper: SleepHelper;
    let foo: Foo;

    const act = async () => target.execute(async () => instance(foo).bar(), [100, 200]);

    beforeEach(() => {
        sleepHelper = mock(SleepHelper);
        target = new RetryHelper(instance(sleepHelper));

        foo = mock<Foo>();

        when(sleepHelper.sleep(anything())).thenResolve();
    });

    it(`should call function once if no failure`, async () => {
        when(foo.bar()).thenResolve('lol');

        const result = await act();

        expect(result).toBe('lol');
        verify(foo.bar()).once();
        verify(sleepHelper.sleep(anything())).never();
    });

    it(`should retry with configured delays`, async () => {
        when(foo.bar())
            .thenReject(new Error('First failure.'))
            .thenReject(new Error('Second failure.'))
            .thenResolve('omg');

        const result = await act();

        expect(result).toBe('omg');
        verify(foo.bar()).times(3);
        verify(sleepHelper.sleep(anything())).times(2);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
    });

    it(`should fail if more failures then configured retries`, async () => {
        when(foo.bar())
            .thenReject(new Error('First failure.'))
            .thenReject(new Error('Second failure.'))
            .thenReject(new Error('Final failure.'));

        await expect(act()).rejects.toEqual(new Error('Final failure.'));
    });
});
