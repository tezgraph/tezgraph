import { nameof } from '../../../../src/utils/reflection';
import { Clock } from '../../../../src/utils/time/clock';

describe(Clock.name, () => {
    const target = new Clock();
    let startTimestamp: number;

    beforeEach(() => {
        startTimestamp = Date.now();
    });

    describe(nameof<Clock>('getNowDate'), () => {
        it('should get current Date', () => {
            const date = target.getNowDate();

            verifyNow(date.getTime());
        });
    });

    describe(nameof<Clock>('getNowTimestamp'), () => {
        it('should get current Date', () => {
            const timestamp = target.getNowTimestamp();

            verifyNow(timestamp);
        });
    });

    function verifyNow(timestamp: number) {
        expect(timestamp).toBeGreaterThanOrEqual(startTimestamp);
        expect(timestamp).toBeLessThanOrEqual(Date.now());
    }
});
