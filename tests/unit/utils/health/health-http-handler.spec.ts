import express from 'express';
import { instance, mock, verify, when } from 'ts-mockito';

import { HealthCheckHttpHandler, HealthStatusHttpHandler } from '../../../../src/utils/health/health-http-handler';
import { HealthProvider, HealthReport } from '../../../../src/utils/health/health-provider';
import { ContentType, HttpHeader, HttpStatusCode } from '../../../../src/utils/http-constants';
import { HttpHandler } from '../../../../src/utils/http-handler';
import { safeJsonStringify } from '../../../../src/utils/safe-json-stringifier';

describe('Health HTTP handlers', () => {
    let target: HttpHandler;
    let healthProvider: HealthProvider;
    let response: express.Response;

    const act = async () => target.handle(null!, instance(response));

    beforeEach(() => {
        healthProvider = mock(HealthProvider);
        response = mock<express.Response>();
    });

    describe(HealthStatusHttpHandler.name, () => {
        beforeEach(() => {
            target = new HealthStatusHttpHandler(instance(healthProvider));
        });

        it('should respond with full health status', async () => {
            const report = setupHealthReport(true);

            await act();

            verify(response.setHeader(HttpHeader.ContentType, ContentType.Json)).once();
            verify(response.send(safeJsonStringify(report, { indent: 2 }))).once();
        });
    });

    describe(HealthCheckHttpHandler.name, () => {
        beforeEach(() => {
            target = new HealthCheckHttpHandler(instance(healthProvider));
        });

        it('should respond with "OK" if healthy', async () => {
            setupHealthReport(true);

            await act();

            verify(response.send('OK')).once();
        });

        it('should respond with 503 (service unavailable) if unhealthy', async () => {
            setupHealthReport(false);

            await act();

            verify(response.status(HttpStatusCode.ServiceUnavailable)).once();
            verify(response.send('Unhealthy')).once();
        });
    });

    function setupHealthReport(isHealthy: boolean) {
        const report = { isHealthy, evaluatedOn: new Date() } as HealthReport;
        when(healthProvider.generateHealthReport()).thenResolve(report);
        return report;
    }
});
