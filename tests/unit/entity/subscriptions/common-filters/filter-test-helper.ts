import { Filter } from '../../../../../src/entity/subscriptions/common-filters/filter';
import { create } from '../../../../../src/utils/conversion';
import { DefaultConstructor, nameof } from '../../../../../src/utils/reflection';

/* eslint-disable jest/no-export */
export interface TestOptions<TValue, TTargetFilter> {
    value: TValue;
    filter?: Partial<TTargetFilter>;
    expectedPassed: boolean;
}

export function getRunTestFunc<TValue, TTargetFilter extends Filter<TValue>>(
    targetFilterType: DefaultConstructor<TTargetFilter>,
) {
    return function (testCondition: string, options: TestOptions<TValue, TTargetFilter>) {
        it(`${nameof<TTargetFilter>('passes')}() should be ${options.expectedPassed} if ${testCondition}`, () => {
            const target = create(targetFilterType, options.filter ?? {});

            const actual = target.passes(options.value);

            expect(actual).toBe(options.expectedPassed);
        });
    };
}
