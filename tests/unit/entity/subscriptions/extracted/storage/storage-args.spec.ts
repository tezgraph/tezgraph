import { AddressFilter, NullableAddressFilter } from '../../../../../../src/entity/subscriptions/common-filters';
import { StorageFilter } from '../../../../../../src/entity/subscriptions/extracted/storage/storage-args';
import { StorageNotification } from '../../../../../../src/entity/subscriptions/extracted/storage/storage-notification';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../../operations/mocks';

describe(`${StorageFilter.name}.${nameof<StorageFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, destinationResult] of getFilterTestCases(2)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult}, destination=${destinationResult}`, () => {
            const notification = {
                parent: {
                    source: 'src',
                    destination: 'dst',
                },
            } as StorageNotification;
            const target = create(StorageFilter, {
                source: mockFilter(AddressFilter, sourceResult, notification.parent.source),
                destination: mockFilter(NullableAddressFilter, destinationResult, notification.parent.destination),
            });

            const passed = target.passes(notification);

            expect(passed).toBe(expectedPassed);
        });
    }
});
