import {
    BalanceUpdateFilter,
    BalanceUpdateKind,
    BalanceUpdateKindFilter,
    BalanceUpdateNotification,
    NullableAddressFilter,
    NullableBalanceUpdateCategoryFilter,
} from '../../../../../../src/entity/subscriptions';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../../operations/mocks';

describe(`${BalanceUpdateFilter.name}.${nameof<BalanceUpdateFilter>('passes')}()`, () => {
    for (const [expectedPassed, kindResult, contractResult, categoryResult, delegateResult] of getFilterTestCases(4)) {
        it(
            `should return ${expectedPassed} if filters are kind=${kindResult}, contract=${contractResult},` +
                ` category=${categoryResult}, delegate=${delegateResult}`,
            () => {
                const notification = {
                    balance_update: {
                        kind: BalanceUpdateKind.contract,
                        contract: 'cont',
                        category: 'cat' as any,
                        delegate: 'del' as any,
                    },
                } as BalanceUpdateNotification;
                const target = create(BalanceUpdateFilter, {
                    kind: mockFilter(BalanceUpdateKindFilter, kindResult, notification.balance_update.kind),
                    contract: mockFilter(NullableAddressFilter, contractResult, notification.balance_update.contract),
                    category: mockFilter(
                        NullableBalanceUpdateCategoryFilter,
                        categoryResult,
                        notification.balance_update.category,
                    ),
                    delegate: mockFilter(NullableAddressFilter, delegateResult, notification.balance_update.delegate),
                });

                const passed = target.passes(notification);

                expect(passed).toBe(expectedPassed);
            },
        );
    }
});
