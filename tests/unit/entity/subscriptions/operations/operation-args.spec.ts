import { Filter } from '../../../../../src/entity/subscriptions/common-filters/filter';
import {
    BlockHashFilter,
    NullableOperationHashFilter,
    ProtocolHashFilter,
} from '../../../../../src/entity/subscriptions/common-filters/hash-filters';
import { createOperationFilterClass } from '../../../../../src/entity/subscriptions/operations/operation-args';
import { RevealNotification } from '../../../../../src/entity/subscriptions/operations/reveal/reveal-notification';
import { create } from '../../../../../src/utils/conversion';
import { getEnumValues, nameof } from '../../../../../src/utils/reflection';
import { combineFilterResults, FilterResult, mockFilter, shouldPass } from './mocks';

class TestSpecificFilter implements Filter<RevealNotification> {
    baseShouldPass = true;
    baseCalls: RevealNotification[] = [];

    passes(value: RevealNotification): boolean {
        this.baseCalls.push(value);
        return this.baseShouldPass;
    }
}

const TargetFilter = createOperationFilterClass(TestSpecificFilter);

enum FilterArrayResult {
    Undefined = 'Undefined',
    Empty = 'Empty',
    AllNotPassed = 'AllNotPassed',
    OnePassed = 'OnePassed',
    AllPassed = ' AllPassed',
}

describe(`${createOperationFilterClass.name}().${nameof<Filter<any>>('passes')}()`, () => {
    for (const [hashResult, protocolResult, branchResult] of combineFilterResults(3)) {
        for (const andResult of getEnumValues(FilterArrayResult)) {
            for (const orResult of getEnumValues(FilterArrayResult)) {
                for (const baseClassResult of [FilterResult.Passed, FilterResult.NotPassed]) {
                    const expectedPassed =
                        shouldPass(hashResult, protocolResult, branchResult, baseClassResult) &&
                        andResult !== FilterArrayResult.AllNotPassed &&
                        andResult !== FilterArrayResult.OnePassed &&
                        orResult !== FilterArrayResult.AllNotPassed;

                    it(
                        `should return ${expectedPassed} if filters are hash=${hashResult}, protocol=${protocolResult},` +
                            ` branch=${branchResult}, and=${andResult}, or=${orResult}, super.passes()=${baseClassResult}`,
                        () => {
                            const operation = {
                                operation_group: {
                                    hash: 'hhh',
                                    protocol: 'ppp',
                                    branch: 'bbb',
                                },
                            } as RevealNotification;
                            const target = create(TargetFilter, {
                                hash: mockFilter(
                                    NullableOperationHashFilter,
                                    hashResult,
                                    operation.operation_group.hash,
                                ),
                                protocol: mockFilter(
                                    ProtocolHashFilter,
                                    protocolResult,
                                    operation.operation_group.protocol,
                                ),
                                branch: mockFilter(BlockHashFilter, branchResult, operation.operation_group.branch),
                                and: mockFilterArray(andResult, operation),
                                or: mockFilterArray(orResult, operation),
                            });
                            const baseTarget: TestSpecificFilter = target as any;
                            baseTarget.baseShouldPass = baseClassResult === FilterResult.Passed;

                            const passed = target.passes(operation);

                            expect(passed).toBe(expectedPassed);
                            if (baseTarget.baseCalls.length > 0) {
                                expect(baseTarget.baseCalls).toEqual([operation]);
                            }
                        },
                    );
                }
            }
        }
    }
});

function mockFilterArray(result: FilterArrayResult, expectedValue: RevealNotification) {
    switch (result) {
        case FilterArrayResult.Undefined:
            return undefined;
        case FilterArrayResult.Empty:
            return [];
        case FilterArrayResult.AllNotPassed:
            return mockFilters(expectedValue, FilterResult.NotPassed, FilterResult.NotPassed, FilterResult.NotPassed);
        case FilterArrayResult.OnePassed:
            return mockFilters(expectedValue, FilterResult.NotPassed, FilterResult.Passed, FilterResult.NotPassed);
        case FilterArrayResult.AllPassed:
            return mockFilters(expectedValue, FilterResult.Passed, FilterResult.Passed, FilterResult.Passed);
    }
}

function mockFilters(expectedValue: RevealNotification, ...results: (FilterResult.Passed | FilterResult.NotPassed)[]) {
    return results.map((r) => mockFilter(TargetFilter, r, expectedValue)!);
}
