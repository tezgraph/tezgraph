import { AddressFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import { ProtocolHashFilter } from '../../../../../../src/entity/subscriptions/common-filters/hash-filters';
import {
    BallotSpecificFilter,
    BallotVoteFilter,
} from '../../../../../../src/entity/subscriptions/operations/ballot/ballot-args';
import { BallotNotification } from '../../../../../../src/entity/subscriptions/operations/ballot/ballot-notification';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${BallotSpecificFilter.name}.${nameof<BallotSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, proposalResult, ballotResult] of getFilterTestCases(3)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult}, proposal=${proposalResult}, ballot=${ballotResult}`, () => {
            const operation = {
                source: 'sss',
                proposal: 'ppp',
                ballot: 'nay',
            } as BallotNotification;
            const target = create(BallotSpecificFilter, {
                source: mockFilter(AddressFilter, sourceResult, operation.source),
                proposal: mockFilter(ProtocolHashFilter, proposalResult, operation.proposal),
                ballot: mockFilter(BallotVoteFilter, ballotResult, operation.ballot),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
