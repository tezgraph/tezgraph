import { NullableAddressFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import { PreendorsementSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/preendorsements/preendorsement-args';
import { PreendorsementNotification } from '../../../../../../src/entity/subscriptions/operations/preendorsements/preendorsement-notification';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${PreendorsementSpecificFilter.name}.${nameof<PreendorsementSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, delegateResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are delegate=${delegateResult}`, () => {
            const operation = {
                metadata: { delegate: 'ddd' },
            } as PreendorsementNotification;
            const target = create(PreendorsementSpecificFilter, {
                delegate: mockFilter(NullableAddressFilter, delegateResult, operation.metadata!.delegate),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
