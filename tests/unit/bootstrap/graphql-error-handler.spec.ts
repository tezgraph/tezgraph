import { GraphQLError } from 'graphql';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { GraphQLErrorHandler } from '../../../src/bootstrap/graphql-error-handler';
import { ResolverContext } from '../../../src/bootstrap/resolver-context';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { LogLevel } from '../../../src/utils/logging';
import { UuidGenerator } from '../../../src/utils/uuid-generator';
import { TestLogger } from '../mocks';

describe(GraphQLErrorHandler.name, () => {
    describe.each(['req', undefined])('with requestId = %s', (requestId) => {
        let target: GraphQLErrorHandler;
        let uuidGenerator: UuidGenerator;
        let envConfig: Writable<EnvConfig>;
        let logger: TestLogger;

        const act = (error: GraphQLError) => target.handle(error, { requestId } as ResolverContext);

        beforeEach(() => {
            uuidGenerator = mock(UuidGenerator);
            envConfig = { enableDebug: false } as EnvConfig;
            logger = new TestLogger();
            target = new GraphQLErrorHandler(instance(uuidGenerator), envConfig, logger);

            when(uuidGenerator.generate()).thenReturn('uuu');
        });

        it('should log error with generated ID', () => {
            const inputError = getGraphQLError(new Error('wtf'));

            const resultError = act(inputError);

            expect(resultError.message).toContain('uuu');
            expect(resultError.message).not.toContain('wtf');
            expect(resultError).toContainAllKeys(['message', 'locations', 'path', 'extensions']);
            logger.loggedSingle().verify(LogLevel.Error, { error: inputError, errorId: 'uuu', requestId });
        });

        function getGraphQLError(original: Error) {
            return new GraphQLError('omg', undefined, undefined, [1, 2], ['root', 'path'], original, { a: 1 });
        }
    });
});
