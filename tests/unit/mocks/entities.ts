import * as rpc from '@taquito/rpc';

import { TransactionParameters } from '../../../src/entity/common/transaction';
import {
    BalanceUpdate,
    BigMapDiff,
    InlinedEndorsement,
    LazyStorageDiff,
    OperationKind,
    OperationNotification,
    ScriptedContracts,
} from '../../../src/entity/subscriptions';

export function mockBalanceUpdate() {
    return { contract: `balance-${randomStr()}` } as BalanceUpdate;
}

export function mockBigMapDiff() {
    return { key_hash: `big-map-${randomStr()}` } as BigMapDiff;
}

export function mockLazyStorageDiff() {
    return { graphQLTypeName: `lazy-storage-${randomStr()}` } as LazyStorageDiff;
}

export function mockTransactionParameters() {
    return { entrypoint: `transaction-params-${randomStr()}` } as TransactionParameters;
}

export function mockScriptedContracts() {
    return { storage: { prim: `script-${randomStr()}` } } as ScriptedContracts;
}

export function mockInlinedEndorsement() {
    return { branch: `inlined-endorsement-${randomStr()}` } as InlinedEndorsement;
}

export function mockMichelson() {
    return { prim: `michelson-${randomStr()}` } as rpc.MichelsonV1Expression;
}

export function mockOperation(properties?: Partial<OperationNotification>) {
    return {
        kind: OperationKind.transaction,
        destination: `dst-${randomStr()}`,
        ...(properties ?? {}),
    } as OperationNotification;
}

let counter = 1;

export function randomStr() {
    return (counter++).toString();
}
