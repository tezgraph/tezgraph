import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    rootDir: '../../',
    preset: 'ts-jest',
    verbose: true,
    testEnvironment: 'node',
    testMatch: ['<rootDir>/tests/**/*.spec.ts'],
    setupFiles: ['<rootDir>/tests/env-setup.ts', 'dotenv/config'],
    setupFilesAfterEnv: ['jest-extended/all'],
    watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
    reporters: ['default', ['jest-junit', { outputDirectory: 'coverage', uniqueOutputName: 'true' }]],
    coverageReporters: ['json', 'text-summary', 'html', 'cobertura'],
    coveragePathIgnorePatterns: ['<rootDir>/tests/'],
    maxWorkers: 10,
    forceExit: true,
    testTimeout: 30_000,
};

export default config;
