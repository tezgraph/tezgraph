import type { Config } from '@jest/types';

import defaultConfig from './base.config';

const config: Config.InitialOptions = {
    ...defaultConfig,
    testTimeout: 45_000,
    testMatch: ['<rootDir>/tests/integration/common/**/*.spec.ts'],
};

export default config;
