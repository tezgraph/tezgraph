import type { Config } from '@jest/types';

import defaultConfig from './base.config';

const config: Config.InitialOptions = {
    ...defaultConfig,
    testMatch: ['<rootDir>/tests/integration/queries/**/*.spec.ts'],
    testTimeout: 1200_000,
};

export default config;
