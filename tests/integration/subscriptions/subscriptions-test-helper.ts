import { InMemoryCache, IntrospectionFragmentMatcher, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { DocumentNode } from 'graphql';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';
import fetch from 'node-fetch';
import { PassThrough } from 'stream';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import ws from 'ws';

import { App } from '../../../src/app';
import { tezosRpcPaths } from '../../../src/rpc/tezos-rpc-paths';
import { Names } from '../../../src/utils/configuration/env-config';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { Logger, LoggerFactory } from '../../../src/utils/logging';
import { safeJsonStringify } from '../../../src/utils/safe-json-stringifier';
import { createTestDIContainer, getPort, readFileText, waitUntil, writeFileText } from '../helpers';
import { rpcMocks } from './rpc-mocks';

export function createSubscriptionsTestHelper(envModules: string[]) {
    let app: App;
    let logger: Logger;
    let blockMonitorStream: PassThrough;
    let mempoolMonitorStream: PassThrough;
    let wsClient: SubscriptionClient | undefined;
    let apolloClient: ApolloClient<NormalizedCacheObject> | undefined;
    let subscriptions: ZenObservable.Subscription[];

    beforeEach(async () => {
        const diContainer = createTestDIContainer({ [Names.Modules]: envModules.join() });
        app = diContainer.resolve(App);
        logger = diContainer.resolve(LoggerFactory).getLogger('IntegrationTests');
        subscriptions = [];

        // Setup RPC mocks.
        blockMonitorStream = new PassThrough();
        mempoolMonitorStream = new PassThrough();
        rpcMocks.setup({
            urlPath: tezosRpcPaths.blockMonitor,
            responseBody: blockMonitorStream,
        });
        rpcMocks.setup({
            urlPath: tezosRpcPaths.block('BLBiBqzhMU1z5VpcTxHZc1wwwvU92LE4YMU96vTfiQSSbVHCqss'),
            responseBody: readTestDataFile('block.json'),
        });
        rpcMocks.setup({
            urlPath: tezosRpcPaths.mempoolMonitor,
            responseBody: mempoolMonitorStream,
        });

        logger.logInformation('Testing with {environmentVariables}.', { environmentVariables: process.env });
        await app.start();

        const protocolRelativeAppUrl = `//localhost:${getPort(app)}${appUrlPaths.graphQL}`;
        const fragmentMatcher = await getFragmentMatcher(`http:${protocolRelativeAppUrl}`);

        wsClient = new SubscriptionClient(`ws:${protocolRelativeAppUrl}`, { reconnect: true }, ws);
        apolloClient = new ApolloClient({
            link: new WebSocketLink(wsClient),
            cache: new InMemoryCache({ fragmentMatcher }),
        });
    });

    afterEach(async () => {
        logger.logInformation('Cleaning up after tests.');
        subscriptions.forEach((s) => s.unsubscribe());
        blockMonitorStream.destroy();
        mempoolMonitorStream.destroy();
        apolloClient?.stop();
        wsClient?.unsubscribeAll();
        await app.stop();
        rpcMocks.cleanAll();
    });

    return {
        get logger() {
            return logger;
        },
        get blockMonitorStream() {
            return blockMonitorStream;
        },
        get mempoolMonitorStream() {
            return mempoolMonitorStream;
        },
        setupSubscriptionToTest(fileNameWithExpected: string, query: DocumentNode, filter?: Record<string, unknown>) {
            const receivedNotifications: unknown[] = [];
            const expectedFilePath = `expected/${fileNameWithExpected}-notifications.json`;
            const expectedNotifications = JSON.parse(readTestDataFile(expectedFilePath));

            if (!Array.isArray(expectedNotifications)) {
                throw new Error(
                    `File with expected notifications must contain JSON array` +
                        ` but '${expectedFilePath}' contains '${typeof expectedNotifications}'.`,
                );
            }

            subscriptions.push(
                apolloClient!
                    .subscribe({
                        query,
                        variables: filter,
                    })
                    .subscribe({
                        next: (d) => receivedNotifications.push(d),
                    }),
            );

            return {
                async verifyReceivedNotifications() {
                    let receivedCount = 0;
                    await waitUntil(() => {
                        try {
                            if (receivedNotifications.length > receivedCount) {
                                receivedCount = receivedNotifications.length;
                                logger.logInformation(
                                    'The {subscription} has {receivedCount} out of {expectedCount}.',
                                    {
                                        subscription: fileNameWithExpected,
                                        receivedCount,
                                        expectedCount: expectedNotifications.length,
                                    },
                                );
                            }

                            writeReceivedDataFileJson(expectedFilePath, receivedNotifications);
                            expect(receivedNotifications).toHaveLength(expectedNotifications.length);
                            expect(receivedNotifications).toEqual(expectedNotifications);
                        } catch (error: unknown) {
                            const errorStr = (error as Error).toString();
                            throw new Error(
                                `Data from '${expectedFilePath}' does not match actually received data. ${errorStr}`,
                            );
                        }
                    });
                },
            };
        },
    };
}

const subscriptionsDir = './tests/integration/subscriptions';

export function readTestDataFile(fileName: string): string {
    return readFileText(`${subscriptionsDir}/test-data/${fileName}`);
}

export function writeReceivedDataFileJson(fileName: string, data: unknown): void {
    const json = safeJsonStringify(data, { indent: 2 });
    writeFileText(`${subscriptionsDir}/received-data/${fileName}`, json);
}

export function updateReceivedOn(currentData: any, expectedData: any): void {
    currentData.received_from_tezos_on = expectedData.received_from_tezos_on;
    currentData.operations.forEach((o: any) => {
        o.operation_group.received_from_tezos_on = expectedData.received_from_tezos_on;
    });
}

export function getRedisPubSub() {
    const connectionStr = process.env.REDIS_CONNECTION_STRING;
    return new RedisPubSub({
        publisher: new Redis(connectionStr),
        subscriber: new Redis(connectionStr),
    });
}

let fragmentMatcherPromise: Promise<IntrospectionFragmentMatcher> | undefined;

async function getFragmentMatcher(queryUrl: string): Promise<IntrospectionFragmentMatcher> {
    if (!fragmentMatcherPromise) {
        fragmentMatcherPromise = getFresh();
    }
    return fragmentMatcherPromise;

    async function getFresh(): Promise<IntrospectionFragmentMatcher> {
        const response = await fetch(queryUrl, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: safeJsonStringify({
                variables: {},
                query: `{
                    __schema {
                        types {
                            kind
                            name
                            possibleTypes {
                                name
                            }
                        }
                    }
                }`,
            }),
        });
        const result = await response.json();

        // Here we're filtering out any type information unrelated to unions or interfaces.
        const filteredData = result.data.__schema.types.filter((t: any) => t.possibleTypes !== null); // eslint-disable-line no-underscore-dangle
        result.data.__schema.types = filteredData; // eslint-disable-line no-underscore-dangle

        writeReceivedDataFileJson('possible-types.json', result.data);
        return new IntrospectionFragmentMatcher({
            introspectionQueryResultData: result.data,
        });
    }
}
