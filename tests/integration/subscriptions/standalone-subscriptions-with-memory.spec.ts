import { CompositeModuleName } from '../../../src/modules/module';
import { runStandaloneSubscriptionsTest } from './standalone-subscriptions';

runStandaloneSubscriptionsTest('Memory PubSub', [CompositeModuleName.SubscriptionsGraphQL]);
