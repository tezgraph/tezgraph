import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const bigMapChanged = gql`
    ${allFragments}
    subscription (
        $replayFromBlockLevel: Int
        $big_map: BigMapDiffBigMapFilter
        $key: MichelsonFilter
        $action: BigMapDiffActionFilter
        $source: AddressFilter
        $destination: NullableAddressFilter
    ) {
        bigMapChanged(
            replayFromBlockLevel: $replayFromBlockLevel
            filter: { big_map: $big_map, key: $key, action: $action, source: $source, destination: $destination }
        ) {
            big_map_diff {
                ...BigMapDiffFragment
            }
            operation {
                source
            }
            parent {
                ...ContractResultParentFragment
            }
        }
    }
`;
