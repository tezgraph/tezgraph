import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const originationAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $source: AddressFilter
        $delegate: NullableAddressFilter
        $originated_contract: NullableAddressArrayFilter
        $status: NullableOperationResultStatusFilter
    ) {
        originationAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                source: $source
                status: $status
                and: [{ delegate: $delegate }, { originated_contract: $originated_contract }]
            }
        ) {
            ...OperationFragment
            ...MoneyOperationFragment
            balance
            delegate
            script {
                ...ScriptedContractsFragment
            }
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
                internal_operation_results {
                    ...InternalOperationResultFragment
                }
                operation_result {
                    ...OriginationResultFragment
                }
            }
        }
    }
`;
