import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const storageChanged = gql`
    ${allFragments}
    subscription ($replayFromBlockLevel: Int, $source: AddressFilter, $destination: NullableAddressFilter) {
        storageChanged(
            replayFromBlockLevel: $replayFromBlockLevel
            filter: { source: $source, destination: $destination }
        ) {
            storage
            operation {
                source
            }
            parent {
                ...ContractResultParentFragment
            }
        }
    }
`;
