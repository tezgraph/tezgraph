import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const activateAccountAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $pkh: AddressFilter
    ) {
        activateAccountAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                pkh: $pkh
            }
        ) {
            ...OperationFragment
            pkh
            secret
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
            }
        }
    }
`;
