import { gql } from 'apollo-server-express';

export const operationResultFragment = gql`
    fragment OperationResultFragment on OperationResult {
        status
        consumed_gas
        consumed_milligas
        errors
    }
`;
