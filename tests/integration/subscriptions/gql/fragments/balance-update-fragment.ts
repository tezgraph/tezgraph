import { gql } from 'apollo-server-express';

export const balanceUpdateFragment = gql`
    fragment BalanceUpdateFragment on BalanceUpdate {
        kind
        change
        origin
        contract
        category
        delegate
        cycle
    }
`;
