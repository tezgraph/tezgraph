import { gql } from 'apollo-server-express';

export const moneyOperationFragment = gql`
    fragment MoneyOperationFragment on MoneyOperationNotification {
        source
        fee
        counter
        gas_limit
        storage_limit
    }
`;
