import { gql } from 'apollo-server-express';

export const inlinedEndorsementFragment = gql`
    fragment InlinedEndorsementFragment on InlinedEndorsement {
        branch
        operations {
            kind
            level
            slot
            round
            block_payload_hash
        }
        signature
    }
`;
