import { gql } from 'apollo-server-express';

export const scriptedContractsFragment = gql`
    fragment ScriptedContractsFragment on ScriptedContracts {
        code
        storage
    }
`;
