import { gql } from 'apollo-server-express';

export const operationFragment = gql`
    fragment OperationFragment on OperationNotification {
        kind
        origin
        operation_group {
            protocol
            chain_id
            hash
            branch
            signature
        }
        block {
            ...BlockFragment
        }
    }
`;
