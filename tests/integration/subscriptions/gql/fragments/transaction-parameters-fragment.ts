import { gql } from 'apollo-server-express';

export const transactionParametersFragment = gql`
    fragment TransactionParametersFragment on TransactionParameters {
        entrypoint
        value
    }
`;
