import { gql } from 'apollo-server-express';

export const blockFragment = gql`
    fragment BlockFragment on BlockNotification {
        hash
        protocol
        chain_id
        header {
            level
            proto
            predecessor
            timestamp
            signature
            validation_pass
            operations_hash
            fitness
            context
            priority
            proof_of_work_nonce
            seed_nonce_hash
            liquidity_baking_escape_vote
            payload_hash
            payload_round
        }
    }
`;
