import { gql } from 'apollo-server-express';

export const registerGlobalConstantResultFragment = gql`
    fragment RegisterGlobalConstantResultFragment on RegisterGlobalConstantNotificationResult {
        ...OperationResultFragment
        balance_updates {
            ...BalanceUpdateFragment
        }
        storage_size
        global_address
    }
`;
