import { gql } from 'apollo-server-express';

export const originationResultFragment = gql`
    fragment OriginationResultFragment on OriginationNotificationResult {
        ...OperationResultFragment
        balance_updates {
            ...BalanceUpdateFragment
        }
        originated_contracts
        storage_size
        paid_storage_size_diff
        big_map_diff {
            ...BigMapDiffFragment
        }
        lazy_storage_diff {
            ...LazyStorageDiffFragment
        }
    }
`;
