import { gql } from 'apollo-server-express';

export const endorsementMetadataFragment = gql`
    fragment EndorsementMetadataFragment on EndorsementNotificationMetadata {
        balance_updates {
            ...BalanceUpdateFragment
        }
        delegate
        slots
    }
`;
