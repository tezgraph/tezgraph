import { gql } from 'apollo-server-express';

export const delegationResultFragment = gql`
    fragment DelegationResultFragment on DelegationResult {
        ...OperationResultFragment
    }
`;
