import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const endorsementWithSlotAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $delegate: NullableAddressFilter
    ) {
        endorsementWithSlotAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                delegate: $delegate
            }
        ) {
            ...OperationFragment
            slot
            endorsement {
                ...InlinedEndorsementFragment
            }
            metadata {
                ...EndorsementMetadataFragment
            }
        }
    }
`;
