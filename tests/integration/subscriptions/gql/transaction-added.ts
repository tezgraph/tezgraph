import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const transactionAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $source: AddressFilter
        $destination: AddressFilter
        $status: NullableOperationResultStatusFilter
    ) {
        transactionAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                source: $source
                destination: $destination
                status: $status
            }
        ) {
            ...OperationFragment
            ...MoneyOperationFragment
            amount
            destination
            parameters {
                ...TransactionParametersFragment
            }
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
                internal_operation_results {
                    ...InternalOperationResultFragment
                }
                operation_result {
                    ...TransactionResultFragment
                }
            }
        }
    }
`;
