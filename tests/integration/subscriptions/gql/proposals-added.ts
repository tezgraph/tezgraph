import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const proposalsAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $source: AddressFilter
        $proposals: NullableProtocolHashArrayFilter
    ) {
        proposalsAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                source: $source
                proposals: $proposals
            }
        ) {
            ...OperationFragment
            source
            period
            proposals
        }
    }
`;
