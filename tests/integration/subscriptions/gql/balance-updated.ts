import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const balanceUpdated = gql`
    ${allFragments}
    subscription (
        $replayFromBlockLevel: Int
        $kind: BalanceUpdateKindFilter
        $category: NullableBalanceUpdateCategoryFilter
        $contract: NullableAddressFilter
        $delegate: NullableAddressFilter
    ) {
        balanceUpdated(
            replayFromBlockLevel: $replayFromBlockLevel
            filter: { kind: $kind, category: $category, contract: $contract, delegate: $delegate }
        ) {
            balance_update {
                ...BalanceUpdateFragment
            }
            operation {
                kind
            }
            parent {
                ... on InternalOperationResult {
                    source
                }
                ... on ActivateAccountNotification {
                    kind
                }
                ... on DelegationNotification {
                    kind
                }
                ... on DoubleBakingEvidenceNotification {
                    kind
                }
                ... on DoubleEndorsementEvidenceNotification {
                    kind
                }
                ... on EndorsementNotification {
                    kind
                }
                ... on OriginationNotification {
                    kind
                }
                ... on RevealNotification {
                    kind
                }
                ... on SeedNonceRevelationNotification {
                    kind
                }
                ... on TransactionNotification {
                    kind
                }
            }
        }
    }
`;
