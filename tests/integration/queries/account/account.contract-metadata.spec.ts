/* eslint-disable max-lines */
import { gql } from 'apollo-server-express';

import { executeTestQuery } from './account.utils';

const testQuery = gql`
    query AccountsQuery($address: Address!) {
        accounts(first: 1, filter: { addresses: [$address] }) {
            edges {
                node {
                    address
                    contract_metadata {
                        name
                        description
                        version
                        authors
                        homepage
                        interfaces
                        license {
                            details
                            name
                        }
                        source {
                            tools
                            location
                        }
                        views {
                            name
                            description
                            pure
                            implementations {
                                michelsonStorageView {
                                    version
                                    parameter
                                    michelson_code
                                    canonical_code
                                    returnType
                                    annotations {
                                        name
                                        description
                                    }
                                }
                                restApiQuery {
                                    specificationUri
                                    baseUri
                                    path
                                    method
                                }
                            }
                        }
                        errors {
                            view
                            languages
                            expansion {
                                int
                                string
                                bytes
                            }
                            error {
                                int
                                string
                                bytes
                            }
                        }
                        raw
                    }
                }
            }
        }
    }
`;

describe('Account Resolver - Contract Metadata - sha256', () => {
    it.each(['KT1HqmWbxe5tFSrmLEnwqxBCj6BjBrDj6Gr1'])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - errors', () => {
    it.each(['KT1KbHwGTqzjQMDoJx4ksdeM3BmKvxLKHTcC'])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - https', () => {
    it.each([
        'KT1MZz5wc2H5FKDWSivkq4vp9yU9peEARfuk',
        'KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV',
        'KT1Ri3KCqgJyZuc2jQbiPfQzBgfRbZTGmpWD',
        'KT1EtmVGhh3t5V2RBevyXjVkCkL5cdNkv6NA',
    ])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - ipfs', () => {
    it.each([
        'KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton',
        'KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD',
        'KT1GbyoDi7H1sfXmimXpptZJuCdHMh66WS9u',
    ])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - tezosStorageSelf', () => {
    it.each([
        'KT18wLKKdo1hUqUXxcMNrjH1AkY8rzLiV6dx',
        'KT1Sn3eNrZ2uATmy7ZGTKZBCtyeVERNqkQFw',
        'KT1C9CeNs62SpFiPHaLwL1bx34k5nFvsqJDL',
        'KT1W8PqJsZcpcAgDQH9SKQSZKvjVbpjUk8Sc',
        'KT19hzFPbMW9cYgUhLNghytkWEymMKsPrdfX',
    ])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - tezosStorageOther', () => {
    it.each([
        'KT1K7whn5yHucGXMN7ymfKiX5r534QeaJM29',
        'KT1FWHLMk5tHbwuSsp31S4Jum4dTVmkXpfJw',
        'KT1SwH9P1Tx8a58Mm6qBExQFTcy2rwZyZiXS',
    ])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - contract-metadata views - ', () => {
    it.each([
        'KT19WrpgbvN76rWQ4NEHWgPnzqDoA7nfbFYM',
        'KT1VCaAEpuKF6EmqpA2VyvkiYLMGJ7XFnHi8',
        'KT1QE4nZiAXbpuDCu4P5QTNibQSx6FFW3y2W',
    ])(`should return contract metadata for %s`, async (uri) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

const contractMetadataViewNamesFilterQuery = gql`
    query AccountsQuery($address: Address!, $view_name: String!) {
        accounts(first: 1, filter: { addresses: [$address] }) {
            edges {
                node {
                    address
                    contract_metadata(view_names: [$view_name]) {
                        views {
                            name
                            description
                            pure
                            implementations {
                                michelsonStorageView {
                                    version
                                    parameter
                                    michelson_code
                                    canonical_code
                                    returnType
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

describe('Account Resolver - Contract Metadata - contract-metadata views filtered -', () => {
    it.each([
        ['KT19WrpgbvN76rWQ4NEHWgPnzqDoA7nfbFYM', 'token_metadata'],
        ['KT1VCaAEpuKF6EmqpA2VyvkiYLMGJ7XFnHi8', 'GetClaims'],
        ['KT1QE4nZiAXbpuDCu4P5QTNibQSx6FFW3y2W', 'token_metadata'],
    ])(`should return contract metadata for %s`, async (uri, view_name) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
                view_name,
            },
            contractMetadataViewNamesFilterQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Contract Metadata - contract-metadata views filtered - expected to be empty -', () => {
    it.each([
        ['KT19WrpgbvN76rWQ4NEHWgPnzqDoA7nfbFYM', 'token_metadata_fail1'],
        ['KT1VCaAEpuKF6EmqpA2VyvkiYLMGJ7XFnHi8', 'GetClaims_fail1'],
        ['KT1QE4nZiAXbpuDCu4P5QTNibQSx6FFW3y2W', 'token_metadata_fail1'],
    ])(`should return contract metadata for %s`, async (uri, view_name) => {
        const res = await executeTestQuery(
            'contract-metadata',
            {
                address: uri,
                view_name,
            },
            contractMetadataViewNamesFilterQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
