import { gql } from 'apollo-server-express';
import { DocumentNode } from 'graphql';

import { DateRangeFilter } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { createTestGraphQLClientUtils } from '../utils/test-client';

export const ID = 'id';
export const ASC = 'asc';

export function buildTestQuery(fieldResolver: string) {
    return gql`
        query AccountsQuery(
            $address: Address!
            $first: Int
            $last: Int
            $before: Cursor
            $after: Cursor
            $order_by: OrderBy
            $date_range: DateRange

        ) {
            accounts(first: 1, filter: { addresses: [$address] }) {
                ${fieldResolver}(
                    first: $first
                    last: $last
                    before: $before
                    after: $after
                    order_by: $order_by
                    date_range: $date_range
                ) {
                    edges {
                        node {
                            hash
                            batch_position
                            internal
                            timestamp
                        }
                    }
                }
            }
        }`;
}

export interface TestQueryOptionsVariables {
    addresses?: string[];
    address?: string;
    first?: number;
    last?: number;
    before?: string;
    after?: string;
    view_name?: string;
    token_id?: number;
    order_by?: { field: string; direction: string };
    date_range?: DateRangeFilter;
    relationship_type?: string;
    filter?: object;
}

const testUtils = createTestGraphQLClientUtils();

export async function executeTestQuery(
    operation: string,
    variables: TestQueryOptionsVariables,
    customQuery?: DocumentNode,
) {
    return testUtils.executeOperation({
        query: customQuery ? customQuery : buildTestQuery(operation),
        variables: {
            ...variables,
        },
    });
}
