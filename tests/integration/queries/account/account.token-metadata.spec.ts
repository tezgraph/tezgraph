import { gql } from 'apollo-server-express';

import { executeTestQuery } from './account.utils';

const testQuery = gql`
    query AccountsQuery($address: Address!, $token_id: Int!) {
        accounts(first: 1, filter: { addresses: [$address] }) {
            edges {
                node {
                    token_metadata(token_id: $token_id) {
                        decimals
                        token_id
                        symbol
                        name
                        raw
                    }
                }
            }
        }
    }
`;

describe('Account Resolver - Token Metadata - on chain -', () => {
    it.each([
        ['KT1G2kUvEg9qJTVEsWQFUTefUzcSzmrG9jyQ', 9],
        ['KT18zVySfjcmot2HdsDyv8ScokDHFS9PmsAW', 0],
    ])(`should return token metadata for %s`, async (uri, token_id) => {
        const res = await executeTestQuery(
            'token-metadata',
            {
                address: uri,
                token_id,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Token Metadata - off chain -', () => {
    it.each([
        ['KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD', 2009],
        ['KT1StkBRUfJD9AuHAE4oQVi49qLQhsgeDcU1', 7],
    ])(`should return token metadata for %s`, async (uri, token_id) => {
        const res = await executeTestQuery(
            'token-metadata',
            {
                address: uri,
                token_id,
            },
            testQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});

describe('Account Resolver - Token Metadata - error -', () => {
    it.each([['KT1KTPdVqmnU1F6v2Q14nmKqyd37t4zsWFUa', 0]])(
        `should return non-compliant standard error for %s`,
        async (uri, token_id) => {
            const res = await executeTestQuery(
                'token-metadata',
                {
                    address: uri,
                    token_id,
                },
                testQuery,
            );
            expect(res.errors?.[0]?.extensions?.error).toMatch(
                'Token metadata is non-compliant with the TZIP-016 standards.',
            );
            expect(res.errors?.[0]?.message).toContain(res.errors?.[0]?.extensions?.error);
        },
    );
});
