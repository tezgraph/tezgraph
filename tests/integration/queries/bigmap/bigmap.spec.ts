/* eslint-disable max-lines */
import { gql } from 'apollo-server-express';

import { createTestGraphQLClientUtils } from '../utils/test-client';

const testUtils = createTestGraphQLClientUtils();

describe('Bigmap query', () => {
    it('should fail if page size is not limited by first or last', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                    }
                    order_by: { field: annots, direction: asc }
                ) {
                    edges {
                        cursor
                        node {
                            annots
                            key_type_micheline_json
                            key_type
                            key_type_michelson
                            block {
                                level
                                hash
                            }
                            contract {
                                address
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response.errors?.[0]?.extensions?.error).toMatch(
            'Please limit the page size to be returned, use first or last',
        );
        expect(response.errors?.[0]?.message).toContain(response.errors?.[0]?.extensions?.error);
    });

    it('should filter and sort on multiple contracts', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                    }
                    first: 10
                    order_by: { field: annots, direction: asc }
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            annots
                            batch_position
                            block {
                                level
                                hash
                            }
                            contract {
                                address
                            }
                            id
                            key_type_micheline_json
                            key_type
                            key_type_michelson
                            value_type_micheline_json
                            value_type
                            value_type_michelson
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should filter by multiple contracts and sort  by annots paginate with last', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                    }
                    last: 10
                    order_by: { field: annots, direction: asc }
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            id
                            annots
                            block {
                                level
                                hash
                            }
                            batch_position
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should filter by multiple contracts and sort by id paginate with last', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                    }
                    last: 10
                    order_by: { field: id, direction: desc }
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            id
                            annots
                            block {
                                level
                                hash
                            }
                            batch_position
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly paginate with after and first', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                    }
                    after: "3907"
                    first: 3
                    order_by: { field: annots, direction: asc }
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            id
                            annots
                            block {
                                level
                                hash
                            }
                            batch_position
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly paginate with before and last', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                    }
                    before: "3907"
                    last: 3
                    order_by: { field: annots, direction: asc }
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            id
                            annots
                            block {
                                level
                                hash
                            }
                            batch_position
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly filter by contract and annots', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(
                    filter: {
                        contract: ["KT1EqjMLXVxVVQBD8XucsMREHPB9UJbvipre", "KT1MMLb2FVrrE9Do74J3FH1RNNc4QhDuVCNX"]
                        annots: ["%dex_lambdas"]
                    }
                    before: "3907"
                    last: 3
                    order_by: { field: annots, direction: asc }
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            id
                            annots
                            block {
                                level
                                hash
                            }
                            batch_position
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly list first 10 keys of bigmap', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(filter: { ids: ["12"] }, first: 2) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            keys(first: 10) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        key_hash
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly paginate keys of bigmap after cursor', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(filter: { ids: ["12"] }, first: 2) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            keys(first: 6, after: "12:exprtpvmHQSdsTfi817fL68X6AsugsEBPbdDbYs2JHDbxfMuopRcsE") {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        key_hash
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly return current_value for keys', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(filter: { ids: ["12"] }, first: 2) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            keys(first: 10) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        key_hash
                                        current_value {
                                            value_micheline_json
                                            value
                                            value_michelson
                                            kind
                                            batch_position
                                            block {
                                                level
                                                hash
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly return values_history for keys', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(filter: { ids: ["12"] }, first: 2) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            keys(first: 4) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        values_history(first: 4) {
                                            total_count
                                            edges {
                                                cursor
                                                node {
                                                    value_micheline_json
                                                    value
                                                    value_michelson
                                                    kind
                                                    batch_position
                                                    block {
                                                        level
                                                        hash
                                                    }
                                                    source {
                                                        address
                                                    }
                                                    contract {
                                                        address
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should properly paginate values_history, first page', async () => {
        const testQuery = gql`
            query BigmapQuery($keys: [Micheline!]) {
                bigmaps(filter: { ids: ["26"] }, first: 1) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            block {
                                hash
                            }
                            keys(first: 1, filter: { keys: $keys }) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        current_value {
                                            value_micheline_json
                                            value
                                            value_michelson
                                            block {
                                                level
                                                hash
                                            }
                                        }
                                        values_history(first: 10) {
                                            total_count
                                            edges {
                                                cursor
                                                node {
                                                    value_micheline_json
                                                    value
                                                    value_michelson
                                                    kind
                                                    batch_position
                                                    block {
                                                        level
                                                        hash
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({
            query: testQuery,
            variables: { keys: ['tz1W6dVQvpeAsnjfwKvoPBJViRKaa7P9HJ99'] },
        });
        expect(response).toMatchSnapshot();
    });

    it('should properly paginate values_history, using after', async () => {
        const testQuery = gql`
            query BigmapQuery($keys: [Micheline!]) {
                bigmaps(filter: { ids: ["26"] }, first: 1) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            block {
                                hash
                            }
                            keys(first: 1, filter: { keys: $keys }) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        current_value {
                                            value_micheline_json
                                            value
                                            value_michelson
                                            block {
                                                level
                                                hash
                                            }
                                        }
                                        values_history(
                                            first: 5
                                            after: "8990440000001:exprtau8jyUVF6aGbMMbaQKAe7VHGP15jD9vHJhrEbDzoMDH7uE8cK"
                                        ) {
                                            total_count
                                            edges {
                                                cursor
                                                node {
                                                    value_micheline_json
                                                    value
                                                    value_michelson
                                                    kind
                                                    batch_position
                                                    block {
                                                        level
                                                        hash
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({
            query: testQuery,
            variables: { keys: ['tz1W6dVQvpeAsnjfwKvoPBJViRKaa7P9HJ99'] },
        });
        expect(response).toMatchSnapshot();
    });

    it('should properly paginate values_history, using before', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(filter: { ids: ["26"] }, first: 1) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            block {
                                hash
                            }
                            keys(first: 1, filter: { keys: ["tz1W6dVQvpeAsnjfwKvoPBJViRKaa7P9HJ99"] }) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        current_value {
                                            value_micheline_json
                                            value
                                            value_michelson
                                            block {
                                                level
                                                hash
                                            }
                                        }
                                        values_history(
                                            last: 5
                                            before: "8990320000001:exprtau8jyUVF6aGbMMbaQKAe7VHGP15jD9vHJhrEbDzoMDH7uE8cK"
                                        ) {
                                            total_count
                                            edges {
                                                cursor
                                                node {
                                                    value_micheline_json
                                                    value
                                                    value_michelson
                                                    kind
                                                    batch_position
                                                    block {
                                                        level
                                                        hash
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should access bigmap keys via bigmap_keys query', async () => {
        const testQuery = gql`
            query BigmapKeysQuery {
                bigmap_keys(filter: { bigmap_id: "26" }, first: 10) {
                    total_count
                    edges {
                        cursor
                        node {
                            key_micheline_json
                            key
                            key_michelson
                            current_value {
                                value_micheline_json
                                value
                                value_michelson
                                block {
                                    level
                                    hash
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should filter bigmap keys by id and keys', async () => {
        const testQuery = gql`
            query BigmapKeysQuery {
                bigmap_keys(
                    filter: {
                        bigmap_id: "26"
                        keys: ["tz1TNcFeYjEbHDjYsvxBtrdMCo6q6bX5fYFf", "tz1bzaysnJE6BwypBaN4B8x76EGjK4tPbqHb"]
                    }
                    first: 10
                ) {
                    total_count
                    edges {
                        cursor
                        node {
                            key_micheline_json
                            key
                            key_michelson
                            current_value {
                                value_micheline_json
                                value
                                value_michelson
                                block {
                                    level
                                    hash
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should access bigmap values through bigmap_values query', async () => {
        const testQuery = gql`
            query BigmapValuesQuery($key: Micheline) {
                bigmap_values(first: 10, filter: { bigmap_id: "26", key: $key }) {
                    total_count
                    edges {
                        cursor
                        node {
                            key_micheline_json
                            key
                            key_michelson
                            key_hash
                            value_micheline_json
                            value
                            value_michelson
                            kind
                            batch_position
                            block {
                                level
                                hash
                            }
                            source {
                                address
                            }
                            operation {
                                hash
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({
            query: testQuery,
            variables: { key: 'tz1W6dVQvpeAsnjfwKvoPBJViRKaa7P9HJ99' },
        });
        expect(response).toMatchSnapshot();
    });

    it('should access bigmap diff through operations query', async () => {
        const testQuery = gql`
            query Operation {
                operations(filter: { hash: "opNA1tQ6mz7BiqnRPrvgN8LYexL3Fj8no9SUjCpuKA1H3zeKaPw" }, first: 10) {
                    total_count
                    edges {
                        node {
                            bigmap_values(first: 3) {
                                total_count
                                edges {
                                    node {
                                        key_micheline_json
                                        key
                                        key_michelson
                                        value_micheline_json
                                        value
                                        value_michelson
                                        kind
                                        bigmap {
                                            id
                                        }
                                        block {
                                            level
                                            hash
                                        }
                                        batch_position
                                        previous {
                                            block {
                                                level
                                                hash
                                            }
                                            batch_position
                                            value_micheline_json
                                            value
                                            value_michelson
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    it('should access bigmap allocations through account query', async () => {
        const testQuery = gql`
            query AccountsQuery($address: Address!) {
                accounts(first: 1, filter: { addresses: [$address] }) {
                    total_count
                    edges {
                        node {
                            address
                            activated
                            public_key
                            bigmaps(first: 2) {
                                total_count
                                edges {
                                    node {
                                        annots
                                        id
                                        key_type_micheline_json
                                        key_type
                                        key_type_michelson
                                        keys(first: 3) {
                                            edges {
                                                node {
                                                    key_micheline_json
                                                    key
                                                    key_michelson
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({
            query: testQuery,
            variables: { address: 'KT19hzFPbMW9cYgUhLNghytkWEymMKsPrdfX' },
        });
        expect(response).toMatchSnapshot();
    });

    it('Should behave properly for copied bigmaps', async () => {
        const testQuery = gql`
            query BigmapQuery {
                bigmaps(filter: { ids: ["1862"] }, first: 1) {
                    total_count
                    edges {
                        node {
                            id
                            annots
                            keys(first: 20) {
                                total_count
                                edges {
                                    cursor
                                    node {
                                        key_michelson
                                        values_history(first: 10) {
                                            total_count
                                            edges {
                                                cursor
                                                node {
                                                    value_michelson
                                                    kind
                                                    batch_position
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const response = await testUtils.executeOperation({ query: testQuery });
        expect(response).toMatchSnapshot();
    });

    const bigmapKeySamples: { id: number; key: object }[] = [
        {
            id: 20619,
            key: {
                addr: 'KT18fp5rcTW7mbWDmzFwjLDUhs5MeJmagDSZ',
                token_id: '19',
            },
        },
        {
            id: 135650,
            key: { fa2_address: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9', token_id: null },
        },
        {
            id: 24009,
            key: {
                0: 'KT1UUjqN2tVHb2xEXS6XHs1GbuV1F6cDTAiT',
                1: '2021-11-10T16:31:28.000Z',
            },
        },
    ];
    bigmapKeySamples.forEach((sample) => {
        const testQuery = gql`
            query BigmapKeys($id: BigNumber!, $key: Micheline!) {
                bigmap_keys(first: 2, filter: { bigmap_id: $id, keys: [$key] }) {
                    edges {
                        node {
                            key
                            current_value {
                                value
                            }
                            values_history(first: 2) {
                                edges {
                                    node {
                                        value
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        it(`Should properly find BigmapKeys and values for bigmap #${sample.id}`, async () => {
            const response = await testUtils.executeOperation({
                query: testQuery,
                variables: { id: sample.id.toString(), key: sample.key },
            });
            expect(response.errors).toBeUndefined();
            expect(response.data?.bigmap_keys.edges.length).toBe(1);
            // This test is disabled, because of problems with BigNumbers------ expect(response.data?.bigmap_keys.edges[0].node.key).toEqual(sample.key);
            expect(response.data?.bigmap_keys.edges[0].node.current_value).toBeDefined();
            expect(response.data?.bigmap_keys.edges[0].node.values_history.edges.length).toBeGreaterThanOrEqual(1);
        });
    });
});
