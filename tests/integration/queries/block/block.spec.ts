import { gql } from 'apollo-server-express';

import { createTestGraphQLClientUtils } from '../utils/test-client';

const testUtils = createTestGraphQLClientUtils();

const query = gql`
    query blocks(
        $first: Int
        $after: Cursor
        $before: Cursor
        $last: Int
        $filter: BlockFilter
        $order_by: BlockOrderByInput
    ) {
        blocks(first: $first, after: $after, before: $before, last: $last, filter: $filter, order_by: $order_by) {
            total_count
            edges {
                cursor
                node {
                    level
                    hash
                    timestamp
                    __typename
                }
            }
        }
    }
`;

const queryWithoutTotalCount = gql`
    query blocks(
        $first: Int
        $after: Cursor
        $before: Cursor
        $last: Int
        $filter: BlockFilter
        $order_by: BlockOrderByInput
    ) {
        blocks(first: $first, after: $after, before: $before, last: $last, filter: $filter, order_by: $order_by) {
            edges {
                cursor
                node {
                    level
                    hash
                    timestamp
                    __typename
                }
            }
        }
    }
`;

describe('Block Resolvers', () => {
    it('should return last block (with filter to return consistent result)', async () => {
        const res = await testUtils.executeOperation({
            query,
            variables: {
                first: 1,
                filter: {
                    level: { lte: 1000 },
                },
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should filter by level', async () => {
        const res = await testUtils.executeOperation({
            query,
            variables: {
                first: 10,
                filter: {
                    level: { lte: 1001, gte: 1000 },
                },
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should filter by timestamp', async () => {
        const res = await testUtils.executeOperation({
            query,
            variables: {
                first: 10,
                filter: {
                    timestamp: {
                        lte: '2018-07-01T10:23:42.000Z',
                        gte: '2018-07-01T10:22:42.000Z',
                    },
                },
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should filter by hash', async () => {
        const res = await testUtils.executeOperation({
            query,
            variables: {
                first: 10,
                filter: {
                    hashes: [
                        'BMbVtqhnzWcXVCLEZvcVVVPYdLLBrFcd2j3bufVY67WWRY8FVuN',
                        'BMR8HKgQLm4f7KTxzq8Q9T57KPBfFTs9UuwvvU2vguGBtQ6rqGc',
                    ],
                },
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should paginate with after', async () => {
        const res = await testUtils.executeOperation({
            query: queryWithoutTotalCount,
            variables: {
                first: 3,
                after: 'BMbVtqhnzWcXVCLEZvcVVVPYdLLBrFcd2j3bufVY67WWRY8FVuN',
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should paginate with before', async () => {
        const res = await testUtils.executeOperation({
            query: queryWithoutTotalCount,
            variables: {
                last: 3,
                before: 'BMbVtqhnzWcXVCLEZvcVVVPYdLLBrFcd2j3bufVY67WWRY8FVuN',
                order_by: {
                    field: 'level',
                    direction: 'asc',
                },
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should return oldest blocks', async () => {
        const res = await testUtils.executeOperation({
            query: queryWithoutTotalCount,
            variables: {
                first: 3,
                order_by: {
                    field: 'level',
                    direction: 'asc',
                },
            },
        });
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
