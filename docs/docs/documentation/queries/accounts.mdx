---
title: 'Accounts'
metaTitle: 'Accounts | TezGraph'
metaDescription: 'This page contains the Tezgraph Accounts documentation and examples.'
---

import GraphiQLComponent from '@site/components/graphiql-component';

# Accounts

:::tip
If you are new to Tezgraph, we recommend reading the [quick start](https://tezgraph.com/docs/quickStart) and [pagination documentation](https://tezgraph.com/docs/documentation/queries/pagination) first.
:::

### Introduction

On the Tezos blockchain, implicit and originated accounts are required and used to perform operations. The Tezgraph `accounts` query provides the ability to retrieve account details, as well as operations and bigmaps related to the account addresses.

On the Tezos blockchain, there are two types of accounts: implicit and originated. See Tezos glossary [here](https://tezos.gitlab.io/ithaca/glossary.html?highlight=glossary). Operations can be executed between any accounts. The Tezgraph `accounts` query provides the ability to retrieve accounts details with operations and bigmaps associated with those account addresses.

:::info
All of the queryable fields can be found under the DOCS tab on the right side of the playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).

-   `DOCS` > `accounts` > `edges` > `node`
:::

---

### Accounts Query

<GraphiQLComponent
    query={`query {
  accounts(
    first: 1
    filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }
  ) {
    edges {
      node {
        operations(
          first: 2
          filter: { relationship_type: source, kind: transaction }
        ) {
          edges {
            cursor
            node {
              hash
              batch_position
            }
          }
        }
      }
    }
  }
}`}
/>

In this example, we are querying for the `hash` and `batch_position` of the two latest (first) transaction `OperationRecords` with the public key hash `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS` as the `source`.

The `accounts` query takes a filter argument. Inside this filter argument is an `addresses` field of type `[Address!]`. Here inside this array is where we insert our target addresses. The public key hashes of both implicit and originated accounts are accepted as valid addresses.

The `accounts` query also requires either the `first` or `last` argument. In this example we are using `first: 1`. In the case of the accounts query, the `first` and `last` arguments determine which of the addresses provided in `filter.addresses` (`filter: { addresses: ["..."]}`) will be used in the query. If `first: 3` is used, Tezgraph would then only return results for the first 3 addresses in from `filter.addresses`.

Inside the `accounts.edges.node` sub-query, we are sub-selecting for operation details using the `operations` sub-query. This `operations` sub-query can also take in arguments such as `first`, `last`, `order_by`, `before`, `after`, `filter`, and `order_by`.

By default, the TezGraph Query API returns all `OperationRecords` in descending chronological order. This means that the first record in the list would be the latest operation, and the last record would be the oldest. This sorting order may be modified with the argument `order_by`, which will be explained in the Advanced Accounts Query example.

Here we are using the argument `first: 2`. In addition, we are using the `relationship_type` and `kind` filters. Since we are using the default sort order, which is in descending chronological order, it will return the two latest transaction `OperationRecords` in which the account of interest (`tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`) is the source (the initiator of the operation) of.

#### Arguments:

-   **accounts**:

    -   `filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }` - This will filter for `AccountRecords` with an account address (public key hash) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`. It is possible include multiple account addresses in this array.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `first: 1` - In the case of the `accounts` query, this argument here will be used to determine which addresses in the `addresses` filter to use, and how many of them to use. The argument, `first: 1` tells the `accounts` query to for the first address in the `addresses` filter.
        -   **OPTIONAL (but required when using the `addresses` filter)** - The `first`/`last` arguments for the `accounts` query are used to set the number and direction of the addresses to use in the `addresses` filter.

-   **accounts.operations**:

    -   `filter: { kind: transaction }` - This will filter the operations for `OperationRecords` with `kind: transaction` key value pair.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `first: 2` - This will return the first 2 operations that meet the given conditions.
        -   **REQUIRED** - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination), which will be explained in the pagination example.

    -   `filter: { relationship_type: source }` - This will filter for `OperationRecords` with a source account address (public key hash) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`.
        -   **REQUIRED** - This is a required field when using the `operations` sub-query inside the `accounts` query. This will tell Tezgraph which operation account address fields (`source`, `destination`, or `contract`) to filter by.

---

### Advanced Accounts Query

<GraphiQLComponent
    query={`query {
  accounts(
    first: 1
    filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }
  ) {
    edges {
      node {
        operations(
          filter: {
            relationship_type: source
            kind: transaction
            timestamp: {
              lte: "2019-06-14T16:22:03Z"
              gte: "2019-06-13T16:22:03Z"
            }
          }
          order_by: { field: chronological_order, direction: asc }
          first: 10
        ) {
          edges {
            cursor
            node {
              hash
              batch_position
              internal
              block {
                timestamp
              }
            }
          }
        }
      }
    }
  }
}`}
/>

In this example, we are querying for the `hash`, `batch_position`, `internal`, and `timestamp` of transaction `OperationRecords` with the public key hash (address) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS` as the `source`.

In this example, we have added the `order_by` and `filter.timestamp` arguments.

#### Arguments:

-   **accounts**:

    -   `filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }` - This will filter for `AccountRecords` with an account address (public key hash) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`. It is possible include multiple account addresses in this array.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `first: 1` - In the case of the `accounts` query, this argument here will be used to determine which addresses in the `addresses` filter to use, and how many of them to use. The argument, `first: 1` tells the `accounts` query to for the first address in the `addresses` filter.
        -   **OPTIONAL (but required when using the `addresses` filter)** - The `first`/`last` arguments for the `accounts` query are used to set the number and direction of the addresses to use in the `addresses` filter.

-   **accounts.operations**:
    -   `order_by: { field: chronological_order, direction: asc }` - This argument will order the records in ascending (asc) chronological order.
        -   **OPTIONAL** - If this argument were not provided, by default, the results would be ordered by the chronological order in which they were indexed. The most recent operation first, and the oldest operation last.

    -   `filter: { kind: transaction }` - This will filter the operations for `OperationRecords` with `kind: transaction` key value pair.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `filter: { timestamp: { gte: "2019-06-13T16:22:03Z", lte: "2019-06-14T16:22:03Z" }}` - This argument will only return the records with a timestamp less than or equal to `2019-06-14T16:22:03Z` and greater than or equal to `2019-06-13T16:22:03Z`.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `first: 10` - Since we are ordering the operations in ascending chronological order, this argument will return the ten oldest records, with the oldest record at the top and the later record at the bottom.
        -   **REQUIRED** - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination), which will be explained in the pagination example.

    -   `filter: { relationship_type: source }` - This will filter for `OperationRecords` with a source account address (public key hash) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`.
        -   **REQUIRED** - This is a required field when using the `operations` sub-query inside the `accounts` query. This will tell Tezgraph which operation account address fields (`source`, `destination`, or `contract`) to filter by.
---

### Accounts Query with Pagination

<GraphiQLComponent
    query={`query {
  accounts(
    first: 1
    filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }
  ) {
    edges {
      node {
        operations(
          filter: { relationship_type: source, kind: transaction }
          first: 10
          after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"
        ) {
          edges {
            cursor
            node {
              hash
              batch_position
              internal
              block {
                timestamp
              }
            }
          }
        }
      }
    }
  }
}`}
/>

In this example, we are querying for the `hash`, `batch_position`, `internal`, and `timestamp` of the 10 latest (first) transaction `OperationRecord` with the public key hash of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS` as the `source`. In addition, we have also added the `cursor` field to the `edges` sub-selection and the argument pagination `after`.

Using the cursor of a record, we can query for the records before or after the record that the cursor references.

To do this, we use the arguments `before` or `after`, along with the arguments `first` or `last`.

-   Pagination requires the use of one of the `before` and `after` arguments and one of the `first` or `last` arguments.

The following pagination queries will succeed:

-   `first: 2, after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"`
-   `last: 20, before: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"`

The following pagination queries will fail:

-   `first: 100, last: 100, after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"`
-   `last: 100, before: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:1", after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"`

In this pagination example, we are querying for the first ten records after the first record in the Accounts Query Example Results. We do this by using the following arguments:

-   `after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"` - With this argument, the results will only return records after the record this cursor references.
-   `first: 10` - Since we are using the default sort order, which is in descending chronological order, it will return the two latest transaction records after the record that the above cursor references.

#### Arguments:

-   **accounts**:

    -   `filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }` - This will filter for `AccountRecords` with an account address (public key hash) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`. It is possible include multiple account addresses in this array.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `first: 1` - In the case of the `accounts` query, this argument here will be used to determine which addresses in the `addresses` filter to use, and how many of them to use. The argument, `first: 1` tells the `accounts` query to for the first address in the `addresses` filter.
        -   **OPTIONAL (but required when using the `addresses` filter)** - The `first`/`last` arguments for the `accounts` query are used to set the number and direction of the addresses to use in the `addresses` filter.

-   **accounts.operations**:

    -   `filter: { kind: transaction }` - This will filter the operation records for operation records with `kind: transaction` key value pair.
        -   **OPTIONAL** - This is an optional argument used to filter the query results.

    -   `after: "opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0"` - This argument will paginate the results and return only the records after the record with the cursor of `opNxsLZeNMttsyh4PxLk35dmRsJyLXQwJjpVp89ip7pU2j5yzyR:0:0`.
        -   **OPTIONAL** - This is an optional argument used to paginate the query results.

    -   `first: 10` - Since we are ordering the operations in ascending chronological order, this argument will return the ten oldest records, with the oldest record at the top and the later record at the bottom.
        -   **REQUIRED** - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination), which will be explained in the pagination example.

    -   `filter: { relationship_type: source }` - This will filter for operation records with a source account address (public key hash) of `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`.
        -   **REQUIRED** - This is a required field when using the `operations` sub-query inside the `accounts` query. This will tell Tezgraph which operation account address fields (`source`, `destination`, or `contract`) to filter by.

:::info
Tezgraph records all connected to each other in different ways through different fields. To explore other queries, records, and fields, visit the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).
.
:::
