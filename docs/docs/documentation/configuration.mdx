---
title: 'Configurations'
metaTitle: 'Configuration | TezGraph'
metaDescription: 'This page describes the configuration of TezGraph web app if you are running it locally on your machine(s).'
---

This page describes the configuration of TezGraph web app if you are running it locally on your machine(s). The app is configured using environment variables or alternatively `.env` root file. Below are the available variables.

<!-- HTTP Server -->

## HOST

- The IP address on which the app is exposed. The value `0.0.0.0` means all available IP addresses.
- Recommended value: `0.0.0.0`

## PORT

- The port number on which the app is exposed.
- Recommended value: `3000`

<!-- App Mode -->

## MODULES

- Parts of the app to be enabled. Supported modules:
  - `QueriesGraphQL` - Exposes GraphQL queries from the indexer database.
  - `SubscriptionsGraphQL` - Exposes GraphQL subscriptions from configured Tezos node. It consists of submodules: `SubscriptionsSubscriber`, `TezosMonitor`, `MemoryPubSub`.
  - `SubscriptionsSubscriber` - Exposes GraphQL subscriptions from a configured PubSub (another module).
  - `TezosMonitor` - Monitors Tezos node for new blocks and mempool operations. Then publishes them to a configured PubSub (another module).
  - `MemoryPubSub` - In-process memory PubSub so this app must be used standalone to work correctly.
  - `RedisPubSub` - External PubSub which uses Redis cache. Useful for fail-over deployment.
- Recommended value: `QueriesGraphQL,SubscriptionsGraphQL`

<!-- Indexer Database for Queries -->

## DATABASE_CONNECTION_STRING

- The connection string to the PostgreSQL indexer database.

## PAGE_SIZE_LIMIT

- Limit for page size that can be queried.
- Recommended value: `200`

## TEZOS_NODE_CHAIN_ID
- The chain ID of the Tezos network node that Tezgraph is connected to. Required for the Taquito RPC Client.
- Default value: `NetXdQprcVkpaWU`
    - This is the chain ID of Mainnet.

## RESOLVE_CONTRACT_BALANCE_FROM_DB
- This configuration will determine where Tezgraph retrieves contract balance values.
- When set to `true`, Tezgraph will retrieve the contract balance values from the database.
    - Prerequisite: The indexer is set to index contract balances to the database.
- When set to `false`, Tezgraph will retrieve the contract balance values using Taquito.
    - When using Taquito, each balance inquiry involves one RPC call, which may put additional stress on the connected node, and may also slow down the query. The RPC call may also fail.
- Default value: `true`

## MAX_HEALTHY_LAG_OF_INDEXED_BLOCKS
- This configuration sets the acceptable number of levels that Indexer is allowed to fall behind the head block level. Once this number is reached, the `IndexerHealth` on the `/health` page will return `Degraded`.
- Default value: `5`

## MAX_DEGRADED_LAG_OF_INDEXED_BLOCKS
- This configuration sets the acceptable number of levels that Indexer is allowed to fall behind the head block level. Once this number is reached, the `IndexerHealth` on the `/health` page will return `Unhealthy`.
- Default value: `50`

## IPFS_GATEWAY
- This configuration sets the IPFS gateway that Tezgraph will use to retrieve IPFS off-chain values.
- Default value: `cloudflare-ipfs.com`

<!-- Tezos Node for Subscriptions -->

## TEZOS_NODE

- The URL of the Tezos node.
- Recommended value: `https://api.tezos.org.ua`
- Alternative recommended value: `https://testnet-tezos.giganode.io`

## RPC_RETRY_DELAYS_MILLIS

- Configures a comma-separated list of retry periods for the requests to Tezos RPC to download blocks. It may fail if the node doesn’t have a new block yet.
- Recommended value: `500,1000,2000,4000,4000`

## RPC_MONITOR_RECONNECT_DELAYS_MILLIS

- The comma-separated list of delays to retry connection to Tezos node RPC to monitor new blocks and mempool. The last delay is retried indefinitely.
- Recommended value: `1,500,1000,2000,4000`

## TEZOS_MONITOR_WARN_CHUNK_COUNT

- A JSON from the Tezos node monitor is split to in multiple chunks -> we concatenate them. App logs a warning after this number of chunks is reached to get a valid JSON.
- Recommended value: `100`

## TEZOS_MONITOR_FAIL_CHUNK_COUNT

- Similar to previous setting. App fails and reconnects if this number of chunks is reached without getting a valid JSON.
- Recommended value: `1000`

## TEZOS_PROCESSING_MAX_AGE_SECONDS_DEGRADED

-  If there is no new item received and processed within the specified period (in seconds) by any Tezos workers for GraphQL subscriptions (e.g. block or mempool monitor worker) then corresponding health check turns degraded.
- Recommended value: `300`

## TEZOS_PROCESSING_MAX_AGE_SECONDS_UNHEALTHY

- Similar to previous setting but the health check turns unhealthy.
- Recommended value: `600`

## HEALTH_STATUS_CACHE_TTL_SECONDS

- Specifies time-to-live for cached health checks.
- Recommended value: `5`

<!-- Redis PubSub -->

## REDIS_CONNECTION_STRING

- Connection string to Redis instance to be used as a PubSub if the corresponding module is enabled.

<!-- GraphQL Limits -->

## GRAPH_QL_DEPTH_LIMIT

- Limits depth of GraphQL queries and subscriptions. "0" means no limit. For more info see [graphql-depth-limit plugin](https://github.com/stems/graphql-depth-limit#readme).
- Recommended value: `10`

## GRAPH_QL_COMPLEXITY_LIMIT

- Limits complexity of GraphQL queries and subscriptions. "0" means no limit. For more info see [graphql-validation-complexity plugin](https://github.com/4Catalyzer/graphql-validation-complexity#readme).
- Recommended value: `10000`

## REPLAY_FROM_BLOCK_LEVEL_MAX_FROM_HEAD

- Limits replay of subscriptions, how many levels back from head block level it can go.
- Recommended value: `100`

## SUBSCRIPTION_KEEP_ALIVE_MILLIS

- Configures how often keep-alive messages to preserve connection are sent. If `0` than no keep-alive messages are sent.
- Recommended value: `10000`

<!-- Authorization -->

## ENABLE_API_KEY

- Enables API key authorization to execute our queries or subscriptions. Schema is still accessible.
- Supported values: `true`, `false`.
- Recommended value: `false`

## API_KEY

- Expected API key that clients must pass in the header/variable `X-TezGraph-Key`.

<!-- Logging -->

## LOG_CONSOLE_MIN_LEVEL

- Minimum level for logging to the console.
- Supported values: `off`, `critical`, `error`, `warning`, `information`, `debug`.
- Recommended value: `information`

## LOG_CONSOLE_FORMAT

- Format of logged entries to the console. It is not needed if `LOG_CONSOLE_MIN_LEVEL` is `off`.
- Supported values: `json`, `messages`.
- Recommended value: `messages`

## LOG_FILE_MIN_LEVEL

- Minimum level for logging to the file.
- Supported values: `off`, `critical`, `error`, `warning`, `information`, `debug`.
- Recommended value: `off`

## LOG_FILE_FORMAT

- Format of logged entries to the file. It is not needed if `LOG_FILE_MIN_LEVEL` is `off`.
- Supported values: `json`, `messages`.
- Recommended value: `json`

## LOG_FILE_PATH

- The path of actual file where entries are written. It is not needed if `LOG_FILE_MIN_LEVEL` is `off`.
- Recommended value: `logs/log.jsonl`

<!-- Diagnostics -->

## ENABLE_DEBUG

- Enables debugging features e.g. error stack traces from GraphQL, debug logs.
- Supported values: `true`, `false`.
- Recommended value: `false`
