import BrowserOnly from '@docusaurus/BrowserOnly';
import GraphQLConsole from './graphql-console';

export default function GraphiQLComponent({ query, variables }) {
    return (
        <BrowserOnly>
            {() => {
                return <GraphQLConsole query={query} variables={variables}></GraphQLConsole>;
            }}
        </BrowserOnly>
    );
}
