import { Field, Int, InterfaceType, ObjectType } from 'type-graphql';

import { removeRequiredPrefix } from '../../../../utils/string-manipulation';
import { scalars } from '../../../scalars';
import {
    AbstractLazyStorageDiff,
    getGraphQLDescription,
    LazyStorageDiffAction,
    LazyStorageDiffKind,
} from './lazy-storage-diff';

@InterfaceType(removeRequiredPrefix(AbstractLazySaplingStateDiff.name, 'Abstract'), {
    resolveType: (d: AbstractLazySaplingStateDiff) => d.graphQLTypeName,
})
abstract class AbstractLazySaplingStateDiff {
    @Field(() => LazyStorageDiffAction)
    abstract readonly action: LazyStorageDiffAction;

    readonly graphQLTypeName!: string;
}

@ObjectType()
export class SaplingCiphertext {
    @Field(() => String)
    readonly cv!: string;

    @Field(() => String)
    readonly epk!: string;

    @Field(() => String)
    readonly payload_enc!: string;

    @Field(() => String)
    readonly nonce_enc!: string;

    @Field(() => String)
    readonly payload_out!: string;

    @Field(() => String)
    readonly nonce_out!: string;
}

@ObjectType()
export class SaplingCommitmentAndCiphertext {
    @Field(() => String)
    readonly commitment!: string;

    @Field(() => SaplingCiphertext)
    readonly ciphertext!: SaplingCiphertext;
}

@ObjectType()
export class LazySaplingStateDiffUpdates {
    @Field(() => [SaplingCommitmentAndCiphertext])
    readonly commitments_and_ciphertexts!: readonly SaplingCommitmentAndCiphertext[];

    @Field(() => [String])
    readonly nullifiers!: readonly string[];
}

@ObjectType({
    implements: AbstractLazySaplingStateDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.sapling_state, LazyStorageDiffAction.alloc),
})
export class LazySaplingStateAlloc extends AbstractLazySaplingStateDiff {
    @Field(() => Int)
    readonly memo_size!: number;

    @Field(() => LazySaplingStateDiffUpdates)
    readonly updates!: LazySaplingStateDiffUpdates;

    readonly action = LazyStorageDiffAction.alloc;
}

@ObjectType({
    implements: AbstractLazySaplingStateDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.sapling_state, LazyStorageDiffAction.copy),
})
export class LazySaplingStateCopy extends AbstractLazySaplingStateDiff {
    @Field(() => scalars.BigNumber)
    readonly source!: bigint;

    @Field(() => LazySaplingStateDiffUpdates)
    readonly updates!: LazySaplingStateDiffUpdates;

    readonly action = LazyStorageDiffAction.copy;
}

@ObjectType({
    implements: AbstractLazySaplingStateDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.sapling_state, LazyStorageDiffAction.remove),
})
export class LazySaplingStateRemove extends AbstractLazySaplingStateDiff {
    readonly action = LazyStorageDiffAction.remove;
}

@ObjectType({
    implements: AbstractLazySaplingStateDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.sapling_state, LazyStorageDiffAction.update),
})
export class LazySaplingStateUpdate extends AbstractLazySaplingStateDiff {
    @Field(() => LazySaplingStateDiffUpdates)
    readonly updates!: LazySaplingStateDiffUpdates;

    readonly action = LazyStorageDiffAction.update;
}

export type LazySaplingStateDiff =
    | LazySaplingStateAlloc
    | LazySaplingStateCopy
    | LazySaplingStateRemove
    | LazySaplingStateUpdate;
export const LazySaplingStateDiff = AbstractLazySaplingStateDiff; // eslint-disable-line @typescript-eslint/no-redeclare

@ObjectType({
    implements: AbstractLazyStorageDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.sapling_state),
})
export class LazySaplingStateStorageDiff extends AbstractLazyStorageDiff {
    @Field(() => LazySaplingStateDiff)
    readonly sapling_state_diff!: LazySaplingStateDiff;

    readonly kind = LazyStorageDiffKind.sapling_state;
}
