import { Field, InterfaceType, ObjectType } from 'type-graphql';

import { removeRequiredPrefix } from '../../../../utils/string-manipulation';
import { Micheline, scalars } from '../../../scalars';
import {
    AbstractLazyStorageDiff,
    getGraphQLDescription,
    LazyStorageDiffAction,
    LazyStorageDiffKind,
} from './lazy-storage-diff';

@ObjectType()
export class LazyBigMapUpdateItem {
    @Field(() => String)
    readonly key_hash!: string;

    @Field(() => scalars.Micheline)
    readonly key!: Micheline;

    @Field(() => scalars.Micheline, { nullable: true })
    readonly value!: Micheline | null;
}

@InterfaceType(removeRequiredPrefix(AbstractLazyBigMapDiff.name, 'Abstract'), {
    description: getGraphQLDescription(LazyStorageDiffKind.big_map, 'diff'),
    resolveType: (d: AbstractLazyBigMapDiff) => d.graphQLTypeName,
})
abstract class AbstractLazyBigMapDiff {
    @Field(() => LazyStorageDiffAction)
    abstract readonly action: LazyStorageDiffAction;

    readonly graphQLTypeName!: string;
}

@ObjectType({ isAbstract: true })
abstract class AbstractLazyBigMapDiffWithUpdates extends AbstractLazyBigMapDiff {
    @Field(() => [LazyBigMapUpdateItem])
    readonly updates!: readonly LazyBigMapUpdateItem[];
}

@ObjectType({
    implements: AbstractLazyBigMapDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.big_map, LazyStorageDiffAction.alloc),
})
export class LazyBigMapAlloc extends AbstractLazyBigMapDiffWithUpdates {
    @Field(() => scalars.Micheline)
    readonly key_type!: Micheline;

    @Field(() => scalars.Micheline)
    readonly value_type!: Micheline;

    readonly action = LazyStorageDiffAction.alloc;
}

@ObjectType({
    implements: AbstractLazyBigMapDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.big_map, LazyStorageDiffAction.copy),
})
export class LazyBigMapCopy extends AbstractLazyBigMapDiffWithUpdates {
    @Field(() => scalars.BigNumber)
    readonly source!: bigint;

    readonly action = LazyStorageDiffAction.copy;
}

@ObjectType({
    implements: AbstractLazyBigMapDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.big_map, LazyStorageDiffAction.remove),
})
export class LazyBigMapRemove extends AbstractLazyBigMapDiff {
    readonly action = LazyStorageDiffAction.remove;

    readonly updates?: undefined; // Unifies API when accessed via unions.
}

@ObjectType({
    implements: AbstractLazyBigMapDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.big_map, LazyStorageDiffAction.update),
})
export class LazyBigMapUpdate extends AbstractLazyBigMapDiffWithUpdates {
    readonly action = LazyStorageDiffAction.update;
}

export type LazyBigMapDiff = LazyBigMapAlloc | LazyBigMapCopy | LazyBigMapRemove | LazyBigMapUpdate;
export const LazyBigMapDiff = AbstractLazyBigMapDiff; // eslint-disable-line @typescript-eslint/no-redeclare

@ObjectType({
    implements: AbstractLazyStorageDiff,
    description: getGraphQLDescription(LazyStorageDiffKind.big_map),
})
export class LazyBigMapStorageDiff extends AbstractLazyStorageDiff {
    @Field(() => LazyBigMapDiff)
    readonly big_map_diff!: LazyBigMapDiff;

    readonly kind = LazyStorageDiffKind.big_map;
}
