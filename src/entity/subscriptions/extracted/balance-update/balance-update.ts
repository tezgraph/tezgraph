import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../../utils/reflection';
import { scalars } from '../../../scalars';
import { graphQLDescriptions } from '../../graphql-descriptions';

export enum BalanceUpdateKind {
    accumulator = 'accumulator',
    burned = 'burned',
    commitment = 'commitment',
    contract = 'contract',
    freezer = 'freezer',
    minted = 'minted',
}

registerEnumType(BalanceUpdateKind, {
    name: getEnumTypeName({ BalanceUpdateKind }),
    description: 'The kind of a balance update.',
});

export enum BalanceUpdateOrigin {
    block = 'block',
    migration = 'migration',
    simulation = 'simulation',
    subsidy = 'subsidy',
}

registerEnumType(BalanceUpdateOrigin, {
    name: getEnumTypeName({ BalanceUpdateOrigin }),
    description: 'The origin of a balance update.',
});

export enum BalanceUpdateCategory {
    baking_rewards = 'baking rewards',
    rewards = 'rewards',
    fees = 'fees',
    deposits = 'deposits',
    legacy_rewards = 'legacy_rewards',
    legacy_fees = 'legacy_fees',
    legacy_deposits = 'legacy_deposits',
    block_fees = 'block fees',
    nonce_revelation_rewards = 'nonce revelation rewards',
    double_signing_evidence_rewards = 'double signing evidence rewards',
    endorsing_rewards = 'endorsing rewards',
    baking_bonuses = 'baking bonuses',
    storage_fees = 'storage fees',
    punishments = 'punishments',
    lost_endorsing_rewards = 'lost endorsing rewards',
    subsidy = 'subsidy',
    burned = 'burned',
    commitment = 'commitment',
    bootstrap = 'bootstrap',
    invoice = 'invoice',
    minted = 'minted',
}

registerEnumType(BalanceUpdateCategory, {
    name: getEnumTypeName({ BalanceUpdateCategory }),
    description: 'The category of a balance update.',
});

@ObjectType({ description: graphQLDescriptions.getRpc(['$operation_metadata.alpha.balance_updates']) })
export class BalanceUpdate {
    @Field(() => BalanceUpdateKind)
    readonly kind!: BalanceUpdateKind;

    @Field(() => scalars.BigNumber)
    readonly change!: bigint;

    @Field(() => BalanceUpdateOrigin)
    readonly origin!: BalanceUpdateOrigin;

    @Field(() => scalars.Address, { nullable: true })
    readonly contract!: string | null;

    @Field(() => BalanceUpdateCategory, { nullable: true })
    readonly category!: BalanceUpdateCategory | null;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate!: string | null;

    @Field(() => Int, { nullable: true })
    readonly cycle!: number | null;
}
