import { InterfaceType } from 'type-graphql';

import { AbstractOperationMetadataWithBalanceUpdates } from '../../operations/operation-metadata';
import { OperationNotification } from '../../operations/operation-notification';

@InterfaceType({
    implements: OperationNotification,
    description: `Corresponds to an \`${OperationNotification.name}\` that can contain balance updates.`,
    resolveType: (o: OperationNotificationWithBalanceUpdates) => o.graphQLTypeName,
})
export abstract class OperationNotificationWithBalanceUpdates extends OperationNotification {
    readonly metadata!: AbstractOperationMetadataWithBalanceUpdates | null;
}
