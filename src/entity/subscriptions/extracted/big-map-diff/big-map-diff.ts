import { Field, InterfaceType, ObjectType, registerEnumType } from 'type-graphql';

import { getEnumTypeName, nameof } from '../../../../utils/reflection';
import { removeRequiredPrefix } from '../../../../utils/string-manipulation';
import { Micheline, scalars } from '../../../scalars';
import { graphQLDescriptions } from '../../graphql-descriptions';

export enum BigMapDiffAction {
    alloc = 'alloc',
    copy = 'copy',
    remove = 'remove',
    update = 'update',
}

registerEnumType(BigMapDiffAction, {
    name: getEnumTypeName({ BigMapDiffAction }),
    description: 'The action executed on the particular big map.',
});

@InterfaceType(removeRequiredPrefix(AbstractBigMapDiff.name, 'Abstract'), {
    description: getGraphQLDescription(),
    resolveType: (d: AbstractBigMapDiff) => d.graphQLTypeName,
})
abstract class AbstractBigMapDiff {
    @Field(() => BigMapDiffAction, {
        description: graphQLDescriptions.getKindProperty({
            parentName: 'BigMapDiff',
            kindPropertyName: nameof<AbstractBigMapDiff>('action'),
            exampleChild: { kind: BigMapDiffAction.update, typeName: 'BigMapUpdate' },
        }),
    })
    abstract readonly action: BigMapDiffAction;

    readonly graphQLTypeName!: string;
}

@ObjectType({ isAbstract: true })
abstract class AbstractBigMapDiffWithBigMap extends AbstractBigMapDiff {
    @Field(() => scalars.BigNumber)
    readonly big_map!: bigint;
}

@ObjectType({
    implements: AbstractBigMapDiff,
    description: getGraphQLDescription(BigMapDiffAction.alloc),
})
export class BigMapAlloc extends AbstractBigMapDiffWithBigMap {
    @Field(() => scalars.Micheline)
    readonly key_type!: Micheline;

    @Field(() => scalars.Micheline)
    readonly value_type!: Micheline;

    readonly action!: BigMapDiffAction.alloc;

    readonly key?: undefined; // Unifies API when accessed via unions.
}

@ObjectType({
    implements: AbstractBigMapDiff,
    description: getGraphQLDescription(BigMapDiffAction.copy),
})
export class BigMapCopy extends AbstractBigMapDiff {
    @Field(() => scalars.BigNumber)
    readonly source_big_map!: bigint;

    @Field(() => scalars.BigNumber)
    readonly destination_big_map!: bigint;

    readonly action!: BigMapDiffAction.copy;

    readonly key?: undefined; // Unifies API when accessed via unions.
}

@ObjectType({
    implements: AbstractBigMapDiff,
    description: getGraphQLDescription(BigMapDiffAction.remove),
})
export class BigMapRemove extends AbstractBigMapDiffWithBigMap {
    readonly action!: BigMapDiffAction.remove;

    readonly key?: undefined; // Unifies API when accessed via unions.
}

@ObjectType({
    implements: AbstractBigMapDiff,
    description: getGraphQLDescription(BigMapDiffAction.update),
})
export class BigMapUpdate extends AbstractBigMapDiffWithBigMap {
    @Field(() => String)
    readonly key_hash!: string;

    @Field(() => scalars.Micheline)
    readonly key!: Micheline;

    @Field(() => scalars.Micheline, { nullable: true })
    readonly value!: Micheline | null;

    readonly action!: BigMapDiffAction.update;
}

export type BigMapDiff = BigMapAlloc | BigMapCopy | BigMapRemove | BigMapUpdate;
export const BigMapDiff = AbstractBigMapDiff; // eslint-disable-line @typescript-eslint/no-redeclare

function getGraphQLDescription(action?: BigMapDiffAction): string {
    return graphQLDescriptions.getRpc(['$contract.big_map_diff', ...(action ? [`[@action = ${action}]`] : [])]);
}
