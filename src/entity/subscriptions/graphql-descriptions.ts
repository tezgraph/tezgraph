import { asReadonly } from '../../utils/conversion';
import { OperationKind } from './operations/operation-kind';
import { InternalOperationKind } from './operations/operation-result';

interface RpcDescriptionOptions {
    capitalize?: boolean;
    trailingPeriod?: boolean;
}

interface FilterDescriptionOptions {
    nullable?: boolean;
}

interface KindDescriptionOptions {
    parentName: string;
    kindPropertyName?: string;
    exampleChild: {
        kind: string;
        typeName: string;
    };
}

export const graphQLDescriptions = asReadonly({
    nullIfMempool: 'It is `null` only if the operation comes from *mempool*.',

    rpcOperation: '$operation.alpha.operation_contents_and_result',

    getRpc(rpcTypePath: string[], options?: RpcDescriptionOptions): string {
        const formattedPath = rpcTypePath.map(quote).join(' -> ');
        const prefix = options?.capitalize === false ? 'c' : 'C';
        const suffix = options?.trailingPeriod === false ? '' : '.';

        return `${prefix}orresponds to Tezos RPC -> ${formattedPath}${suffix}`;
    },

    getRpcOperation(kind: OperationKind, rpcTypePath: string[] = [], options?: RpcDescriptionOptions): string {
        const fullRpcTypePath = [this.rpcOperation, `[@kind = ${kind}]`, ...rpcTypePath];
        return this.getRpc(fullRpcTypePath, options);
    },

    getRpcOperationMetadata(kind: OperationKind): string {
        return this.getRpcOperation(kind, ['metadata']);
    },

    getRpcOperationResult(kind: OperationKind): string {
        return this.getRpc([`$operation.alpha.operation_result.${kind}`]);
    },

    getRpcInternalOperationResult(kind?: InternalOperationKind): string {
        return this.getRpc(['$operation.alpha.internal_operation_result', ...(kind ? [`[@kind = ${kind}]`] : [])]);
    },

    getKindProperty(options: KindDescriptionOptions): string {
        return (
            `The ${quote(options.kindPropertyName ?? 'kind')} of the ${quote(
                options.parentName,
            )}. It is constant for a concrete *type*` +
            ` e.g. it is always \`${options.exampleChild.kind}\` for a \`${options.exampleChild.typeName}\`.`
        );
    },

    getFilterProperty(comparison: string, options?: { propertyPlural?: boolean; filterPlural?: boolean }): string {
        const propertyValue = options?.propertyPlural === true ? 'values' : 'value';
        const filterValue = options?.filterPlural === true ? 'these filter values' : 'this filter value';
        return `Matches if the property ${propertyValue} ${comparison} ${filterValue}.`;
    },

    getEqualityFilter(scalar: string, options?: FilterDescriptionOptions): string {
        return getFilter(scalar, 'checking equality', options);
    },

    getComparableFilter(scalar: string, options?: FilterDescriptionOptions): string {
        return getFilter(scalar, 'relational comparison', options);
    },

    getArrayFilter(scalar: string, options?: FilterDescriptionOptions): string {
        return getFilter(scalar, 'checking the filter value against array', options, 'values');
    },
});

function getFilter(
    scalar: string,
    comparison: string,
    options: FilterDescriptionOptions | undefined,
    propertyValue = 'value',
): string {
    const nullablePrefix = options?.nullable === true ? 'nullable ' : '';
    return `Provides filters for ${comparison} of the property ${propertyValue} of ${nullablePrefix}\`${scalar}\` type.`;
}

function quote(code: string): string {
    return `\`${code}\``;
}
