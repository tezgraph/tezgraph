import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../../../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../../../common/operation';
import { Reveal, RevealMetadata, RevealResult } from '../../../common/reveal';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { MoneyOperationNotification } from '../money-operation-notification';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdatesAndInternalResults } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';
import { InternalOperationResult, InternalOperationKind } from '../operation-result';

@ObjectType({
    implements: [OperationResult, RevealResult],
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.reveal),
})
export class RevealNotificationResult extends OperationResult implements RevealResult {}

@ObjectType({
    implements: InternalOperationResult,
    description: graphQLDescriptions.getRpcInternalOperationResult(InternalOperationKind.reveal),
})
export class InternalRevealResult extends InternalOperationResult {
    @Field(() => InternalOperationKind)
    readonly kind!: InternalOperationKind.reveal;

    @Field(() => scalars.PublicKey)
    readonly public_key!: string;

    @Field(() => RevealNotificationResult)
    readonly result!: RevealNotificationResult;
}

@ObjectType({
    implements: [OperationMetadata, RevealMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.reveal),
})
export class RevealNotificationMetadata
    extends AbstractOperationMetadataWithBalanceUpdatesAndInternalResults
    implements RevealMetadata
{
    @Field(() => RevealNotificationResult)
    readonly operation_result!: RevealNotificationResult;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    implements: [
        Operation,
        OperationNotification,
        OperationNotificationWithBalanceUpdates,
        MoneyOperation,
        MoneyOperationNotification,
        Reveal,
    ],
    description: graphQLDescriptions.getRpcOperation(OperationKind.reveal),
})
export class RevealNotification
    extends MoneyOperationNotification
    implements OperationNotificationWithBalanceUpdates, Reveal
{
    @Field(() => scalars.PublicKey)
    readonly public_key!: string;

    @Field(() => RevealNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: RevealNotificationMetadata | null;

    readonly kind = OperationKind.reveal;
}
