import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, Filter, NullableOperationResultStatusFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { RevealNotification } from './reveal-notification';

@InputType({ isAbstract: true })
export class RevealSpecificFilter implements Filter<RevealNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    passes(operation: RevealNotification): boolean {
        return (
            (!this.source || this.source.passes(operation.source)) &&
            (!this.status || this.status.passes(operation.metadata?.operation_result.status))
        );
    }
}

export const RevealFilter = createOperationFilterClass(RevealSpecificFilter);
export const RevealArgs = createOperationArgsClass(RevealFilter);
