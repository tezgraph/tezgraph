import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, Filter, NullableOperationResultStatusFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { RegisterGlobalConstantNotification } from './register-global-constant-notification';

@InputType({ isAbstract: true })
export class RegisterGlobalConstantSpecificFilter implements Filter<RegisterGlobalConstantNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    passes(operation: RegisterGlobalConstantNotification): boolean {
        return (
            (!this.source || this.source.passes(operation.source)) &&
            (!this.status || this.status.passes(operation.metadata?.operation_result.status))
        );
    }
}

export const RegisterGlobalConstantFilter = createOperationFilterClass(RegisterGlobalConstantSpecificFilter);
export const RegisterGlobalConstantArgs = createOperationArgsClass(RegisterGlobalConstantFilter);
