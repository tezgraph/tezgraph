import { Field, Int, ObjectType } from 'type-graphql';

import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdates } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';

@ObjectType({ description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.preendorsement) })
export class PreendorsementNotificationMetadata extends AbstractOperationMetadataWithBalanceUpdates {
    @Field(() => scalars.Address)
    readonly delegate!: string;

    @Field(() => Int)
    readonly preendorsement_power!: number;
}

@ObjectType({
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
    description: graphQLDescriptions.getRpcOperation(OperationKind.preendorsement),
})
export class PreendorsementNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => Int)
    readonly slot!: number;

    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int)
    readonly round!: number;

    @Field(() => scalars.PayloadHash)
    readonly block_payload_hash!: string;

    @Field(() => PreendorsementNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: PreendorsementNotificationMetadata | null;

    readonly kind = OperationKind.preendorsement;
}
