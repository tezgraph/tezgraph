import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../../../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../../../common/operation';
import { Origination, OriginationMetadata, OriginationResult } from '../../../common/origination';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { MoneyOperationNotification, MoneyOperationResult } from '../money-operation-notification';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdatesAndInternalResults } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';
import { InternalOperationKind, InternalOperationResult } from '../operation-result';
import { ScriptedContracts } from './scripted-contracts';

@ObjectType({
    implements: [OperationResult, OriginationResult],
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.origination),
})
export class OriginationNotificationResult extends MoneyOperationResult implements OriginationResult {}

@ObjectType({
    implements: [InternalOperationResult],
    description: graphQLDescriptions.getRpcInternalOperationResult(InternalOperationKind.origination),
})
export class InternalOriginationResult extends InternalOperationResult {
    @Field(() => InternalOperationKind)
    readonly kind!: InternalOperationKind.origination;

    @Field(() => scalars.Mutez)
    readonly balance!: bigint;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate!: string | null;

    @Field(() => ScriptedContracts)
    readonly script!: ScriptedContracts;

    @Field(() => OriginationNotificationResult)
    readonly result!: OriginationNotificationResult;

    readonly destination?: undefined; // Unifies API when accessed via unions.
}

@ObjectType({
    implements: [OperationMetadata, OriginationMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.delegation),
})
export class OriginationNotificationMetadata
    extends AbstractOperationMetadataWithBalanceUpdatesAndInternalResults
    implements OriginationMetadata
{
    @Field(() => OriginationNotificationResult)
    readonly operation_result!: OriginationNotificationResult;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    implements: [
        Operation,
        OperationNotification,
        OperationNotificationWithBalanceUpdates,
        MoneyOperation,
        MoneyOperationNotification,
        Origination,
    ],
    description: graphQLDescriptions.getRpcOperation(OperationKind.origination),
})
export class OriginationNotification
    extends MoneyOperationNotification
    implements OperationNotificationWithBalanceUpdates, Origination
{
    @Field(() => scalars.Mutez)
    readonly balance!: bigint;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate!: string | null;

    @Field(() => ScriptedContracts)
    readonly script!: ScriptedContracts;

    @Field(() => OriginationNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: OriginationNotificationMetadata | null;

    readonly kind = OperationKind.origination;
    readonly destination?: undefined; // Unifies API when accessed via unions.
}
