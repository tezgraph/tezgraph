import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { Filter, NullableAddressArrayFilter } from '../../common-filters';
import { getDelegates } from '../double-baking-evidence/double-baking-evidence-args';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { DoubleEndorsementEvidenceNotification } from './endorsement-notification';

@InputType({ isAbstract: true })
export class DoubleEndorsementEvidenceSpecificFilter implements Filter<DoubleEndorsementEvidenceNotification> {
    @Field(() => NullableAddressArrayFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressArrayFilter>;

    passes(operation: DoubleEndorsementEvidenceNotification): boolean {
        return !this.delegate || this.delegate.passes(getDelegates(operation));
    }
}

export const DoubleEndorsementEvidenceFilter = createOperationFilterClass(DoubleEndorsementEvidenceSpecificFilter);
export const DoubleEndorsementEvidenceArgs = createOperationArgsClass(DoubleEndorsementEvidenceFilter);
