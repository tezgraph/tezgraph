import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../../utils/reflection';
import { Endorsement, EndorsementMetadata } from '../../../common/endorsement';
import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdates, SimpleOperationMetadata } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';

export enum InlinedEndorsementKind {
    endorsement = 'endorsement',
}

registerEnumType(InlinedEndorsementKind, {
    name: getEnumTypeName({ InlinedEndorsementKind }),
    description: 'Inlined endorsement kind.',
});

@ObjectType({ description: graphQLDescriptions.getRpc(['$inlined.endorsement.contents']) })
export class InlinedEndorsementContents {
    @Field(() => InlinedEndorsementKind)
    readonly kind!: InlinedEndorsementKind;

    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int, { nullable: true })
    readonly slot!: number | null;

    @Field(() => Int, { nullable: true })
    readonly round!: number | null;

    @Field(() => scalars.PayloadHash, { nullable: true })
    readonly block_payload_hash!: string | null;
}

@ObjectType({ description: graphQLDescriptions.getRpc(['$inlined.endorsement']) })
export class InlinedEndorsement {
    @Field(() => scalars.BlockHash)
    readonly branch!: string;

    @Field(() => InlinedEndorsementContents)
    readonly operations!: InlinedEndorsementContents;

    @Field(() => scalars.Signature, { nullable: true })
    readonly signature!: string | null;
}

@ObjectType({
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
    description: graphQLDescriptions.getRpcOperation(OperationKind.double_endorsement_evidence),
})
export class DoubleEndorsementEvidenceNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => InlinedEndorsement)
    readonly op1!: InlinedEndorsement;

    @Field(() => InlinedEndorsement)
    readonly op2!: InlinedEndorsement;

    @Field(() => Int)
    readonly slot!: number;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | null;

    readonly kind = OperationKind.double_endorsement_evidence;
}

@ObjectType({
    implements: [EndorsementMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.endorsement),
})
export class EndorsementNotificationMetadata
    extends AbstractOperationMetadataWithBalanceUpdates
    implements EndorsementMetadata
{
    @Field(() => scalars.Address)
    readonly delegate!: string;

    @Field(() => [Int], { nullable: true })
    readonly slots!: readonly number[] | null;

    @Field(() => Int, { nullable: true })
    readonly endorsement_power!: number | null;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    isAbstract: true,
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
})
export abstract class AbstractEndorsementNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => EndorsementNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: EndorsementNotificationMetadata | null;
}

@ObjectType({
    implements: [Endorsement],
    description: graphQLDescriptions.getRpcOperation(OperationKind.endorsement),
})
export class EndorsementNotification extends AbstractEndorsementNotification {
    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int, { nullable: true })
    readonly slot!: number | null;

    @Field(() => Int, { nullable: true })
    readonly round!: number | null;

    @Field(() => scalars.PayloadHash, { nullable: true })
    readonly block_payload_hash!: string | null;

    @Field(() => EndorsementNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: EndorsementNotificationMetadata | null;

    readonly kind = OperationKind.endorsement;
}

@ObjectType({
    description: graphQLDescriptions.getRpcOperation(OperationKind.endorsement_with_slot),
})
export class EndorsementWithSlotNotification extends AbstractEndorsementNotification {
    @Field(() => InlinedEndorsement)
    readonly endorsement!: InlinedEndorsement;

    @Field(() => Int)
    readonly slot!: number;

    @Field(() => EndorsementNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: EndorsementNotificationMetadata | null;

    readonly kind = OperationKind.endorsement_with_slot;
}
