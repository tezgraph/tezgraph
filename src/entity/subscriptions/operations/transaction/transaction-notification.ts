import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../../../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../../../common/operation';
import {
    Transaction,
    TransactionMetadata,
    TransactionParameters,
    TransactionResult,
} from '../../../common/transaction';
import { Micheline, scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { MoneyOperationNotification, MoneyOperationResult } from '../money-operation-notification';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdatesAndInternalResults } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';
import { InternalOperationKind, InternalOperationResult } from '../operation-result';

@ObjectType({
    implements: [OperationResult, TransactionResult],
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.transaction),
})
export class TransactionNotificationResult extends MoneyOperationResult implements TransactionResult {
    @Field(() => scalars.Micheline, { nullable: true })
    readonly storage!: Micheline | null;

    @Field(() => Boolean, { nullable: true })
    readonly allocated_destination_contract!: boolean | null;
}

@ObjectType({
    implements: [InternalOperationResult],
    description: graphQLDescriptions.getRpcInternalOperationResult(InternalOperationKind.transaction),
})
export class InternalTransactionResult extends InternalOperationResult {
    @Field(() => InternalOperationKind)
    readonly kind!: InternalOperationKind.transaction;

    @Field(() => scalars.Mutez)
    readonly amount!: bigint;

    @Field(() => scalars.Address)
    readonly destination!: string;

    @Field(() => TransactionParameters, { nullable: true })
    readonly parameters!: TransactionParameters | null;

    @Field(() => TransactionNotificationResult)
    readonly result!: TransactionNotificationResult;
}

@ObjectType({
    implements: [OperationMetadata, TransactionMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.transaction),
})
export class TransactionNotificationMetadata
    extends AbstractOperationMetadataWithBalanceUpdatesAndInternalResults
    implements TransactionMetadata
{
    @Field(() => TransactionNotificationResult)
    readonly operation_result!: TransactionNotificationResult;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    implements: [
        Operation,
        OperationNotification,
        OperationNotificationWithBalanceUpdates,
        MoneyOperation,
        MoneyOperationNotification,
        Transaction,
    ],
    description: graphQLDescriptions.getRpcOperation(OperationKind.transaction),
})
export class TransactionNotification
    extends MoneyOperationNotification
    implements OperationNotificationWithBalanceUpdates, Transaction
{
    @Field(() => scalars.Mutez)
    readonly amount!: bigint;

    @Field(() => scalars.Address)
    readonly destination!: string;

    @Field(() => TransactionParameters, { nullable: true })
    readonly parameters!: TransactionParameters | null;

    @Field(() => TransactionNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: TransactionNotificationMetadata | null;

    readonly kind = OperationKind.transaction;
}
