import { createUnionType, Field, ObjectType } from 'type-graphql';

import { MoneyOperationNotification } from './money-operation-notification';
import { InternalOriginationResult, OriginationNotification } from './origination/origination-notification';
import { InternalTransactionResult, TransactionNotification } from './transaction/transaction-notification';

const ContractResultParent = createUnionType({
    name: 'ContractResultParent',
    description:
        'Parent entity that contains the change - either directly in its `metadata.result` or internally in `internal_operation_results`.',
    types: () =>
        [
            OriginationNotification,
            InternalOriginationResult,
            TransactionNotification,
            InternalTransactionResult,
        ] as const,
    resolveType: (value) => value.graphQLTypeName,
});
// eslint-disable-next-line @typescript-eslint/no-redeclare
export type ContractResultParent = typeof ContractResultParent;

@ObjectType({ isAbstract: true })
export abstract class ContractRelatedNotification {
    @Field(() => MoneyOperationNotification, {
        description: 'Associated operation that contains this change.',
    })
    readonly operation!: MoneyOperationNotification;

    @Field(() => ContractResultParent, {
        description:
            'Parent entity that contains this change.' +
            ' It is either an operation if the change was done directly in its result.' +
            ' Or it is an internal result if the change was done internally.',
    })
    readonly parent!: ContractResultParent;
}
