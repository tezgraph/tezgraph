import { isWhiteSpace } from '../../../utils/string-manipulation';
import { OperationKind } from './operation-kind';
import { OperationNotification } from './operation-notification-union';

export const numberOfLastMempoolGroupsToDeduplicate = 3_000;

export type MempoolOperationGroup = readonly OperationNotification[];

export function getSignature(group: MempoolOperationGroup): string {
    const operation = group[0];
    if (!operation) {
        throw new Error('Unexpectedly, there are no operations in the mempool operation group.');
    }

    // EndorsementWithSlot uses a dummy signature -> wouldn't deduplicate the group -> fallback to inner signature.
    const signature =
        operation.kind === OperationKind.endorsement_with_slot
            ? operation.endorsement.signature
            : operation.operation_group.signature;

    if (isWhiteSpace(signature)) {
        throw new Error('Unexpectedly, there is no signature on the first operation in the mempool operation group.');
    }
    return signature;
}
