import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, Filter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { ActivateAccountNotification } from './activate-account-notification';

@InputType({ isAbstract: true })
export class ActivateAccountSpecificFilter implements Filter<ActivateAccountNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly pkh: Nullish<AddressFilter>;

    passes(operation: ActivateAccountNotification): boolean {
        return !this.pkh || this.pkh.passes(operation.pkh);
    }
}

export const ActivateAccountFilter = createOperationFilterClass(ActivateAccountSpecificFilter);
export const ActivateAccountArgs = createOperationArgsClass(ActivateAccountFilter);
