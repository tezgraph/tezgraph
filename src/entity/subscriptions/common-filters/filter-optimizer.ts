import { singleton } from 'tsyringe';

import { isNullish } from '../../../utils/reflection';
import { Filter } from './filter';

/** Tree-shakes filter properties which are empty. */
@singleton()
export class FilterOptimizer {
    optimize<TValue, TFilter extends Filter<TValue>>(filter: TFilter): TFilter | null {
        const optimizedEntries = Object.entries(filter).map(([key, value]) => {
            const optimizedValue = isFilter(value) ? this.optimize(value) : (value as unknown);
            return [key, optimizedValue];
        });

        const isEmpty = optimizedEntries.every(([_k, v]) => isNullish(v) || typeof v === 'function');
        if (isEmpty) {
            return null;
        }

        const optimizedFilter = new (<any>filter.constructor)(); // eslint-disable-line
        return Object.assign(optimizedFilter, Object.fromEntries(optimizedEntries)) as TFilter;
    }
}

function isFilter(obj: unknown): obj is Filter<unknown> {
    return typeof obj === 'object' && obj !== null && 'passes' in obj;
}
