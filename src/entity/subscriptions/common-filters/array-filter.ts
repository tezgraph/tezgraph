import { Field, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { DefaultConstructor, isNullish, NonNullishValue, Nullish } from '../../../utils/reflection';
import { graphQLDescriptions } from '../graphql-descriptions';
import { Filter } from './filter';

export interface ArrayFilter<TItemScalar, TProperty = readonly TItemScalar[]> extends Filter<TProperty> {
    includes: Nullish<TItemScalar>;
    notIncludes: Nullish<TItemScalar>;
}

export function createArrayFilterClass<TItemScalar extends NonNullishValue>(
    itemScalarType: ReturnTypeFuncValue,
): DefaultConstructor<ArrayFilter<TItemScalar>> {
    return createArrayFilterClassForProperty(itemScalarType, (propertyValue, filterValue) =>
        propertyValue.includes(filterValue),
    );
}

export type IncludesFunction<TProperty, TFilterScalar> = (
    propertyValue: TProperty,
    filterValue: TFilterScalar,
) => boolean;

export function createArrayFilterClassForProperty<TItemScalar extends NonNullishValue, TProperty>(
    itemScalarType: ReturnTypeFuncValue,
    includes: IncludesFunction<TProperty, TItemScalar>,
): DefaultConstructor<ArrayFilter<TItemScalar, TProperty>> {
    @InputType({
        isAbstract: true,
        description: 'Filters for checking equality of any of the property values.',
    })
    class ArrayFilterClass implements Filter<TProperty> {
        @Field(() => itemScalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('include', { propertyPlural: true }),
        })
        includes: Nullish<TItemScalar>;

        @Field(() => itemScalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('do NOT include', { propertyPlural: true }),
        })
        notIncludes: Nullish<TItemScalar>;

        passes(propertyValue: TProperty): boolean {
            // Using isNullish() instead of ! operator b/c filters can be empty string/zero/false.
            return (
                (isNullish(this.includes) || includes(propertyValue, this.includes)) &&
                (isNullish(this.notIncludes) || !includes(propertyValue, this.notIncludes))
            );
        }
    }
    return ArrayFilterClass;
}
