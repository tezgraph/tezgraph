export { BlockArgs } from './block-args';

export { BlockHeader, BlockNotification, numberOfLastBlocksToDeduplicate } from './block-notification';
