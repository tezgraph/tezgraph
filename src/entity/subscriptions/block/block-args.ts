import { ArgsType } from 'type-graphql';

import { Filter } from '../common-filters';
import { AbstractSubscriptionArgs, FilteringArgs } from '../subscription-args';
import { BlockNotification } from './block-notification';

@ArgsType()
export class BlockArgs extends AbstractSubscriptionArgs implements FilteringArgs<Filter<BlockNotification>> {
    filter: undefined;
}
