import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from '../../scalars';
import { OperationNotification } from '../operations/operation-notification-union';

export const numberOfLastBlocksToDeduplicate = 100;

@ObjectType()
export class BlockHeader {
    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int)
    readonly proto!: number;

    @Field(() => scalars.BlockHash)
    readonly predecessor!: string;

    @Field(() => Date)
    readonly timestamp!: Date;

    @Field(() => scalars.Signature)
    readonly signature!: string;

    @Field(() => Int)
    readonly validation_pass!: number;

    @Field(() => scalars.OperationsHash)
    readonly operations_hash!: string;

    @Field(() => [String])
    readonly fitness!: readonly string[];

    @Field(() => scalars.ContextHash)
    readonly context!: string;

    @Field(() => Int, { nullable: true })
    readonly priority!: number | null;

    @Field(() => String)
    readonly proof_of_work_nonce!: string;

    @Field(() => scalars.NonceHash, { nullable: true })
    readonly seed_nonce_hash!: string | null;

    @Field(() => Boolean)
    readonly liquidity_baking_escape_vote!: boolean;

    @Field(() => scalars.PayloadHash, { nullable: true })
    readonly payload_hash!: string | null;

    @Field(() => Int, { nullable: true })
    readonly payload_round!: number | null;
}

@ObjectType()
export class BlockNotification {
    @Field(() => scalars.BlockHash)
    readonly hash!: string;

    @Field(() => BlockHeader)
    readonly header!: BlockHeader;

    @Field(() => scalars.ProtocolHash)
    readonly protocol!: string;

    @Field(() => scalars.ChainId)
    readonly chain_id!: string;

    // Internal property for calculating metrics.
    readonly received_from_tezos_on!: Date;

    // Operations are exposed per kind in a corresponding resolver.
    readonly operations!: readonly OperationNotification[];
}
