import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { RelayConnection, RelayEdge } from '../relay';
import { Micheline } from '../scalars';
import { BigmapOperationKind } from './bigmap-operation-kind';
import { AccountIdAndAddress } from './bigmap-record';

@ObjectType()
export class BigmapValue {
    @Field(() => String, { description: 'The key hash of this bigmap value' })
    key_hash!: string;

    @Field(() => BigmapOperationKind, { description: 'The bigmap_operation_kind that created this entry' })
    kind!: BigmapOperationKind;

    key!: Prisma.JsonValue;
    value?: Micheline | null;
    block_level!: number;
    address_addressTobigmap_receiver_id?: AccountIdAndAddress;
    address_addressTobigmap_sender_id?: AccountIdAndAddress;
    block_hash?: string;
    i!: bigint;
    id!: bigint;
    operation_id!: bigint;
}

@ObjectType()
export class BigmapValueRecord extends BigmapValue {}

export interface BigmapValueRecordData extends BigmapValueRecord {
    cursor: string;
}

@ObjectType()
export class BigmapValueRecordEdge extends RelayEdge(BigmapValueRecord) {}

@ObjectType()
export class BigmapValueRecordConnection extends RelayConnection(BigmapValueRecordEdge) {}
