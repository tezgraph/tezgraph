import { registerEnumType } from 'type-graphql';

export enum BigmapOperationKind {
    alloc = 0,
    update = 1,
    clear = 2,
    copy = 3,
}

registerEnumType(BigmapOperationKind, {
    name: 'BigmapOperationKind',
    description: 'The kind of bigmap change.',
});
