import { Field, ObjectType } from 'type-graphql';

import { Micheline, scalars } from '../scalars';

@ObjectType({
    description: `The code and the initial storage of a contract.`,
})
export class ContractScript {
    @Field(() => [scalars.Micheline], {
        description: 'The code of the contract.',
    })
    readonly code!: Micheline;

    @Field(() => scalars.Micheline)
    readonly storage!: Micheline;

    @Field(() => scalars.Micheline)
    readonly storage_micheline_json!: string | null;

    @Field(() => scalars.Micheline)
    readonly storage_michelson!: string | null;
}
