/* istanbul ignore file */

import { Prisma } from '.prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../common/operation';
import { Origination, OriginationMetadata, OriginationResult } from '../common/origination';
import { scalars } from '../scalars';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperationRecord } from './money-operation-record';
import { OperationRecord } from './operation-record';

@ObjectType({ implements: [OperationResult, OriginationResult] })
export class OriginationResultRecord extends OriginationResult implements OperationResult {}

@ObjectType({ implements: [OperationMetadata, OriginationMetadata] })
export class OriginationRecordMetadata extends OriginationMetadata implements OperationMetadata {
    @Field(() => OriginationResultRecord)
    readonly operation_result!: OriginationResultRecord;
}

@ObjectType({ implements: [Operation, OperationRecord, MoneyOperation, MoneyOperationRecord, Origination] })
export class OriginationRecord extends MoneyOperationRecord implements Origination {
    @Field(() => scalars.Address, { description: 'The address of an originated account or an implicit account.' })
    readonly contract_address?: string | null;

    @Field(() => scalars.Decimal, { nullable: true, description: 'The storage size of an operation.' })
    readonly storage_size?: Prisma.Decimal | null;

    @Field(() => scalars.Mutez, {
        nullable: true,
        description: 'The initial balance of the originated contract.',
    })
    readonly balance!: bigint | null;

    @Field(() => scalars.Mutez, { nullable: true })
    readonly burned!: bigint | null;

    @Field(() => OriginationRecordMetadata, { nullable: true })
    readonly metadata!: OriginationRecordMetadata | null;

    readonly kind!: OperationKind.origination;

    readonly delegate_address!: string | null;
    readonly delegate_id!: bigint | null;
}

export interface OriginationRecordData extends OriginationRecord {
    readonly cursor: string;
}
