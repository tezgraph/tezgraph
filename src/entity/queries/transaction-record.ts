/* istanbul ignore file */

import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../common/operation';
import { Transaction, TransactionMetadata, TransactionParameters, TransactionResult } from '../common/transaction';
import { scalars } from '../scalars';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperationRecord } from './money-operation-record';
import { OperationRecord } from './operation-record';

@ObjectType({ implements: [OperationResult, TransactionResult] })
export class TransactionResultRecord extends TransactionResult implements OperationResult {}

@ObjectType({ implements: [OperationMetadata, TransactionMetadata] })
export class TransactionRecordMetadata extends TransactionMetadata implements OperationMetadata {
    @Field(() => TransactionResultRecord)
    readonly operation_result!: TransactionResultRecord;
}

@ObjectType({ implements: [Operation, OperationRecord, MoneyOperation, MoneyOperationRecord, Transaction] })
export class TransactionRecord extends MoneyOperationRecord implements Transaction {
    @Field(() => scalars.Mutez, { nullable: true, description: 'The amount of tz transferred.' })
    readonly amount?: bigint | null;

    @Field(() => TransactionParameters, { nullable: true })
    readonly parameters!: TransactionParameters | null;

    @Field(() => TransactionRecordMetadata, { nullable: true })
    readonly metadata!: TransactionRecordMetadata | null;

    @Field(() => scalars.Decimal, { nullable: true, description: 'The storage size of an operation.' })
    readonly storage_size?: Prisma.Decimal | null;

    readonly destination_address?: string | null;

    readonly destination_id?: bigint | null;

    readonly parameters_json?: Prisma.JsonValue;

    readonly kind!: OperationKind.transaction;
}

export interface TransactionRecordData extends TransactionRecord {
    readonly cursor: string;
}
