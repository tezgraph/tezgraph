/* istanbul ignore file */
import { Field, Int, ObjectType } from 'type-graphql';

import { Endorsement, EndorsementMetadata } from '../common/endorsement';
import { Operation } from '../common/operation';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { AccountRecord } from './account-record';
import { OperationRecord } from './operation-record';

@ObjectType({
    implements: [EndorsementMetadata],
})
export class EndorsementRecordMetadata implements EndorsementMetadata {
    @Field(() => AccountRecord, {
        description: 'An Implicit account to which an account has delegated their baking and endorsement rights.',
    })
    readonly delegate!: AccountRecord;

    @Field(() => [Int], { nullable: true })
    readonly slots!: number[] | null;

    readonly graphQLTypeName!: string;
}

@ObjectType({ implements: [Operation, OperationRecord, Endorsement] })
export class EndorsementRecord extends OperationRecord implements Endorsement {
    @Field(() => EndorsementRecordMetadata, { nullable: true })
    readonly metadata!: EndorsementRecordMetadata | null;

    readonly kind!: OperationKind.endorsement;
}

export interface EndorsementRecordData extends EndorsementRecord {
    readonly cursor: string;
}
