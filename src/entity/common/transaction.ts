import { Field, InterfaceType, ObjectType } from 'type-graphql';

import { Micheline, scalars } from '../scalars';
import { graphQLDescriptions } from '../subscriptions/graphql-descriptions';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperation } from './money-operation';
import { Operation, OperationMetadata, OperationResult } from './operation';

@InterfaceType({ implements: [OperationResult] })
export class TransactionResult extends OperationResult {}

@InterfaceType({
    implements: [OperationMetadata],
    resolveType: (metadata: TransactionMetadata): string => metadata.graphQLTypeName,
})
export class TransactionMetadata extends OperationMetadata {
    @Field(() => TransactionResult)
    readonly operation_result!: TransactionResult;
}

@ObjectType({
    description:
        `${graphQLDescriptions.getRpcOperation(OperationKind.transaction, ['parameters'], { trailingPeriod: false })}` +
        ` or ${graphQLDescriptions.getRpc(['$operation.alpha.internal_operation_result', 'parameters'], {
            capitalize: false,
        })}`,
})
export class TransactionParameters {
    @Field()
    readonly entrypoint!: string;

    @Field(() => scalars.Micheline)
    readonly value!: Micheline;
}

@InterfaceType({
    implements: [Operation, MoneyOperation],
})
export class Transaction extends MoneyOperation implements Operation {
    @Field(() => scalars.Mutez, {
        nullable: true,
        description: 'The amount of tz transferred.',
    })
    readonly amount?: bigint | null;

    @Field(() => TransactionParameters, { nullable: true })
    readonly parameters!: TransactionParameters | null;

    @Field(() => TransactionMetadata, { nullable: true })
    readonly metadata!: TransactionMetadata | null;

    readonly kind = OperationKind.transaction;
}
