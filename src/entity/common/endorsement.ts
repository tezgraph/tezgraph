import { Field, Int, InterfaceType } from 'type-graphql';

import { OperationKind } from '../subscriptions/operations/operation-kind';
import { Operation } from './operation';

@InterfaceType({
    resolveType: (metadata: EndorsementMetadata): string => metadata.graphQLTypeName,
})
export class EndorsementMetadata {
    @Field(() => [Int], { nullable: true })
    readonly slots!: readonly number[] | null;

    readonly graphQLTypeName!: string;
}

@InterfaceType({ implements: [Operation] })
export class Endorsement extends Operation {
    @Field(() => EndorsementMetadata, { nullable: true })
    readonly metadata!: EndorsementMetadata | null;

    readonly kind!: OperationKind.endorsement;
}
