import { Field, InterfaceType } from 'type-graphql';

import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperation } from './money-operation';
import { Operation, OperationMetadata, OperationResult } from './operation';

@InterfaceType({ implements: [OperationResult] })
export class DelegationResult extends OperationResult {}

@InterfaceType({
    implements: [OperationMetadata],
    resolveType: (metadata: DelegationMetadata): string => metadata.graphQLTypeName,
})
export class DelegationMetadata extends OperationMetadata {
    @Field(() => DelegationResult)
    readonly operation_result!: DelegationResult;
}

@InterfaceType({
    implements: [Operation, MoneyOperation],
    resolveType: (delegation: Delegation): string => delegation.graphQLTypeName,
})
export class Delegation extends MoneyOperation implements Operation {
    @Field(() => DelegationMetadata, { nullable: true })
    readonly metadata!: DelegationMetadata | null;

    readonly kind!: OperationKind.delegation;
}
