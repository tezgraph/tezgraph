// Disabling these rules because, for generic types, TypeGraphQL has to rely on inference (no explicit return type)
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */

import { Field, Int, ObjectType } from 'type-graphql';

import { AbstractClassType } from '../utils/types';
import { PageInfo } from './pageInfo';
import { scalars } from './scalars';

export function RelayConnection<TOperationEdge>(OperationEdgeClass: AbstractClassType<TOperationEdge>) {
    @ObjectType({ isAbstract: true })
    abstract class RelayConnectionClass {
        @Field(() => PageInfo, { description: 'An object containing the details of the current page.' })
        page_info!: PageInfo;

        @Field(() => [OperationEdgeClass], { nullable: true, description: 'A list containing the operation data.' })
        edges?: TOperationEdge[] | null;

        @Field(() => Int, { nullable: false, description: 'The total number of items matching the query.' })
        total_count?: number;
    }

    return RelayConnectionClass;
}

export function RelayEdge<TOperation>(OperationClass: AbstractClassType<TOperation>) {
    @ObjectType({ isAbstract: true })
    abstract class RelayEdgeClass {
        @Field(() => scalars.Cursor, {
            description:
                'A string that is used to paginate query results. This value can be used as the `before` or `after` query arguments.',
        })
        cursor!: string;

        @Field(() => OperationClass, { nullable: true, description: 'An object containing operation data.' })
        node?: TOperation | null;
    }

    return RelayEdgeClass;
}
