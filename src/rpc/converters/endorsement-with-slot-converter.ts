import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EndorsementWithSlotNotification, OperationKind } from '../../entity/subscriptions';
import { EndorsementMetadataConverter, InlinedEndorsementConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcEndorsementWithSlot =
    | rpc.OperationContentsEndorsementWithSlot
    | rpc.OperationContentsAndResultEndorsementWithSlot;

@singleton()
export class EndorsementWithSlotConverter {
    constructor(
        private readonly metadataConverter: EndorsementMetadataConverter,
        private readonly inlinedEndorsementConverter: InlinedEndorsementConverter,
    ) {}

    convert(
        rpcOperation: RpcEndorsementWithSlot,
        baseProperties: BaseOperationProperties,
    ): EndorsementWithSlotNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: EndorsementWithSlotNotification.name,
            kind: OperationKind.endorsement_with_slot,
            slot: primitives.int.convert(rpcOperation.slot),
            endorsement: this.inlinedEndorsementConverter.convert(rpcOperation.endorsement),
            metadata: this.metadataConverter.convert(rpcOperation),
        };
    }
}
