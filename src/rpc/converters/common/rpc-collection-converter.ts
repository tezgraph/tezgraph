import { isNotNullish, Nullish } from '../../../utils/reflection';
import { primitives } from './primitives';
import { RpcConverter } from './rpc-converter';

export abstract class RpcCollectionConverter<TRpc, TTarget> extends RpcConverter<TRpc, TTarget> {
    convertNullableArray(rpcArray: Nullish<TRpc[]>): readonly TTarget[] | null {
        return isNotNullish(rpcArray) ? this.convertArray(rpcArray) : null;
    }

    convertArray(rpcArray: TRpc[]): readonly TTarget[] {
        return primitives.array.convert(rpcArray, (i) => this.convert(i));
    }
}
