import { PrimitiveConverter } from './primitive-converter';

export class StringConverter extends PrimitiveConverter<string, string> {
    protected readonly typeDescription = 'string';

    protected convertInternal(rpcValue: unknown): string {
        if (typeof rpcValue !== 'string') {
            throw new Error('The value must be a string.');
        }
        return rpcValue;
    }
}
