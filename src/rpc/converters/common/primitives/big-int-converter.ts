import { isWhiteSpace } from '../../../../utils/string-manipulation';
import { PrimitiveConverter } from './primitive-converter';

export class BigIntConverter extends PrimitiveConverter<string, bigint> {
    protected readonly typeDescription: string = 'big int';

    protected convertInternal(rpcValue: unknown): bigint {
        // Beware: BigInt(empty/white-space string) returns zero.
        if (typeof rpcValue !== 'string' || isWhiteSpace(rpcValue)) {
            throw new Error('The value must be a non-empty string.');
        }
        return BigInt(rpcValue);
    }
}
