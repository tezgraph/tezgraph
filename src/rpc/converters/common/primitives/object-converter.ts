import { isNotNullish, isObjectLiteral, Nullish } from '../../../../utils/reflection';
import { PrimitiveConverter } from './primitive-converter';

type ConvertObjectFunc<TRpcObject, TTargetObject> = (rpcObject: TRpcObject) => TTargetObject;

export class ObjectConverter {
    guard<TRpcObject extends object>(rpcObject: TRpcObject): TRpcObject {
        return this.guardNonNullable(rpcObject);
    }

    guardNullable<TRpcObject extends object>(rpcObject: Nullish<TRpcObject>): TRpcObject | null {
        return isNotNullish(rpcObject) ? this.guard(rpcObject) : null;
    }

    /** Overload useful when common interface (e.g. Taquito) has nullable property but we know it must be non-nullable. */
    guardNonNullable<TRpcObject extends object>(rpcObject: Nullish<TRpcObject>): TRpcObject {
        return rawConverter.convertToNonNullable(rpcObject) as TRpcObject;
    }

    convert<TRpcObject extends object, TTargetObject extends object>(
        rpcObject: TRpcObject,
        convertObject: ConvertObjectFunc<TRpcObject, TTargetObject>,
    ): TTargetObject {
        return this.convertToNonNullable(rpcObject, convertObject);
    }

    convertNullable<TRpcObject extends object, TTargetObject extends object>(
        rpcObject: Nullish<TRpcObject>,
        convertObject: ConvertObjectFunc<TRpcObject, TTargetObject>,
    ): TTargetObject | null {
        return isNotNullish(rpcObject) ? this.convert(rpcObject, convertObject) : null;
    }

    /** Overload useful when common interface (e.g. Taquito) has nullable property but we know it must be non-nullable. */
    convertToNonNullable<TRpcObject extends object, TTargetObject extends object>(
        rpcObject: Nullish<TRpcObject>,
        convertObject: ConvertObjectFunc<TRpcObject, TTargetObject>,
    ): TTargetObject {
        const checkedRpcObject = this.guardNonNullable(rpcObject);
        return convertObject(checkedRpcObject);
    }
}

const rawConverter = new (class extends PrimitiveConverter<object, object> {
    protected readonly typeDescription = 'object literal';

    protected convertInternal(rpcValue: unknown): object {
        if (!isObjectLiteral(rpcValue)) {
            throw new Error('The value must be an object literal.');
        }
        return rpcValue;
    }
})();
