import { PrimitiveConverter } from './primitive-converter';

export class IntConverter extends PrimitiveConverter<number, number> {
    protected readonly typeDescription = 'integer';

    protected convertInternal(rpcValue: unknown): number {
        if (typeof rpcValue !== 'number' || rpcValue % 1 !== 0) {
            throw new Error('The value must be a number with no fractional part.');
        }
        return rpcValue;
    }
}
