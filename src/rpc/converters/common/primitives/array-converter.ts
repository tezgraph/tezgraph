import { isNotNullish, Nullish } from '../../../../utils';
import { PrimitiveConverter } from './primitive-converter';

type ConvertItemFunc<TRpcItem, TTargetItem> = (rpcItem: TRpcItem) => TTargetItem;

export class ArrayConverter {
    guard<TRpcItem>(rpcArray: TRpcItem[]): TRpcItem[] {
        return this.guardNonNullable(rpcArray);
    }

    guardNullable<TRpcItem>(rpcArray: Nullish<TRpcItem[]>): TRpcItem[] | null {
        return isNotNullish(rpcArray) ? this.guard(rpcArray) : null;
    }

    /** Overload useful when common interface (e.g. Taquito) has nullable property but we know it must be non-nullable. */
    guardNonNullable<TRpcItem>(rpcArray: Nullish<TRpcItem[]>): TRpcItem[] {
        return rawConverter.convertToNonNullable(rpcArray) as TRpcItem[];
    }

    convert<TRpcItem, TTargetItem>(
        rpcArray: TRpcItem[],
        convertItem: ConvertItemFunc<TRpcItem, TTargetItem>,
    ): TTargetItem[] {
        return this.convertToNonNullable(rpcArray, convertItem);
    }

    convertNullable<TRpcItem, TTargetItem>(
        rpcArray: Nullish<TRpcItem[]>,
        convertItem: ConvertItemFunc<TRpcItem, TTargetItem>,
    ): TTargetItem[] | null {
        return isNotNullish(rpcArray) ? this.convert(rpcArray, convertItem) : null;
    }

    /** Overload useful when common interface (e.g. Taquito) has nullable property but we know it must be non-nullable. */
    convertToNonNullable<TRpcItem, TTargetItem>(
        rpcArray: Nullish<TRpcItem[]>,
        convertItem: ConvertItemFunc<TRpcItem, TTargetItem>,
    ): TTargetItem[] {
        return this.guardNonNullable(rpcArray).map(convertItem);
    }
}

const rawConverter = new (class extends PrimitiveConverter<unknown[], unknown[]> {
    protected readonly typeDescription = 'array';

    protected convertInternal(rpcValue: unknown): unknown[] {
        if (!Array.isArray(rpcValue)) {
            throw new Error('The value must be an array.');
        }
        return rpcValue;
    }
})();
