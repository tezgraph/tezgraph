import * as rpc from '@taquito/rpc';
import { OperationContentsAndResultMetadata } from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BalanceUpdate, InternalOperationResult, SimpleOperationMetadata } from '../../../entity/subscriptions';
import { isNotNullish } from '../../../utils';
import { InternalOperationResultConverter } from '../operation-results/internal-operation-result-converter';
import { BalanceUpdateConverter } from './balance-update-converter';
import { primitives } from './primitives';

export interface RpcOperation<TMetadata extends object = OperationContentsAndResultMetadata> {
    kind: rpc.OpKind; // Used by compiler to make sure it's an operation.
    metadata?: TMetadata;
}

export interface RpcMetadataWithResult<TRpcResult> extends OperationContentsAndResultMetadata {
    operation_result: TRpcResult;
    internal_operation_results?: rpc.InternalOperationResult[];
}

export interface MetadataWithResult<TRpcResult> {
    balance_updates: readonly BalanceUpdate[];
    operation_result: TRpcResult;
    internal_operation_results: readonly InternalOperationResult[] | null;
    graphQLTypeName: string;
}

export interface OperationResultConverter<TRpcResult, TResult> {
    convert(rpcResult: TRpcResult): TResult;
}

@singleton()
export class OperationMetadataConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly internalOperationResultConverter: InternalOperationResultConverter,
    ) {}

    convertSimple(rpcOperation: RpcOperation): SimpleOperationMetadata | null {
        return convertMetadata(rpcOperation, (rpcMetadata) => ({
            balance_updates: this.balanceUpdateConverter.convertArray(rpcMetadata.balance_updates),
        }));
    }

    convert<TRpcResult, TResult>(
        rpcOperation: RpcOperation<RpcMetadataWithResult<TRpcResult>>,
        resultConverter: OperationResultConverter<TRpcResult, TResult>,
        graphQLTypeName: string,
    ): MetadataWithResult<TResult> | null {
        return convertMetadata(rpcOperation, (rpcMetadata) => ({
            balance_updates: isNotNullish(rpcMetadata.balance_updates)
                ? this.balanceUpdateConverter.convertArray(rpcMetadata.balance_updates)
                : [],
            operation_result: resultConverter.convert(rpcMetadata.operation_result),
            internal_operation_results: this.internalOperationResultConverter.convertNullableArray(
                rpcMetadata.internal_operation_results,
            ),
            graphQLTypeName,
        }));
    }
}

function convertMetadata<TRpcMetadata extends object, TMetadata extends object>(
    rpcOperation: RpcOperation<TRpcMetadata>,
    convertFunc: (m: TRpcMetadata) => TMetadata,
): TMetadata | null {
    primitives.object.guard(rpcOperation);
    return primitives.object.convertNullable(rpcOperation.metadata, convertFunc);
}
