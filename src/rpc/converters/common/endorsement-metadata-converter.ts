import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EndorsementNotificationMetadata } from '../../../entity/subscriptions';
import { isNotNullish } from '../../../utils';
import { BalanceUpdateConverter } from './balance-update-converter';
import { RpcOperation } from './operation-metadata-converter';
import { primitives } from './primitives';

@singleton()
export class EndorsementMetadataConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(
        rpcOperation: RpcOperation<rpc.OperationContentsAndResultMetadataExtended>,
    ): EndorsementNotificationMetadata | null {
        primitives.object.guard(rpcOperation);
        return primitives.object.convertNullable(rpcOperation.metadata, (rpcMetadata) => ({
            balance_updates: isNotNullish(rpcOperation.metadata?.balance_updates)
                ? this.balanceUpdateConverter.convertArray(rpcMetadata.balance_updates)
                : [],
            delegate: primitives.string.convert(rpcMetadata.delegate),
            slots: primitives.array.convertNullable(rpcMetadata.slots, (s) => primitives.int.convert(s)),
            endorsement_power: primitives.int.convertNullable(rpcMetadata.endorsement_power),
            graphQLTypeName: EndorsementNotificationMetadata.name,
        }));
    }
}
