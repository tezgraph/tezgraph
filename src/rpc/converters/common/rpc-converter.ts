import { isNotNullish, Nullish } from '../../../utils';

/** Base class for converters from RPC. In general, we do not trust values -> strict checks. */
export abstract class RpcConverter<TRpc, TTarget> {
    convertNullable(rpcValue: Nullish<TRpc>): TTarget | null {
        // Use isNotNullish() to pass zero, empty string etc.
        return isNotNullish(rpcValue) ? this.convert(rpcValue) : null;
    }

    /** Overload useful when common interface (e.g. Taquito) has nullable property but we know it must be non-nullable. */
    convertToNonNullable(rpcValue: Nullish<TRpc>): TTarget {
        return this.convert(rpcValue!); // eslint-disable-line @typescript-eslint/no-non-null-assertion
    }

    /** Should implement strict checks. */
    abstract convert(rpcValue: TRpc): TTarget;
}
