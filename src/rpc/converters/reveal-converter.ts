import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind, RevealNotification, RevealNotificationMetadata } from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';
import { RevealResultConverter } from './operation-results/reveal-result-converter';

type RpcReveal = rpc.OperationContentsReveal | rpc.OperationContentsAndResultReveal;

@singleton()
export class RevealConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: RevealResultConverter,
    ) {}

    convert(rpcOperation: RpcReveal, baseProperties: BaseOperationProperties): RevealNotification {
        primitives.object.guard(rpcOperation);
        const metadata = this.metadataConverter.convert(
            rpcOperation,
            this.resultConverter,
            RevealNotificationMetadata.name,
        );
        return {
            ...baseProperties,
            graphQLTypeName: RevealNotification.name,
            kind: OperationKind.reveal,
            source: primitives.string.convert(rpcOperation.source),
            fee: primitives.mutez.convert(rpcOperation.fee),
            counter: primitives.bigInt.convert(rpcOperation.counter),
            gas_limit: primitives.bigInt.convert(rpcOperation.gas_limit),
            storage_limit: primitives.bigInt.convert(rpcOperation.storage_limit),
            public_key: primitives.string.convert(rpcOperation.public_key),
            metadata,
        };
    }
}
