import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind, SeedNonceRevelationNotification } from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcSeedNonceRevelation = rpc.OperationContentsRevelation | rpc.OperationContentsAndResultRevelation;

@singleton()
export class SeedNonceRevelationConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(
        rpcOperation: RpcSeedNonceRevelation,
        baseProperties: BaseOperationProperties,
    ): SeedNonceRevelationNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: SeedNonceRevelationNotification.name,
            kind: OperationKind.seed_nonce_revelation,
            level: primitives.int.convert(rpcOperation.level),
            nonce: primitives.string.convert(rpcOperation.nonce),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }
}
