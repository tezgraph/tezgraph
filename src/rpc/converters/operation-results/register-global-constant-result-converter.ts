import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { RegisterGlobalConstantNotificationResult } from '../../../entity/subscriptions';
import { BalanceUpdateConverter, primitives } from '../common';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class RegisterGlobalConstantResultConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(rpcResult: rpc.OperationResultRegisterGlobalConstant): RegisterGlobalConstantNotificationResult {
        return {
            ...getBaseOperationResultProperties(rpcResult),
            balance_updates: this.balanceUpdateConverter.convertNullableArray(rpcResult.balance_updates),
            global_address: primitives.string.convertNullable(rpcResult.global_address),
            storage_size: primitives.bigInt.convertNullable(rpcResult.storage_size),
            graphQLTypeName: RegisterGlobalConstantNotificationResult.name,
        };
    }
}
