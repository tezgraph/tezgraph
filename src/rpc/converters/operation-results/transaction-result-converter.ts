import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { TransactionNotificationResult } from '../../../entity/subscriptions';
import { BalanceUpdateConverter, BigMapDiffConverter, LazyStorageDiffConverter, primitives } from '../common';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class TransactionResultConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly bigMapDiffConverter: BigMapDiffConverter,
        private readonly lazyStorageDiffConverter: LazyStorageDiffConverter,
    ) {}

    convert(rpcResult: rpc.OperationResultTransaction): TransactionNotificationResult {
        primitives.object.guard(rpcResult);
        return {
            ...getBaseOperationResultProperties(rpcResult),
            storage: primitives.michelson.convertNullable(rpcResult.storage),
            big_map_diff: this.bigMapDiffConverter.convertNullableArray(rpcResult.big_map_diff),
            balance_updates: this.balanceUpdateConverter.convertNullableArray(rpcResult.balance_updates),
            lazy_storage_diff: this.lazyStorageDiffConverter.convertNullableArray(rpcResult.lazy_storage_diff),
            originated_contracts: primitives.array.convertNullable(rpcResult.originated_contracts, (c) =>
                primitives.string.convert(c),
            ),
            storage_size: primitives.bigInt.convertNullable(rpcResult.storage_size),
            paid_storage_size_diff: primitives.bigInt.convertNullable(rpcResult.paid_storage_size_diff),
            allocated_destination_contract: primitives.boolean.convertNullable(
                rpcResult.allocated_destination_contract,
            ),
            graphQLTypeName: TransactionNotificationResult.name,
        };
    }
}
