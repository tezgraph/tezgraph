import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind, ProposalsNotification } from '../../entity/subscriptions';
import { primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcProposals = rpc.OperationContentsProposals | rpc.OperationContentsAndResultProposals;

@singleton()
export class ProposalsConverter {
    convert(rpcOperation: RpcProposals, baseProperties: BaseOperationProperties): ProposalsNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: ProposalsNotification.name,
            kind: OperationKind.proposals,
            source: primitives.string.convert(rpcOperation.source),
            period: primitives.int.convert(rpcOperation.period),
            proposals: primitives.array.convert(rpcOperation.proposals, (p) => primitives.string.convert(p)),
        };
    }
}
