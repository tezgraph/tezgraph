import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EndorsementNotification, OperationKind } from '../../entity/subscriptions';
import { EndorsementMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcEndorsement = rpc.OperationContentsEndorsement | rpc.OperationContentsAndResultEndorsement;

@singleton()
export class EndorsementConverter {
    constructor(private readonly metadataConverter: EndorsementMetadataConverter) {}

    convert(rpcOperation: RpcEndorsement, baseProperties: BaseOperationProperties): EndorsementNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: EndorsementNotification.name,
            kind: OperationKind.endorsement,
            level: primitives.int.convert(rpcOperation.level),
            round: primitives.int.convertNullable(rpcOperation.round),
            slot: primitives.int.convertNullable(rpcOperation.slot),
            block_payload_hash: primitives.string.convertNullable(rpcOperation.block_payload_hash),
            metadata: this.metadataConverter.convert(rpcOperation),
        };
    }
}
