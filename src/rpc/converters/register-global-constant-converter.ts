import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    OperationKind,
    RegisterGlobalConstantNotification,
    RegisterGlobalConstantNotificationMetadata,
} from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';
import { RegisterGlobalConstantResultConverter } from './operation-results/register-global-constant-result-converter';

export type RpcRegisterGlobalConstant =
    | rpc.OperationContentsRegisterGlobalConstant
    | rpc.OperationContentsAndResultRegisterGlobalConstant;

@singleton()
export class RegisterGlobalConstantConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: RegisterGlobalConstantResultConverter,
    ) {}

    convert(
        rpcOperation: RpcRegisterGlobalConstant,
        baseProperties: BaseOperationProperties,
    ): RegisterGlobalConstantNotification {
        primitives.object.guard(rpcOperation);
        const metadata = this.metadataConverter.convert(
            rpcOperation,
            this.resultConverter,
            RegisterGlobalConstantNotificationMetadata.name,
        );
        return {
            ...baseProperties,
            graphQLTypeName: RegisterGlobalConstantNotification.name,
            kind: OperationKind.register_global_constant,
            source: primitives.string.convert(rpcOperation.source),
            fee: primitives.mutez.convert(rpcOperation.fee),
            counter: primitives.bigInt.convert(rpcOperation.counter),
            gas_limit: primitives.bigInt.convert(rpcOperation.gas_limit),
            storage_limit: primitives.bigInt.convert(rpcOperation.storage_limit),
            value: primitives.michelson.convertToNonNullable(rpcOperation.value),
            metadata,
        };
    }
}
