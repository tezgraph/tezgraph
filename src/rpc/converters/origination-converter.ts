import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind, OriginationNotification } from '../../entity/subscriptions';
import { OriginationNotificationMetadata } from '../../entity/subscriptions/operations/origination/origination-notification';
import { OperationMetadataConverter, primitives, ScriptedContractsConverter } from './common';
import { BaseOperationProperties } from './operation-converter';
import { OriginationResultConverter } from './operation-results/origination-result-converter';

export type RpcOrigination = rpc.OperationContentsOrigination | rpc.OperationContentsAndResultOrigination;

@singleton()
export class OriginationConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: OriginationResultConverter,
        private readonly scriptConverter: ScriptedContractsConverter,
    ) {}

    convert(rpcOperation: RpcOrigination, baseProperties: BaseOperationProperties): OriginationNotification {
        primitives.object.guard(rpcOperation);
        const metadata = this.metadataConverter.convert(
            rpcOperation,
            this.resultConverter,
            OriginationNotificationMetadata.name,
        );
        return {
            ...baseProperties,
            graphQLTypeName: OriginationNotification.name,
            kind: OperationKind.origination,
            source: primitives.string.convert(rpcOperation.source),
            fee: primitives.mutez.convert(rpcOperation.fee),
            counter: primitives.bigInt.convert(rpcOperation.counter),
            gas_limit: primitives.bigInt.convert(rpcOperation.gas_limit),
            storage_limit: primitives.bigInt.convert(rpcOperation.storage_limit),
            balance: primitives.mutez.convert(rpcOperation.balance),
            delegate: primitives.string.convertNullable(rpcOperation.delegate),
            script: this.scriptConverter.convertToNonNullable(rpcOperation.script),
            metadata,
        };
    }
}
