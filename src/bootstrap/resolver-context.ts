import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { ExecutionParams } from 'subscriptions-transport-ws';
import { DependencyContainer } from 'tsyringe';

import { BigmapDataSource } from '../modules/queries-graphql/datasources/bigmap-datasource';
import { ContractStorageDataSource } from '../modules/queries-graphql/datasources/contract-storage-datasource';

/** Enhanced context used for processing requests. */
export interface ResolverContext extends Readonly<Partial<ExpressContext>> {
    /** Container so that we can resolve dependencies also in JavaScript decorators. */
    readonly container: DependencyContainer;

    /** UUID identifying the request so that logged entries can be paired. */
    readonly requestId: string;

    connection?: ExecutionParams;

    dataSources: { bigmap: BigmapDataSource; contractStorage: ContractStorageDataSource };
}
