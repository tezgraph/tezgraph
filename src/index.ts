import 'reflect-metadata';
// We need to load .env files correctly before prisma kicks in because it overwrites it itself.
// eslint-disable-next-line import/order
import './utils/configuration/env-config-initialization';

import { AbortController } from 'abort-controller';

import { App } from './app';
import { createDIContainer } from './dependency-injection-container';
import { addAbortListener } from './utils/abortion';
import { Logger, LoggerFactory } from './utils/logging';

async function bootstrap(): Promise<void> {
    const container = createDIContainer(process.env);
    const logger = container.resolve(LoggerFactory).getLogger('Index');

    const abortController = container.resolve(AbortController);
    ['SIGINT', 'SIGTERM'].forEach((s) => process.on(s, () => abortController.abort()));
    setupUnhandledErrors(abortController, logger);

    const app = container.resolve(App);
    await app.start();

    addAbortListener(abortController.signal, () => void app.stop());
}

function setupUnhandledErrors(abortController: AbortController, logger: Logger): void {
    process.on('unhandledRejection', (reason, _promise) => {
        logCriticalAndExit(abortController, logger, 'Unhandled Rejection at Promise because of {error}.', reason);
    });

    process.on('uncaughtException', (error) => {
        logCriticalAndExit(abortController, logger, 'Uncaught Exception throw. {error}', error);
    });
}

function logCriticalAndExit(controller: AbortController, logger: Logger, message: string, error: unknown): void {
    logger.logCritical(message, { error });
    process.exitCode = 1;
    controller.abort();
}

void bootstrap();
