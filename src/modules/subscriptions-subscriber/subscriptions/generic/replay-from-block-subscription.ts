import { ReplayFromBlockArgs } from '../../../../entity/subscriptions';
import { Logger } from '../../../../utils/logging';
import { isNotNullish } from '../../../../utils/reflection';
import { FromBlockExtractor } from '../../from-block-extractors/extractor';
import { ReplayBlockProvider } from '../../helpers/replay-block-provider';
import { Subscription, SubscriptionOptions } from '../subscription';

/** Decorates a subscription with a replay from a block level specified by user. */
export class ReplayFromBlockSubscription<TItem, TArgs extends ReplayFromBlockArgs>
    implements Subscription<TItem, TArgs>
{
    constructor(
        private readonly replayBlockProvider: ReplayBlockProvider,
        private readonly logger: Logger,
        private readonly itemExtractor: FromBlockExtractor<TItem>,
        private readonly innerSubscription: Subscription<TItem, TArgs>,
    ) {}

    async *subscribe(options: SubscriptionOptions<TItem, TArgs>): AsyncIterableIterator<TItem> {
        const fromBlockLevel = options.args.replayFromBlockLevel;
        if (isNotNullish(fromBlockLevel)) {
            const requestId = options.context.requestId;
            const itemType = options.itemType.name;
            this.logger.logInformation('Serving historical items of {itemType} from {blockLevel} for {requestId}.', {
                itemType,
                blockLevel: fromBlockLevel,
                requestId,
            });

            for await (const block of this.replayBlockProvider.iterateFrom(fromBlockLevel, requestId)) {
                for (const item of this.itemExtractor.extract(block)) {
                    this.logger.logDebug('Serving {item} of {itemType} from historical {blockLevel} for {requestId}.', {
                        item,
                        itemType,
                        blockLevel: block.header.level,
                        requestId,
                    });
                    yield item;
                }
            }

            this.logger.logDebug('Switching to fresh item of {itemType} from head block for {requestId}.', {
                itemType,
                requestId,
            });
        }
        yield* this.innerSubscription.subscribe(options);
    }
}
