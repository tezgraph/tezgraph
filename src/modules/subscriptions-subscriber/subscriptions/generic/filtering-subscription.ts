import { FilteringArgs, FilterOptimizer } from '../../../../entity/subscriptions';
import { filterIterable } from '../../../../utils/collections/async-iterable-utils';
import { Subscription, SubscriptionOptions } from '../subscription';

/** Decorates a subscription with filtering of items by filter specified by user in input args. */
export class FilteringSubscription<TItem, TArgs extends FilteringArgs<TItem>> implements Subscription<TItem, TArgs> {
    constructor(
        private readonly filterOptimizer: FilterOptimizer,
        private readonly innerSubscription: Subscription<TItem, TArgs>,
    ) {}

    subscribe(options: SubscriptionOptions<TItem, TArgs>): AsyncIterableIterator<TItem> {
        const filter = options.args.filter ? this.filterOptimizer.optimize(options.args.filter) : null;
        const iterable = this.innerSubscription.subscribe(options);

        return filter ? filterIterable(iterable, (o) => filter.passes(o)) : iterable;
    }
}
