/* istanbul ignore file */

import { InjectionToken } from 'tsyringe';
import { Args, Authorized, Resolver, Root, Subscription as GraphQLSubscription } from 'type-graphql';

import { ResolverContext } from '../../../../bootstrap/resolver-context';
import { DefaultConstructor } from '../../../../utils/reflection';
import { createArgsInstance } from '../../helpers/args-instance-factory';
import { Subscription } from '../subscription';

export interface GraphQLResolverOptions<TItem, TArgs> {
    subscriptionName: string;
    description: string;
    itemType: DefaultConstructor<TItem>;
    argsType: DefaultConstructor<TArgs>;
    subscriptionDIToken: InjectionToken<Subscription<TItem, TArgs>>;
}

/** Creates a GraphQL resolver which exposes particular subscription. */
export function createGraphQLResolverClass<TItem, TArgs>(
    options: GraphQLResolverOptions<TItem, TArgs>,
): DefaultConstructor {
    @Resolver()
    class GraphQLResolverClass {
        @Authorized()
        @GraphQLSubscription(() => options.itemType, {
            description: options.description,
            subscribe: (_root, argsDictionary, context: ResolverContext) => {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
                const args = createArgsInstance(options.argsType, argsDictionary);
                const subscription = context.container.resolve(options.subscriptionDIToken);
                return subscription.subscribe({ args, context, ...options });
            },
        })
        [options.subscriptionName](
            @Root() item: TItem,
            @Args(() => options.argsType) _args: TArgs, // eslint-disable-line @typescript-eslint/indent
        ): TItem {
            return item;
        }
    }
    return GraphQLResolverClass;
}
