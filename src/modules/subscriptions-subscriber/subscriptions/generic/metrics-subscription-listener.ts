import {
    BlockNotification,
    OperationArgs,
    OperationNotification,
    OperationOrigin,
    ReplayFromBlockArgs,
} from '../../../../entity/subscriptions';
import { MetricsContainer, SubscriptionCountLabel } from '../../../../utils/metrics/metrics-container';
import { isNotNullish } from '../../../../utils/reflection';
import { Clock } from '../../../../utils/time/clock';
import { SubscriptionOptions } from '../subscription';
import { SubscriptionEventsListener } from './listening-subscription';

type PossibleArgs = Partial<ReplayFromBlockArgs & OperationArgs<OperationNotification>>;

/** Mimics operation for convenience. */
export interface ItemMetricsData {
    operation_group: { received_from_tezos_on: Date };
    block: BlockNotification | null;
}

export class MetricsSubscriptionListener<TItem, TArgs> implements SubscriptionEventsListener<TItem, TArgs> {
    constructor(
        private readonly metrics: MetricsContainer,
        private readonly clock: Clock,
        private readonly getItemMetrics: (i: TItem) => ItemMetricsData,
    ) {}

    onConnectionOpen(options: SubscriptionOptions<TItem, TArgs>): void {
        const metricsData = this.getMetricsData(options);
        this.metrics.subscriptionsCurrent.inc(metricsData);
        this.metrics.subscriptionsTotal.inc(metricsData);
    }

    onItemPublish(item: TItem, options: SubscriptionOptions<TItem, TArgs>): void {
        const metricsData = this.getMetricsData(options);
        this.metrics.graphQLSubscriptionMethodCounter.inc({
            method: metricsData.name,
            mempool: metricsData.mempool,
            replay: metricsData.replay,
        });

        const itemMetrics = this.getItemMetrics(item);
        const nowTimestamp = this.clock.getNowTimestamp();

        const origin = itemMetrics.block ? OperationOrigin.block : OperationOrigin.mempool;
        const relativeDuration = nowTimestamp - itemMetrics.operation_group.received_from_tezos_on.getTime();
        this.metrics.processNotificationRelativeDuration.observe({ origin }, relativeDuration);

        if (itemMetrics.block) {
            const absoluteDuration = nowTimestamp - itemMetrics.block.header.timestamp.getTime();
            this.metrics.processNotificationAbsoluteDuration.observe(absoluteDuration);
        }
    }

    onConnectionClose(options: SubscriptionOptions<TItem, TArgs>): void {
        const metricsData = this.getMetricsData(options);
        this.metrics.subscriptionsCurrent.dec(metricsData);
    }

    private getMetricsData(options: SubscriptionOptions<TItem, TArgs>): Record<SubscriptionCountLabel, string> {
        const name = options.subscriptionName;
        const args = options.args as PossibleArgs;
        const replay = isNotNullish(args.replayFromBlockLevel).toString();
        const mempool = (args.includeMempool === true).toString();
        return { name, replay, mempool };
    }
}
