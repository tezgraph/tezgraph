import { Subscription, SubscriptionOptions } from '../subscription';

export interface SubscriptionEventsListener<TItem, TArgs> {
    onConnectionOpen?(options: SubscriptionOptions<TItem, TArgs>): void;
    onItemPublish?(item: TItem, options: SubscriptionOptions<TItem, TArgs>): void;
    onConnectionClose?(options: SubscriptionOptions<TItem, TArgs>): void;
}

/** Decorates a subscription with events listeners. */
export class ListeningSubscription<TItem, TArgs extends object> implements Subscription<TItem, TArgs> {
    constructor(
        private readonly listeners: readonly SubscriptionEventsListener<TItem, TArgs>[],
        private readonly innerSubscription: Subscription<TItem, TArgs>,
    ) {}

    subscribe(options: SubscriptionOptions<TItem, TArgs>): AsyncIterableIterator<TItem> {
        this.listeners.forEach((l) => l.onConnectionOpen?.(options));

        const onItemPublish = (item: TItem): void => this.listeners.forEach((l) => l.onItemPublish?.(item, options));
        const onConnectionClose = (): void => this.listeners.forEach((l) => l.onConnectionClose?.(options));

        const innerIterable = this.innerSubscription.subscribe(options);

        return {
            async next(): Promise<IteratorResult<TItem, unknown>> {
                const result = await innerIterable.next();
                if (result.done !== true) {
                    onItemPublish(result.value);
                }
                return result;
            },
            async return(value: unknown): Promise<IteratorResult<TItem, unknown>> {
                onConnectionClose();
                return (await innerIterable.return?.(value)) ?? { done: true, value };
            },
            async throw(error: unknown): Promise<IteratorResult<TItem, unknown>> {
                onConnectionClose();
                return (await innerIterable.throw?.(error)) ?? { done: true, value: error };
            },
            [Symbol.asyncIterator](): AsyncIterableIterator<TItem> {
                return this;
            },
        };
    }
}
