import { ResolverContext } from '../../../bootstrap/resolver-context';
import { DefaultConstructor } from '../../../utils/reflection';

export interface SubscriptionOptions<TItem, TArgs> {
    readonly args: TArgs;
    readonly context: ResolverContext;
    readonly subscriptionName: string;
    readonly itemType: DefaultConstructor<TItem>;
}

export interface Subscription<TItem, TArgs> {
    subscribe(options: SubscriptionOptions<TItem, TArgs>): AsyncIterableIterator<TItem>;
}
