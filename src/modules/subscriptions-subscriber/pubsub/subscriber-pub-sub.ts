import { AbortSignal } from 'abort-controller';
import { PubSub } from 'graphql-subscriptions';
import { singleton } from 'tsyringe';

import {
    BalanceUpdateNotification,
    BigMapDiffNotification,
    BlockNotification,
    LazyStorageDiffNotification,
    OperationKind,
    OperationNotification,
    StorageNotification,
} from '../../../entity/subscriptions';
import { PubSubTrigger, TypedPubSub } from '../../../utils';
import { asReadonly } from '../../../utils/conversion';
import { injectLogger, Logger } from '../../../utils/logging';

/** Strongly-typed triggers for particular payloads. */
export const subscriberTriggers = {
    blocks: new PubSubTrigger<BlockNotification>('BLOCKS'),
    bigMapDiffs: new PubSubTrigger<BigMapDiffNotification>('BIG_MAP_DIFFS'),
    storages: new PubSubTrigger<StorageNotification>('STORAGES'),
    balanceUpdates: new PubSubTrigger<BalanceUpdateNotification>('BALANCE_UPDATES'),
    lazyStorageDiff: new PubSubTrigger<LazyStorageDiffNotification>('LAZY_STORAGE_DIFFS'),

    // Separate mempool b/c it's not included by default -> don't evaluate all consumers -> performance.
    blockOperations: getTriggerForEachKind('BLOCK_OPERATIONS'),
    mempoolOperations: getTriggerForEachKind('MEMPOOL_OPERATIONS'),
};

function getTriggerForEachKind(
    namePrefix: string,
): Readonly<Record<OperationKind, PubSubTrigger<OperationNotification>>> {
    const result: Record<string, PubSubTrigger<OperationNotification>> = {};
    for (const kind of Object.keys(OperationKind)) {
        result[kind] = new PubSubTrigger<OperationNotification>(`${namePrefix}:${kind}`);
    }
    return asReadonly(result);
}

/** Local memory PubSub used only by this module in order to multiply operations per kind. */
@singleton()
export class SubscriberPubSub extends TypedPubSub {
    constructor(@injectLogger(SubscriberPubSub) logger: Logger, globalAbortSignal: AbortSignal) {
        super(new PubSub(), logger, globalAbortSignal);
    }
}
