import { AbortSignal } from 'abort-controller';
import { singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions';
import { AsyncIterableProvider, ExternalPubSub, externalTriggers } from '../../../../utils';

@singleton()
export class MempoolPubSubProvider implements AsyncIterableProvider<MempoolOperationGroup> {
    constructor(private readonly externalPubSub: ExternalPubSub) {}

    iterate(abortSignal: AbortSignal): AsyncIterableIterator<MempoolOperationGroup> {
        return this.externalPubSub.iterate([externalTriggers.mempoolOperationGroups], abortSignal);
    }
}
