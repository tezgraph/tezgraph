import { BlockNotification } from '../../../../entity/subscriptions';
import { SubscriberPubSub, subscriberTriggers } from '../subscriber-pub-sub';
import { PubSubPublisher } from './block-pubsub-processor';

/** Publishes GraphQL operations from a block. */
export class OperationsPubSubPublisher implements PubSubPublisher {
    async publish(block: BlockNotification, subscriberPubSub: SubscriberPubSub): Promise<void> {
        // The order of operations within a block is important e.g. a transaction cannot precede a reveal operation to an empty address.
        for (const operation of block.operations) {
            const trigger = subscriberTriggers.blockOperations[operation.kind];
            await subscriberPubSub.publish(trigger, operation); // It's a memory PubSub so await-s are insignificant -> no parallelism is needed.
        }
    }
}
