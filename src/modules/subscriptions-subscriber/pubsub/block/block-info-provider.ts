import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions';
import { InfoProvider } from '../../../../utils';

/** Collects generic diagnostic info about a block. */
@singleton()
export class BlockInfoProvider implements InfoProvider<BlockNotification> {
    getInfo(block: BlockNotification): unknown {
        return {
            hash: block.hash,
            level: block.header.level,
            chainId: block.chain_id,
            timestamp: block.header.timestamp,
            operationCount: block.operations.length,
        };
    }
}
