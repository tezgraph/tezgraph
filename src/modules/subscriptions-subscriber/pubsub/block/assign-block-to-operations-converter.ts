import { singleton } from 'tsyringe';

import { BlockNotification, OperationNotification } from '../../../../entity/subscriptions';
import { ItemConverter } from '../../../../utils';

@singleton()
export class AssignBlockToOperationsConverter implements ItemConverter<BlockNotification, BlockNotification> {
    convert(block: BlockNotification): BlockNotification {
        const alignedOperations: OperationNotification[] = [];
        const alignedBlock: BlockNotification = {
            ...block,
            operations: alignedOperations,
        };

        for (const operation of block.operations) {
            alignedOperations.push({
                ...operation,
                block: alignedBlock,
            });
        }
        return alignedBlock;
    }
}
