import { AbortSignal } from 'abort-controller';
import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions';
import { AsyncIterableProvider, ExternalPubSub, externalTriggers } from '../../../../utils';

@singleton()
export class BlockPubSubProvider implements AsyncIterableProvider<BlockNotification> {
    constructor(private readonly externalPubSub: ExternalPubSub) {}

    iterate(abortSignal: AbortSignal): AsyncIterableIterator<BlockNotification> {
        return this.externalPubSub.iterate([externalTriggers.blocks], abortSignal);
    }
}
