import { singleton } from 'tsyringe';

import { TezosRpcClient } from '../../../rpc/tezos-rpc-client';
import { ExternalPubSub, externalTriggers } from '../../../utils';

interface SimpleBlockHeader {
    hash: string;
    level: number;
}

/** Provides the header of the head block. Usually it comes via PubSub from monitor. */
@singleton()
export class HeadBlockProvider {
    private header: SimpleBlockHeader | undefined;

    constructor(pubSub: ExternalPubSub, private readonly rpcClient: TezosRpcClient) {
        pubSub.subscribe(externalTriggers.blocks, (block) => {
            this.header = {
                hash: block.hash,
                level: block.header.level,
            };
        });
    }

    async getHeader(): Promise<SimpleBlockHeader> {
        if (!this.header) {
            this.header = await this.rpcClient.getHeadBlockHeader();
        }
        return this.header;
    }
}
