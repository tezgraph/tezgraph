import { camelCase } from 'lodash';
import { DependencyContainer } from 'tsyringe';

import { graphQLResolversDIToken } from '../../bootstrap/graphql-resolver-type';
import {
    ActivateAccountArgs,
    ActivateAccountNotification,
    BalanceUpdateArgs,
    BalanceUpdateNotification,
    BallotArgs,
    BallotNotification,
    BigMapDiffArgs,
    BigMapDiffNotification,
    BlockArgs,
    BlockNotification,
    DelegationArgs,
    DelegationNotification,
    DoubleBakingEvidenceArgs,
    DoubleBakingEvidenceNotification,
    DoubleEndorsementEvidenceArgs,
    DoubleEndorsementEvidenceNotification,
    DoublePreendorsementEvidenceArgs,
    DoublePreendorsementEvidenceNotification,
    EndorsementArgs,
    EndorsementNotification,
    EndorsementWithSlotArgs,
    EndorsementWithSlotNotification,
    FilteringArgs,
    getOperationKind,
    getSignature,
    LazyStorageDiffArgs,
    LazyStorageDiffNotification,
    numberOfLastBlocksToDeduplicate,
    numberOfLastMempoolGroupsToDeduplicate,
    OperationArgs,
    OperationKind,
    OperationNotification,
    OriginationArgs,
    OriginationNotification,
    PreendorsementArgs,
    PreendorsementNotification,
    ProposalsArgs,
    ProposalsNotification,
    RegisterGlobalConstantArgs,
    RegisterGlobalConstantNotification,
    ReplayFromBlockArgs,
    RevealArgs,
    RevealNotification,
    SeedNonceRevelationArgs,
    SeedNonceRevelationNotification,
    SetDepositsLimitArgs,
    SetDepositsLimitNotification,
    StorageArgs,
    StorageNotification,
    TransactionArgs,
    TransactionNotification,
} from '../../entity/subscriptions';
import { PubSubTrigger, registerAsyncIterableWorker } from '../../utils';
import { ConvertItemProcessor } from '../../utils/async-iterable/processors/convert-item-processor';
import { DeduplicateItemProcessor } from '../../utils/async-iterable/processors/deduplicate-item-processor';
import { FreezeItemProcessor } from '../../utils/async-iterable/processors/freeze-item-processor';
import { Constructor, DefaultConstructor } from '../../utils/reflection';
import { humanReadable, removeRequiredSuffix } from '../../utils/string-manipulation';
import { Module, ModuleName } from '../module';
import { registerTezosRpcHealthWatcher, RequirePubSubDependency } from '../module-helpers';
import { BalanceUpdateExtractor } from './from-block-extractors/balance-update-extractor';
import { BigMapDiffExtractor } from './from-block-extractors/big-map-diff-extractor';
import { BlockExtractor } from './from-block-extractors/block-extractor';
import { FromBlockExtractor } from './from-block-extractors/extractor';
import { LazyStorageDiffExtractor } from './from-block-extractors/lazy-storage-diff-extractor';
import { OperationExtractor } from './from-block-extractors/operation-extractor';
import { StorageExtractor } from './from-block-extractors/storage-extractor';
import { AssignBlockToOperationsConverter } from './pubsub/block/assign-block-to-operations-converter';
import { BlockInfoProvider } from './pubsub/block/block-info-provider';
import { BlockPubSubProcessor, pubSubPublishersDIToken } from './pubsub/block/block-pubsub-processor';
import { BlockPubSubProvider } from './pubsub/block/block-pubsub-provider';
import { ExtractedItemsPubSubPublisher } from './pubsub/block/extracted-items-pubsub-publisher';
import { OperationsPubSubPublisher } from './pubsub/block/operations-pubsub-publisher';
import { MempoolOperationGroupInfoProvider } from './pubsub/mempool/mempool-operation-group-info-provider';
import { MempoolPubSubProvider } from './pubsub/mempool/mempool-pubsub-provider';
import { MempoolPubSubPublisher } from './pubsub/mempool/mempool-pubsub-publisher';
import { SubscriberPubSub, subscriberTriggers } from './pubsub/subscriber-pub-sub';
import { PubSubSubscription } from './subscriptions/generic/pubsub-subscription';
import {
    registerSubscription,
    SubscriptionRegistrationOptions,
} from './subscriptions/generic/subscription-registration';
import { PubSubOperationSubscription } from './subscriptions/operation-pubsub-subscription';
import { operationsOnBlockResolvers } from './subscriptions/operations-on-block-resolvers';

export const subscriptionsSubscriberModule: Module = {
    name: ModuleName.SubscriptionsSubscriber,

    dependencies: [new RequirePubSubDependency({ reason: 'subscriptions can be created from a PubSub' })],

    initialize(diContainer: DependencyContainer): void {
        operationsOnBlockResolvers.forEach((t) => diContainer.registerInstance(graphQLResolversDIToken, t));

        registerSimpleSubscriptions(diContainer);
        registerOperationSubscriptions(diContainer);
        registerTezosRpcHealthWatcher(diContainer);

        registerBlockPubSubPipeline(diContainer);
        registerMempoolPubSubPipeline(diContainer);
    },
};

function registerSimpleSubscriptions(diContainer: DependencyContainer): void {
    registerSimpleSubscription(diContainer, {
        subscriptionName: 'blockAdded',
        description: 'Subscribes to newly baked *block*-s on Tezos network.',
        itemType: BlockNotification,
        argsType: BlockArgs,
        itemExtractorType: BlockExtractor,
        pubSubTrigger: subscriberTriggers.blocks,
        getItemMetrics: (block) => ({
            operation_group: { received_from_tezos_on: block.received_from_tezos_on },
            block,
        }),
    });
    registerSimpleSubscription(diContainer, {
        subscriptionName: 'bigMapChanged',
        itemType: BigMapDiffNotification,
        argsType: BigMapDiffArgs,
        itemExtractorType: BigMapDiffExtractor,
        pubSubTrigger: subscriberTriggers.bigMapDiffs,
        getItemMetrics: (bigMap) => bigMap.operation,
    });
    registerSimpleSubscription(diContainer, {
        subscriptionName: 'storageChanged',
        itemType: StorageNotification,
        argsType: StorageArgs,
        itemExtractorType: StorageExtractor,
        pubSubTrigger: subscriberTriggers.storages,
        getItemMetrics: (storage) => storage.operation,
    });
    registerSimpleSubscription(diContainer, {
        subscriptionName: 'balanceUpdated',
        itemType: BalanceUpdateNotification,
        argsType: BalanceUpdateArgs,
        itemExtractorType: BalanceUpdateExtractor,
        pubSubTrigger: subscriberTriggers.balanceUpdates,
        getItemMetrics: (balanceUpdate) => balanceUpdate.operation,
    });
    registerSimpleSubscription(diContainer, {
        subscriptionName: 'lazyStorageChanged',
        itemType: LazyStorageDiffNotification,
        argsType: LazyStorageDiffArgs,
        itemExtractorType: LazyStorageDiffExtractor,
        pubSubTrigger: subscriberTriggers.lazyStorageDiff,
        getItemMetrics: (lazyStorageDiff) => lazyStorageDiff.operation,
    });
}

interface SimpleSubscriptionRegistrationOptions<TItem, TArgs>
    extends Omit<
        SubscriptionRegistrationOptions<TItem, TArgs>,
        'resolveItemExtractor' | 'resolveRootSubscription' | 'description'
    > {
    itemExtractorType: Constructor<FromBlockExtractor<TItem>>;
    pubSubTrigger: PubSubTrigger<TItem>;
    description?: string;
}

function registerSimpleSubscription<TItem, TArgs extends FilteringArgs<TItem> & ReplayFromBlockArgs>(
    diContainer: DependencyContainer,
    options: SimpleSubscriptionRegistrationOptions<TItem, TArgs>,
): void {
    registerSubscription(diContainer, {
        ...options,
        description:
            options.description ??
            `Subscribes to \`${options.itemType.name}\`-s from all *operation*-s of newly baked *block*-s on Tezos network.`,
        resolveItemExtractor: () => diContainer.resolve(options.itemExtractorType),
        resolveRootSubscription: () =>
            new PubSubSubscription(diContainer.resolve(SubscriberPubSub), [options.pubSubTrigger]),
    });

    diContainer.register(pubSubPublishersDIToken, {
        useFactory: () =>
            new ExtractedItemsPubSubPublisher<TItem>(
                diContainer.resolve(options.itemExtractorType),
                options.pubSubTrigger,
            ),
    });
}

function registerOperationSubscriptions(diContainer: DependencyContainer): Record<OperationKind, void> {
    diContainer.registerInstance(pubSubPublishersDIToken, new OperationsPubSubPublisher());

    return {
        // Enforces all operations to be handled.
        activate_account: register(ActivateAccountNotification, ActivateAccountArgs),
        ballot: register(BallotNotification, BallotArgs),
        delegation: register(DelegationNotification, DelegationArgs),
        double_baking_evidence: register(DoubleBakingEvidenceNotification, DoubleBakingEvidenceArgs),
        double_endorsement_evidence: register(DoubleEndorsementEvidenceNotification, DoubleEndorsementEvidenceArgs),
        double_preendorsement_evidence: register(
            DoublePreendorsementEvidenceNotification,
            DoublePreendorsementEvidenceArgs,
        ),
        endorsement: register(EndorsementNotification, EndorsementArgs),
        endorsement_with_slot: register(EndorsementWithSlotNotification, EndorsementWithSlotArgs),
        origination: register(OriginationNotification, OriginationArgs),
        preendorsement: register(PreendorsementNotification, PreendorsementArgs),
        proposals: register(ProposalsNotification, ProposalsArgs),
        register_global_constant: register(RegisterGlobalConstantNotification, RegisterGlobalConstantArgs),
        reveal: register(RevealNotification, RevealArgs),
        seed_nonce_revelation: register(SeedNonceRevelationNotification, SeedNonceRevelationArgs),
        set_deposits_limit: register(SetDepositsLimitNotification, SetDepositsLimitArgs),
        transaction: register(TransactionNotification, TransactionArgs),
    };

    function register<TOperation extends OperationNotification, TArgs extends OperationArgs<TOperation>>(
        operationType: DefaultConstructor<TOperation>,
        argsType: DefaultConstructor<TArgs>,
    ): void {
        const operationName = removeRequiredSuffix(operationType.name, 'Notification');

        registerSubscription(diContainer, {
            subscriptionName: `${camelCase(operationName)}Added`,
            description: `Subscribes to *${humanReadable(
                operationName,
            )} operation*-s from newly baked *block*-s and/or mempool on Tezos network.`,
            itemType: operationType,
            argsType,
            resolveItemExtractor: () => new OperationExtractor(getOperationKind(operationType)),
            resolveRootSubscription: () => diContainer.resolve(PubSubOperationSubscription),
            getItemMetrics: (operation) => operation,
        });
    }
}

function registerBlockPubSubPipeline(diContainer: DependencyContainer): void {
    registerAsyncIterableWorker(
        diContainer,
        'BlockSubscriber',
        (c) => c.resolve(BlockPubSubProvider),
        (c) =>
            new DeduplicateItemProcessor(
                numberOfLastBlocksToDeduplicate,
                (b) => b.hash,
                new ConvertItemProcessor(
                    c.resolve(AssignBlockToOperationsConverter),
                    new FreezeItemProcessor(c.resolve(BlockPubSubProcessor)),
                ),
            ),
        BlockInfoProvider,
    );
}

function registerMempoolPubSubPipeline(diContainer: DependencyContainer): void {
    registerAsyncIterableWorker(
        diContainer,
        'MempoolSubscriber',
        (c) => c.resolve(MempoolPubSubProvider),
        (c) =>
            new DeduplicateItemProcessor(
                numberOfLastMempoolGroupsToDeduplicate,
                getSignature,
                new FreezeItemProcessor(c.resolve(MempoolPubSubPublisher)),
            ),
        MempoolOperationGroupInfoProvider,
    );
}
