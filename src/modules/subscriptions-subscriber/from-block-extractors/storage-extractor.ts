import { singleton } from 'tsyringe';

import {
    InternalOperationKind,
    InternalOperationResult,
    MoneyOperationNotification,
    OperationKind,
    OperationNotification,
    StorageNotification,
} from '../../../entity/subscriptions';
import { Nullish } from '../../../utils/reflection';
import { FromOperationExtractor } from './extractor';

/** Extracts all storages from an operation of a block and converts them to notifications. */
@singleton()
export class StorageExtractor extends FromOperationExtractor<StorageNotification> {
    *extractFromOperation(operation: OperationNotification): Iterable<StorageNotification> {
        switch (operation.kind) {
            case OperationKind.delegation:
            case OperationKind.reveal:
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            case OperationKind.origination:
                yield {
                    storage: operation.script.storage,
                    operation,
                    parent: operation,
                };
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            case OperationKind.transaction:
                if (operation.metadata?.operation_result.storage) {
                    yield {
                        storage: operation.metadata.operation_result.storage,
                        operation,
                        parent: operation,
                    };
                }
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            default:
                break;
        }
    }
}

function* fromInternalResults(
    internalResults: Nullish<readonly InternalOperationResult[]>,
    operation: MoneyOperationNotification,
): Iterable<StorageNotification> {
    for (const internalResult of internalResults ?? []) {
        switch (internalResult.kind) {
            case InternalOperationKind.origination:
                yield {
                    storage: internalResult.script.storage,
                    operation,
                    parent: internalResult,
                };
                break;
            case InternalOperationKind.transaction:
                if (internalResult.result.storage) {
                    yield {
                        storage: internalResult.result.storage,
                        operation,
                        parent: internalResult,
                    };
                }
                break;
            default:
                break;
        }
    }
}
