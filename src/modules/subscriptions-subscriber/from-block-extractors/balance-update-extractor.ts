import { singleton } from 'tsyringe';

import {
    BalanceUpdate,
    BalanceUpdateNotification,
    BalanceUpdateParent,
    InternalOperationKind,
    InternalOperationResult,
    OperationKind,
    OperationNotification,
    OperationNotificationWithBalanceUpdates,
} from '../../../entity/subscriptions';
import { Nullish } from '../../../utils/reflection';
import { FromOperationExtractor } from './extractor';

/** Extracts all balance updates from an operation of a block and converts them to notifications. */
@singleton()
export class BalanceUpdateExtractor extends FromOperationExtractor<BalanceUpdateNotification> {
    *extractFromOperation(operation: OperationNotification): Iterable<BalanceUpdateNotification> {
        switch (operation.kind) {
            case OperationKind.activate_account:
            case OperationKind.double_baking_evidence:
            case OperationKind.double_endorsement_evidence:
            case OperationKind.endorsement:
            case OperationKind.endorsement_with_slot:
            case OperationKind.seed_nonce_revelation:
                yield* iterate(operation.metadata?.balance_updates, operation, operation);
                break;
            case OperationKind.delegation:
            case OperationKind.reveal:
                yield* iterate(operation.metadata?.balance_updates, operation, operation);
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            case OperationKind.origination:
            case OperationKind.register_global_constant:
            case OperationKind.transaction:
                yield* iterate(operation.metadata?.balance_updates, operation, operation);
                yield* iterate(operation.metadata?.operation_result.balance_updates, operation, operation);
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            default:
                break;
        }
    }
}

function* fromInternalResults(
    internalResults: Nullish<readonly InternalOperationResult[]>,
    operation: OperationNotificationWithBalanceUpdates,
): Iterable<BalanceUpdateNotification> {
    for (const internalResult of internalResults ?? []) {
        if (
            internalResult.kind === InternalOperationKind.origination ||
            internalResult.kind === InternalOperationKind.transaction
        ) {
            yield* iterate(internalResult.result.balance_updates, operation, internalResult);
        }
    }
}

function* iterate(
    updates: Nullish<readonly BalanceUpdate[]>,
    operation: OperationNotificationWithBalanceUpdates,
    parent: BalanceUpdateParent,
): Iterable<BalanceUpdateNotification> {
    for (const balance_update of updates ?? []) {
        yield { balance_update, operation, parent };
    }
}
