import { BlockNotification, OperationNotification } from '../../../entity/subscriptions';

/** Extracts particular type of the data from a block. */
export interface FromBlockExtractor<TItem> {
    extract(block: BlockNotification): Iterable<TItem>;
}

/** Helper class which helps to extracts particular type of the data from an operation of a block. */
export abstract class FromOperationExtractor<TItem> implements FromBlockExtractor<TItem> {
    *extract(block: BlockNotification): Iterable<TItem> {
        for (const operation of block.operations) {
            yield* this.extractFromOperation(operation);
        }
    }

    abstract extractFromOperation(operation: OperationNotification): Iterable<TItem>;
}
