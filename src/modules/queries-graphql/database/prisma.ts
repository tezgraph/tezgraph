import { PrismaClient } from '@prisma/client';
import { InjectionToken, singleton } from 'tsyringe';

import { PrismaEnvConfig } from './prisma-env-config';

export const prismaClientDIToken: InjectionToken<PrismaClient> = 'prismaClient';

@singleton()
export class PrismaClientFactory {
    constructor(private readonly envConfig: PrismaEnvConfig) {}

    create(): PrismaClient {
        return new PrismaClient({
            datasources: { db: { url: this.envConfig.databaseConnectionString } },
        });
    }
}
