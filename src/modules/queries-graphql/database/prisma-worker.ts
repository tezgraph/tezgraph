import { PrismaClient } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import { BackgroundWorker } from '../../../bootstrap/background-worker';
import { SleepHelper } from '../../../utils';
import { injectLogger, Logger } from '../../../utils/logging';
import DatabaseHealthCheck from './database-health-check';
import { prismaClientDIToken } from './prisma';

export const CONNECTION_RETRY_PERIOD_MILLIS = 30_000; // 30 seconds

@singleton()
export class PrismaDatabaseWorker implements BackgroundWorker {
    readonly name = 'PrismaDatabase';

    constructor(
        @inject(prismaClientDIToken) private readonly prismaClient: PrismaClient,
        @inject(delay(() => DatabaseHealthCheck)) private readonly databaseHealthCheck: DatabaseHealthCheck,
        private readonly sleepHelper: SleepHelper,
        @injectLogger(PrismaDatabaseWorker) private readonly logger: Logger,
    ) {}

    async start(): Promise<void> {
        this.databaseHealthCheck.setupMonitoring();
        await this.connect();
    }

    async stop(): Promise<void> {
        try {
            await this.prismaClient.$disconnect();
        } catch (error: unknown) {
            this.logger.logWarning('Unable to disconnect from database due to {error}.', { error });
        }
    }

    scheduleReconnect(): void {
        void this.sleepHelper.sleep(CONNECTION_RETRY_PERIOD_MILLIS).then(async () => this.connect());
    }

    private async connect(): Promise<boolean> {
        try {
            await this.prismaClient.$connect();
            this.logger.logInformation('Database connected.');
            this.databaseHealthCheck.setConnected();
            return true;
        } catch (error: unknown) {
            this.logger.logWarning('Unable to connect to database due to {error}.', { error });
            this.databaseHealthCheck.setUnableToConnect();
            return false;
        }
    }
}
