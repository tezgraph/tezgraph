import { RpcClient } from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { TezosRpcHealthWatcher } from '../../../rpc/tezos-rpc-health-watcher';
import { isNullish } from '../../../utils';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { HealthStatus, HealthCheck, HealthCheckResult } from '../../../utils/health/health-check';
import IndexerStatusProvider, { IndexerStatus } from './indexer-status-provider';

@singleton()
export default class IndexerHealthCheck implements HealthCheck {
    readonly name = 'IndexerHealth';

    constructor(
        private readonly indexerStatusProvider: IndexerStatusProvider,
        private readonly tezosRpcHealthWatcher: TezosRpcHealthWatcher,
        private readonly envConfig: EnvConfig,
        private readonly rpcClient: RpcClient,
    ) {}

    async checkHealth(): Promise<HealthCheckResult> {
        const { status, lastBlockLevel, lastCheckedAt } = this.indexerStatusProvider;
        switch (status) {
            case IndexerStatus.NOT_CHECKED:
                return {
                    status: HealthStatus.Degraded,
                    data: {
                        reason: 'The last indexed block is not queried yet',
                    },
                    evaluatedOn: lastCheckedAt,
                };
            case IndexerStatus.UNABLE_TO_QUERY:
                return {
                    status: HealthStatus.Unhealthy,
                    data: {
                        reason: `TezGraph's connection with the database is broken`,
                    },
                    evaluatedOn: lastCheckedAt,
                };
            case IndexerStatus.CHECKED: {
                // eslint-disable-next-line @typescript-eslint/init-declarations
                let lastHealthyBlockLevel = this.tezosRpcHealthWatcher.lastHealthyBlock?.level;
                if (isNullish(this.tezosRpcHealthWatcher.lastHealthyBlock?.level)) {
                    const header = await this.rpcClient.getBlockHeader();
                    lastHealthyBlockLevel = header.level;
                }
                if (lastHealthyBlockLevel !== undefined && lastBlockLevel !== null) {
                    let healthStatus = HealthStatus.Healthy;
                    if (lastHealthyBlockLevel - lastBlockLevel > this.envConfig.maxDegradedLagOfIndexedBlocks) {
                        healthStatus = HealthStatus.Unhealthy;
                    } else if (lastHealthyBlockLevel - lastBlockLevel > this.envConfig.maxHealthyLagOfIndexedBlocks) {
                        healthStatus = HealthStatus.Degraded;
                    }
                    return {
                        status: healthStatus,
                        data: {
                            lastIndexedLevel: lastBlockLevel,
                            lag: lastHealthyBlockLevel - lastBlockLevel,
                        },
                        evaluatedOn: lastCheckedAt,
                    };
                }
                return {
                    status: HealthStatus.Degraded,
                    data: {
                        lastIndexedLevel: lastBlockLevel,
                        reason: 'One of last indexed block level or last rpc level is not available',
                    },
                    evaluatedOn: lastCheckedAt,
                };
            }
        }
    }
}
