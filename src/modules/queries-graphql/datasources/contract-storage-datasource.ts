import { DataSource } from 'apollo-datasource';
import DataLoader from 'dataloader';
import { injectable, InjectionToken } from 'tsyringe';

import { Account } from '../../../entity/queries/account-record';
import { Micheline } from '../../../entity/scalars';
import { isNotNullish, isNullish, Nullish } from '../../../utils';
import ContractStorageRepository from '../repositories/contract-storage-repository';

export const contractStorageDataSourceDIToken: InjectionToken<ContractStorageDataSource> = 'contractStorageDataSource';

@injectable()
export class ContractStorageDataSource extends DataSource {
    private readonly loader = new DataLoader<bigint, Nullish<{ address_id: bigint; storage: Micheline }>>(
        async (addressIds) => this.loadContractStorages(addressIds),
    );

    constructor(private readonly contractStorageRepository: ContractStorageRepository) {
        super();
    }

    didEncounterError(error: Error): void {
        throw error;
    }

    async get(account: Account): Promise<{ address_id: bigint; storage: Micheline }> {
        const contractStorage = await this.loader.load(BigInt(account.address_id));
        if (isNullish(contractStorage)) {
            throw new Error(
                `Could not load contract storage data for ${account.address}. This error is to be expected if the address is for an implicit contract since they do not have contract storage.`,
            );
        }
        return contractStorage;
    }

    private async loadContractStorages(
        ids: readonly bigint[],
    ): Promise<Nullish<{ address_id: bigint; storage: Micheline }>[]> {
        const contractStorages = await this.contractStorageRepository.findManyCurrentContractStorageFromDB(ids);
        return ids.map((id) =>
            contractStorages?.find(
                (contractStorage) =>
                    isNotNullish(contractStorage.address_id) && BigInt(contractStorage.address_id) === id,
            ),
        );
    }
}
