/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/unbound-method */
import { Prisma, PrismaClient } from '@prisma/client';
import { merge } from 'lodash';
import { inject, singleton } from 'tsyringe';

import { Block, BlockFilter } from '../../../entity/queries/block-record';
import { isNotNullish, isNullish } from '../../../utils';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult, getAggregations } from '../helpers/aggregation';
import { PaginationArgs } from '../helpers/pagination';
import { sortPaginateAndRunQuery } from '../helpers/prisma-pagination';
import { OrderByDirection } from '../helpers/types';
import { BlockArgs } from '../resolvers/block-resolver';
import { BlockOrderByField, blockOrderByInfo, BlockOrderByInput } from '../resolvers/types/block-order-by';

export interface FindBlockResult {
    level: number;
    hash: string;
    timestamp: Date;
}

interface BlockModel {
    level: number;
    hash: string;
    timestamp: Date;
}

export interface FindBlockByIdResult {
    level: number;
    timestamp: Date;
    hash: string;
    hash_id: number;
}

@singleton()
export default class BlockRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        private readonly envConfig: EnvConfig,
    ) {}

    async getBlocks({
        filter,
        order_by: orderBy,
        aggregationInput,
        ...pagination
    }: BlockArgs): Promise<{ data: Block[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const findManyBlocksOptions = this.createFindManyBlockOptions({ filter });

        const dataPromise = this.sortPaginateAndRunBlockQuery({
            prismaFindManyArgs: findManyBlocksOptions,
            orderBy,
            pagination,
        });
        const aggregationPromise = getAggregations<Prisma.blockCountArgs, Prisma.blockWhereInput>({
            aggregationInput,
            prismaFindManyArgs: { where: findManyBlocksOptions.where },
            prismaCountFunction: this.prisma.block.count,
        });

        const [{ data: blocks, hasMorePages }, aggregationResult] = await Promise.all([
            dataPromise,
            aggregationPromise,
        ]);
        const data = blocks.map((blk) => ({
            timestamp: blk.timestamp,
            level: blk.level,
            hash: blk.hash,
        }));
        return { data, hasMorePages, aggregationResult };
    }

    async findByLevel(level: number): Promise<FindBlockResult | null> {
        const block = await this.prisma.block.findFirst({
            where: {
                level,
            },
            select: {
                hash: true,
                timestamp: true,
            },
        });
        if (!block) {
            return null;
        }
        return {
            hash: block.hash,
            level,
            timestamp: block.timestamp,
        };
    }

    async findByHash(hash: string): Promise<FindBlockResult | null> {
        const block = await this.prisma.block.findFirst({
            where: {
                hash,
            },
            select: {
                level: true,
                timestamp: true,
            },
        });
        if (!block) {
            return null;
        }
        return {
            hash,
            level: block.level,
            timestamp: block.timestamp,
        };
    }

    async getBlocksByHashIds(hashIds: number[]): Promise<FindBlockByIdResult[]> {
        const blocks = await this.prisma.block.findMany({
            where: {
                level: { in: hashIds },
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
        });
        return blocks.map((block) => ({
            level: block.level,
            hash: block.hash,
            hash_id: block.level,
            timestamp: block.timestamp,
        }));
    }

    async findByHashOrLevel({ hash, level }: Pick<Block, 'hash' | 'level'>): Promise<FindBlockResult> {
        if (isNotNullish(hash)) {
            const data = await this.findByHash(hash);
            if (isNullish(data)) {
                throw new Error(`The provided block hash ${hash} does not exist`);
            }
            return data;
        } else if (isNotNullish(level)) {
            const data = await this.findByLevel(level);
            if (isNullish(data)) {
                throw new Error(`The provided block level ${level} does not exist`);
            }
            return data;
        }
        throw new Error('When creating a block, at least one of hash and level should be provided');
    }

    async findLastIndexedBlockWithContractBalance(): Promise<Block | null> {
        /*
         * The following comments only apply when the indexer is set to index contract balances.
         * A block is considered to be fully indexed when the c.contract_balance rows related to the block has no null values in the balance column. Otherwise, the block data is still being indexed.
         * Here we check to see if there are c.contract_balance rows with a null value in the balance column.
         * If there are such rows, we can find the latest fully indexed block by taking the smallest block level from these rows and minusing 1.
         */
        const [minBlockHashIdWithContractBalanceInProgress, latestBlockLevelWithContractBalance] = await Promise.all([
            this.findMinBlockHashIdWithContractBalanceInProgress(),
            this.findLatestBlockHashIdWithContractBalance(),
        ]);
        const minBlockLevelInProgress: number | null = minBlockHashIdWithContractBalanceInProgress[0].min;
        /*
         * If there are no rows in the c.contract_balance table with null values in the balance column, then the latest fully indexed block would be the largest block level from c.contract_balance.
         */
        const latestBlockLevel: number = latestBlockLevelWithContractBalance[0].max;

        const block = await this.prisma.block.findFirst({
            orderBy: {
                level: 'desc',
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
            where: {
                level: isNotNullish(minBlockLevelInProgress) ? minBlockLevelInProgress - 1 : latestBlockLevel,
            },
        });

        if (isNullish(block)) {
            return null;
        }
        return {
            hash: block.hash,
            level: block.level,
            timestamp: block.timestamp,
        };
    }

    async findLastIndexedBlock(): Promise<Block | null> {
        const block = await this.prisma.block.findFirst({
            orderBy: {
                level: 'desc',
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
        });

        if (isNullish(block)) {
            return null;
        }
        return {
            hash: block.hash,
            level: block.level,
            timestamp: block.timestamp,
        };
    }

    async findLastBlock(): Promise<Block | null> {
        if (this.envConfig.resolveContractBalanceFromDB) {
            return await this.findLastIndexedBlockWithContractBalance();
        }
        return this.findLastIndexedBlock();
    }

    private async findLatestBlockHashIdWithContractBalance(): Promise<[{ max: number }]> {
        return this.prisma.$queryRaw`select max(block_level) from c.contract_balance cb;`;
    }

    private async findMinBlockHashIdWithContractBalanceInProgress(): Promise<[{ min: number | null }]> {
        return this.prisma.$queryRaw`select min(block_level) from c.contract_balance cb where balance is null`;
    }

    private async sortPaginateAndRunBlockQuery<T extends Prisma.blockFindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.blockFindManyArgs>;
        orderBy: BlockOrderByInput | undefined;
        pagination: PaginationArgs;
    }): Promise<{ data: BlockModel[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            BlockModel,
            Prisma.blockFindManyArgs,
            T,
            BlockOrderByField,
            Prisma.blockGetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? {
                field: BlockOrderByField.level,
                direction: OrderByDirection.desc,
            },
            sortInfo: blockOrderByInfo,
            pagination,
            maxPageSize: 100,
            prismaFindManyFunction: this.prisma.block.findMany,
            getCursorObject: async (cursor) => this.getFromCursor(cursor),
        });
    }

    private async getFromCursor(cursor: string): Promise<FindBlockResult> {
        const blockObject = await this.prisma.block.findFirst({
            where: {
                hash: cursor,
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
        });
        if (isNullish(blockObject)) {
            throw new Error(`No block found for cursor ${cursor}`);
        }
        return {
            hash: blockObject.hash,
            level: blockObject.level,
            timestamp: blockObject.timestamp,
        };
    }

    private createFindManyBlockOptions({ filter }: { filter?: BlockFilter }): Prisma.blockFindManyArgs {
        let where: Prisma.blockWhereInput = {};
        if (filter?.hashes) {
            where = merge(
                where,
                Prisma.validator<Prisma.blockWhereInput>()({
                    hash: {
                        in: filter.hashes,
                    },
                }),
            );
        }

        if (isNotNullish(filter?.level?.gte) || isNotNullish(filter?.level?.lte)) {
            where = merge(
                where,
                Prisma.validator<Prisma.blockWhereInput>()({
                    level: filter?.level,
                }),
            );
        }

        if (isNotNullish(filter?.timestamp?.gte) || isNotNullish(filter?.timestamp?.lte)) {
            where = merge(
                where,
                Prisma.validator<Prisma.blockWhereInput>()({
                    timestamp: filter?.timestamp,
                }),
            );
        }
        return {
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
            where,
        };
    }
}
