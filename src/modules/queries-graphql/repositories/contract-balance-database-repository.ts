import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { AccountRecord } from '../../../entity/queries/account-record';
import { DelegationRecord } from '../../../entity/queries/delegation-record';
import { isNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';

@singleton()
export default class ContractBalanceDatabaseRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async findBalanceByAddressAndBlockLevel(delegation: DelegationRecord): Promise<bigint | null> {
        if (isNullish(delegation.senderAddressID)) {
            return null;
        }

        const res = await this.prisma.contract_balance.findUnique({
            where: {
                address_id_block_level: {
                    address_id: delegation.senderAddressID,
                    block_level: delegation.block.level,
                },
            },
            select: {
                balance: true,
            },
        });

        return res?.balance ?? null;
    }

    async findBalanceByAddressId(account: AccountRecord): Promise<bigint | null> {
        const res = await this.prisma.contract_balance.findFirst({
            where: {
                address_id: account.address_id,
                NOT: [{ balance: null }],
            },
            orderBy: {
                block_level: 'desc',
            },
            select: {
                balance: true,
                block_level: true,
            },
            take: 1,
        });

        /*
         * Prior to this method, the `account` parameter would have an address_id retrieved from the database, ensuring that the address is recorded in the database.
         * If there are no contract balance results from the database, that means that there were no balance updates. Therefore the balance is 0.
         */

        if (isNullish(res)) {
            return 0n;
        }
        return res.balance;
    }
}
