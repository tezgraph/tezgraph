import { bigmap, PrismaClient } from '@prisma/client';
import { bytes2Char, validateAddress, ValidationResult } from '@taquito/utils';
import { inject, singleton } from 'tsyringe';

import { Account } from '../../../entity/queries/account-record';
import {
    BigmapRecordWithMetadataValue,
    ContractMetadata,
    ViewDefinition,
} from '../../../entity/queries/contract-metadata';
import { isNotNullish, isNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { findMetadataStorageString, stringIsInteger } from '../helpers/contract-metadata';
import { ContractMetadataArgs } from '../resolvers/account-fields-resolver';
import { BigmapRecordKind } from '../resolvers/types/bigmap-kind';
import AddressRepository from './address-repository';
import TaquitoRepository from './taquito-repository';

@singleton()
export default class ContractMetadataRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        private readonly addressRepository: AddressRepository,
        @inject(TaquitoRepository) private readonly taquitoRepository: TaquitoRepository,
    ) {}

    async findByAccount(account: Account, args: ContractMetadataArgs): Promise<ContractMetadata | null> {
        const metadataBigmaps = await this.findContractMetadataBigmaps(account);
        if (isNullish(metadataBigmaps)) {
            return null;
        }

        const metadataStorageString = findMetadataStorageString(metadataBigmaps);
        if (isNullish(metadataStorageString)) {
            return null;
        }

        let contractMetadata: ContractMetadata | null = null;

        if (metadataStorageString.startsWith('tezos-storage')) {
            contractMetadata =
                (await this.findContractMetadataFromStorage(metadataStorageString, metadataBigmaps, account.address)) ??
                null;
        } else {
            contractMetadata =
                (await this.taquitoRepository.findContractMetadataFromTaquito(
                    metadataStorageString,
                    account.address,
                )) ?? null;
        }

        if (args.view_names && contractMetadata) {
            contractMetadata.views = this.findContractMetadataViewByViewName(args.view_names, contractMetadata.views);
        }

        return contractMetadata;
    }

    async findContractMetadataBigmaps(account: Account): Promise<bigmap[] | null> {
        const bigmapWithMetadataAnnot = await this.prisma.bigmap.findFirst({
            where: {
                annots: '%metadata',
                receiver_id: account.address_id,
                kind: BigmapRecordKind.alloc,
            },
        });

        if (isNullish(bigmapWithMetadataAnnot)) {
            return null;
        }

        const otherBigmapsUnderSameId = await this.prisma.bigmap.findMany({
            where: {
                NOT: { kind: BigmapRecordKind.alloc },
                id: bigmapWithMetadataAnnot.id,
            },
            orderBy: { operation_id: 'desc' },
        });

        return otherBigmapsUnderSameId;
    }

    async getOtherContractAddressIdAndStorageKey(metadataStorageString: string): Promise<string[]> {
        const contractAddressWithStorageKeyString = metadataStorageString.replace('tezos-storage://', '');
        const contractAddressArr = contractAddressWithStorageKeyString.split('/');
        const contractAddress = contractAddressArr[0];
        const storageKey = contractAddress
            ? contractAddressWithStorageKeyString.substring(contractAddress.length + 1)
            : undefined;
        if (!contractAddress || !storageKey || validateAddress(contractAddress) !== ValidationResult.VALID) {
            throw new Error(
                `Error occurred while parsing contract address and storage key from tezos storage reference for ${metadataStorageString}.`,
            );
        }
        const { data: addresses } = await this.addressRepository.getAddresses({
            first: 1,
            filter: { addresses: [contractAddress] },
        });
        const contractRefAddressId = addresses[0]?.address_id.toString();

        if (contractRefAddressId === undefined) {
            throw new Error(
                `Unable to find address_id while retrieving contract metadata from tezos storage for ${contractAddress}`,
            );
        }
        return [contractRefAddressId, storageKey];
    }

    async findContractMetadataFromOtherContractStorage(
        metadataStorageString: string,
        address: string,
    ): Promise<ContractMetadata | null> {
        const [otherContractAddressId, storageKey] = await this.getOtherContractAddressIdAndStorageKey(
            metadataStorageString,
        );

        if (!otherContractAddressId || !storageKey) {
            throw new Error(
                `Error occurred while parsing contract address and storage key from tezos storage reference for ${metadataStorageString}.`,
            );
        }

        if (!stringIsInteger(otherContractAddressId)) {
            throw new Error(
                `Error occurred while attempting to retrieve contract metadata from another contract's storage. The other contract's storage id is not an integer.`,
            );
        }

        const metadataBigmaps = await this.findContractMetadataBigmaps({
            address,
            address_id: BigInt(otherContractAddressId),
        });
        if (isNullish(metadataBigmaps)) {
            return null;
        }

        return this.findContractMetadataFromStorage(`tezos-storage:${storageKey}`, metadataBigmaps, address);
    }

    async findContractMetadataFromCurrentContractStorage(
        metadataStorageString: string,
        bigmapMetadataArr: bigmap[],
        address: string,
    ): Promise<ContractMetadata | null> {
        const tezosStorageKey = metadataStorageString.replace('tezos-storage:', ''); // Removes 'tezos-storage:'

        const bigmapRecordWithMetadata = bigmapMetadataArr.find((obj) => {
            return (obj.key as Record<string, unknown>).string === tezosStorageKey;
        });

        if (bigmapRecordWithMetadata?.strings[0] === undefined) {
            const contractMetadataFromTaquito = await this.taquitoRepository.findContractMetadataFromTaquito(
                metadataStorageString,
                address,
            );
            return contractMetadataFromTaquito;
        }

        const contractMetadataBytes = (bigmapRecordWithMetadata.value as BigmapRecordWithMetadataValue).bytes;

        if (isNullish(contractMetadataBytes)) {
            return null;
        }

        const contractMetadata = JSON.parse(bytes2Char(contractMetadataBytes)) as object;

        return {
            ...contractMetadata,
            raw: contractMetadata,
        };
    }

    async findContractMetadataFromStorage(
        metadataStorageString: string,
        bigmapMetadataArr: bigmap[],
        address: string,
    ): Promise<ContractMetadata | null> {
        if (metadataStorageString.startsWith('tezos-storage://')) {
            return this.findContractMetadataFromOtherContractStorage(metadataStorageString, address);
        }
        return this.findContractMetadataFromCurrentContractStorage(metadataStorageString, bigmapMetadataArr, address);
    }

    private findContractMetadataViewByViewName(
        view_names: string[],
        availableViews: ViewDefinition[] | undefined | null,
    ): ViewDefinition[] {
        if (isNullish(availableViews) || view_names.length === 0 || availableViews.length === 0) {
            return [];
        }
        return view_names
            .map<ViewDefinition | undefined>((name) => availableViews.find((view) => view.name === name))
            .filter(isNotNullish);
    }
}
