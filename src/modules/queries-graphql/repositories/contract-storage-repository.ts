import { PrismaClient } from '.prisma/client';
import { inject, singleton } from 'tsyringe';

import { Micheline } from '../../../entity/scalars';
import { prismaClientDIToken } from '../database/prisma';

@singleton()
export default class ContractStorageRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async findManyCurrentContractStorageFromDB(
        addressIds: readonly bigint[],
    ): Promise<{ address_id: bigint; storage: Micheline }[] | null> {
        const result: { address_id: bigint; storage: Micheline }[] | null = await this.prisma.$queryRawUnsafe(`
        select
        cs.address_id,
        coalesce ((select storage from c.tx t where t.destination_id = cs.address_id and storage is not null order by t.operation_id desc limit 1), script->'storage') as storage
        from c.contract_script cs
        where cs.address_id in (${addressIds.toString()})`);
        return result ?? null;
    }
}
