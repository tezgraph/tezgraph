import { inject, singleton } from 'tsyringe';

import { AccountRecord } from '../../../entity/queries/account-record';
import { DelegationRecord } from '../../../entity/queries/delegation-record';
import { isNullish } from '../../../utils';
import BlockRepository from './block-repository';
import TaquitoRepository from './taquito-repository';

@singleton()
export default class ContractBalanceTaquitoRepository {
    constructor(
        @inject(TaquitoRepository) private readonly taquitoRepository: TaquitoRepository,
        private readonly blockRepository: BlockRepository,
    ) {}

    async findBalanceByAddressAndBlockLevel(delegation: DelegationRecord): Promise<bigint | null> {
        const blockHash =
            delegation.block.hash ?? (await this.blockRepository.findByHashOrLevel(delegation.block)).hash;

        if (isNullish(delegation.senderAddress)) {
            return null;
        }

        return await this.taquitoRepository.getAccountBalanceAtBlockHashFromTaquito({
            block_hash: blockHash,
            source_address: delegation.senderAddress,
        });
    }

    async findBalanceByAddressId(account: AccountRecord): Promise<bigint | null> {
        const currentHeadBlockHash = (await this.taquitoRepository.getCurrentHeadBlockLevel()).hash;
        const accountBalanceAtCurrentLevel = await this.taquitoRepository.getAccountBalanceAtBlockHashFromTaquito({
            source_address: account.address,
            block_hash: currentHeadBlockHash,
        });
        return accountBalanceAtCurrentLevel;
    }
}
