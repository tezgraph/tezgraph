/* eslint-disable @typescript-eslint/restrict-template-expressions */

import { singleton } from 'tsyringe';

import { AggregationResult } from '../helpers/aggregation';
import { QueryInfo } from '../helpers/query-builder';
import { BigmapMetadata } from '../helpers/query-metadata';
import { OrderByDirection } from '../helpers/types';
import { BigmapOrderByField, BigmapSelection } from '../resolvers/types/bigmap-filters';
import AggregationUtils from '../utils/AggregationUtils';
import PaginationUtils from '../utils/PaginationUtils';
import { FindManyBigmapsResult } from './bigmap-repository';
import BlockRepository from './block-repository';
import { DataFetchOptions } from './data-fetch-options';

@singleton()
export default class BigmapRawRepository {
    constructor(
        private readonly blockRepository: BlockRepository,
        private readonly aggregationUtils: AggregationUtils,
        private readonly paginationUtils: PaginationUtils,
    ) {}

    async findManyBigmaps(
        args: BigmapSelection,
        options?: DataFetchOptions,
    ): Promise<{ data: FindManyBigmapsResult[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const { data: bigmaps, hasMorePages, aggregationResult } = await this.buildAndRunQuery(args, options);
        const data = await this.fetchAndSetRelatedData(bigmaps);
        return {
            data,
            hasMorePages,
            aggregationResult,
        };
    }

    private async buildAndRunQuery(
        args: BigmapSelection,
        options?: DataFetchOptions,
    ): Promise<{ data: FindManyBigmapsResult[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const query = new QueryInfo<FindManyBigmapsResult, BigmapOrderByField>(BigmapMetadata, this.paginationUtils);
        this.applyFilterToQuery(query, args);
        const rowsQuery = query.clone<FindManyBigmapsResult>();
        rowsQuery.addSelectColumns(
            ['bigmap'],
            [
                `bigmap.id`,
                `bigmap.annots`,
                `bigmap.sender_id`,
                `bigmap.receiver_id`,
                `bigmap.key_type`,
                `bigmap.value_type`,
                `bigmap.block_level`,
                `bigmap.i`,
                `bigmap.operation_id`,
            ],
        );

        const dataPromise = rowsQuery.run(
            args.order_by ?? {
                field: BigmapOrderByField.id,
                direction: OrderByDirection.asc,
            },
            args,
            options,
        );
        const aggregationPromise = this.aggregationUtils.buildAndRunAggregationsQuery<BigmapOrderByField>({
            args,
            query,
            requiredTables: ['bigmap'],
            columns: ['count(*) as "totalCount"'],
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    private applyFilterToQuery(
        query: QueryInfo<FindManyBigmapsResult, BigmapOrderByField>,
        args: BigmapSelection,
    ): void {
        if (args.filter?.annots !== undefined) {
            query.appendWhere('bigmap', query.sql`bigmap.annots = Any(${args.filter.annots})`);
        }
        if (args.filter?.ids !== undefined) {
            // The casts are workarounds for a Prisma bug, see https://github.com/prisma/prisma/issues/10424
            query.appendWhere('bigmap', query.sql`bigmap.id = Any(${args.filter.ids}::text[]::bigint[])`);
        }
        if (args.filter?.contract !== undefined) {
            query.appendWhere('receiver', query.sql`receiver.address = Any(${args.filter.contract})`);
        }
    }

    private async fetchAndSetRelatedData(bigmaps: FindManyBigmapsResult[]): Promise<FindManyBigmapsResult[]> {
        const blocksIds = [...new Set(bigmaps.map((bigmap) => bigmap.block_level))];
        const blocks = await this.blockRepository.getBlocksByHashIds(blocksIds);

        return bigmaps.map((item) => ({
            block_hash: blocks.find((b) => b.hash_id === item.block_level)?.hash,
            ...item,
        }));
    }
}
