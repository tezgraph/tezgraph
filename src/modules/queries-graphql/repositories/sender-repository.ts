import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';

export interface SenderResult {
    operation_id: bigint;
    sender_id: bigint;
    sender_address: string;
}

@singleton()
export default class SenderRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<SenderResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const senders = await this.prisma.operation_sender_and_receiver.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                sender_id: true,
                address_addressTooperation_sender_and_receiver_sender_id: {
                    select: {
                        address: true,
                    },
                },
            },
        });
        return senders.map((sender) => ({
            operation_id: sender.operation_id,
            sender_id: sender.sender_id,
            sender_address: sender.address_addressTooperation_sender_and_receiver_sender_id.address,
        }));
    }
}
