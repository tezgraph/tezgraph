import { Prisma, PrismaClient } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import {
    OriginationRecord,
    OriginationRecordMetadata,
    OriginationResultRecord,
    OriginationRecordData,
} from '../../../entity/queries/origination-record';
import { OperationKind } from '../../../entity/subscriptions';
import { prismaClientDIToken } from '../database/prisma';
import { OperationMapper } from '../helpers/operation-mapper';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { FindManyOperationsResult } from './operation-alpha-repository';

export interface FindOriginationByIdResult {
    operation_id: bigint;
    origination_k_address: string | null;
    origination_delegate_address: string | null;
    origination_delegate_id: bigint | null;
    consumed_milligas: Prisma.Decimal | null;
    fee: bigint;
    status: number;
    error_trace: Prisma.JsonValue | null;
    origination_balance: bigint;
    storage_size: Prisma.Decimal | null;
}

@singleton()
export default class OriginationRepository {
    constructor(
        @inject(delay(() => OperationMapper))
        private readonly operationUtils: OperationMapper,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
    ) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<FindOriginationByIdResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const originations = await this.prisma.origination.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                consumed_milligas: true,
                fee: true,
                status: true,
                error_trace: true,
                credit: true,
                storage_size: true,
                delegate_id: true,
                address_addressToorigination_delegate_id: {
                    select: {
                        address: true,
                    },
                },
                address_addressToorigination_k_id: {
                    select: {
                        address: true,
                    },
                },
            },
        });
        return originations.map((origination) => ({
            operation_id: origination.operation_id,
            origination_k_address: origination.address_addressToorigination_k_id?.address ?? null,
            origination_delegate_address: origination.address_addressToorigination_delegate_id?.address ?? null,
            origination_delegate_id: origination.delegate_id ?? null,
            consumed_milligas: origination.consumed_milligas,
            fee: origination.fee,
            status: origination.status,
            error_trace: origination.error_trace,
            origination_balance: origination.credit,
            storage_size: origination.storage_size ?? null,
        }));
    }

    mapResultToEntity(result: FindManyOperationsResult): OriginationRecordData {
        if (result.operation_kind !== OperationRecordKind.origination) {
            throw new Error(
                `Invalid origination.mapResultToEntity invocation for operation kind: ${result.operation_kind}`,
            );
        }

        return {
            kind: OperationKind.origination,
            graphQLTypeName: OriginationRecord.name,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            metadata: this.operationUtils.mapResultToOperationMetadata(result, {
                metadataTypeName: OriginationRecordMetadata.name,
                resultTypeName: OriginationResultRecord.name,
            }),
            contract_address: result.origination_k_address,
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            fee: result.fee!,
            storage_size: result.storage_size ?? null,
            delegate_address: result.origination_delegate_address,
            delegate_id: result.origination_delegate_id,
            balance: result.origination_balance,
            burned: null,
        };
    }
}
