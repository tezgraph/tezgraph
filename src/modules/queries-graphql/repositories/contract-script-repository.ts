import { Prisma, PrismaClient } from '.prisma/client';
import { inject, singleton } from 'tsyringe';

import { Account } from '../../../entity/queries/account-record';
import { ContractScript } from '../../../entity/queries/contract-script';
import { Micheline } from '../../../entity/scalars';
import { injectLogger, isNullish, Logger, Nullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import MichelsonParser from './michelson-parser';

export interface ContractSource {
    code: Micheline;
    storage: Micheline;
}

@singleton()
export default class ContractScriptRepository {
    constructor(
        private readonly michelsonParser: MichelsonParser,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @injectLogger(ContractScriptRepository) private readonly logger: Logger,
    ) {}

    async getContractScript(account: Account): Promise<ContractScript | null> {
        const contractSource = await this.getContractSourceFromDB(account.address_id);
        if (contractSource === null) {
            return null;
        }

        /*
         * If currentStorage === {prim: "Unit"}, then the storage is empty (https://opentezos.com/michelson/smart-contracts/).
         * When the storage is empty, the decodedStorage will return `Symbol()`. This empty Symbol will cause GraphQL to not return the key value pair from the results.
         * To avoid confusion for the users we are returning an empty object.
         */
        if (JSON.stringify(contractSource.storage) === `{"prim":"Unit"}`) {
            return {
                code: contractSource.code,
                storage: {},
                storage_micheline_json: this.michelsonParser.convertMichelsonJsonToCanonicalForm(
                    contractSource.storage,
                ),
                storage_michelson: this.michelsonParser.convertMichelsonJsonToMichelsonString(contractSource.storage),
            };
        }
        const decodedStorage =
            await this.michelsonParser.convertContractStorageFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
                account.address,
                contractSource.storage,
            );
        return {
            code: contractSource.code,
            storage: decodedStorage,
            storage_micheline_json: this.michelsonParser.convertMichelsonJsonToCanonicalForm(contractSource.storage),
            storage_michelson: this.michelsonParser.convertMichelsonJsonToMichelsonString(contractSource.storage),
        };
    }

    async getContractSourceFromDB(addressId: bigint): Promise<ContractSource | null> {
        const data = await this.prisma.contract_script.findUnique({
            where: {
                address_id: addressId,
            },
            select: {
                script: true,
            },
        });
        return this.mapContractScriptToContractSource(addressId, data?.script);
    }

    private mapContractScriptToContractSource(
        addressId: bigint,
        contractScript: Nullish<Prisma.JsonValue>,
    ): ContractSource | null {
        if (isNullish(contractScript) || typeof contractScript !== 'object' || Array.isArray(contractScript)) {
            this.logger.logWarning(
                'Getting contract_script for address {{addressId}} returned unexpected type of data.',
                { addressId, contractScript },
            );
            return null;
        }

        if (isNullish(contractScript.code) || isNullish(contractScript.storage)) {
            this.logger.logWarning(
                'Data of contract_script for address {{addressId}} has missing `code` or `storage` properties.',
                { addressId, contractScript },
            );
            return null;
        }

        return {
            code: contractScript.code as Micheline,
            storage: contractScript.storage as Micheline,
        };
    }
}
