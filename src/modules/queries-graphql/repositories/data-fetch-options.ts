export interface DataFetchOptions {
    bypassPageSizeLimit?: boolean;
}
