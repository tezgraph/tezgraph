import { singleton } from 'tsyringe';

import { isNotNullish } from '../../../utils';
import { AggregationResult } from '../helpers/aggregation';
import { QueryInfo } from '../helpers/query-builder';
import { BigmapKeyMetadata } from '../helpers/query-metadata';
import { OrderByDirection } from '../helpers/types';
import { BigmapKeyOrderByFieldInternal, BigmapKeySelectionInBigmap } from '../resolvers/types/bigmap-filters';
import AggregationUtils from '../utils/AggregationUtils';
import PaginationUtils from '../utils/PaginationUtils';
import { BigmapKeyParentInfo, FindManyBigmapKeyResult } from './bigmap-key-repository';

@singleton()
export default class BigmapKeyRawRepository {
    constructor(
        private readonly aggregationUtils: AggregationUtils,
        private readonly paginationUtils: PaginationUtils,
    ) {}

    async findManyBigmapKeys(
        parentInfo: BigmapKeyParentInfo,
        args: BigmapKeySelectionInBigmap,
    ): Promise<{ data: FindManyBigmapKeyResult[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const query = this.getFilter(parentInfo, args);
        const rowsQuery = query.clone<FindManyBigmapKeyResult>();
        rowsQuery.addSelectColumns('bigmap', [
            `DISTINCT ON (bigmap.id, bigmap.key) bigmap.id, bigmap.key, bigmap.key_hash`,
        ]);

        const dataPromise = rowsQuery.run(
            args.order_by ?? {
                field: BigmapKeyOrderByFieldInternal.key,
                direction: OrderByDirection.asc,
            },
            args,
        );
        const aggregationPromise = this.aggregationUtils.buildAndRunAggregationsQuery<BigmapKeyOrderByFieldInternal>({
            args,
            query,
            requiredTables: ['bigmap'],
            columns: ['count(distinct key) as "totalCount"'],
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    private getFilter(
        parentInfo: BigmapKeyParentInfo,
        args: BigmapKeySelectionInBigmap,
    ): QueryInfo<FindManyBigmapKeyResult, BigmapKeyOrderByFieldInternal> {
        const query = new QueryInfo<FindManyBigmapKeyResult, BigmapKeyOrderByFieldInternal>(
            BigmapKeyMetadata,
            this.paginationUtils,
        );
        if (isNotNullish(parentInfo.bigmap_id)) {
            query.appendWhere('bigmap', query.sql`bigmap.id = ${parentInfo.bigmap_id}`);
        }
        if ((args.filter?.keys?.length ?? 0) > 0) {
            query.appendWhere('bigmap', query.sql`bigmap.key = Any(${args.filter?.keys})`);
        }
        return query;
    }
}
