/* eslint-disable @typescript-eslint/unbound-method */

import { Prisma, PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { BigmapOperationKind } from '../../../entity/queries/bigmap-operation-kind';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult, getAggregations } from '../helpers/aggregation';
import { BigmapSelection } from '../resolvers/types/bigmap-filters';
import BigmapPaginationUtils from '../utils/BigmapPaginationUtils';
import BlockRepository from './block-repository';
import { DataFetchOptions } from './data-fetch-options';

export interface FindManyBigmapsResult {
    id?: bigint | null;
    sender_id?: bigint | null;
    receiver_id?: bigint | null;
    key_type: Prisma.JsonValue;
    value_type: Prisma.JsonValue;
    annots?: string | null;
    block_level: number;
    i: bigint;
    block_hash?: string;
    operation_id?: bigint | null;
}

@singleton()
export default class BigmapRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        private readonly blockRepository: BlockRepository,
        private readonly bigmapPaginationUtils: BigmapPaginationUtils,
    ) {}

    async findManyBigmaps(
        args: BigmapSelection,
        options?: DataFetchOptions,
    ): Promise<{ data: FindManyBigmapsResult[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const findManyBigmapsOptions = this.getFindManyBigmapOptions(args);

        const dataPromise = this.getBigmapRows({ findManyBigmapsOptions, args, options });
        const aggregationPromise = getAggregations<Prisma.bigmapCountArgs, Prisma.bigmapWhereInput | undefined>({
            prismaCountFunction: this.prisma.bigmap.count,
            prismaFindManyArgs: findManyBigmapsOptions,
            aggregationInput: args.aggregationInput,
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    private async getBigmapRows({
        findManyBigmapsOptions,
        args,
        options,
    }: {
        findManyBigmapsOptions: Prisma.bigmapFindManyArgs;
        args: BigmapSelection;
        options?: DataFetchOptions;
    }): Promise<{ data: FindManyBigmapsResult[]; hasMorePages: boolean }> {
        const { data: rawData, hasMorePages } = await this.bigmapPaginationUtils.sortPaginateAndRunBigmapQuery({
            prismaFindManyArgs: findManyBigmapsOptions,
            orderBy: args.order_by,
            pagination: args,
            options,
        });
        const data = await this.fetchAndSetRelatedData(rawData);
        return { data, hasMorePages };
    }

    private async fetchAndSetRelatedData(bigmaps: FindManyBigmapsResult[]): Promise<FindManyBigmapsResult[]> {
        const blocksIds = [...new Set(bigmaps.map((bigmap) => bigmap.block_level))];
        const blocks = await this.blockRepository.getBlocksByHashIds(blocksIds);

        return bigmaps.map((item) => ({
            block_hash: blocks.find((b) => b.hash_id === item.block_level)?.hash,
            ...item,
        }));
    }

    private getFindManyBigmapOptions(args: BigmapSelection): Prisma.bigmapFindManyArgs {
        const isBigmapAllocation = {
            kind: { not: BigmapOperationKind.clear },
            key_type: { not: Prisma.JsonNullValueFilter.AnyNull },
        };
        return {
            where: {
                annots: args.filter?.annots && {
                    in: args.filter.annots,
                },
                address_addressTobigmap_receiver_id: args.filter?.contract && {
                    address: { in: args.filter.contract },
                },
                id: args.filter?.ids && {
                    in: args.filter.ids,
                },
                ...isBigmapAllocation,
            },
            select: {
                id: true,
                annots: true,
                sender_id: true,
                receiver_id: true,
                key_type: true,
                value_type: true,
                block_level: true,
                i: true,
                operation_id: true,
            },
        };
    }
}
