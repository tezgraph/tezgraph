/* eslint-disable no-param-reassign */

// This file will be removed in the near future.
import { InputType, Field } from 'type-graphql';

// DateRange Argument Fields
@InputType()
export class DateRangeFilter {
    @Field({ nullable: true })
    gte?: string;

    @Field({ nullable: true })
    lte?: string;
}
