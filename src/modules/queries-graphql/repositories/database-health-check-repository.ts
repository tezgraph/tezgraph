import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';

@singleton()
export default class DatabaseHealthCheckRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getIndexerVersion(): Promise<string | null> {
        const versionRow: { version: string }[] = await this.prisma.$queryRawUnsafe(
            `SELECT * FROM public.indexer_version order by autoid desc limit 1;`,
        );
        return versionRow[0]?.version ?? null;
    }

    async getChainId(): Promise<string | null> {
        const chainRow: { hash: string }[] = await this.prisma.$queryRawUnsafe(`SELECT * FROM c.chain`);
        return chainRow[0]?.hash ?? null;
    }
}
