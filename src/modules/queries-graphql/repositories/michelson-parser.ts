/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Prisma } from '@prisma/client';
import { Parser, emitMicheline, Protocol, Expr } from '@taquito/michel-codec';
import { Schema } from '@taquito/michelson-encoder';
import { MichelsonV1Expression } from '@taquito/rpc';
import { TezosToolkit } from '@taquito/taquito';
import { MichelineTzip16Expression } from '@taquito/tzip16';
import { inject, injectable } from 'tsyringe';

import { Micheline } from '../../../entity/scalars';
import { injectLogger, Logger } from '../../../utils/logging';
import { tezosToolkitDIToken } from '../../../utils/query/tezos-toolkit-factory';
import { safeJsonStringify } from '../../../utils/safe-json-stringifier';
import { BigmapDataSource } from '../datasources/bigmap-datasource';
import { optimizedEncoding } from '../helpers/micheline-optimized-encoding';

@injectable()
export default class MichelsonParser {
    constructor(
        @injectLogger(MichelsonParser) private readonly logger: Logger,
        @inject(tezosToolkitDIToken) private readonly tezosToolkit: TezosToolkit,
    ) {}

    convertMichelsonJsonToMichelsonString(
        parameter: Micheline | MichelineTzip16Expression | null | undefined,
    ): string | null {
        if (parameter === null || parameter === undefined) {
            return null;
        }
        const p = new Parser({
            protocol: Protocol.PtJakart2,
        });
        try {
            const michelsonExpr = p.parseJSON(parameter);
            const michelsonStr = emitMicheline(michelsonExpr);
            return michelsonStr;
        } catch (error: unknown) {
            this.logger.logWarning('Error occurred attempting to convert {michelson} to string: {error} .', {
                error,
                michelson: parameter,
            });
            return null;
        }
    }

    convertMichelsonJsonToCanonicalForm(parameter: Micheline): string | null {
        const p = new Parser();
        try {
            const michelsonExpr = p.parseJSON(parameter);
            const michelsonJson = safeJsonStringify(michelsonExpr);
            return michelsonJson;
        } catch (error: unknown) {
            this.logger.logWarning('Error occurred attempting to convert {michelson} to string: {error} .', {
                error,
                michelson: parameter,
            });
            return null;
        }
    }

    convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
        keyOrValue: Micheline,
        type: MichelsonV1Expression,
    ): Expr {
        const schema = new Schema(type);
        return schema.Execute(keyOrValue);
    }

    convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMicheline(
        keyOrValue: Micheline,
        type: MichelsonV1Expression,
    ): Expr {
        const schema = new Schema(type);
        const jsObject = schema.Execute(keyOrValue);
        return schema.Encode(jsObject);
    }

    convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMichelson(
        keyOrValue: Micheline,
        type: MichelsonV1Expression,
    ): string | null {
        const micheline = this.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMicheline(keyOrValue, type);
        return this.convertMichelsonJsonToMichelsonString(micheline);
    }

    async convertBigmapKeyMichelineToJsonWithBinaryFields(
        key: Micheline,
        bigmapId: bigint,
        bigmapDataSource: BigmapDataSource,
    ): Promise<Prisma.JsonObject> {
        const bigmap = await bigmapDataSource.get(bigmapId);
        const keyTypeSchema = new Schema(bigmap.key_type as MichelsonV1Expression);
        return keyTypeSchema.Encode(key, optimizedEncoding);
    }

    async convertContractStorageFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
        address: string,
        storage: Micheline,
    ): Promise<Expr> {
        if (JSON.stringify(storage) === `{"prim":"Unit"}`) {
            // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
            return {} as Expr;
        }
        const script = await this.tezosToolkit.rpc.getScript(address);
        const storageSchema = Schema.fromRPCResponse({ script });
        return storageSchema.Execute(storage);
    }
}
