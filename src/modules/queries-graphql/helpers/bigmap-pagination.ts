/* eslint-disable @typescript-eslint/unbound-method */

import { isNullish } from '../../../utils';
import { FindManyBigmapKeyResult } from '../repositories/bigmap-key-repository';
import { FindManyBigmapsResult } from '../repositories/bigmap-repository';
import { FindManyBigmapValueResult } from '../repositories/bigmap-value-repository';
import { NULL_CURSOR, NULL_CURSOR_TEXT } from './pagination-helper';

export function packBigmapValueCursor(bigmapValue: FindManyBigmapValueResult): string {
    if (isNullish(bigmapValue.key_hash)) {
        throw new Error('Unexpected null value for BigmapValue.block_hash.');
    }
    return `${bigmapValue.i}:${bigmapValue.key_hash}`;
}

export function packBigmapCursor(b: FindManyBigmapsResult): string {
    return `${b.id ?? NULL_CURSOR_TEXT}`;
}

export function unpackBigmapValueCursor(cursor: string): { i: bigint; key_hash: string } {
    const [i_string, key_hash] = cursor.split(':');
    if (!i_string || !key_hash) {
        throw new Error(`Invalid cursor ${cursor} for Bigmap`);
    }
    const i = BigInt(i_string);
    return { i, key_hash };
}

export function unpackBigmapCursor(cursor: string): { id: bigint | NULL_CURSOR } {
    const id = cursor === NULL_CURSOR_TEXT ? 'NULL_CURSOR' : BigInt(cursor);
    return { id };
}

export function packBigmapKeyCursor(bigmapKey: FindManyBigmapKeyResult): string {
    return `${bigmapKey.id ?? NULL_CURSOR_TEXT}:${bigmapKey.key_hash}`;
}

export function unpackBigmapKeyCursor(cursor: string): { id: bigint | NULL_CURSOR; key_hash: string } {
    const [id_string, key_hash] = cursor.split(':');
    if (!id_string || !key_hash) {
        throw new Error(`Invalid cursor ${cursor} for BigmapKey`);
    }
    const id = id_string === NULL_CURSOR_TEXT ? 'NULL_CURSOR' : BigInt(id_string);
    return { id, key_hash };
}
