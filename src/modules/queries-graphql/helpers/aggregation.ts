/* eslint-disable @typescript-eslint/no-unsafe-argument */
import { Prisma, PrismaPromise } from '@prisma/client';
import { GraphQLResolveInfo } from 'graphql';
import graphqlFields from 'graphql-fields';

export interface AggregationInput {
    totalCount: boolean;
}

export interface AggregationResult {
    totalCount?: number;
}

export function getAggregationInput(info: GraphQLResolveInfo): AggregationInput {
    return { totalCount: hasTotalCount(info) };
}

export function hasTotalCount(info: GraphQLResolveInfo): boolean {
    return Object.prototype.hasOwnProperty.call(graphqlFields(info), 'total_count');
}

export async function getAggregations<TPrismaCountArgs, TWhereInput>({
    prismaFindManyArgs,
    aggregationInput,
    prismaCountFunction,
}: {
    prismaFindManyArgs: Prisma.SelectSubset<{ where?: TWhereInput }, TPrismaCountArgs>;
    aggregationInput?: AggregationInput;
    prismaCountFunction(args?: Prisma.Subset<{ where?: TWhereInput }, TPrismaCountArgs>): PrismaPromise<number>;
}): Promise<AggregationResult> {
    if (aggregationInput?.totalCount !== true) {
        return {};
    }
    const totalCount = await prismaCountFunction(prismaFindManyArgs);
    if (typeof totalCount !== 'number') {
        throw new Error('Unexpected counting, expected number');
    }
    return {
        totalCount,
    };
}
