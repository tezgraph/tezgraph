/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/init-declarations */
/* eslint-disable max-statements */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prisma, PrismaPromise } from '@prisma/client';
import { UserInputError } from 'apollo-server-express';
import { cloneDeep, merge } from 'lodash';

import { isNotNullish, isNullish } from '../../../utils';
import { DataFetchOptions } from '../repositories/data-fetch-options';
import { PaginationArgs } from './pagination';
import { getOrderByDirection } from './pagination-helper';
import { OrderByDirection } from './types';
import { OrderByItem, SortFieldInfo } from './utils';

export async function sortPaginateAndRunQuery<
    TModel,
    TPrismaFindManyArgs,
    TActualSelectionArgs extends TPrismaFindManyArgs,
    TOrderByField,
    TPrismaGetPayload,
>({
    prismaFindManyArgs,
    orderBy,
    sortInfo,
    pagination,
    maxPageSize,
    options,
    prismaFindManyFunction,
    getCursorObject,
}: {
    prismaFindManyArgs: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>;
    orderBy: OrderByItem<TOrderByField>;
    sortInfo: Record<string, SortFieldInfo<TOrderByField>>;
    pagination: PaginationArgs;
    maxPageSize?: number;
    options?: DataFetchOptions;
    prismaFindManyFunction(
        args?: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>,
    ): Prisma.CheckSelect<TActualSelectionArgs, PrismaPromise<TModel[]>, PrismaPromise<TPrismaGetPayload[]>>;
    getCursorObject(cursor: string): Promise<object>;
}): Promise<{ data: TModel[]; hasMorePages: boolean }> {
    const shouldReverseOrder = pagination.last !== undefined;
    let fields: OrderByItem<TOrderByField>[];
    ({ fields, prismaFindManyArgs } = sortQuery({ prismaFindManyArgs, orderBy, sortInfo, shouldReverseOrder }));

    const { queries, take } = await getPaginatedQueries({
        fields,
        getCursorObject,
        pagination,
        prismaFindManyArgs,
        sortInfo,
        maxPageSize,
        options,
    });

    let allResults = await runAndConcatenateQueries({ queries, prismaFindManyFunction });

    const hasMorePages = isNotNullish(take) ? allResults.length > take : false;
    if (isNotNullish(take)) {
        allResults = allResults.slice(0, take);
    }
    if (shouldReverseOrder) {
        allResults = allResults.reverse();
    }
    return { data: allResults, hasMorePages };
}

async function getPaginatedQueries<
    TPrismaFindManyArgs,
    TActualSelectionArgs extends TPrismaFindManyArgs,
    TOrderByField,
>({
    prismaFindManyArgs,
    sortInfo,
    pagination,
    fields,
    maxPageSize,
    getCursorObject,
    options,
}: {
    prismaFindManyArgs: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>;
    sortInfo: Record<string, SortFieldInfo<TOrderByField>>;
    pagination: PaginationArgs;
    fields: OrderByItem<TOrderByField>[];
    maxPageSize?: number;
    options?: DataFetchOptions;
    getCursorObject(cursor: string): Promise<object>;
}): Promise<{
    queries: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>[];
    take: number | null | undefined;
}> {
    const take = pagination.first ?? pagination.last;
    if (options?.bypassPageSizeLimit !== true) {
        if (isNotNullish(maxPageSize) && isNullish(take)) {
            throw new UserInputError('Please limit the page size to be returned, use first or last');
        }
        if (isNotNullish(maxPageSize) && isNotNullish(take) && take > maxPageSize) {
            throw new UserInputError(`Cannot return more than ${maxPageSize} results in a page`);
        }
    }
    let cursorObject: object | null = null;
    const queries: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>[] = [];
    const currentPaginationCondition = {};
    if (isNullish(take) || (!pagination.after && !pagination.before)) {
        let query: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>;
        if (isNotNullish(take)) {
            query = merge(prismaFindManyArgs, { take: take + 1 });
        } else {
            query = prismaFindManyArgs;
        }
        queries.push(query);
    } else {
        for (const field of fields) {
            const fieldInfo = sortInfo[field.field as unknown as string];
            if (fieldInfo === undefined) {
                throw new Error('This should never happen: there should always be information for all sortable fields');
            }
            const paginationCondition = cloneDeep(currentPaginationCondition);
            let lastObject: Record<string, object>;
            ({ lastObject } = getPathObject(fieldInfo.prismaModelPath, false, paginationCondition));
            let cursorValue: any = null;
            ({ cursorValue, cursorObject } = await getCursorValue<TOrderByField>({
                fieldInfo,
                pagination,
                cursorObject,
                getCursorObject,
            }));
            lastObject[getPaginationFilterOperator(field.direction, pagination)] = cursorValue;
            let query = merge(cloneDeep(prismaFindManyArgs), { where: paginationCondition });
            if (take) {
                query = merge(query, { take: take + 1 });
            }
            queries.push(query);

            ({ lastObject } = getPathObject(fieldInfo.prismaModelPath, false, currentPaginationCondition));
            lastObject.equals = cursorValue;
        }
    }
    queries.reverse();
    return { queries, take };
}

async function runAndConcatenateQueries<
    TModel,
    TPrismaFindManyArgs,
    TActualSelectionArgs extends TPrismaFindManyArgs,
    TPrismaGetPayload,
>({
    queries,
    prismaFindManyFunction,
}: {
    queries: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>[];
    prismaFindManyFunction(
        args?: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>,
    ): Prisma.CheckSelect<TActualSelectionArgs, PrismaPromise<TModel[]>, PrismaPromise<TPrismaGetPayload[]>>;
}): Promise<TModel[]> {
    const results = (await Promise.all(
        queries.map(async (query) => await prismaFindManyFunction(query)),
    )) as unknown as TModel[][];
    return results.flat();
}

async function getCursorValue<TOrderByField>({
    fieldInfo,
    pagination,
    cursorObject,
    getCursorObject,
}: {
    fieldInfo: SortFieldInfo<TOrderByField>;
    pagination: PaginationArgs;
    cursorObject: Record<string, any> | null;
    getCursorObject(cursor: string): Promise<Record<string, any>>;
}): Promise<{ cursorValue: any; cursorObject: any }> {
    const cursor = pagination.after ?? pagination.before;
    if (isNullish(cursor)) {
        throw new Error(
            'This should never happen: At this point, the pagination should have either of after and before',
        );
    }
    let cursorValue: any = null;
    if (fieldInfo.getValueFromCursor) {
        cursorValue = fieldInfo.getValueFromCursor(cursor);
    } else {
        if (isNullish(cursorObject)) {
            cursorObject = await getCursorObject(cursor);
        }
        if (fieldInfo.getValueFromCursorObject) {
            cursorValue = fieldInfo.getValueFromCursorObject(cursorObject);
        } else {
            let currentObject: Record<string, any> = cursorObject;
            fieldInfo.prismaModelPath.split('.').forEach((part) => {
                currentObject = currentObject[part];
            });
            cursorValue = currentObject;
        }
    }
    return { cursorValue, cursorObject };
}

function getPaginationFilterOperator(sortDirection: OrderByDirection, pagination: PaginationArgs): 'lt' | 'gt' {
    if ((sortDirection === OrderByDirection.asc) !== (pagination.before === undefined)) {
        return 'lt';
    }
    return 'gt';
}

function sortQuery<TPrismaFindManyArgs, TActualSelectionArgs extends TPrismaFindManyArgs, TOrderByField>({
    prismaFindManyArgs,
    orderBy,
    sortInfo,
    shouldReverseOrder,
}: {
    prismaFindManyArgs: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>;
    orderBy: OrderByItem<TOrderByField>;
    sortInfo: Record<string, SortFieldInfo<TOrderByField>>;
    shouldReverseOrder: boolean;
}): {
    fields: OrderByItem<TOrderByField>[];
    prismaFindManyArgs: Prisma.SelectSubset<TActualSelectionArgs, TPrismaFindManyArgs>;
} {
    const fieldName = orderBy.field as unknown as string;
    const fieldSortInfo = sortInfo[fieldName];
    if (fieldSortInfo === undefined) {
        throw new Error(`The sort info for field ${fieldName} not found`);
    }

    const makeUniqueBy = fieldSortInfo.isUnique
        ? []
        : fieldSortInfo.makeUniqueBy.map((f) => ({
              field: f.field,
              direction: getOrderByDirection(f.direction, orderBy.direction),
              prepend: f.prepend,
          }));

    const makeUniqueByWithPrepend = makeUniqueBy.filter((f) => f.prepend === true);
    const makeUniqueByWithAppend = makeUniqueBy.filter((f) => f.prepend !== true);

    const fields = [...makeUniqueByWithPrepend, orderBy, ...makeUniqueByWithAppend];

    const orderByArray: object[] = [];
    fields.forEach((field) => {
        const fieldInfo = sortInfo[field.field as unknown as string];
        if (fieldInfo === undefined) {
            throw new Error('This should never happen: there should always be information for all sortable fields');
        }
        const { rootObject, lastObject, lastPart } = getPathObject(fieldInfo.prismaModelPath, true);
        lastObject[lastPart] =
            (field.direction === OrderByDirection.desc) !== shouldReverseOrder
                ? Prisma.SortOrder.desc
                : Prisma.SortOrder.asc;
        orderByArray.push(rootObject);
    });
    (prismaFindManyArgs as Record<string, any>).orderBy = orderByArray;
    return { fields, prismaFindManyArgs };
}

function getPathObject(
    fieldPath: string,
    skipLastPart: boolean,
    rootObject?: Record<string, any>,
): { rootObject: object; lastObject: Record<string, any>; lastPart: string } {
    if (!rootObject) {
        rootObject = {};
    }
    let currentObject = rootObject;
    const pathParts = fieldPath.split('.');
    pathParts.forEach((part, index, array) => {
        if (index === array.length - (skipLastPart ? 1 : 0)) {
            return;
        }
        if (currentObject[part] === undefined) {
            currentObject[part] = {};
        }
        currentObject = currentObject[part];
    });
    const lastPart = pathParts.slice(-1).pop();
    if (lastPart === undefined) {
        throw new Error('This should never happen: there should always be an element in sort paths');
    }
    return { rootObject, lastObject: currentObject, lastPart };
}
