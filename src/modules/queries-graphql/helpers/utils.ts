/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { Field, InputType } from 'type-graphql';

import { isNotNullish } from '../../../utils';
import { OrderByDirection } from './types';

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function createOrderByInput<TOrderByField extends object>(orderByFieldEnum: TOrderByField) {
    @InputType()
    class OrderByInput {
        @Field(() => orderByFieldEnum)
        field!: TOrderByField[keyof TOrderByField];

        @Field(() => OrderByDirection)
        direction!: OrderByDirection;
    }
    return OrderByInput;
}

export interface OrderByItem<TOrderByField> {
    field: TOrderByField[keyof TOrderByField];
    direction: OrderByDirection;
}

export enum UniquenessFieldDirection {
    asc = 'asc',
    desc = 'desc',
    sameAsMainSortOrder = 'sameAsMainSortOrder',
    oppositeOfMainSortOrder = 'oppositeOfMainSortOrder',
}

export interface UniquenessFieldInfo<TOrderByField> {
    field: TOrderByField[keyof TOrderByField];
    direction: UniquenessFieldDirection;
    prepend?: boolean;
}

export interface DatabaseColumnInfo {
    tableAlias: string;
    columnName: string;
    dependsOnTableAliases?: string[];
}

export interface EquivalencyGroupColumnInfo {
    equivalentGroupName: string;
}

export type ColumnInfo = EquivalencyGroupColumnInfo | DatabaseColumnInfo;

export function getAliasForColumn(column: ColumnInfo): string {
    const equivalenceGroup = column as EquivalencyGroupColumnInfo;
    if (isNotNullish(equivalenceGroup.equivalentGroupName)) {
        return equivalenceGroup.equivalentGroupName;
    }
    return (column as DatabaseColumnInfo).columnName;
}

export interface EquivalentColumnsGroupInfo {
    groupName: string;
    columns: DatabaseColumnInfo[];
}

export type SortFieldInfo<TOrderByField> = {
    prismaModelPath: string;
    rawModel: ColumnInfo;
    getValueFromCursor?(cursor: string): any;
    getValueFromCursorObject?(cursor: any): any;
} & (
    | {
          isUnique: false;
          makeUniqueBy: UniquenessFieldInfo<TOrderByField>[];
      }
    | {
          isUnique: true;
      }
);
