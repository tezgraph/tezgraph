import { UserInputError } from 'apollo-server-express';

import { isNotNullish, isNullish } from '../../../utils';
import { PaginationArgs } from './pagination';
import { QueryMetadata } from './query-metadata';
import { OrderByDirection } from './types';
import { OrderByItem, SortFieldInfo, UniquenessFieldDirection } from './utils';

export function checkTakeLimit<TOrderByField>(
    metadata: QueryMetadata<TOrderByField>,
    take: number | null | undefined,
): void {
    if (isNotNullish(metadata.maxPageSize)) {
        if (isNullish(take)) {
            throw new UserInputError('Please limit the page size to be returned, use first or last');
        } else if (take > metadata.maxPageSize) {
            throw new UserInputError(`Cannot return more than ${metadata.maxPageSize} results in a page`);
        }
    }
}

export function getPaginationFilterOperator(sortDirection: OrderByDirection, pagination: PaginationArgs): '<' | '>' {
    if ((sortDirection === OrderByDirection.asc) !== (pagination.before === undefined)) {
        return '<';
    }
    return '>';
}

/*
 * If the query is ordered by fieldA, and this field in metadata is marked to not be unique,
 * it adds the required fields with correct direction to make this field unique.
 */
export function expandOrderByForNonUniqueFields<TOrderByField extends string>(
    orderBy: OrderByItem<TOrderByField>,
    metadata: QueryMetadata<TOrderByField>,
): { fieldSortInfo: SortFieldInfo<TOrderByField>; direction: OrderByDirection }[] {
    const fieldSortInfo = getSortInfo(orderBy.field, metadata);

    const makeUniqueBy = fieldSortInfo.isUnique
        ? []
        : fieldSortInfo.makeUniqueBy.map((f) => ({
              fieldSortInfo: getSortInfo(f.field, metadata),
              direction: getOrderByDirection(f.direction, orderBy.direction),
              prepend: f.prepend,
          }));

    const makeUniqueByWithPrepend = makeUniqueBy.filter((f) => f.prepend === true);
    const makeUniqueByWithAppend = makeUniqueBy.filter((f) => f.prepend !== true);

    const allOrderBy = [
        ...makeUniqueByWithPrepend,
        { fieldSortInfo, direction: orderBy.direction },
        ...makeUniqueByWithAppend,
    ];

    return allOrderBy;
}

export function getOrderByDirection(
    newDirection: UniquenessFieldDirection,
    mainDirection: OrderByDirection,
): OrderByDirection {
    switch (newDirection) {
        case UniquenessFieldDirection.asc:
            return OrderByDirection.asc;
        case UniquenessFieldDirection.desc:
            return OrderByDirection.desc;
        case UniquenessFieldDirection.sameAsMainSortOrder:
            return mainDirection;
        case UniquenessFieldDirection.oppositeOfMainSortOrder:
            return mainDirection === OrderByDirection.asc ? OrderByDirection.desc : OrderByDirection.asc;
    }
}

function getSortInfo<TOrderByField extends string>(
    field: TOrderByField[keyof TOrderByField],
    metadata: QueryMetadata<TOrderByField>,
): SortFieldInfo<TOrderByField> {
    const fieldInfo = metadata.sortInfo[field as string];
    if (fieldInfo === undefined) {
        throw new Error(`Sort Information for field ${field as string} is missing in ${metadata.name} Metadata`);
    }
    return fieldInfo;
}

export function qualifiedColumn(rawModel: { tableAlias: string; columnName: string }): string {
    return `${rawModel.tableAlias}.${rawModel.columnName}`;
}

export const NULL_CURSOR_TEXT = 'NULL';

export type NULL_CURSOR = 'NULL_CURSOR';
