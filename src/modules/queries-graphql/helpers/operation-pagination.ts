import { UserInputError } from 'apollo-server-errors';

export function getOperationCursor({
    hash,
    batch_position,
    internal,
}: {
    hash: string;
    batch_position: number;
    internal: number;
}): string {
    return `${hash}:${batch_position}:${internal}`;
}

export function unpackOperationCursor(cursor: string): {
    operationHash: string;
    batchPosition: number;
    internal: number;
} {
    const [operationHash, rawBatchPosition, rawInternal] = cursor.split(':');
    if (!operationHash || !rawBatchPosition || !rawInternal) {
        throw new UserInputError(
            `The operation's cursor should be of form: 'operation_hash:batch_position:internal' but '${cursor}' is provided`,
        );
    }

    const batchPosition = Number.parseInt(rawBatchPosition, 10);
    if (!Number.isInteger(batchPosition)) {
        throw new UserInputError(
            `The operation's cursor should be of form: 'operation_hash:batch_position:internal' but batch_position in '${cursor}' is not an integer number`,
        );
    }

    const internal = Number.parseInt(rawInternal, 10);
    if (!Number.isInteger(internal)) {
        throw new UserInputError(
            `The operation's cursor should be of form: 'operation_hash:batch_position:internal' but internal in '${cursor}' is not an integer number`,
        );
    }

    return { operationHash, batchPosition, internal };
}
