/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { delay, inject, singleton } from 'tsyringe';
import { ClassType } from 'type-graphql';

import { ActivateAccountRecord } from '../../../entity/queries/activate-account-record';
import { BallotRecord } from '../../../entity/queries/ballot-record';
import { DelegationRecord } from '../../../entity/queries/delegation-record';
import { DoubleBakingEvidenceRecord } from '../../../entity/queries/double-baking-evidence-record';
import { DoubleEndorsementEvidenceRecord } from '../../../entity/queries/double-endorsement-evidence-record';
import { EndorsementRecord } from '../../../entity/queries/endorsement-record';
import {
    OperationRecordData,
    OperationRecordStatus,
    MAP_OPERATION_STATUS_TO_RESULT_STATUS,
} from '../../../entity/queries/operation-record';
import { OriginationRecord } from '../../../entity/queries/origination-record';
import { ProposalRecord } from '../../../entity/queries/proposal-record';
import { RevealRecord } from '../../../entity/queries/reveal-record';
import { SeedNonceRevelationRecord } from '../../../entity/queries/seed-nonce-revelation-record';
import { TransactionRecord } from '../../../entity/queries/transaction-record';
import { OperationKind } from '../../../entity/subscriptions';
import { isNullish } from '../../../utils';
import DelegationRepository from '../repositories/delegation-repository';
import EndorsementRepository from '../repositories/endorsement-repository';
import { FindManyOperationsResult } from '../repositories/operation-alpha-repository';
import OriginationRepository from '../repositories/origination-repository';
import RevealRepository from '../repositories/reveal-repository';
import TransactionRepository from '../repositories/transaction-repository';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { getOperationCursor } from './operation-pagination';

type OperationRecordSubclasses =
    | EndorsementRecord
    | RevealRecord
    | DelegationRecord
    | TransactionRecord
    | OriginationRecord
    | ActivateAccountRecord
    | BallotRecord
    | DoubleBakingEvidenceRecord
    | DoubleEndorsementEvidenceRecord
    | ProposalRecord
    | SeedNonceRevelationRecord;

const operationKindToEntityMap: Partial<Record<OperationRecordKind, ClassType<OperationRecordSubclasses>>> = {
    [OperationRecordKind.endorsement]: EndorsementRecord,
    [OperationRecordKind.reveal]: RevealRecord,
    [OperationRecordKind.delegation]: DelegationRecord,
    [OperationRecordKind.transaction]: TransactionRecord,
    [OperationRecordKind.origination]: OriginationRecord,
    [OperationRecordKind.endorsement_with_slot]: EndorsementRecord,
    [OperationRecordKind.activate_account]: ActivateAccountRecord,
    [OperationRecordKind.ballot]: BallotRecord,
    [OperationRecordKind.double_baking_evidence]: DoubleBakingEvidenceRecord,
    [OperationRecordKind.double_endorsement_evidence]: DoubleEndorsementEvidenceRecord,
    [OperationRecordKind.proposals]: ProposalRecord,
    [OperationRecordKind.seed_nonce_revelation]: SeedNonceRevelationRecord,
};

@singleton()
export class OperationMapper {
    constructor(
        @inject(delay(() => EndorsementRepository))
        private readonly endorsementRepository: EndorsementRepository,
        @inject(delay(() => RevealRepository))
        private readonly revealRepository: RevealRepository,
        @inject(delay(() => DelegationRepository))
        private readonly delegationRepository: DelegationRepository,
        @inject(delay(() => OriginationRepository))
        private readonly originationRepository: OriginationRepository,
        @inject(delay(() => TransactionRepository))
        private readonly transactionRepository: TransactionRepository,
    ) {}

    mapBaseResultToBaseOperationData(result: FindManyOperationsResult) {
        const hash = result.operation_hash;
        const batch_position = result.id;
        const internal = result.internal;

        return {
            batch_position,
            internal,
            block: { hash: result.block_hash, level: result.block_level, timestamp: result.block_timestamp },
            cursor: getOperationCursor({
                hash,
                batch_position,
                internal,
            }),
            hash,
            autoid: result.autoid,
            level: result.block_level,
            timestamp: result.block_timestamp,
            receiverAddress: result.receiver_address,
            receiverAddressID: result.receiver_id,
            senderAddress: result.sender_address,
            senderAddressID: result.sender_id,
        };
    }

    mapManagerNumbersResultToOperationCostData(result: FindManyOperationsResult) {
        if (
            !result.manager_numbers_counter &&
            !result.manager_numbers_gas_limit &&
            !result.manager_numbers_storage_limit
        ) {
            return;
        }

        return {
            counter: result.manager_numbers_counter && BigInt(result.manager_numbers_counter.toNumber()),
            gas_limit: result.manager_numbers_gas_limit && BigInt(result.manager_numbers_gas_limit.toNumber()),
            storage_limit:
                result.manager_numbers_storage_limit && BigInt(result.manager_numbers_storage_limit.toNumber()),
        };
    }

    mapResultToOperationMetadata(
        result: FindManyOperationsResult,
        { metadataTypeName, resultTypeName }: { metadataTypeName: string; resultTypeName: string },
    ) {
        return {
            operation_result: {
                consumed_milligas: result.consumed_milligas ? BigInt(result.consumed_milligas.toNumber()) : null,
                consumed_gas: result.consumed_milligas
                    ? BigInt(Math.floor(result.consumed_milligas.toNumber() / 1000))
                    : null,
                errors: result.error_trace !== null ? (result.error_trace as object[]) : null,
                status: MAP_OPERATION_STATUS_TO_RESULT_STATUS[result.status as OperationRecordStatus],
                graphQLTypeName: resultTypeName,
            },
            graphQLTypeName: metadataTypeName,
        };
    }

    mapOperationsResultToSpecificOperation(result: FindManyOperationsResult): OperationRecordData {
        const OperationEntity = operationKindToEntityMap[result.operation_kind];
        if (isNullish(OperationEntity)) {
            throw new Error(
                `The Map operationKindToEntityMap has no entry for operation kind: ${result.operation_kind}`,
            );
        }
        const operationData = this.mapSpecificOperationData(result, OperationEntity);
        return Object.assign(new OperationEntity(), operationData);
    }

    getOperationKind(operationRecordKind: OperationRecordKind): OperationKind {
        return OperationKind[OperationRecordKind[operationRecordKind] as keyof typeof OperationKind];
    }

    private mapSpecificOperationData(
        result: FindManyOperationsResult,
        OperationEntity: ClassType<OperationRecordSubclasses>,
    ): OperationRecordData {
        switch (result.operation_kind) {
            case OperationRecordKind.endorsement:
            case OperationRecordKind.endorsement_with_slot: {
                return this.endorsementRepository.mapResultToEntity(result);
            }
            case OperationRecordKind.reveal: {
                return this.revealRepository.mapResultToEntity(result);
            }
            case OperationRecordKind.delegation: {
                return this.delegationRepository.mapResultToEntity(result);
            }
            case OperationRecordKind.transaction: {
                return this.transactionRepository.mapResultToEntity(result);
            }
            case OperationRecordKind.origination: {
                return this.originationRepository.mapResultToEntity(result);
            }
            default: {
                return {
                    kind: this.getOperationKind(result.operation_kind),
                    graphQLTypeName: OperationEntity.name,
                    ...this.mapBaseResultToBaseOperationData(result),
                };
            }
        }
    }
}
