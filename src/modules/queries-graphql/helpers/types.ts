/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prisma } from '@prisma/client';
import { registerEnumType } from 'type-graphql';

import { safeJsonStringify } from '../../../utils/safe-json-stringifier';

export type JsonObject = { [Key in string]?: JsonValue };
export type JsonArray = (JsonValue | string | number | boolean | null)[];
export type JsonValue = string | number | boolean | null | JsonObject | JsonArray;

export type Value = string | number | boolean | object | null | undefined;

export type DataType = 'string' | 'number' | 'bigint' | 'boolean' | 'json';

export function parseConstant(value: string, dataType: DataType): any {
    switch (dataType) {
        case 'boolean':
            return value === 'true';
        case 'number':
            return Number(value);
        case 'bigint':
            return BigInt(value);
        case 'string':
            return value;
        case 'json':
            return safeJsonStringify(value) as Prisma.JsonValue;
    }
}

export function getValueForQuery(value: Value): string {
    if (value instanceof Prisma.Decimal) {
        return `'${value.toString()}'`;
    }
    switch (typeof value) {
        case 'bigint':
            return `'${(value as bigint).toString()}'`;
        case 'string':
            return `'${value}'`;
        default:
            return value?.toString() ?? '';
    }
}

// OrderBy Direction Enums
export enum OrderByDirection {
    asc = 'asc',
    desc = 'desc',
}

registerEnumType(OrderByDirection, {
    name: 'OrderByDirection',
    description: 'The available options for the order_by.direction field value.',
});

export interface HasHash {
    hash: string;
}

export type ValueOrArray<T> = T | T[];

export function toArray<T>(values: ValueOrArray<T>): T[] {
    return new Array<T>().concat(values);
}
