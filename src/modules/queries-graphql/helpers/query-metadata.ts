/* eslint-disable @typescript-eslint/no-explicit-any */
import { BigmapOperationKind } from '../../../entity/queries/bigmap-operation-kind';
import { Nullish } from '../../../utils';
import {
    BigmapKeyOrderByFieldInternal,
    bigmapKeyOrderByInfo,
    BigmapOrderByField,
    bigmapOrderByInfo,
} from '../resolvers/types/bigmap-filters';
import { operationAlphaOrderByInfo, OperationsOrderByFieldInternal } from '../resolvers/types/operations-order-by';
import { DataType, ValueOrArray } from './types';
import { EquivalentColumnsGroupInfo, SortFieldInfo } from './utils';

/*
 * Used by the query builder to add correct tables to where clause
 * and to sort and paginate the query
 */
export interface QueryMetadata<TOrderByField> {
    name: string;
    tables: Record<string, TableInfo>;
    sortInfo: Record<string, SortFieldInfo<TOrderByField>>;
    EquivalentColumnGroups?: EquivalentColumnsGroupInfo[];
    maxPageSize?: number;
    cursorFields: string[];
    fields: Record<string, FieldInfo>;
    defaultWhereClauses?: { tableAliases: string[]; where: string }[];
    getCursorObject?(cursor: string): Promise<any>;
}

export interface FieldInfo {
    dataType: DataType;
}

export interface FromClause {
    fromClause: string;
}

export interface JoinInfo {
    tableName: string;
    equivalentColumnGroupName: string;
}

export type TableInfo = {
    isMain?: true;
    dependsOn?: Nullish<ValueOrArray<string>>;
} & (FromClause | JoinInfo);

export const OperationAlphaMetadata: QueryMetadata<OperationsOrderByFieldInternal> = {
    name: 'Operation',
    cursorFields: ['operation.hash', 'operation_alpha.id', 'operation_alpha.internal'],
    sortInfo: operationAlphaOrderByInfo,
    maxPageSize: 100,
    tables: {
        operation_alpha: { tableName: 'c.operation_alpha', equivalentColumnGroupName: 'OperationAlpha_AutoID' },
        operation: {
            fromClause: 'LEFT JOIN c.operation ON operation.hash_id = operation_alpha.hash_id',
            dependsOn: 'operation_alpha',
        },
        block: {
            fromClause: 'LEFT JOIN c.block ON block.level = operation_alpha.block_level',
            dependsOn: 'operation_alpha',
        },
        operation_sender_and_receiver: {
            tableName: 'c.operation_sender_and_receiver',
            equivalentColumnGroupName: 'OperationAlpha_AutoID',
        },
        sender: {
            fromClause:
                'LEFT JOIN c.addresses as sender ON sender.address_id = operation_sender_and_receiver.sender_id',
            dependsOn: 'operation_sender_and_receiver',
        },
        receiver: {
            fromClause:
                'LEFT JOIN c.addresses as receiver ON receiver.address_id = operation_sender_and_receiver.receiver_id',
            dependsOn: 'operation_sender_and_receiver',
        },
        delegation: { tableName: 'c.delegation', equivalentColumnGroupName: 'OperationAlpha_AutoID' },
        deleg_addr_pkh: {
            fromClause: 'LEFT JOIN c.addresses as deleg_addr_pkh ON deleg_addr_pkh.address_id = delegation.pkh_id',
            dependsOn: 'delegation',
        },
        endorsement: {
            tableName: 'c.endorsement',
            equivalentColumnGroupName: 'OperationAlpha_AutoID',
        },
        endors_addr: {
            fromClause: 'LEFT JOIN c.addresses as endors_addr ON endors_addr.address_id = endorsement.delegate_id',
            dependsOn: 'endorsement',
        },
        reveal: { tableName: 'c.reveal', equivalentColumnGroupName: 'OperationAlpha_AutoID' },
        origination: {
            tableName: 'c.origination',
            equivalentColumnGroupName: 'OperationAlpha_AutoID',
        },
        orig_k_addr: {
            fromClause: 'LEFT JOIN c.addresses as orig_k_addr ON orig_k_addr.address_id = origination.k_id',
            dependsOn: 'origination',
        },
        tx: { tableName: 'c.tx', equivalentColumnGroupName: 'OperationAlpha_AutoID' },
        dest: {
            fromClause: 'LEFT JOIN c.addresses as dest ON dest.address_id = tx.destination_id',
            dependsOn: 'tx',
        },
        manager_numbers: {
            tableName: 'c.manager_numbers',
            equivalentColumnGroupName: 'OperationAlpha_AutoID;',
        },
        contract_balance: {
            fromClause: `LEFT JOIN contract_balance ON contract_balance.address_id = delegation.source_id
            AND contract_balance.block_level = operation_alpha.block_level`,
            dependsOn: ['operation_alpha', 'delegation'],
        },
    },
    fields: {
        'operation.hash': { dataType: 'string' },
        'operation_alpha.id': { dataType: 'number' },
        'operation_alpha.internal': { dataType: 'number' },
    },
    EquivalentColumnGroups: [
        {
            groupName: 'OperationAlpha_AutoID',
            columns: [
                { tableAlias: 'c.operation_alpha', columnName: 'autoid' },
                { tableAlias: 'c.operation_sender_and_receiver', columnName: 'operation_id' },
                { tableAlias: 'c.origination', columnName: 'operation_id' },
                { tableAlias: 'c.preendorsement', columnName: 'operation_id' },
                { tableAlias: 'c.proposal', columnName: 'operation_id' },
                { tableAlias: 'c.register_global_constant', columnName: 'operation_id' },
                { tableAlias: 'c.reveal', columnName: 'operation_id' },
                { tableAlias: 'c.sc_rollup_add_messages', columnName: 'operation_id' },
                { tableAlias: 'c.sc_rollup_originate', columnName: 'operation_id' },
                { tableAlias: 'c.seed_nonce_revelation', columnName: 'operation_id' },
                { tableAlias: 'c.set_deposits_limit', columnName: 'operation_id' },
                { tableAlias: 'c.tx', columnName: 'operation_id' },
                { tableAlias: 'c.tx_rollup_origination', columnName: 'operation_id' },
                { tableAlias: 'c.activation', columnName: 'operation_id' },
                { tableAlias: 'c.ballot', columnName: 'operation_id' },
                { tableAlias: 'c.bigmap', columnName: 'operation_id' },
                { tableAlias: 'c.delegation', columnName: 'operation_id' },
                { tableAlias: 'c.double_baking_evidence', columnName: 'operation_id' },
                { tableAlias: 'c.double_endorsement_evidence', columnName: 'operation_id' },
                { tableAlias: 'c.double_preendorsement_evidence', columnName: 'operation_id' },
                { tableAlias: 'c.endorsement', columnName: 'operation_id' },
                { tableAlias: 'c.manager', columnName: 'operation_id' },
                { tableAlias: 'c.manager_numbers', columnName: 'operation_id' },
            ],
        },
    ],
};

export const BigmapMetadata: QueryMetadata<BigmapOrderByField> = {
    name: 'Bigmap',
    cursorFields: ['bigmap.id'],
    sortInfo: bigmapOrderByInfo,
    maxPageSize: 100,
    tables: {
        bigmap: { fromClause: 'bigmap', isMain: true },
        receiver: { fromClause: 'JOIN c.addresses as receiver on receiver.address_id = bigmap.receiver_id' },
    },
    fields: {
        'bigmap.id': { dataType: 'bigint' },
    },
    defaultWhereClauses: [
        {
            tableAliases: ['bigmap'],
            where: `bigmap.kind <> ${BigmapOperationKind.clear} and bigmap.key_type is not null and id > 0`,
        },
    ],
};

export const BigmapKeyMetadata: QueryMetadata<BigmapKeyOrderByFieldInternal> = {
    name: 'BigmapKey',
    cursorFields: ['bigmap.id', 'bigmap.key_hash'],
    sortInfo: bigmapKeyOrderByInfo,
    maxPageSize: 100,
    tables: {
        bigmap: {
            fromClause: `bigmap`,
        },
    },
    defaultWhereClauses: [
        { tableAliases: ['bigmap'], where: 'bigmap.kind <> 2 and bigmap.key_type is null and id > 0' },
    ],
    fields: {
        'bigmap.id': { dataType: 'bigint' },
        'bigmap.key': { dataType: 'json' },
        'bigmap.key_hash': { dataType: 'string' },
    },
};
