import { format } from 'fecha';

export function timestampStrToISODateTime(timestampStr: string): Date {
    const organizedTimestampStr = organizeTimestampStr(timestampStr);
    const isoDateTimestampStr = formatTimestamp(organizedTimestampStr, 'isoDateTime').substring(0, 19);
    return new Date(isoDateTimestampStr);
}

function formatTimestamp(timestampStr: string, fmt: string): string {
    try {
        const timestampDate = new Date(timestampStr);
        return format(timestampDate, fmt);
    } catch (err: unknown) {
        throw new Error(`The DateRange value ${timestampStr} is in a invalid format.`);
    }
}

function organizeTimestampStr(timestamp: string): string {
    let ts = timestamp;

    /*
     * This is used to handle the case in which the user only enters the year.
     * Depending on the timezone, the date may be changed to something different.
     * This way we ensure that we use the 01/01 of the year the user entered.
     */
    if (ts.length === 7 || ts.length === 4) {
        ts += '/01';
    }

    /*
     * This is used to handle the case in which the user only enters a date without time.
     * Depending on the timezone, the date may be changed to something different.
     * This way we ensure that we use the 00:00:00 of the date the user entered.
     */
    if (ts.length < 11) {
        try {
            ts = formatTimestamp(ts, 'shortDate');
        } catch (err: unknown) {
            throw new Error(`The DateRange value ${ts} passed is in a invalid format.`);
        }
    }

    switch (true) {
        case ts.includes('PM'):
            ts = ts.replace('PM', ' PM');
            break;
        case ts.includes('pm'):
            ts = ts.replace('pm', ' pm');
            break;
        case ts.includes('AM'):
            ts = ts.replace('AM', ' AM');
            break;
        case ts.includes('am'):
            ts = ts.replace('am', ' am');
            break;
    }

    switch (true) {
        case ts.includes('Z'):
            ts = ts.replace('Z', '');
            break;
    }
    ts = `${ts}Z`;
    return ts;
}
