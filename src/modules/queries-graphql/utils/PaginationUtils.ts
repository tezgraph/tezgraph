/* eslint-disable max-statements */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { isNullish, isNotNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { PaginationArgs } from '../helpers/pagination';
import {
    checkTakeLimit,
    expandOrderByForNonUniqueFields,
    getPaginationFilterOperator,
    qualifiedColumn,
} from '../helpers/pagination-helper';
import { Direct, QueryInfo, raw, Tokenized } from '../helpers/query-builder';
import { QueryMetadata } from '../helpers/query-metadata';
import { OrderByDirection, parseConstant } from '../helpers/types';
import {
    DatabaseColumnInfo,
    EquivalencyGroupColumnInfo,
    getAliasForColumn,
    OrderByItem,
    SortFieldInfo,
} from '../helpers/utils';
import { DataFetchOptions } from '../repositories/data-fetch-options';

@singleton()
export default class PaginationUtils {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async runQueriesAndConcatenateResults<TResult, TOrderByField extends string>(
        queries: QueryInfo<TResult, TOrderByField>[],
    ): Promise<TResult[]> {
        const promises = queries.map((query) => {
            return this.prisma.$queryRawUnsafe<TResult[]>(query.build(), ...query.values);
        });
        const allResults = await Promise.all(promises);
        const concatenated = allResults.flat(1);
        return concatenated;
    }

    async sortPaginateAndRunQuery<TResult, TOrderByField extends string>({
        queryInfo,
        orderBy,
        pagination,
        metadata,
        options,
    }: {
        queryInfo: QueryInfo<TResult, TOrderByField>;
        orderBy: OrderByItem<TOrderByField>;
        pagination: PaginationArgs;
        metadata: QueryMetadata<TOrderByField>;
        options?: DataFetchOptions;
    }): Promise<{ data: TResult[]; hasMorePages: boolean }> {
        const shouldReverseOrder = pagination.last !== undefined;
        const allOrderByFields = expandOrderByForNonUniqueFields<TOrderByField>(orderBy, metadata);

        queryInfo.setSort(allOrderByFields, shouldReverseOrder);

        const { queries, take } = await this.getPaginatedQueries<TResult, TOrderByField>({
            sortedQuery: queryInfo,
            allOrderByFields,
            pagination,
            metadata,
            options,
        });

        let allResults = await this.runQueriesAndConcatenateResults(queries);

        const hasMorePages = isNullish(take) ? false : allResults.length > take;
        if (isNotNullish(take)) {
            allResults = allResults.slice(0, take);
        }
        if (shouldReverseOrder) {
            allResults = allResults.reverse();
        }
        return { data: allResults, hasMorePages };
    }

    /*
     * Creates a query and fetches the cursor object from the DB.
     * for instance, if the cursor is made from fieldX:fieldY and the query has sent the value `X:Y`,
     * And if the order fields (after making unique) are: fieldA, fieldB, and fieldC
     * the query should be: `Select fieldA, fieldB, fieldC from tableName where fieldX = X and fieldY = Y
     */
    async fetchCursorObject<TResult, TOrderByField extends string>(
        cursor: string,
        metadata: QueryMetadata<TOrderByField>,
        allOrderByFields: { fieldSortInfo: SortFieldInfo<TOrderByField>; direction: OrderByDirection }[],
    ): Promise<Record<string, any>> {
        const parts = cursor.split(':');

        if (parts.length !== metadata.cursorFields.length) {
            throw new Error(
                `The cursor for ${metadata.name} should have ${metadata.cursorFields.length} parts but got: ${cursor}`,
            );
        }

        const query = new QueryInfo<TResult, TOrderByField>(metadata, this);
        const allTables: string[] = [];

        const selectColumns: string[] = [];

        allOrderByFields.forEach((f) => {
            const equivalenceGroup = f.fieldSortInfo.rawModel as EquivalencyGroupColumnInfo;
            if (isNotNullish(equivalenceGroup.equivalentGroupName)) {
                selectColumns.push(equivalenceGroup.equivalentGroupName);
            } else {
                const databaseColumnInfo = f.fieldSortInfo.rawModel as DatabaseColumnInfo;
                if (databaseColumnInfo.dependsOnTableAliases) {
                    allTables.push(...databaseColumnInfo.dependsOnTableAliases);
                }
                allTables.push(databaseColumnInfo.tableAlias);
                selectColumns.push(qualifiedColumn(databaseColumnInfo));
            }
        });

        query.addSelectColumns(allTables, selectColumns);

        metadata.cursorFields.forEach((field, index) => {
            const part = parts[index]!;
            const [tableAlias] = field.split('.');
            if (!tableAlias) {
                throw new Error(`Cursor column name should be fully qualified, but got: ${field}`);
            }
            const value = parseConstant(part, metadata.fields[field]!.dataType);
            query.appendWhere([tableAlias], query.sql`${raw(field)} = ${value}`);
        });

        const list = await this.prisma.$queryRawUnsafe<any[]>(query.build(), ...query.values);
        const cursorObject = list[0] ?? {};

        return cursorObject;
    }

    /*
     * If the cursor object is not already loaded from the database, loads it
     * Then extracts the orderBy field's value from the cursor
     */
    async getFieldValueFromCursor<TOrderByField extends string>({
        fieldInfo,
        pagination,
        cursorObject,
        allOrderByFields,
        metadata,
    }: {
        fieldInfo: SortFieldInfo<TOrderByField>;
        pagination: PaginationArgs;
        cursorObject: Record<string, any> | null;
        allOrderByFields: { fieldSortInfo: SortFieldInfo<TOrderByField>; direction: OrderByDirection }[];
        metadata: QueryMetadata<TOrderByField>;
    }): Promise<{ fieldValueFromCursor: any; cursorObject: Record<string, any> | null }> {
        const cursor = pagination.after ?? pagination.before;
        if (isNullish(cursor)) {
            throw new Error(
                'This should never happen: At this point, the pagination should have exactly one of after and before',
            );
        }
        let fieldValueFromCursor: any = null;
        // TODO: If the field is part of cursor, we can directly extract it from cursor, no need to fetch cursorObject from DB
        if (fieldInfo.getValueFromCursor) {
            fieldValueFromCursor = fieldInfo.getValueFromCursor(cursor);
        } else {
            if (cursorObject === null) {
                cursorObject = await this.fetchCursorObject(cursor, metadata, allOrderByFields);
            }
            if (fieldInfo.getValueFromCursorObject) {
                fieldValueFromCursor = fieldInfo.getValueFromCursorObject(cursorObject);
            } else {
                fieldValueFromCursor = cursorObject[getAliasForColumn(fieldInfo.rawModel)];
            }
        }
        return { fieldValueFromCursor, cursorObject };
    }

    /*
     * Assume we have an `order by fieldA asc` and made unique by `fieldB asc, fieldC asc`
     * and now the values for these in the cursor are: A, B and C respectively
     * This should create 3 queries:
     * `where fieldA = A and fieldB = B and fieldC > C`
     * `where fieldA = A and fieldB > B`
     * `where fieldA > A`
     */
    async getPaginatedQueries<TResult, TOrderByField extends string>({
        sortedQuery,
        allOrderByFields,
        pagination,
        metadata,
        options,
    }: {
        sortedQuery: QueryInfo<TResult, TOrderByField>;
        allOrderByFields: { fieldSortInfo: SortFieldInfo<TOrderByField>; direction: OrderByDirection }[];
        pagination: PaginationArgs;
        metadata: QueryMetadata<TOrderByField>;
        options?: DataFetchOptions;
    }): Promise<{ queries: QueryInfo<TResult, TOrderByField>[]; take: number | null | undefined }> {
        const take = pagination.first ?? pagination.last;
        if (options?.bypassPageSizeLimit !== true) {
            checkTakeLimit(metadata, take);
        }

        let cursorObject: Record<string, any> | null = null;
        const queries: QueryInfo<TResult, TOrderByField>[] = [];
        let currentPaginationCondition = new Tokenized();

        if (isNullish(take) || (!pagination.after && !pagination.before)) {
            if (isNotNullish(take)) {
                sortedQuery.appendOtherStatement([], sortedQuery.sql`limit ${take + 1}`);
            }
            return { queries: [sortedQuery], take };
        }

        for (const field of allOrderByFields) {
            let fieldValueFromCursor: any = null;
            ({ fieldValueFromCursor, cursorObject } = await this.getFieldValueFromCursor<TOrderByField>({
                fieldInfo: field.fieldSortInfo,
                pagination,
                cursorObject,
                allOrderByFields,
                metadata,
            }));

            // From the previous orderBy field we have: `fieldA = A`. Now we amend it: `fieldA = A AND fieldB < B`
            const separator = raw(currentPaginationCondition.strings.length ? 'AND ' : '');
            const query = sortedQuery.clone<TResult>();
            let column: Direct | null = null;
            const equivalenceGroup = field.fieldSortInfo.rawModel as EquivalencyGroupColumnInfo;
            if (isNotNullish(equivalenceGroup.equivalentGroupName)) {
                column = raw(
                    `${query.processColumnOrEquivalencyGroupName(equivalenceGroup.equivalentGroupName, false)}`,
                );
            } else {
                const databaseColumnInfo = field.fieldSortInfo.rawModel as DatabaseColumnInfo;
                column = raw(qualifiedColumn(databaseColumnInfo));
            }
            const operator = raw(getPaginationFilterOperator(field.direction, pagination));
            const paginationCondition = currentPaginationCondition.clone()
                .append`${separator}${column} ${operator} ${fieldValueFromCursor}`;

            query.appendWhere([], query.tokenized(paginationCondition));
            if (take) {
                query.appendOtherStatement([], query.sql` limit ${take + 1}`);
            }
            queries.push(query);

            // Amend equality condition for next field(s): It was `fieldA = A`, now it becomes: `fieldA = A and fieldB = B`
            currentPaginationCondition = currentPaginationCondition.append`${column} = ${fieldValueFromCursor}`;
        }
        queries.reverse();
        return { queries, take };
    }
}
