import { bigmap, bigmap_keys2, Prisma, PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { BigmapOperationKind } from '../../../entity/queries/bigmap-operation-kind';
import { isNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { unpackBigmapCursor, unpackBigmapKeyCursor, unpackBigmapValueCursor } from '../helpers/bigmap-pagination';
import { PaginationArgs } from '../helpers/pagination';
import { sortPaginateAndRunQuery } from '../helpers/prisma-pagination';
import { OrderByDirection } from '../helpers/types';
import { FindManyBigmapsResult } from '../repositories/bigmap-repository';
import { FindManyBigmapValueResult } from '../repositories/bigmap-value-repository';
import { DataFetchOptions } from '../repositories/data-fetch-options';
import {
    BigmapKeyOrderByField,
    BigmapKeyOrderByFieldInternal,
    bigmapKeyOrderByInfo,
    BigmapKeyOrderByInput,
    BigmapOrderByField,
    bigmapOrderByInfo,
    BigmapOrderByInput,
    BigmapValueOrderByField,
    BigmapValueOrderByFieldInternal,
    bigmapValueOrderByInfo,
    BigmapValueOrderByInput,
} from '../resolvers/types/bigmap-filters';

@singleton()
export default class BigmapPaginationUtils {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getBigmapKeyByCursor(
        cursor: string,
    ): Promise<{ key_hash: string | null; key: Prisma.JsonValue; id: bigint | null }> {
        const { id, key_hash } = unpackBigmapKeyCursor(cursor);
        const where = Prisma.validator<Prisma.bigmapWhereInput>()({
            id: id === 'NULL_CURSOR' ? { equals: null } : id,
            key_hash,
        });
        const select = Prisma.validator<Prisma.bigmapSelect>()({
            key_hash: true,
            key: true,
            id: true,
        });
        const bigmapKey = await this.prisma.bigmap.findFirst({
            where,
            select,
        });
        if (isNullish(bigmapKey)) {
            throw new Error(`No BigmapKey found for cursor ${cursor}`);
        }
        return bigmapKey;
    }

    async sortPaginateAndRunBigmapKeyQuery<T extends Prisma.bigmap_keys2FindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.bigmap_keys2FindManyArgs>;
        orderBy: BigmapKeyOrderByInput | undefined;
        pagination: PaginationArgs;
    }): Promise<{ data: bigmap_keys2[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            bigmap_keys2,
            Prisma.bigmap_keys2FindManyArgs,
            T,
            BigmapKeyOrderByFieldInternal,
            Prisma.bigmap_keys2GetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? { field: BigmapKeyOrderByField.key, direction: OrderByDirection.asc },
            sortInfo: bigmapKeyOrderByInfo,
            pagination,
            maxPageSize: 100,
            prismaFindManyFunction: this.prisma.bigmap_keys2.findMany.bind(this),
            getCursorObject: async (cursor) => this.getBigmapKeyByCursor(cursor),
        });
    }

    async sortPaginateAndRunBigmapValueQuery<T extends Prisma.bigmapFindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.bigmapFindManyArgs>;
        orderBy: BigmapValueOrderByInput | undefined;
        pagination: PaginationArgs;
    }): Promise<{ data: FindManyBigmapValueResult[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            FindManyBigmapValueResult,
            Prisma.bigmapFindManyArgs,
            T,
            BigmapValueOrderByFieldInternal,
            Prisma.bigmapGetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? { field: BigmapValueOrderByField.block_level, direction: OrderByDirection.desc },
            sortInfo: bigmapValueOrderByInfo,
            pagination,
            maxPageSize: 100,
            prismaFindManyFunction: this.prisma.bigmap.findMany.bind(this),
            getCursorObject: async (cursor) => this.getBigmapValueByCursor(cursor),
        });
    }

    async sortPaginateAndRunBigmapQuery<T extends Prisma.bigmapFindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
        options,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.bigmapFindManyArgs>;
        orderBy: BigmapOrderByInput | undefined;
        pagination: PaginationArgs;
        options?: DataFetchOptions;
    }): Promise<{ data: FindManyBigmapsResult[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            FindManyBigmapsResult,
            Prisma.bigmapFindManyArgs,
            T,
            BigmapOrderByField,
            Prisma.bigmapGetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? { field: BigmapOrderByField.id, direction: OrderByDirection.asc },
            sortInfo: bigmapOrderByInfo,
            pagination,
            maxPageSize: 100,
            prismaFindManyFunction: this.prisma.bigmap.findMany.bind(this),
            getCursorObject: async (cursor) => this.getBigmapByCursor(cursor),
            options,
        });
    }

    async getBigmapByCursor(cursor: string): Promise<bigmap> {
        const { id } = unpackBigmapCursor(cursor);
        const bigmapObject = await this.prisma.bigmap.findFirst({
            where: {
                id: id === 'NULL_CURSOR' ? { equals: null } : id,
                kind: { not: BigmapOperationKind.clear },
                key_type: { not: Prisma.JsonNullValueFilter.AnyNull },
            },
        });
        if (isNullish(bigmapObject)) {
            throw new Error(`No bigmap found for cursor ${cursor}`);
        }
        return bigmapObject;
    }

    async getBigmapValueByCursor(cursor: string): Promise<bigmap> {
        const { i, key_hash } = unpackBigmapValueCursor(cursor);
        const bigmapValue = await this.prisma.bigmap.findFirst({
            where: {
                i,
                key_hash,
            },
        });
        if (isNullish(bigmapValue)) {
            throw new Error(`No BigmapValue found for cursor ${cursor}`);
        }
        return bigmapValue;
    }
}
