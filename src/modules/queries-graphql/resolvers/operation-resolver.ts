/* eslint-disable @typescript-eslint/init-declarations */
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Args, ArgsType, Field, FieldResolver, Info, Query, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { BigmapValueRecordConnection, BigmapValueRecordEdge } from '../../../entity/queries/bigmap-value-record';
import {
    OperationRecord,
    OperationRecordConnection,
    OperationRecordEdge,
} from '../../../entity/queries/operation-record';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { injectLogger, Logger } from '../../../utils/logging';
import { isNullish } from '../../../utils/reflection';
import { getAggregationInput } from '../helpers/aggregation';
import { mapBigmapValuesToBigmapValuesData } from '../helpers/bigmap-mappers';
import { OperationMapper } from '../helpers/operation-mapper';
import { checkPaginationArgs, PaginationArgs, wrapIntoConnection } from '../helpers/pagination';
import BigmapValueRepository from '../repositories/bigmap-value-repository';
import OperationAlphaRawRepository from '../repositories/operation-alpha-raw-repository';
import OperationAlphaRepository from '../repositories/operation-alpha-repository';
import { BigmapValueSelectionInOperation } from './types/bigmap-filters';
import { OperationsFilter } from './types/operations-filter';
import { OperationsOrderByInput } from './types/operations-order-by';

@ArgsType()
class OperationsArgs extends PaginationArgs {
    @Field({ nullable: true })
    filter?: OperationsFilter;

    @Field({ nullable: true })
    order_by?: OperationsOrderByInput;
}

@injectable()
@Resolver(() => OperationRecord)
export class OperationResolver {
    constructor(
        private readonly operationUtils: OperationMapper,
        private readonly bigmapValueRepository: BigmapValueRepository,
        private readonly operationAlphaRepository: OperationAlphaRepository,
        private readonly operationAlphaRawRepository: OperationAlphaRawRepository,
        @injectLogger(OperationResolver) private readonly logger: Logger,
        private readonly envConfig: EnvConfig,
    ) {}

    @Query(() => OperationRecordConnection, { nullable: true })
    async operations(
        @Args() { filter = {}, order_by: orderBy, ...pagination }: OperationsArgs,
        @Info() info: GraphQLResolveInfo,
    ): Promise<OperationRecordConnection | undefined> {
        checkPaginationArgs(pagination);
        const repository = this.envConfig.useRawRepo ? this.operationAlphaRawRepository : this.operationAlphaRepository;
        const {
            data: operationAlphasData,
            hasMorePages,
            aggregationResult,
        } = await repository.findManyOperations({
            filter,
            orderBy,
            pagination,
            aggregationInput: getAggregationInput(info),
        });

        if (operationAlphasData.length === 0) {
            this.logger.logInformation('No OperationAlpha rows found for {filter}.', { filter });
            return;
        }

        const operations = operationAlphasData.map((data) =>
            this.operationUtils.mapOperationsResultToSpecificOperation(data),
        );

        return wrapIntoConnection(
            OperationRecordConnection,
            OperationRecordEdge,
            operations,
            pagination,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        // eslint-disable-next-line max-len
        description:
            'The implicit account for the source of the operation. An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.',
    })
    source(@Root() operation: OperationRecord): AccountRecord | undefined {
        if (isNullish(operation.senderAddress) || isNullish(operation.senderAddressID)) {
            return;
        }
        return { address: operation.senderAddress, address_id: operation.senderAddressID };
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        // eslint-disable-next-line max-len
        description: 'The account for the receiver of the operation.',
    })
    receiver(@Root() operation: OperationRecord): AccountRecord | undefined {
        if (isNullish(operation.receiverAddress) || isNullish(operation.receiverAddressID)) {
            return;
        }
        return { address: operation.receiverAddress, address_id: operation.receiverAddressID };
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        // eslint-disable-next-line max-len
        description: 'The account for the sender of the operation.',
    })
    sender(@Root() operation: OperationRecord): AccountRecord | undefined {
        if (isNullish(operation.senderAddress) || isNullish(operation.senderAddressID)) {
            return;
        }
        return { address: operation.senderAddress, address_id: operation.senderAddressID };
    }

    @FieldResolver(() => BigmapValueRecordConnection, {
        description: 'The array containing the values added or modified in this operation',
    })
    async bigmap_values(
        @Root() operation: OperationRecord,
        @Args() args: BigmapValueSelectionInOperation,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapValueRecordConnection> {
        const { bigmapValuesData, hasMorePages, aggregationResult } =
            await this.bigmapValueRepository.findManyBigmapValues(
                { operationId: operation.autoid },
                {
                    ...args,
                    aggregationInput: getAggregationInput(info),
                },
            );
        const bigmapValueObjects = bigmapValuesData.map((bigmapValue) =>
            mapBigmapValuesToBigmapValuesData(bigmapValue),
        );
        return wrapIntoConnection(
            BigmapValueRecordConnection,
            BigmapValueRecordEdge,
            bigmapValueObjects,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }
}
