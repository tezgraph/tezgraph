/* eslint-disable max-params */
/* eslint-disable @typescript-eslint/init-declarations */
import { GraphQLError } from 'graphql';
import { inject, injectable } from 'tsyringe';
import { Resolver, Args, FieldResolver, Root, Ctx, ArgsType, Field, Int } from 'type-graphql';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { Account, AccountRecord } from '../../../entity/queries/account-record';
import { ContractMetadata } from '../../../entity/queries/contract-metadata';
import { ContractScript } from '../../../entity/queries/contract-script';
import { TokenMetadata } from '../../../entity/queries/token-metadata';
import { Micheline, scalars } from '../../../entity/scalars';
import { isNullish } from '../../../utils';
import { contractBalanceRepositoryDIToken } from '../../../utils/query/contract-balance-repository-factory';
import AddressRepository from '../repositories/address-repository';
import ContractBalanceDatabaseRepository from '../repositories/contract-balance-database-repository';
import ContractBalanceTaquitoRepository from '../repositories/contract-balance-taquito-repository';
import ContractMetadataRepository from '../repositories/contract-metadata-repository';
import ContractScriptRepository from '../repositories/contract-script-repository';
import MichelsonParser from '../repositories/michelson-parser';
import RevealRepository from '../repositories/reveal-repository';
import TokenMetadataRepository from '../repositories/token-metadata-repository';

@ArgsType()
export class ContractMetadataArgs {
    @Field(() => [String], { nullable: true })
    view_names?: string[];
}

@ArgsType()
export class TokenMetadataArgs {
    @Field(() => Int, { nullable: true })
    token_id?: number;
}

@injectable()
@Resolver(() => AccountRecord)
export class AccountFieldsResolver {
    constructor(
        private readonly revealRepository: RevealRepository,
        private readonly addressRepository: AddressRepository,
        private readonly michelsonParser: MichelsonParser,
        private readonly contractMetadataRepository: ContractMetadataRepository,
        private readonly tokenMetadataRepository: TokenMetadataRepository,
        private readonly contractScriptRepository: ContractScriptRepository,
        @inject(contractBalanceRepositoryDIToken)
        private readonly contractBalanceRepository:
            | ContractBalanceDatabaseRepository
            | ContractBalanceTaquitoRepository,
    ) {}

    @FieldResolver(() => String, { nullable: true })
    async public_key(@Root() { address_id }: Account): Promise<string | undefined> {
        const reveal = await this.revealRepository.findLatestByAddressId(address_id);
        return reveal?.pk;
    }

    @FieldResolver(() => Date, {
        description:
            'The timestamp of the activate_account operation applied to the specified account address. The activate_account operation is an anonymous operation that bootstraps the account with tez purchased during Tezos ICO.',
        nullable: true,
    })
    async activated(@Root() { address }: Account, @Ctx() { requestId }: ResolverContext): Promise<Date | undefined> {
        return this.addressRepository.getActivated(address, requestId);
    }

    @FieldResolver(() => ContractMetadata, { nullable: true })
    async contract_metadata(
        @Root() address: Account,
        @Args() args: ContractMetadataArgs,
    ): Promise<ContractMetadata | null> {
        const contractMetadata = await this.contractMetadataRepository.findByAccount(address, args);
        return contractMetadata;
    }

    @FieldResolver(() => TokenMetadata, { nullable: true })
    async token_metadata(@Root() address: Account, @Args() args: TokenMetadataArgs): Promise<TokenMetadata | null> {
        return await this.tokenMetadataRepository.findTokenMetadataWithTokenId(address, args);
    }

    @FieldResolver(() => ContractScript, { nullable: true })
    async script(@Root() account: Account): Promise<ContractScript | null> {
        const contractScript = await this.contractScriptRepository.getContractScript(account);
        return contractScript;
    }

    @FieldResolver(() => scalars.Micheline, { nullable: true })
    async storage(
        @Root() account: Account,
        @Ctx() { dataSources: datasources }: ResolverContext,
    ): Promise<Micheline | null> {
        const contractStorage = await datasources.contractStorage.get(account);
        return await this.michelsonParser.convertContractStorageFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
            account.address,
            contractStorage.storage,
        );
    }

    @FieldResolver(() => scalars.Micheline, { nullable: true })
    async storage_micheline_json(
        @Root() account: Account,
        @Ctx() { dataSources: datasources }: ResolverContext,
    ): Promise<string | null> {
        const contractStorage = await datasources.contractStorage.get(account);
        return this.michelsonParser.convertMichelsonJsonToCanonicalForm(contractStorage.storage);
    }

    @FieldResolver(() => scalars.Micheline, { nullable: true })
    async storage_michelson(
        @Root() account: Account,
        @Ctx() { dataSources: datasources }: ResolverContext,
    ): Promise<string | null> {
        const contractStorage = await datasources.contractStorage.get(account);
        return this.michelsonParser.convertMichelsonJsonToMichelsonString(contractStorage.storage);
    }

    @FieldResolver(() => scalars.Mutez, {
        nullable: true,
        description: 'The balance of the account at the time of the query.',
    })
    async balance(@Root() account: AccountRecord): Promise<bigint | null | undefined> {
        const balance = await this.contractBalanceRepository.findBalanceByAddressId(account);
        if (isNullish(balance)) {
            throw new GraphQLError(`Error occurred while retrieving the account balance value for ${account.address}.`);
        }
        return balance;
    }
}
