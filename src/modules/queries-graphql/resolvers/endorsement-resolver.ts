import { injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { EndorsementRecord } from '../../../entity/queries/endorsement-record';
import { scalars } from '../../../entity/scalars';
import { isNullish } from '../../../utils';
import BalanceUpdateRepository from '../repositories/balance-update-repository';

@injectable()
@Resolver(() => EndorsementRecord)
export class EndorsementRecordResolver {
    constructor(private readonly balanceUpdateRepository: BalanceUpdateRepository) {}

    @FieldResolver(() => scalars.Mutez, {
        nullable: true,
        description: 'The rewards related to the endorsement operation in mutez.',
    })
    async reward(@Root() endorsement: EndorsementRecord): Promise<bigint | null> {
        if (isNullish(endorsement.senderAddressID)) {
            return null;
        }
        return this.balanceUpdateRepository.getSumByOperationIdContractAddressBalanceKind({
            operation_id: endorsement.autoid,
            contract_address_id: endorsement.senderAddressID,
        });
    }

    @FieldResolver(() => scalars.Mutez, {
        nullable: true,
        description: 'The rewards related to the deposit operation in mutez.',
    })
    async deposit(@Root() endorsement: EndorsementRecord): Promise<bigint | null> {
        if (isNullish(endorsement.senderAddressID)) {
            return null;
        }
        return this.balanceUpdateRepository.getSumByOperationIdContractAddressBalanceKind({
            operation_id: endorsement.autoid,
            contract_address_id: endorsement.senderAddressID,
            balance_kind: 3,
        });
    }
}
