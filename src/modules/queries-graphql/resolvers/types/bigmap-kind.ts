import { registerEnumType } from 'type-graphql';

export enum BigmapRecordKind {
    alloc = 0,
    update = 1,
    clear = 2,
    copy = 3,
}

registerEnumType(BigmapRecordKind, { name: 'BigmapRecordKind' });
