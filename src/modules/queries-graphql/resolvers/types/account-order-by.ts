import { InputType, registerEnumType } from 'type-graphql';

import { createOrderByInput, SortFieldInfo } from '../../helpers/utils';

export enum AccountOrderByField {
    address = 'address',
}

registerEnumType(AccountOrderByField, {
    name: 'AccountOrderByField',
    description: 'Account fields that are used for sorting account records.',
});

@InputType()
export class AccountOrderByInput extends createOrderByInput(AccountOrderByField) {}

export const accountOrderByInfo: Record<string, SortFieldInfo<AccountOrderByField>> = {
    [AccountOrderByField.address]: {
        isUnique: true,
        prismaModelPath: 'address',
        rawModel: { tableAlias: 'a', columnName: 'address' },
    },
};
