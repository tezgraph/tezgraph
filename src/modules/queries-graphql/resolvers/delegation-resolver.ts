import { inject, injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { DelegationRecord } from '../../../entity/queries/delegation-record';
import { scalars } from '../../../entity/scalars';
import { isNullish } from '../../../utils';
import { contractBalanceRepositoryDIToken } from '../../../utils/query/contract-balance-repository-factory';
import ContractBalanceDatabaseRepository from '../repositories/contract-balance-database-repository';
import ContractBalanceTaquitoRepository from '../repositories/contract-balance-taquito-repository';

@injectable()
@Resolver(() => DelegationRecord)
export class DelegationRecordResolver {
    constructor(
        @inject(contractBalanceRepositoryDIToken)
        private readonly contractBalanceRepository:
            | ContractBalanceDatabaseRepository
            | ContractBalanceTaquitoRepository,
    ) {}

    @FieldResolver(() => scalars.Mutez, { nullable: true, description: 'The amount delegated in this operation.' })
    async amount(@Root() delegation: DelegationRecord): Promise<bigint | null> {
        if (isNullish(delegation.senderAddressID) || isNullish(delegation.senderAddress)) {
            return null;
        }

        const balance = await this.contractBalanceRepository.findBalanceByAddressAndBlockLevel(delegation);
        if (isNullish(balance)) {
            return null;
        }

        return balance;
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        description: 'An Implicit account to which an account has delegated their baking and endorsement rights.',
    })
    delegate(@Root() delegation: DelegationRecord): AccountRecord | null {
        if (isNullish(delegation.delegate_address) || isNullish(delegation.delegate_id)) {
            return null;
        }
        return { address: delegation.delegate_address, address_id: delegation.delegate_id };
    }
}
