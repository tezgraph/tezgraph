import { AbortSignal } from 'abort-controller';

import { throwIfAborted } from '../../abortion';
import { AsyncIterableProvider } from '../async-iterable-provider';

export class AbortableIterableProvider<TItem> implements AsyncIterableProvider<TItem> {
    constructor(private readonly innerProvider: AsyncIterableProvider<TItem>) {}

    async *iterate(abortSignal: AbortSignal): AsyncIterableIterator<TItem> {
        for await (const item of this.innerProvider.iterate(abortSignal)) {
            throwIfAborted(abortSignal);
            yield item;
            throwIfAborted(abortSignal);
        }
    }
}
