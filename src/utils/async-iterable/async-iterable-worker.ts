import { AbortController, AbortSignal } from 'abort-controller';

import { BackgroundWorker } from '../../bootstrap/background-worker';
import { AbortControllerFactory } from '../app-control/abort-controller-factory';
import { Nullish } from '../reflection';
import { AsyncIterableProcessor } from './async-iterable-processor';

export enum ProcessingStatus {
    Stopped = 'Stopped',
    Stopping = 'Stopping',
    Running = 'Running',
}

/** Executes given iterable processor on the background of the app. */
export class AsyncIterableWorker implements BackgroundWorker {
    private currentAbortController: Nullish<AbortController>;
    private currentProcessingPromise: Nullish<Promise<void>>;

    constructor(
        readonly name: string,
        private readonly processor: AsyncIterableProcessor,
        private readonly globalAbortSignal: AbortSignal,
        private readonly abortControllerFactory: AbortControllerFactory,
    ) {}

    get status(): ProcessingStatus {
        if (!this.currentAbortController || !this.currentProcessingPromise) {
            return ProcessingStatus.Stopped;
        }
        if (this.currentAbortController.signal.aborted) {
            return ProcessingStatus.Stopping;
        }
        return ProcessingStatus.Running;
    }

    start(): void {
        if (this.currentAbortController || this.currentProcessingPromise) {
            throw new Error('This cannot be started because it is already running.');
        }
        this.currentAbortController = this.abortControllerFactory.createLinked(this.globalAbortSignal);
        this.currentProcessingPromise = this.processor.processItems(this.currentAbortController.signal);
    }

    async stop(): Promise<void> {
        if (!this.currentAbortController || !this.currentProcessingPromise) {
            throw new Error('This cannot be stopped because it is NOT running.');
        }
        this.currentAbortController.abort();
        await this.currentProcessingPromise;

        this.currentAbortController = null;
        this.currentProcessingPromise = null;
    }
}
