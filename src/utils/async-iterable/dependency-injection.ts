import { AbortSignal } from 'abort-controller';
import { DependencyContainer, FactoryFunction, InjectionToken } from 'tsyringe';

import { backgroundWorkersDIToken } from '../../bootstrap/background-worker';
import { AbortControllerFactory } from '../app-control/abort-controller-factory';
import { EnvConfig } from '../configuration/env-config';
import { getDIToken, registerSingleton } from '../dependency-injection';
import { healthChecksDIToken } from '../health/health-check';
import { getLogger } from '../logging';
import { Clock } from '../time/clock';
import { AsyncIterableProcessor, InfoProvider } from './async-iterable-processor';
import { AsyncIterableProcessorHealthCheck } from './async-iterable-processor-health-check';
import { AsyncIterableProvider } from './async-iterable-provider';
import { AsyncIterableWorker } from './async-iterable-worker';
import { ItemProcessor } from './processors/item-processor';
import { AbortableIterableProvider } from './providers/abortable-iterable-provider';

/** Registers iterable processor based on given token with related worker and health check. */
export function registerAsyncIterableWorker<TItem>(
    diContainer: DependencyContainer,
    name: string,
    createIterableProvider: FactoryFunction<AsyncIterableProvider<TItem>>,
    createItemProcessor: FactoryFunction<ItemProcessor<TItem>>,
    itemInfoProviderDIToken: InjectionToken<InfoProvider<TItem>>,
): void {
    const processorDIToken = getDIToken<AsyncIterableProcessor<TItem>>(`${name}Processor`);
    registerSingleton(
        diContainer,
        processorDIToken,
        () =>
            new AsyncIterableProcessor(
                new AbortableIterableProvider(createIterableProvider(diContainer)),
                createItemProcessor(diContainer),
                diContainer.resolve(itemInfoProviderDIToken),
                diContainer.resolve(Clock),
                getLogger(diContainer, name),
            ),
    );

    const workerDIToken = getDIToken<AsyncIterableWorker>(`${name}Worker`);
    registerSingleton(
        diContainer,
        workerDIToken,
        () =>
            new AsyncIterableWorker(
                `${name}Worker`,
                diContainer.resolve(processorDIToken),
                diContainer.resolve(AbortSignal),
                diContainer.resolve(AbortControllerFactory),
            ),
    );
    diContainer.register(backgroundWorkersDIToken, { useToken: workerDIToken });

    registerSingleton(
        diContainer,
        healthChecksDIToken,
        () =>
            new AsyncIterableProcessorHealthCheck(
                name,
                diContainer.resolve(processorDIToken),
                diContainer.resolve(workerDIToken),
                diContainer.resolve(EnvConfig),
                diContainer.resolve(Clock),
                getLogger(diContainer, name),
            ),
    );
}
