import { AbortSignal } from 'abort-controller';

import { isAbortError } from '../abortion';
import { Logger } from '../logging';
import { Clock } from '../time/clock';
import { AsyncIterableProvider } from './async-iterable-provider';
import { ItemProcessor } from './processors/item-processor';

export interface InfoProvider<TValue> {
    getInfo(value: TValue): unknown;
}

/** Processes items from given iterable one by one in a loop. */
export class AsyncIterableProcessor<TItem = unknown> {
    lastItemInfo: unknown;
    lastError: unknown;
    lastSuccessTime: Date = new Date(0);

    constructor(
        private readonly iterableProvider: AsyncIterableProvider<TItem>,
        private readonly itemProcessor: ItemProcessor<TItem>,
        private readonly itemInfoProvider: InfoProvider<TItem>,
        private readonly clock: Clock,
        private readonly logger: Logger,
    ) {}

    async processItems(abortSignal: AbortSignal): Promise<void> {
        this.logger.logInformation('Starting to process items.');
        while (!abortSignal.aborted) {
            try {
                for await (const item of this.iterableProvider.iterate(abortSignal)) {
                    try {
                        this.lastItemInfo = this.itemInfoProvider.getInfo(item);
                        await this.itemProcessor.processItem(item);

                        this.lastError = undefined;
                        this.lastSuccessTime = this.clock.getNowDate();
                    } catch (error: unknown) {
                        this.lastError = error;
                        this.logger.logError('Failed to process {item} because of {error}.', {
                            item: this.lastItemInfo,
                            error,
                        });
                    }
                }
                throw new Error('Iteration ended which should not happen.');
            } catch (error: unknown) {
                if (!isAbortError(error)) {
                    this.lastError = error;
                    this.lastItemInfo = undefined;
                    this.logger.logError('Failed iterating items. It will retry. {error}.', { error });
                }
            }
        }
        this.logger.logInformation('Iteration has stopped (aborted).');
    }
}
