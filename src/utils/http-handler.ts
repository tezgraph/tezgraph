import express from 'express';
import { AsyncOrSync } from 'ts-essentials';
import { InjectionToken } from 'tsyringe';

export const appUrlPaths = {
    root: '/',
    graphQL: '/graphql',
    health: '/health',
    check: '/check',
    metrics: '/metrics',
} as const;

export interface HttpHandler {
    readonly urlPath: string;
    handle(request: express.Request, response: express.Response): AsyncOrSync<void>;
}

export const httpHandlersDIToken: InjectionToken<HttpHandler> = 'HttpHandlers';
