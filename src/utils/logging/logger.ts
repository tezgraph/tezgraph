import { MetricsContainer } from '../metrics/metrics-container';
import { Clock } from '../time/clock';
import { LogData, LogLevel } from './log-entry';
import { RootLogger } from './root-logger-factory';

/** Main API for logging. */
export class Logger {
    constructor(
        private readonly category: string,
        private readonly rootLogger: RootLogger,
        private readonly clock: Clock,
        private readonly metrics: MetricsContainer,
    ) {}

    get isCriticalEnabled(): boolean {
        return this.isEnabled(LogLevel.Critical);
    }

    get isErrorEnabled(): boolean {
        return this.isEnabled(LogLevel.Error);
    }

    get isWarningEnabled(): boolean {
        return this.isEnabled(LogLevel.Warning);
    }

    get isInformationEnabled(): boolean {
        return this.isEnabled(LogLevel.Information);
    }

    get isDebugEnabled(): boolean {
        return this.isEnabled(LogLevel.Debug);
    }

    isEnabled(level: LogLevel): boolean {
        return this.rootLogger.isLevelEnabled(level);
    }

    log(level: LogLevel, message: string, data?: LogData): void {
        if (!this.isEnabled(level)) {
            return;
        }

        this.rootLogger.log({
            timestamp: this.clock.getNowDate(),
            category: this.category,
            level,
            message,
            data,
        });
        this.metrics.loggerLogCount.inc({ level: level.toString() });
    }

    logCritical(message: string, data?: LogData): void {
        this.log(LogLevel.Critical, message, data);
    }

    logError(message: string, data?: LogData): void {
        this.log(LogLevel.Error, message, data);
    }

    logWarning(message: string, data?: LogData): void {
        this.log(LogLevel.Warning, message, data);
    }

    logInformation(message: string, data?: LogData): void {
        this.log(LogLevel.Information, message, data);
    }

    logDebug(message: string, data?: LogData): void {
        this.log(LogLevel.Debug, message, data);
    }

    close(): void {
        this.rootLogger.close();
    }
}
