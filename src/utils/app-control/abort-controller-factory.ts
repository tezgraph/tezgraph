import { AbortController, AbortSignal } from 'abort-controller';
import { singleton } from 'tsyringe';

import { addAbortListener } from '../abortion';

@singleton()
export class AbortControllerFactory {
    createLinked(signal: AbortSignal): AbortController {
        const controller = new AbortController();

        if (signal.aborted) {
            controller.abort();
            return controller;
        }

        const unlink = addAbortListener(signal, () => controller.abort());
        return {
            signal: controller.signal,
            abort: (): void => {
                controller.abort();
                unlink(); // B/c it would produce a memory leak if parent signal has longer lifetime.
            },
        };
    }
}
