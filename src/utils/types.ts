export type AbstractClassType<T = unknown> = abstract new (...args: unknown[]) => T;
