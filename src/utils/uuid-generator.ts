import { singleton } from 'tsyringe';
import { v4 } from 'uuid';

@singleton()
export class UuidGenerator {
    generate(): string {
        return v4();
    }
}
