import http from 'http';
import { delay, inject, singleton } from 'tsyringe';

import { MetricsUpdater } from '../metrics-collector';
import { MetricsContainer } from '../metrics-container';

@singleton()
export class HttpServerMetricsUpdater implements MetricsUpdater {
    constructor(
        private readonly metrics: MetricsContainer,
        @inject(delay(() => http.Server)) private readonly httpServer: http.Server,
    ) {}

    async update(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.httpServer.getConnections((error, count) => {
                if (!error) {
                    this.metrics.httpServerConnectionCount.set(count);
                    resolve();
                } else {
                    reject(error);
                }
            });
        });
    }
}
