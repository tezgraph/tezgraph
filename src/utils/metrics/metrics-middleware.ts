import { singleton } from 'tsyringe';
import { MiddlewareInterface, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../bootstrap/resolver-context';
import { ResolveInfoParser } from '../query/resolve-info-parser';
import { isNotNullish } from '../reflection';
import { MetricsContainer } from './metrics-container';
import { QueryMetricsUtils } from './query-metrics-utils';

export const SERVICE_NAME = 'tezgraph';

export enum ParentType {
    Query = 'Query',
    Mutation = 'Mutation',
}

@singleton()
export class MetricsMiddleware implements MiddlewareInterface<ResolverContext> {
    constructor(
        private readonly metrics: MetricsContainer,
        private readonly queryMetricsUtils: QueryMetricsUtils,
        private readonly resolveInfoParser: ResolveInfoParser,
    ) {}

    async use(resolverData: ResolverData<ResolverContext>, next: () => Promise<unknown>): Promise<unknown> {
        const typeName = resolverData.info.parentType.name;
        const fieldName = resolverData.info.fieldName;
        switch (typeName) {
            case ParentType.Query:
            case ParentType.Mutation:
                if (this.internalQueryCaller(resolverData.context)) {
                    this.queryMetricsUtils.countQueryFields(
                        this.resolveInfoParser.parse(resolverData.info),
                        'internal',
                    );
                    break;
                }
                this.metrics.graphQLQueryMethodCounter.inc({
                    method: fieldName,
                    service: SERVICE_NAME,
                });
                this.queryMetricsUtils.countQueryFields(this.resolveInfoParser.parse(resolverData.info), 'external');
                break;
        }

        try {
            return await next();
        } catch (error: unknown) {
            this.metrics.graphQLErrorCounter.inc({
                type: typeName,
                field: fieldName,
                service: SERVICE_NAME,
            });
            throw error;
        }
    }

    internalQueryCaller(context: ResolverContext | undefined): boolean {
        if (isNotNullish(context?.req?.headers) && context?.req?.headers.querycaller === 'internal') {
            return true;
        }
        return false;
    }
}
