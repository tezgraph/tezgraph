import { DependencyContainer, InjectionToken, instanceCachingFactory } from 'tsyringe';

export const diContainerDIToken: InjectionToken<DependencyContainer> = 'Container';

export function registerSingleton<T>(
    diContainer: DependencyContainer,
    token: InjectionToken<T>,
    factory: (c: DependencyContainer) => T,
): void {
    diContainer.register(token, { useFactory: instanceCachingFactory(factory) });
}

/** Makes sure that DI token is not treated as a string. */
export function getDIToken<T>(uniqueName: string): InjectionToken<T> {
    return uniqueName;
}
