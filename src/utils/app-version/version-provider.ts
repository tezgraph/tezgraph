import { inject, singleton } from 'tsyringe';

import { processEnvDIToken } from '../configuration/env-config-provider';
import { Version } from './version';

export enum VersionVariables {
    AppName = 'APP_NAME',
    GitSha = 'GIT_SHA',
    GitTag = 'GIT_TAG',
    AppUrl = 'APP_URL',
}

@singleton()
export class VersionProvider {
    readonly version: Version;

    constructor(@inject(processEnvDIToken) processEnv: NodeJS.ProcessEnv) {
        this.version = {
            name: processEnv[VersionVariables.AppName] ?? null,
            git_sha: processEnv[VersionVariables.GitSha] ?? null,
            tag: processEnv[VersionVariables.GitTag] ?? null,
            release_notes: processEnv[VersionVariables.AppUrl]
                ? `${processEnv[VersionVariables.AppUrl] ?? ''}/-/releases/${processEnv.GIT_TAG ?? ''}`
                : null,
        };
    }
}
