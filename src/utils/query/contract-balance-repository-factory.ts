import { PrismaClient } from '@prisma/client';
import { inject, InjectionToken, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../../modules/queries-graphql/database/prisma';
import BlockRepository from '../../modules/queries-graphql/repositories/block-repository';
import ContractBalanceDatabaseRepository from '../../modules/queries-graphql/repositories/contract-balance-database-repository';
import ContractBalanceTaquitoRepository from '../../modules/queries-graphql/repositories/contract-balance-taquito-repository';
import TaquitoRepository from '../../modules/queries-graphql/repositories/taquito-repository';
import { EnvConfig } from '../configuration/env-config';

export const contractBalanceRepositoryDIToken: InjectionToken<
    ContractBalanceDatabaseRepository | ContractBalanceTaquitoRepository
> = 'contractBalanceRepository';

@singleton()
export class ContractBalanceRepositoryFactory {
    constructor(
        private readonly envConfig: EnvConfig,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @inject(TaquitoRepository) private readonly taquitoRepository: TaquitoRepository,
        private readonly blockRepository: BlockRepository,
    ) {}

    create(): ContractBalanceDatabaseRepository | ContractBalanceTaquitoRepository {
        const resolveContractBalanceFromDB = this.envConfig.resolveContractBalanceFromDB;
        if (resolveContractBalanceFromDB) {
            return new ContractBalanceDatabaseRepository(this.prisma);
        }
        return new ContractBalanceTaquitoRepository(this.taquitoRepository, this.blockRepository);
    }
}
