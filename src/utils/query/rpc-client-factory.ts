import { RpcClient } from '@taquito/rpc';
import { InjectionToken, singleton } from 'tsyringe';

import { EnvConfig } from '../configuration/env-config';

export const taquitoRpcClientDIToken: InjectionToken<RpcClient> = 'taquitoRpcClient';

@singleton()
export class TaquitoRpcClientFactory {
    constructor(private readonly envConfig: EnvConfig) {}

    create(): RpcClient {
        const client = new RpcClient(`${this.envConfig.tezosNodeUrl}`, `${this.envConfig.tezosNodeChainId}`);
        return client;
    }
}
