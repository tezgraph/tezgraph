import { GraphQLResolveInfo } from 'graphql';
import { parseResolveInfo, ResolveTree } from 'graphql-parse-resolve-info';

const paginationTypeSuffixes = ['Edge', 'Connection', 'PageInfo', 'edge', 'connection', 'page_info'];

export class ResolveInfoParser {
    parse(resolveInfo: GraphQLResolveInfo): ResolveTree {
        return parseResolveInfo(resolveInfo) as ResolveTree;
    }

    getParsedResolveInfoFragmentFields(parsedResolveInfoFragment: ResolveTree): string[] | undefined {
        const keyFields = Object.keys(parsedResolveInfoFragment.fieldsByTypeName);

        if (keyFields[0] === undefined) {
            return;
        }

        return keyFields;
    }

    isIgnoredOpType(opType: string): boolean {
        const regexFromMyArray = new RegExp(paginationTypeSuffixes.join('|'), 'u');
        return regexFromMyArray.test(opType);
    }
}
