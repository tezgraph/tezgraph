import { snakeCase } from 'lodash';

import { isNullish, Nullish } from './reflection';

export function removeRequiredPrefix(str: string, prefixToRemove: string): string {
    if (!str.startsWith(prefixToRemove)) {
        throw new Error(
            `Given string '${str}' does not start with prefix '${prefixToRemove}' which should be removed.`,
        );
    }
    return str.substring(prefixToRemove.length);
}

export function removeRequiredSuffix(str: string, suffixToRemove: string): string {
    if (!str.endsWith(suffixToRemove)) {
        throw new Error(`Given string '${str}' does not end with suffix '${suffixToRemove}' which should be removed.`);
    }
    return str.substring(0, str.length - suffixToRemove.length);
}

const whiteSpaceRegex = /^\s*$/u;

export function isWhiteSpace(str: Nullish<string>): str is null | undefined | '' {
    return isNullish(str) || str.length === 0 || whiteSpaceRegex.test(str);
}

export function joinQuoted(strings: readonly string[]): string {
    return strings.map((s) => `'${s}'`).join(', ');
}

export function humanReadable(str: string): string {
    return replaceAll(snakeCase(str), '_', ' ');
}

export function replaceAll(str: string, search: string, replacement: string): string {
    return str.split(search).join(replacement);
}
